﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mime;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Account;


namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IAccountService
    {
        Task<bool> CreateAccount(CreateAccountRequest model);
        Task<IEnumerable<GetAccountBySaleResult>> AC_GE_GetListAccountBySale(GetAccountBySaleRequest model);
        Task<IEnumerable<GetAccountBySaleResult>> AC_GE_GetListAccountFillBySale(GetAccountFillBySaleRequest model);
        
        Task<bool> OP_CA_CheckAccountNumExisted(string accountNo);
        Task<bool> CheckCardIdExsted(string cardID);
        Task<GetAccountDetailBySaleResult> OP_CA_GetAccountDetailBySale(GetAccountDetailBySaleRequest model);
        Task<bool> OP_DE_DeleteOpenAccount(GetAccountDetailBySaleRequest model);
        Task<IEnumerable<GetCardIssue>> OP_GE_GetListCardIssue(BaseRequest model);

        Task<bool> InsertOpenAccDocument(CreateAccountRequest model);
        

        Task<string> GetBankName(string bankNo);
        Task<string> GetSubBranchName(string bankNo, string branchNo);
        Task<string> GetProvinceName(string provinceId);

        Task<List<BankInfo>> GetListBank();
        Task<List<SubBranchInfo>> GetListBranch(BranchRequest model);
        Task<List<ProvinceInfo>> GetListProvince();
        Task<IEnumerable<GetAccountStatusResult>> GetAccountStatus();
        Task<IEnumerable<GetAccountStatusResult>> ProfileStatus();
        Task<bool> InsertCalllog(CalllogRequest model);
        Task<IEnumerable<ProvinceResult>> GetProvinces();
        Task<IEnumerable<DistrictResult>> GetDistricts(string provinceCode);
        Task<IEnumerable<WardResult>> GetWards(string districtCode);



        Task<string> GetIssuedByName(int id);
        Task<SaleProfileModel> GE_FO_GetProfileUser(BaseRequest model);
        Task<bool> ChangeImageUser(ChangeImageUser model);


        Task<ManagementCodeList> GetManagementCode(string saleID);
        //Validate form dang ky kh ca nhan trong nuoc
        Task<IEnumerable<AccountValidation>> AccountRegisterValidation(CreateAccountRequest model);
        

        // Validate Account API
        Task<string> GetCustCode (string accountNo);
        
        //Stream Image base64
        Task<string> Base64Decode(string base64EncodedData);
        
        //Stored

        Task<bool> StoredInsertAccountCommond(CreateAccountRequest model,string addService, 
            string transferType,string description,string statusAccount,string statusProfile,int statusService
            ,int statusServiceBank01, int statusServiceBank02,int statusServiceBank03,int statusServiceBank04, int statusReciprocal_01, int statusReciprocal_02, int statusServiceChannelInfo
            ,string branchID, string branchName);

        
        //Other Validate
        Task<int> ValidateAccount(string accountNo, int custType);
        Task<int> ValidateCustCode(string accountNo);

        //Other 

        //Other 


        Task<AccountInfoReponse> GetAccountInfo(DetailAccountRequest model);


        Task<bool> UpdateAccountInfo(AccountInfoReponse model);

        Task<bool> UpdateAccountCommond(AccountInfoReponse model,string addService,
            string transferType, string description, string statusAccount, string statusProfile, int statusService
            , int statusServiceBank01, int statusServiceBank02, int statusReciprocal_01, int statusReciprocal_02, int statusServiceChannelInfo
            , string branchID, string branchName);

        Task<bool> CheckAccountPermission(int level,string saleID );


        /// <summary>
        /// Lấy danh sách mở tài khoản qua website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<IEnumerable<AccWebListResult>> GetListAccWeb(AccWebListRequest model); 
        
        
        /// <summary>
        /// đẩy tài khoản từ crm sang commond
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> PushAccountWebsite(AccountDetailRequest model);


        /// <summary>
        /// insert thong tin mo rong kh
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> InsertExtendCust(AccountDetailRequest model);




        /// <summary>
        /// Chi tiết tài khoản website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<AccountDetailResult> RQ_GE_DetailAccountWebsite(AccountDetailWebsiteRequest model);
        
        /// <summary>
        /// Update trạng thái hồ sơ mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> UpdateAccWebInfo(AccWebEditRequest model);

        /// <summary>
        /// Xóa hồ sơ mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<bool> DeleteAccWebInfo(AccWebDeleteRequest model);   
        
        /// <summary>
        /// Lấy chi tiet người đại diện
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<GetManInfoResult> GetManInfo(GetManInfoRequest model);  
        
        /// <summary>
        /// Lấy danh sách người đại diện
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<IEnumerable<GetManInfoListResult>> GetManInfoList(BaseRequest model);

        Task<Byte[]> Resize2Max50Kbytes(byte[] byteImageIn);
    }
}