﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class ActivityCommandText : IActivityCommandText
    {
        #region SQL Command List
        public string GetNext3Task => @"SELECT TOP(3) *, Activity.AssignUser, Activity.Status, Activity.ActivityType, Activity.Description  FROM Activity WHERE CAST(Activity.StartDate as Date) = @Date AND Activity.Status <> 99 AND Activity.AssignUser = @userAssign Order by Activity.StartDate DESC";
        public string GetListActivity => @"SELECT Activity.ActivityID, Activity.EventType, Activity.ActivityName, Activity.BranchID, Activity.AssignUser, Activity.ActivityType, Activity.StartDate, Activity.EndDate, 
                                            Activity.Status, Activity.Priority, Activity.Location, Activity.Recurrence, Activity.Notification, Activity.Description, Activity.LeadID, Activity.CustCode, Activity.ConfirmStatus, 
                                            Activity.ConfirmBy, Activity.ConfirmDate, Activity.ConfirmCode, Activity.RejectCode, Activity.RejectReason, Activity.ReasonType, Lead.LeadName, CustInfo.CustName 
                        FROM Activity LEFT JOIN Lead ON Lead.LeadID = Activity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Activity.CustCode 
                        WHERE Activity.StartDate <= @dateStart AND Activity.EndDate >= @dateEnd 
                        AND Activity.Status <> 99 AND Activity.AssignUser = @userAssign 
                        Order by Activity.StartDate DESC";
        public string GetListActivityByEvenType => @"SELECT Activity.ActivityID, Activity.EventType, Activity.ActivityName, Activity.BranchID, Activity.AssignUser, Activity.ActivityType, Activity.StartDate, Activity.EndDate, 
                                            Activity.Status, Activity.Priority, Activity.Location, Activity.Recurrence, Activity.Notification, Activity.Description, Activity.LeadID, Activity.CustCode, Activity.ConfirmStatus, 
                                            Activity.ConfirmBy, Activity.ConfirmDate, Activity.ConfirmCode, Activity.RejectCode, Activity.RejectReason, Activity.ReasonType, Lead.LeadName, CustInfo.CustName  
                        FROM Activity LEFT JOIN Lead ON Lead.LeadID = Activity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Activity.CustCode 
                        WHERE Activity.StartDate <= @dateStart AND Activity.EndDate >= @dateEnd AND Activity.Status <> 99 AND Activity.EventType = @eventType AND Activity.AssignUser = @userAssign 
                        Order by Activity.StartDate DESC";
        public string GetListActivityType => @"SELECT ActivityTypeID,ActivityTypeName,Status,CreatedBy,CratedDate,UpdatedBy,UpdatedDate FROM ActivityType WHERE ActivityTypeID <> 5";

        public string GetCountActivity => @"SELECT count(*) FROM Activity WHERE StartDate <= @dateStart AND EndDate >= @dateEnd AND Status <> 99 AND AssignUser = @userAssign ";

        public string GetCountActivityByEvenType => @"SELECT count(*) FROM Activity WHERE StartDate <= @dateStart AND EndDate >= @dateEnd AND Status <> 99 AND EventType = @eventType AND AssignUser = @userAssign ";

        public string GetDetailActivity => @"SELECT Activity.ActivityID, Activity.EventType, Activity.ActivityName, Activity.BranchID, Activity.AssignUser, Activity.ActivityType, Activity.StartDate, Activity.EndDate, 
                                            Activity.Status, Activity.Priority, Activity.Location, Activity.Recurrence, Activity.Notification, Activity.Description, Activity.LeadID, Activity.CustCode, Activity.ConfirmStatus, 
                                            Activity.ConfirmBy, Activity.ConfirmDate, Activity.ConfirmCode, Activity.RejectCode, Activity.RejectReason, Activity.ReasonType, Lead.LeadName, CustInfo.CustName  
                        FROM Activity LEFT JOIN Lead ON Lead.LeadID = Activity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Activity.CustCode WHERE Activity.ActivityID = @activityId AND Activity.Status <> 99";

        public string PostActivity => @"INSERT INTO Activity (EventType,ActivityName,AssignUser,ActivityType,StartDate,EndDate,Status,Priority,
                                                            Location,Recurrence,Notification,Description,LeadID,CustCode,CreatedBy,CreatedDate,ReasonType)
                                        VALUES (@eventType,@activityName,@assignUser,@activityType,@startDate,@endDate,@status,@priority,
                                                @location,@recurrence,@notification,@description,@leadID,@custCode,@createdBy,@createdDate,@ReasonType)";

        public string PostBOActivity => @"INSERT INTO BOActivity (EventType,ActivityName,AssignUser,ActivityType,StartDate,EndDate,Status,Priority,
                                                            Location,Description,CreatedBy,CreatedDate)
                                        VALUES (@eventType,@activityName,@assignUser,@activityType,@startDate,@endDate,@status,@priority,
                                                @location,@description,@createdBy,@createdDate)";
        public string PostCFActivity => @"INSERT INTO CFActivity (EventType,ActivityName,AssignUser,TvsiUserName,DirectContact,ActivityType,StartDate,EndDate,
                                                            Status,Priority,Location,Recurrence,Notification,Description,LeadID,CustCode,CreatedBy,CreatedDate)
                                        VALUES (@eventType,@activityName,@assignUser,@tvsiUserName,@directContact,@activityType,@startDate,@endDate,
                                                @status,@priority,@location,@recurrence,@notification,@description,@leadID,@custCode,@createdBy,@createdDate)";

        public string PutActivity => @"UPDATE Activity SET EventType = @eventType,ActivityName = @activityName,AssignUser = @assignUser,
                                                            ActivityType = @activityType,StartDate = @startDate,EndDate = @endDate,Status = @status,Priority = @priority,
                                                            Location = @location,Recurrence = @recurrence,Notification = @notification,Description = @description,
                                                            LeadID = @leadID, CustCode = @custCode,UpdatedBy = @updatedBy,UpdatedDate = @updatedDate,ReasonType = @ReasonType 
                                        WHERE ActivityID = @activityID AND Status <> 99";
                                                            // ConfirmStatus = @confirmStatus,ConfirmBy = @confirmBy,ConfirmDate = @confirmDate,
                                                            // ConfirmCode = @confirmCode,RejectCode = @rejectCode,RejectReason = @rejectReason,ReasonType = @reasonType

        public string PutBOActivity => @"UPDATE BOActivity SET EventType = @eventType,ActivityName = @activityName,BranchID = @branchID,AssignUser = @assignUser,
                                                            ActivityType = @activityType,StartDate = @startDate,EndDate = @endDate,Status = @status,Priority = @priority,
                                                            Location = @location,Description = @description,UpdatedBy = @updatedBy,UpdatedDate = @updatedDate
                                        WHERE BOActivityID = @activityID AND Status <> 99";
                                                            // ConfirmStatus = @confirmStatus,ConfirmBy = @confirmBy,ConfirmDate = @confirmDate,ConfirmCode = @confirmCode,
                                                            // RejectCode = @rejectCode,RejectReason @rejectReason 
        public string PutCFActivity => @"UPDATE CFActivity SET EventType = @eventType,ActivityName = @activityName,BranchID = @branchID,AssignUser = @assignUser,
                                                            TvsiUserName = @tvsiUserName,DirectContact = @directContact,ActivityType = @activityType,StartDate = @startDate,
                                                            EndDate = @endDate,Status = @status,Priority = @priority,Location = @location,Recurrence = @recurrence,Notification = @notification,
                                                            Description = @description,LeadID = @leadID, CustCode = @custCode,UpdatedBy = @updatedBy,UpdatedDate = @updatedDate
                                        WHERE ActivityID = @activityID AND Status <> 99";
                                                            // ConfirmStatus = @confirmStatus,ConfirmBy = @confirmBy,ConfirmDate = @confirmDate,
                                                            // ConfirmCode = @confirmCode,RejectCode = @rejectCode,RejectReason = @rejectReason
        public string DeleteActivity => @"UPDATE Activity SET status = 99 WHERE ActivityID = @activityID";

        public string UpdateListActivity => @"UPDATE Activity SET status = @status WHERE ActivityID IN @activityIDs";

        public string CheckDataByMonth => @"SELECT COUNT(ActivityID) as CountActivity, CAST(StartDate AS DATE) as StartDate FROM Activity WHERE AssignUser = @userName AND Status <> 99 GROUP BY CAST(StartDate AS DATE)";

        public string GetHisTimeWorkStaffByMonth => @"SELECT COUNT(ActivityID) as CountActivity, CAST(StartDate AS DATE) as StartDate FROM Activity WHERE AssignUser = @userName AND EventType = @type AND Status <> 99 GROUP BY CAST(StartDate AS DATE)";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
