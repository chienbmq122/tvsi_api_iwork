﻿namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public static class AccountStatus
    {
        public static string AccountStatusCommon(this string status)
        {
            if (status != null)
            {
                if (status.Equals("000"))
                    return "Chờ xử lý";
                if (status.Equals("001"))
                    return "Chờ BU_MAN duyệt";  
                if (status.Equals("002"))
                    return "Chờ FS duyệt";
                if (status.Equals("003"))
                    return "GDMG duyệt"; 
                if (status.Equals("099"))
                    return "BU_MAN từ chối";
                if (status.Equals("100") || status.Equals("101"))
                    return "Đang xử lý";
                if (status.Equals("102"))
                    return "Kích hoạt(KSV SS)";
                if (status.Equals("200"))
                    return "Hoàn thành";  
                if (status.Equals("399"))
                    return "FS từ chối"; 
                if (status.Equals("300"))
                    return "Hoàn thành";  
                if (status.Equals("199") || status.Equals("299"))
                    return "Từ chối";
                if (status.Equals("999"))
                    return "Đã hủy";     
            }
            return "";
        }

        public static string AccountAction(this string status)
        {
            if (status != null)
            {
                if (status.Equals("000"))
                    return "Chờ xử lý";
                if (status.Equals("100"))
                    return "Đã liên hệ";
                if (status.Equals("200"))
                    return "Không nghe máy lần 1";
                if (status.Equals("300"))
                    return "Không nghe máy lần 2";
                if (status.Equals("400"))
                    return "Không nghe máy lần 3";
                if (status.Equals("999"))
                    return "Đã hủy";   
            }
            return "";
        }

        public static string SourceAccount(this int source)
        {
            if (source == 4)
                return "KH eKYC Website";
            if (source == 6)
                return "KH eKYC Mobile";
            if (source == 3)
                return "KH Form Website";
            if (source == 2)
                return "KH CRM";
            if (source == 1)
                return "KH EMS";
            return "";
        }

        public static string StatusProfile(this string status)
        {
            if (status != null)
            {
                if (status.Equals("000"))
                    return "Chờ xử lý";
                if (status.Equals("100"))
                    return "Chờ duyệt";
                if (status.Equals("101"))
                    return "Nợ hồ sơ";
                if (status.Equals("102"))
                    return "Không hồ sơ";
                if (status.Equals("200"))
                    return "Hoàn thành";
                if (status.Equals("199"))
                    return "Từ chối";
                if (status.Equals("299"))
                    return "Từ chối";
                if (status.Equals("999"))
                    return "Đã hủy";   
                
            }
            return "";
        }

        public static string ConfirmStatus(this int value)
        {
            if (value == 0 || value == 1)
            {
                if (value == 0)
                    return "Chưa kích hoạt";
                return "Đã kích hoạt";
            }
            return "";
        }
    }
}