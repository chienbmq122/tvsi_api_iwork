﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.CRM.WebAPI.Lib.Models.System;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using TVSI.CRM.WebAPI.SqlCommand;


namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class SystemService : BaseService, ISystemService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SystemService));
        private ISystemCommandText _commandText;
        public SystemService(ISystemCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<UserLoginResult> Login(string userName, string password)
        {
            return await WithEmsDbConnection(async conn =>
            {
                // Check user và mật khẩu đăng nhập trên LDAP
                var userDomain =
                    await conn.QueryFirstOrDefaultAsync<string>(_commandText.GetUserDomain, new {userId = userName});

                if (string.IsNullOrEmpty(userDomain))
                    return null;

                var isLoginDomainSuccess = CheckLdap(userDomain, password);

                if (!isLoginDomainSuccess)
                {
                    // Check EMS
                    var sql = @"SELECT A.ten_dang_nhap as UserName, B.ho_va_ten as FullName,
                                   B.ma_nhan_vien_quan_ly as SaleID, B.cap_quan_ly as Level,
                                   A.cap_do_he_thong_id as SystemID, C.ma_chi_nhanh as BranchID,
                                   A.nhan_su_id as UserID, C.ma_cap_do_he_thong as SystemCode
                                FROM TVSI_THONG_TIN_TRUY_CAP A 
                                INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id 
                                LEFT JOIN TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id 
                                WHERE A.ten_dang_nhap = @ten_dang_nhap AND a.mat_khau_dang_nhap = @mat_khau_dang_nhap AND a.trang_thai = 1";

                    var userEmsInfo = conn.Query<UserLoginResult>(sql, new
                                                                       {
                                                                           ten_dang_nhap = userName,
                                                                           mat_khau_dang_nhap =
                                                                               AccountUtis.EncriptMd5(userName +
                                                                                                      password)
                                                                       }).FirstOrDefault();


                    isLoginDomainSuccess = userEmsInfo != null;
                }

                if (isLoginDomainSuccess)
                {
                    var userInfo = await conn.QueryFirstOrDefaultAsync<UserLoginResult>(_commandText.GetUserInfo,
                            new
                            {
                                ten_dang_nhap = userName,
                        
                            });

                    if (userInfo != null)
                    {
                        var roleList = await conn.QueryAsync<TVSI_sDANH_MUC_QUYEN_ByTenTruyCap_Result>(
                                    "TVSI_sDANH_MUC_QUYEN_SU_DUNG_ByTenTruyCap",
                                    new { ten_dang_nhap = userName }, commandType: CommandType.StoredProcedure);


                        var filterFunctionList = new List<string> {"CRM_SALE", "CRM_BB"};
                        userInfo.RoleList = new List<UserRole>();

                        var roleCode = "CRM_BO";
                        
                        foreach (var roleItem in roleList)
                        {
                            if (filterFunctionList.Contains(roleItem.ma_chuc_nang))
                            {
                                if (!"CRM_BB".Equals(roleCode))
                                    roleCode = roleItem.ma_chuc_nang;
                            }
                        }
                        userInfo.RoleList.Add(new UserRole
                        {
                            FunctionCode = roleCode,
                            IsAdd = true,
                            IsEdit = true,
                            IsRead = true,
                            IsDelete = true
                        });

                        return userInfo;
                    }

                }

                return null;
            });
        }

        #region Private function
        public bool CheckLdap(string username, string pass)
        {
            string ldapServerIp = ConfigurationManager.AppSettings["LDAP_SERVER_IP"];
            // This function requires Imports System.Net and Imports System.DirectoryServices.Protocols
            System.DirectoryServices.Protocols.LdapConnection ldapConnection = new System.DirectoryServices.Protocols.LdapConnection(new LdapDirectoryIdentifier(ldapServerIp));
            //  Use the TimeSpan constructor to specify...
            //  ... days, hours, minutes, seconds, milliseconds.
            TimeSpan mytimeout = new TimeSpan(0, 0, 0, 1);
            try
            {
                ldapConnection.AuthType = AuthType.Negotiate;
                ldapConnection.AutoBind = false;
                ldapConnection.Timeout = mytimeout;
                ldapConnection.Credential = new NetworkCredential(username, pass);
                ldapConnection.Bind();
                return true;

            }
            catch (LdapException ex)
            {
                log.Error("CheckLdap:username=" + username + ":" + ex.Message);
                log.Error(ex.InnerException);
                return false;
            }
        }
        #endregion


        public async Task<NotificationResult> GetNotificationList(int pageIndex, int pageSize,
         string userName, string lang)
        {
            try
            {
                var start = (pageIndex - 1) * pageSize;

                return await WithCrmDbConnection(async conn =>
                {
                    var sql = _commandText.GetNotificationList;

                    if (CommonConst.LANGUAGE_EN.Equals(lang))
                        sql = sql.Replace(" Message,", "COALESCE(MessageEn, Message) Message,");

                    var notificationData = await conn.QueryAsync<NotificationData>(sql,
                            new { UserName = userName, Start = start, PageSize = pageSize });

                    var notificationCount = await conn.QueryFirstOrDefaultAsync<int>(_commandText.GetNumOfUnreadNotification,
                        new { UserName = userName });

                    return new NotificationResult
                    {
                        NotificationCount = notificationCount,
                        NotificationDatas = notificationData
                    };
                });

            }
            catch (Exception ex)
            {
                log.Error("GetNotificationList:userName=" + userName + ":" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> SyncNotificationRead(List<long> idList, string userName)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.SyncNotificationRead, new
                    {
                        MessageIDList = @idList,
                        UserName = userName,
                        UpdatedBy = userName,
                        UpdatedDate = DateTime.Now
                    });

                    return 1;
                });
            }
            catch (Exception ex)
            {
                log.Error("SyncNotificationRead:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
