﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.AssetManagement;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class AssetService : BaseService, IAssetService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private IAssetManagementCommandText _commandText;
        public AssetService(IAssetManagementCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<RoleUserAsset> GetRoleUser(BaseRequest model)
        {
            var listUserAdmin = ConfigurationManager.AppSettings["ASSET_MANAGEMENT_USER_ROLE_ADMIN"];
            RoleUserAsset data = new RoleUserAsset();
            data.UserName = model.UserName;
            if (model.UserName.Equals("tunt3"))
            {
                data.RoleUser = "SYSTERM_ADMIN";
            }else if (listUserAdmin.Contains(model.UserName))
            {
                data.RoleUser = "ADMIN";
            }
            else
            {
                data.RoleUser = "USER";
            }
            return data;
        }

        public async Task<List<AssetState>> GetListStateAsset()
        {
            var sql = _commandText.GetListStateAsset;
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetState> data =  await conn.QueryAsync<AssetState>(sql, new { });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListStateAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<AssetType>> GetListTypeAsset()
        {
            var sql = _commandText.GetListTypeAsset;
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetType> data = await conn.QueryAsync<AssetType>(sql, new { });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListTypeAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<DepartmentManagementAsset>> GetListDepartment()
        {
            try
            {
                return await WithEmsDbConnection(async conn =>
                {
                    IEnumerable<DepartmentManagementAsset> data = await conn.QueryAsync<DepartmentManagementAsset>(_commandText.GetListDepartment, new { });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListDepartment:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<UserManagementAsset>> GetListUser(UserManagementAssetRequest model)
        {
            try
            {
                return await WithEmsDbConnection(async conn =>
                {
                    IEnumerable<UserManagementAsset> data = await conn.QueryAsync<UserManagementAsset>(_commandText.GetListUserFromDepartment, new { DepartmentId = model.DepartmentId });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListUser:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListCommonResponse> GetListAsset(AssetListRequest model)
        {
            var sql = _commandText.GetListAsset;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListCommonResponse dataResult = new AssetListCommonResponse();
                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetDetailModel> data = await conn.QueryAsync<AssetDetailModel>(sql, new { typeId = model.TypeAssetId, Start = start, PageSize = model.PageSize });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountListAsset, new { typeId = model.TypeAssetId });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetListAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListResponse> GetListConfirmAsset(AssetListConfirmRequest model)
        {
            var sql = _commandText.GetListAssetConfirm;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListResponse dataResult = new AssetListResponse();
                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetModel> data = await conn.QueryAsync<AssetModel>(sql, new { Start = start, PageSize = model.PageSize });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountListAssetConfirm, new { });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetListConfirmAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateAsset(CreateAsset model)
        {
            try
            {
                return await WithAssetDbConnection(async conn => 
                {
                    await conn.ExecuteAsync(_commandText.CreateAsset, new
                    {
                        type = model.TypeAssetId,
                        state = model.StateAssetId,
                        money = model.Price,
                        quantity = model.quantity,
                        name = model.NameAsset,
                        serial_number = model.SerialNumber,
                        date_out_warranty = model.DateOutWarranty,
                        date_buy_asset = model.DateBuyAsset,
                        detail = model.Detail,
                        userId = model.UserName
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> UpdateAsset(UpdateAsset model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.UpdateAsset, new
                    {
                        type_assetid = model.TypeAssetId,
                        state_assetid = model.StateAssetId,
                        money = model.Price,
                        quantity = model.quantity,
                        name_asset = model.NameAsset,
                        serial_number_asset = model.NameAsset,
                        date_out_warranty = model.DateOutWarranty,
                        date_buy_asset = model.DateBuyAsset,
                        detail = model.Detail,
                        updatedBy = model.UserName,
                        assetid = model.AssetId,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> DeleteAsset(DeleteAsset model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.DeleteAsset, new
                    {
                        userId = model.UserName,
                        assetid = model.AssetId,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("DeleteAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetDetailModel> GetDetailAsset(DeleteAsset model)
        {
            try
            {
                return await WithAssetDbConnection(async conn => 
                {
                    return await conn.QueryFirstOrDefaultAsync<AssetDetailModel>(_commandText.GetDetailAsset, new { assetId = model.AssetId });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetDetailAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListCommonResponse> FilterAsset(FilteAsset model)
        {
            var sql = _commandText.FilterAsset;
            var sqlCount = _commandText.FilterCount;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListCommonResponse dataResult = new AssetListCommonResponse();

                if (!String.IsNullOrEmpty(model.NameAsset))
                {
                    sql += " AND TVSI_ASSET.name_asset LIKE @NameAsset";
                    sqlCount += " AND TVSI_ASSET.name_asset LIKE @NameAsset";
                }

                if (!String.IsNullOrEmpty(model.SerialNumberAsset))
                {
                    sql += " AND TVSI_ASSET.serial_number_asset LIKE @SerialNumberAsset";
                    sqlCount += " AND TVSI_ASSET.serial_number_asset LIKE @SerialNumberAsset";
                }

                if (!String.IsNullOrEmpty(model.DateBuyAsset))
                {
                    sql += " AND TVSI_ASSET.date_buy_asset = @DateBuyAsset";
                    sqlCount += " AND TVSI_ASSET.date_buy_asset = @DateBuyAsset";
                }

                if (model.TypeAssetId > 0)
                {
                    sql += " AND TVSI_ASSET.type_assetid = @TypeAssetId";
                    sqlCount += " AND TVSI_ASSET.type_assetid = @TypeAssetId";
                }

                if (model.StateAssetId > 0)
                {
                    sql += " AND TVSI_ASSET.state_assetid = @StateAssetId";
                    sqlCount += " AND TVSI_ASSET.state_assetid = @StateAssetId";
                }

                sql += " ORDER BY TVSI_ASSET.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetDetailModel> data = await conn.QueryAsync<AssetDetailModel>(sql, new
                    {
                        NameAsset = "%" + model.NameAsset + "%",
                        SerialNumberAsset = "%" + model.SerialNumberAsset + "%",
                        DateBuyAsset = model.DateBuyAsset,
                        StateAssetId = model.StateAssetId,
                        TypeAssetId = model.TypeAssetId,
                        Start = start,
                        PageSize = model.PageSize
                    });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(sqlCount, new
                    {
                        NameAsset = "%" + model.NameAsset + "%",
                        SerialNumberAsset = "%" + model.SerialNumberAsset + "%",
                        DateBuyAsset = model.DateBuyAsset,
                        StateAssetId = model.StateAssetId,
                        TypeAssetId = model.TypeAssetId,
                    });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetListAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<AssetChildModelSelect>> GetListAssetChildSeclect(AssetChildModelSelectReq model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetChildModelSelect> data = await conn.QueryAsync<AssetChildModelSelect>(_commandText.GetListAssetSelect, new { typeId = model.TypeAssetId });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAssetChildSeclect:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListCommonResponse> GetListAssetUser(AssetListConfirmRequest model)
        {
            var sql = _commandText.GetListAssetUser;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListCommonResponse dataResult = new AssetListCommonResponse();
                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetDetailModel> data = await conn.QueryAsync<AssetDetailModel>(sql, new { Start = start, PageSize = model.PageSize, userid = model.UserName });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountListAssetUser, new { userid = model.UserName });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetListAssetUser:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListResponse> GetHistoryAssetUser(AssetListConfirmRequest model)
        {
            var sql = _commandText.GetHistoryAssetUser;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListResponse dataResult = new AssetListResponse();
                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetModel> data = await conn.QueryAsync<AssetModel>(sql, new { Start = start, PageSize = model.PageSize, userid = model.UserName });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountHistoryAssetUser, new { userid = model.UserName });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetHistoryAssetUser:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<AssetChildModel>> GetListAssetChild(RequestAssetChild model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetChildModel> data = await conn.QueryAsync<AssetChildModel>(_commandText.GetListAssetChild, new { assetId = model.AssetId });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAssetChild:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> AsignAssetChild(AginAssetChild model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.AsignAssetChild, new
                    {
                        assetParentId = model.AssetParentId,
                        assetChildId = model.AssetChildId,
                        quantity = model.Quantity,
                        userId = model.UserName,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("AsignAssetChild:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> RemoveAssetChild(RemoveAssetChild model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.RemoveAssetChild, new
                    {
                        relationshipId = model.RelationshipId,
                        userId = model.UserName,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("RemoveAssetChild:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<AssetManagement>> GetHistoryReceiveAsset(DeleteAsset model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetManagement> data = await conn.QueryAsync<AssetManagement>(_commandText.GetHistoryManagementAsset, new { assetId = model.AssetId });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAssetChild:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateManagement(RequestCreateManagement model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateManagementAsset, new
                    {
                        assetid = model.AssetId,
                        receiveUserid = model.ReceiveUserid,
                        dateReceiveAsset = model.DateReceiveAsset,
                        managementState = model.Status,
                        departmentid = model.DepartmentId,
                        quantity = model.Quantity,
                        userId = model.UserName,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateManagement:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> UpdateManagement(RequestUpdateManagement model)
        {
            var sql = _commandText.UpdateManagementAsset;
            try
            {
                var StateUse = 0;
                switch (model.Status)
                {
                    case "PrePass":
                        StateUse = 4;
                        break;
                    case "Pass":
                        StateUse = 2;
                        break;
                    case "Response":
                        StateUse = 3;
                        break;
                    case "ConfirmRespon":
                        StateUse = 11;
                        break;
                    default:
                        StateUse = 1;
                        break;
                }
                if(!String.IsNullOrEmpty(model.DateResponseAsset) && StateUse == 11)
                {
                    sql += " date_response_asset = @date_response_asset, ";
                }
                if (!String.IsNullOrEmpty(model.DateReceiveAsset))
                {
                    sql += " date_receive_asset = @date_receive_asset, ";
                }
                if(!String.IsNullOrEmpty(model.Quantity.ToString()))
                {
                    sql += " quantity = @Quantity, ";
                }
                sql += "management_state = @Status ,updated_by = @userId WHERE management_assetid = @ManagementId";

                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(sql, new
                    {
                        date_response_asset = model.DateResponseAsset,
                        date_receive_asset = model.DateReceiveAsset,
                        Quantity = model.Quantity,
                        ManagementId = model.ManagementId,
                        Status = StateUse,
                        userId = model.UserName
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateManagement:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetListResponse> FilterManagementAsset(FliterManagementAsset model)
        {
            var sql = _commandText.FilterManagementAsset;
            var sqlCount = _commandText.FilterCountManagement;
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                AssetListResponse dataResult = new AssetListResponse();

                if (!String.IsNullOrEmpty(model.UserId))
                {
                    sql += " AND TVSI_MANAGEMENT_ASSET.userid = @UserId";
                    sqlCount += " AND TVSI_MANAGEMENT_ASSET.userid = @UserId";
                }

                if (!String.IsNullOrEmpty(model.DepartmentId.ToString()))
                {
                    sql += " AND TVSI_MANAGEMENT_ASSET.departmentid = @DepartmentId";
                    sqlCount += " AND TVSI_MANAGEMENT_ASSET.departmentid = @DepartmentId";
                }

                if (!String.IsNullOrEmpty(model.DateReceiveAsset))
                {
                    sql += " AND TVSI_MANAGEMENT_ASSET.date_receive_asset = @DateReceiveAsset";
                    sqlCount += " AND TVSI_MANAGEMENT_ASSET.date_receive_asset = @DateReceiveAsset";
                }

                if (!String.IsNullOrEmpty(model.DateResponseAsset))
                {
                    sql += " AND TVSI_MANAGEMENT_ASSET.date_response_asset = @DateResponseAsset";
                    sqlCount += " AND TVSI_MANAGEMENT_ASSET.date_response_asset = @DateResponseAsset";
                }
                else
                {
                    sql += " AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL";
                    sqlCount += " AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL";
                }
                sql += " ORDER BY TVSI_MANAGEMENT_ASSET.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

                log.Error(sql);

                await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetModel> data = await conn.QueryAsync<AssetModel>(sql, new
                    {
                        UserId = model.UserId,
                        DepartmentId = model.DepartmentId,
                        DateReceiveAsset = model.DateReceiveAsset,
                        DateResponseAsset = model.DateResponseAsset,
                        Start = start,
                        PageSize = model.PageSize
                    });
                    dataResult.Items = data.ToList();

                    dataResult.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(sqlCount, new
                    {
                        UserId = model.UserId,
                        DepartmentId = model.DepartmentId,
                        DateReceiveAsset = model.DateReceiveAsset,
                        DateResponseAsset = model.DateResponseAsset,
                    });
                });
                return dataResult;
            }
            catch (Exception ex)
            {
                log.Error("GetListAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<AssetInspectionDetailModel> GetInspectionDetail(InspectionDeleteRequest model)
        {
            try
            {
                var sqlChecked = _commandText.GetListAssetInIds;
                var sqlNotChecked = _commandText.GetListAssetInIds;
                return await WithAssetDbConnection(async conn =>
                {
                    AssetInspectionDetailModel data = new AssetInspectionDetailModel();
                    AssetInspectionModel datainspection = await conn.QueryFirstOrDefaultAsync<AssetInspectionModel>(_commandText.GetInspectionDetail, new { InspectionId = model.InspectionId });

                    var listIdChecked = datainspection.AssetCheckedInspection;
                    var listIdNotChecked = datainspection.AssetNotCheckedInspection;
                    List<AssetDetailModel> listDataChecked = new List<AssetDetailModel>();
                    List<AssetDetailModel> listDataNotChecked = new List<AssetDetailModel>();
                    if (!string.IsNullOrEmpty(listIdChecked))
                    {
                        sqlChecked = sqlChecked.Replace("{ids}", "(" + listIdChecked + ")");
                        IEnumerable<AssetDetailModel> dataChecked = await conn.QueryAsync<AssetDetailModel>(sqlChecked, new { });
                        listDataChecked = dataChecked.ToList();
                    }
                    if (!string.IsNullOrEmpty(listIdNotChecked))
                    {
                        sqlNotChecked = sqlNotChecked.Replace("{ids}", "(" + listIdNotChecked + ")");
                        IEnumerable<AssetDetailModel> dataNotChecked = await conn.QueryAsync<AssetDetailModel>(sqlNotChecked, new { });
                        listDataNotChecked = dataNotChecked.ToList();
                    }
                    data.DateInspection = datainspection.DateInspection;
                    data.DepartmentId = datainspection.DepartmentId;
                    data.InspectionId = datainspection.InspectionId;
                    data.CreatedBy = datainspection.CreatedBy;
                    data.CreatedDate = datainspection.CreatedDate;
                    data.UpdatedBy = datainspection.UpdatedBy;
                    data.UpdatedDate = datainspection.UpdatedDate;
                    data.AssetCheckedInspection = listDataChecked;
                    data.AssetNotCheckedInspection = listDataNotChecked;
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetDetailAsset:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<AssetInspectionModel>> GetListInspection()
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetInspectionModel> data = await conn.QueryAsync<AssetInspectionModel>(_commandText.GetListInspection, new { });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListInspection:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<AssetInspectionModel>> GetHistoryInspection(RequestAssetInspectionModel model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    IEnumerable<AssetInspectionModel> data = await conn.QueryAsync<AssetInspectionModel>(_commandText.GetHistoryInspection, new { DepartmentId = model.DepartmentId });
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetHistoryInspection:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateInspectionAsset(InspectionCreateRequest model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateInspectionAsset, new
                    {
                        dateInspection = model.DateInspection,
                        departmentid = model.DepartmentId,
                        assetCheckedInspection = model.AssetCheckedInspection,
                        assetNotCheckedInspection = model.AssetNotCheckedInspection,
                        userId = model.UserName,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateManagement:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> UpdateInspectionAsset(InspectionUpdateRequest model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.UpdateInspectionAsset, new
                    {
                        InspectionId = model.InspectionId,
                        assetCheckedInspection = model.AssetCheckedInspection,
                        assetNotCheckedInspection = model.AssetNotCheckedInspection,
                        userId = model.UserName,
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateManagement:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> DeleteInspectionAsset(InspectionDeleteRequest model)
        {
            try
            {
                return await WithAssetDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.DeleteInspectionAsset, new
                    {
                        inspectionId = model.InspectionId
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateManagement:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
