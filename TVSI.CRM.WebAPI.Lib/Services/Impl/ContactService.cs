﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Contact;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class ContactService : BaseService, IContactService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private IContactCommandText _commandText;
        public ContactService(IContactCommandText commandText)
        {
            _commandText = commandText;
        }
        public async Task<ContactStaffResponse> GetListContactStaff(ContactStaffRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                var sqlUserStaf = _commandText.GetListUserStaf;
                var sqlCountStaf = _commandText.GetCountContact;
                return await WithEmsDbConnection(async conn =>
                {
                    var filterFunctionList = new List<string> { "CRM_BO", "CRM_SALE", "CRM_BB", "CRM_HR", "CRM_CF" };

                    ContactStaffResponse data = new ContactStaffResponse();
                    
                    List<ContactStaff> dataStaff = new List<ContactStaff>();
                    if (!String.IsNullOrEmpty(model.KeySearch))
                    {
                        sqlUserStaf = sqlUserStaf.Replace("{CON1}", "AND B.ho_va_ten LIKE N'%" + model.KeySearch + "%'");
                        sqlCountStaf = sqlCountStaf.Replace("{CON1}", "AND B.ho_va_ten LIKE N'%" + model.KeySearch + "%'");
                    }
                    else
                    {
                        sqlUserStaf = sqlUserStaf.Replace("{CON1}", "");
                        sqlCountStaf = sqlCountStaf.Replace("{CON1}", "");
                    }

                    IEnumerable<ContactStaff> ListContactStaff = await conn.QueryAsync<ContactStaff>(sqlUserStaf,
                            new { start = start, pageSize = model.PageSize });

                    int contactCount = await conn.QueryFirstOrDefaultAsync<int>(sqlCountStaf, new { });
                    dataStaff = ListContactStaff.ToList();

                    List<string> listUserNames = new List<string>();
                    foreach(var item in ListContactStaff)
                    {
                        listUserNames.Add(item.UserName);
                    }
                    
                    IEnumerable<ContactRole> ListRole = await conn.QueryAsync<ContactRole>(_commandText.GetListRoleStaf,
                            new { tenDangNhap = listUserNames });

                    foreach (var roleItem in ListRole)
                    {
                        if (filterFunctionList.Contains(roleItem.ma_chuc_nang))
                        {
                            ContactStaff contactStaff = dataStaff.FirstOrDefault(e => e.UserName == roleItem.ten_dang_nhap);
                            contactStaff.UserRole = roleItem.ma_chuc_nang;

                            if ("CR M_HR".Equals(roleItem.ma_chuc_nang) || "CRM_CF".Equals(roleItem.ma_chuc_nang))
                                contactStaff.UserRole = "CRM_BO";
                        }
                    }

                    data.ListContactStaffs = dataStaff;
                    data.TotalContact = contactCount;
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListContactStaff:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ContactCustomerResponse> GetListContactCust(ContactStaffRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                var sql = _commandText.GetListContacCustomer;
                var count = _commandText.GetCountContacCustomer;
                return await WithInnoDBConnection(async conn => 
                {
                    if (!String.IsNullOrEmpty(model.KeySearch))
                    {
                        sql = sql.Replace("{CON1}", "AND (CUSTOMER.CUSTOMERNAME LIKE N'%" + model.KeySearch + "%' OR CUSTOMER.CUSTOMERID LIKE N'%" + model.KeySearch + "%')");
                        count = count.Replace("{CON1}", "AND (CUSTOMER.CUSTOMERNAME LIKE N'%" + model.KeySearch + "%' OR CUSTOMER.CUSTOMERID LIKE N'%" + model.KeySearch + "%')");
                    }
                    else
                    {
                        sql = sql.Replace("{CON1}", "");
                        count = count.Replace("{CON1}", "");
                    }
                    log.Error(sql);

                    ContactCustomerResponse data = new ContactCustomerResponse();
                    IEnumerable<int> contactCount = await conn.QueryAsync<int>(count, 
                        new { UserName = model.UserName.Substring(0, 4) });

                    IEnumerable<ContactCustomer> ListContactCustomer = await conn.QueryAsync<ContactCustomer>(sql,
                            new { start = start, pageSize = model.PageSize,
                                UserName = model.UserName.Substring(0, 4) });

                    data.TotalContact = contactCount.ToList()[0];
                    data.ListContactCustomers = ListContactCustomer.ToList();
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListContactCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ContactInfoStaff> GetInfoContactStaff(ContactinfoStaffRequest model)
        {
            try
            {
                return await WithEmsDbConnection(async conn => {
                    return await conn.QueryFirstOrDefaultAsync<ContactInfoStaff>(_commandText.GetInfoStaff, new { SaleId = model.StaffID });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInfoContactStaff:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ContactInfoLead> GetInfoContactLead(ContactinfoLeadRequest model)
        {
            try
            {
                ContactInfoStaff dataUser = await WithEmsDbConnection(async conn => {
                    return await conn.QueryFirstOrDefaultAsync<ContactInfoStaff>(_commandText.GetInfoStaff, new { SaleId = model.SaleID });
                });
                return await WithCrmDbConnection(async conn => {

                    ContactInfoLead data = await conn.QueryFirstOrDefaultAsync<ContactInfoLead>(_commandText.GetInfoLead, new { LeadID = model.LeadID });
                    log.Error("GetInfoContactLead:" + data.LeadName);
                    data.SaleID = dataUser.SaleID;
                    data.SaleName = dataUser.SaleName;
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInfoContactLead:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ContactInfoCustomer> GetInfoContactCust(ContactinfoCustRequest model)
        {
            try
            {
                ContactInfoStaff dataUser = await WithEmsDbConnection(async conn => {
                    return await conn.QueryFirstOrDefaultAsync<ContactInfoStaff>(_commandText.GetInfoStaff, new { SaleId = model.SaleID });
                });
                return await WithCrmDbConnection(async conn => {

                    ContactInfoCustomer data = await conn.QueryFirstOrDefaultAsync<ContactInfoCustomer>(_commandText.GetInfoCust, new { CustCode = model.CustCode });
                    data.SaleID = dataUser.SaleID;
                    data.SaleName = dataUser.SaleName;
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInfoContactCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
