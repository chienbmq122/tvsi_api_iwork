﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IAttributeCommandText
    {
        /// <summary>
        /// Get list attribute 
        /// </summary>
        string GetListAttribute { get; }

        /// <summary>
        /// Get list attribute for Lead 
        /// </summary>
        string GetListAttributeLead { get; }

        /// <summary>
        /// Get list attribute for customer 
        /// </summary>
        string GetListAttributeCus { get; }

        /// <summary>
        /// Create attribute for lead 
        /// </summary>
        string CreateAttributeLead { get; }


        /// <summary>
        /// Create attribute for cust 
        /// </summary>
        string CreateAttributeCust { get; }

        /// <summary>
        /// delete attribute for lead 
        /// </summary>
        string DeleteAttributeLead { get; }

        /// <summary>
        /// Delete attribute for cust 
        /// </summary>
        string DeleteAttributeCust { get; }

        /// <summary>
        /// check lead attribute for cust 
        /// </summary>
        string CheckLeadAttribute { get; }

        /// <summary>
        /// check cust attribute for cust 
        /// </summary>
        string CheckCustAttribute { get; }

        /// <summary>
        /// get list category
        /// </summary>
        string GetListCategory { get; }
    }
}
