﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.AssetManagement
{
    public class AssetChildModel
    {
        [Description("id quan hệ")]
        public int RelationshipId { get; set; }
        [Description("id thiết bị")]
        public int AssetId { get; set; }
        [Description("Id asset type")]
        public int TypeAssetId { get; set; }
        [Description("Tình trạng tài sản")]
        public int StateAssetId { get; set; }
        [Description("Giá tiền")]
        public int Money { get; set; }
        [Description("Mã serial")]
        public string SerialNumberAsset { get; set; }
        [Description("Số lượng trong kho")]
        public int Quantity { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        [Description("Ngày nhập")]
        public string DateBuyAsset { get; set; }
        [Description("Hạn bảo hành")]
        public string DateOutWarranty { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Id mượn trả")]
        public int ManagementAssetId { get; set; }
        [Description("Ngày gán")]
        public string CreatedDate { get; set; }
        [Description("Ngày cập nhật")]
        public string UpdatedDate { get; set; }
        [Description("Ngày gỡ")]
        public string DeletedDate { get; set; }
    }

    public class RequestAssetChild : BaseRequest
    {
        [Description("Id thiết bị")]
        public int AssetId { get; set; }
        [Description("0: thiết bị đang gán, 1: lịch sử gán gỡ")]
        public int Type { get; set; }
    }

    public class AginAssetChild : BaseRequest
    {
        [Description("Id thiết bị")]
        public int AssetParentId { get; set; }
        [Description("Thiết bị cần gán")]
        public int AssetChildId { get; set; }
        [Description("Số lượng")]
        public int Quantity { get; set; }
    }

    public class RemoveAssetChild : BaseRequest
    {
        [Description("Id quan hệ")]
        public int RelationshipId { get; set; }
    }

    public class AssetChildModelSelect
    {
        [Description("Id asset type")]
        public int AssetId { get; set; }
        [Description("Mã serial")]
        public string SerialNumberAsset { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Số lượng trong kho")]
        public int Quantity { get; set; }
    }

    public class AssetChildModelSelectReq : BaseRequest
    {
        [Description("Loai tài sản")]
        public int TypeAssetId { get; set; }
    }
}
