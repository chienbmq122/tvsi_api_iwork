﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class CustomerListRequest : BaseRequest
    {
        [Description("Từ khóa tìm kiếm : Họ tên, mã khách hàng")]
        public string keySearch { get; set; }

        [Description("Trạng thái : Null lấy tất cả, 1 hoạt động, 99 không hoạt động")]
        [Required]
        public int Status { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class CustomerListResult
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        [Description("Họ và tên")]
        public string CustName { get; set; }
        [Description("Số điện thoại")]
        public string Mobile { get; set; }
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        [Description("ID khách hàng - Cá nhân: 1, Tổ chức: 2")]
        public int ProfileType { get; set; }
        [Description("Loại khách hàng - Cá nhân: 1, Tổ chức: 2")]
        public string ProfileTypeName => ResourceFile.SystemModule.ResourceManager.GetString("ProfileType_" + ProfileType);
        [Description("Trạng thái tài khoản")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.SystemModule.ResourceManager.GetString("CustStatus_" + Status);
        [Description("Mã nhân viên môi giới quản lý")]
        public string AssignUser { get; set; }
     
        public string MobileStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Mobile))
                {
                    return Mobile.Length >= 3 ? Mobile.Remove(Mobile.Length - 3) + "***" : "***";
                }

                return Mobile;
            }
        }

        public string EmailStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Email))
                {
                    return Email.Length >= 3 ? "***" + Email.Substring(3) : "***";
                }

                return Email;
            }
        }
    }

    public class CustomerListSelectRequest : BaseRequest
    {
        [Description("Code custome")]
        [Required]
        public string CustCode { get; set; }
        [Description("Trang")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi 1 trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class CustomerListSelectRessult
    {
        [Description("custome code")]
        public string CustCode { get; set; }
        [Description("Họ và tên")]
        public string CustName { get; set; }
    }

    public class CustomerListSelectResponse
    {
        [Description("Tổng số custome")]
        public int totalItem { get; set; }
        [Description("Danh sách custome")]
        public IEnumerable<CustomerListSelectRessult> Items { get; set; }
    }

}
