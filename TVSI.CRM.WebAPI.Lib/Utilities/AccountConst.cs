﻿using System.ComponentModel;

namespace TVSI.CRM.WebAPI.Lib.Utilities
{

    public enum AdditionContractConstEnum
    {
        Normal = 0,     // HĐ thường
        Margin = 1,     // HĐ Margin
        Derivative = 2  // HĐ phái sinh
    }
    

    public class AdditionContractConst
    {
        public const int Normal = 0;
        public const int Margin = 1;
        public const int Derivative = 2;
        
    }
    
    public class OpenAccConst
    {
        public const string BRANCH_ID_DEFAULT = "01";
        public const string BRANCH_NAME_DEFAULT = "HỘI SỞ CHÍNH";

        public const string REGIST_SYMBOL = "w:char=\"F053\"";
        public const string DEFAULT_SYMBOL = "w:char=\"F0A3\"";

        public const string PRINT_MAU_01A_CNTN = "01_Mau_01A_HDMTK_CNTN";
        public const string PRINT_MAU_01A_CNTN_BR60 = "01_Mau_01A_HDMTK_CNTN_BR60";
        public const string PRINT_MAU_02A_CNTN = "02_Mau_02A_HDGDTT_CNTN";
        public const string PRINT_MAU_03A_CNTN = "03_Mau_03A_PhieuYC_CNTN";
        public const string PRINT_MAU_05A_CNTN = "05_Mau_05_PhieuDK_KenhTD_CNTN";
        public const string PRINT_MAU_00_CNTN = "00_Mau_00_XNHoSo_CNTN";
        public const string PRINT_MAU_FS02_CNTN = "FS02_Mau_CapT0_CNTN";
        public const string PRINT_MAU_FS05_CNTN = "FS05_Mau_DKyHuyActiveMargin";

        public const string PRINT_MAU_01B_TCTN = "01_Mau_01B_HDMTK_TCTN";
        public const string PRINT_MAU_02A_TCTN = "02_Mau_02A_HDGDTT_TCTN";
        public const string PRINT_MAU_03B_TCTN = "03_Mau_03B_PhieuYC_TCTN";
        public const string PRINT_MAU_05A_TCTN = "05_Mau_05_PhieuDK_KenhTD_TCTN";
        public const string PRINT_MAU_00_TCTN = "00_Mau_00_XNHoSo_TCTN";
        public const string PRINT_MAU_FS02_TCTN = "FS02_Mau_CapT0_TCTN";
        public const string PRINT_MAU_FS05_TCTN = "FS05_Mau_DKyHuyActiveMargin_TCTN";


        public const string PRINT_MAU_00_CNNN = "00_Mau_00_XNHoSo_CNNN";
        public const string PRINT_MAU_01A_CNNN = "01_Mau_01A_HDMTK_CNNN";
        public const string PRINT_MAU_02A_CNNN = "02_Mau_02A_HDGDTT_CNNN";
        public const string PRINT_MAU_03A_CNNN = "03_Mau_03A_PhieuYC_CNNN";
        public const string PRINT_MAU_05A_CNNN = "05_Mau_05_UQTradingCode_CNNN";
        public const string PRINT_MAU_06A_CNNN = "06_Mau_06_DKTradingCode_CNNN";

    }
    public enum ServiceStatusConst
    {
        [Description("Không đăng ký")]
        KO_DANG_KY = -1,
        [Description("Chờ xử lý")]
        CHO_XU_LY = 0,
        [Description("Chờ HO xác nhận")]
        CHO_HO_XAC_NHAN = 1,
        [Description("Chờ KH xác nhận")]
        CHO_KH_XAC_NHAN = 2,
        [Description("Từ chối")]
        BUMAN_TU_CHOI = 99,
        [Description("Từ chối")]
        KSV_TU_CHOI = 98
    }
    public enum TransferMoneyConst
    {
        Bank = 1, // sang tk ngan hang
        Account = 2, // chuyen tien noi bo
        Bidv = 3, // tai khoan nuoc ngoai
        Phone = 4,
        iTradeHome = 5
    }
    public static class DocumentStatusConst
    {
        public const string TAO_MOI_000 = "000";
        public const string CHO_XU_LU_100 = "100";
        public const string HOAN_THANH_200 = "200";
        public const string TU_CHOI_199 = "199";
        public const string NO_HO_SO_101 = "101";
    }
    public enum DocumentTypeConst
    {
        OPEN_DOCUMENT = 1,
        ADDITION_DOCUMENT = 2
    }
    public enum SourceInputTypeConst
    {
        EMS = 1,
        CRM = 2,
        WEBSITE = 3
    }
    
    
}
public class AccountConst
{
    public const string VALID_CUSTOMER_E800  = "E800";           // Không tìm thấy dữ liệu
}