﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class CommentText : ICommentText
    {
        #region SQL Command List
        public string GetListCommentLead => @"SELECT * FROM Comment WHERE LeadID = @LeadID AND Status = 1";

        public string GetListCommentCust => @"SELECT * FROM Comment WHERE CustCode = @CustCode AND Status = 1";

        public string GetListReply => @"SELECT * FROM Reply WHERE CommentID IN @CommentIDs AND Status = 1";

        public string CreateComment => @"INSERT INTO Comment(LeadID,CustCode,Content,UserName,FullName,Rate1,Rate2,Rate3,Status,CreatedBy,CreatedDate) VALUES 
                                        (@LeadID,@CustCode,@Content,@UserName,@FullName,@Rate,0,0,1,@UserName,@createdDate)";

        public string CreateReply => @"INSERT INTO Reply(CommentID,Reply,UserName,FullName,Status,CreatedBy,CreatedDate) VALUES 
                                        (@CommentID,@Reply,@UserName,@FullName,1,@UserName,@CreatedDate)";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
