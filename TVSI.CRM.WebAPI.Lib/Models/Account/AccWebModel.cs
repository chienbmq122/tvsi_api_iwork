﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.Utilities;

namespace TVSI.CRM.WebAPI.Lib.Models.Account
{
    public class AccWebListRequest : BaseRequest
    {

        [Description("Từ ngày : dd/MM/yyyy")]
        public string FromDate { get; set; }

        [Description("Đến ngày: dd/MM/yyyy")]
        public string ToDate { get; set; }
        
        [Description("Từ khóa tìm kiếm : Họ tên, mã khách hàng")]
        public string keySearch { get; set; }

        [Description("Trạng thái : Null lấy tất cả, 1 hoạt động, 99 không hoạt động")]
        [Required]
        public string Status { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }

        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }


    public class AccWebListResult
    {
         [Description("Id hồ sơ")]
         public int ProfileId { get; set; }
         
         [Description("mã khách hàng")]
         public string CustCode { get; set; }
         
         [Description("số CMND")]
         public string CardID { get; set; }
         

        [Description("Họ và tên")]
         public string CustName { get; set; }
        [Description("Số điện thoại")]
        public string Mobile { get; set; }

        [Description("ID khách hàng - Cá nhân: 1, Tổ chức: 2")]
        public int ProfileType { get; set; }

        [Description("Loại khách hàng - Cá nhân: 1, Tổ chức: 2")]
        public string ProfileTypeName =>
            ResourceFile.SystemModule.ResourceManager.GetString("ProfileType_" + ProfileType);

        [Description("Trạng thái tài khoản")]
        public string Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName
        {
            get
            {
              return Status.AccountStatusCommon();
            }
        }
        
        
        [Description("trạng thái tương tác")]
        public string StatusAction { get; set; }
        
        [Description("tên trạng thái tương tác ")]
        public string StatusActionName {
            get
            {
                return StatusAction.AccountAction();
            }
            
        }
        
        [Description("Email khách hàng")]
        public string Email { get; set; }
        
        [Description("KÊNH MỞ TK")]
        public int SourceAccount { get; set; }
        
        [Description("tên Kênh mở tk")]
        public string SourceAccountName {
            get
            {
                return SourceAccount.SourceAccount();
            }
            
        }
        
        [Description("trạng thái hồ sơ")]
        public string StatusProfile { get; set; }

        [Description("tên trạng thái hồ sợ")]
        public string StatusProfileName
        {
            get
            {
                return StatusProfile.StatusProfile();
            }
        }
        
        
        
        

        [Description("Mã nhân viên môi giới quản lý")]
        public string AssignUser { get; set; }

        public string MobileStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Mobile))
                {
                    return Mobile.Length >= 3 ? Mobile.Remove(Mobile.Length - 3) + "***" : "***";
                }

                return Mobile;
            }
        }

        [Description("Ngày tạo")]
        public string CreateDate { get; set; }
        
        [Description("Người tạo")]
        public string CreateBy { get; set; }
        
        [Description("Người cập nhật")]
        public string UpdateBy { get; set; }
        
        [Description("Nội dung xử lý")]
        public string Description { get; set; }
        
        [Description("Nhân viên xử lý")]
        public string HandledBy { get; set; }
        
        [Description("Loại Khách hàng")]
        public string CustomerType { get; set; }
        
        [JsonIgnore]
        public string CodeConfirm { get; set; }
        
        [Description("mã code xác thực")]
        public string ConfirmSms { get; set; }
        
        [Description("trạng thái eKYC int")]
        public int? StatusEKYC { get; set; }
        
        [Description("trạng thái eKYC text")]
        public string StatusEKYCStr { get; set; }
        
    }

    public class AccountDetailWebsiteRequest : BaseRequest
    {
        [Description("Mã Hồ Sơ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.CommonErrors.E222),
            ErrorMessageResourceType = typeof(ResourceFile.CommonErrors))]
        public int ProfileId { get; set; }
        [Description("Mã chi nhánh")]
        public string BranchID { get; set; }
    }

    public class AccountDetailResult
    {
        // Đã có
        [Description("Họ và Tên")]
        public string FullName { get; set; }

        //Đã có
        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        public string CardId { get; set; }

        //Đã có
        [Description("Ngày cấp")]
        public string IssueDate {
            get
            {
                return IssueDateOn.ToString("dd/MM/yyyy");
            } }
        
        [Description("Ngày cấp")]
        [JsonIgnore]
        public DateTime IssueDateOn { get; set; }

        //Đã có
        [Description("Nơi cấp CMND/CCCD của KH")]
        public string CardIssuer { get; set; }

        //Đã có
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        public string Sex { get; set; }
        
        //Đã có
        [Description("Ngày sinh dd/MM/yyyy")] 
        public string Birthday
        {
            get { return BirthdayOn.ToString("dd/MM/yyyy"); }
        }   
        
        [Description("Ngày sinh dd/MM/yyyy")] 
        [JsonIgnore]
        public DateTime BirthdayOn { get; set; }

        // Đã có
        [Description("Địa chỉ liên hệ")] 
        public string Address_01 { get; set; }
        
        //Đã có
        [Description("Số điện thoại của KH 01")]
        public string Phone_01 { get; set; }


        [Description("Số điện thoại của KH 02")]
        public string Phone_02 { get; set; }


        //Đã có
        [Description("Địa chỉ email")]
        public string Email { get; set; }


        //Đã có
        [Description("Mã nhân viên tư vấn. ")]
        public string SaleID { get; set; }

        //Đã có
        [Description("nhân viên xử lý. ")]
        public string HandledBy { get; set; }

        [Description("Yêu cầu")]
        public string NoteRequest { get; set; }

        [Description("Trạng thái string")]
        public string StatusName {
            get
            {
                return StatusNumber.AccountStatusCommon();
            } }
        
        [Description("Trạng thái Number")]
        public string StatusNumber { get; set; }
        
        [Description("Trạng thái tương tác")]
        public string StatusAction { get; set; } 
        [Description("Trạng thái tương tác number")]
        public string StatusActionName {
            get
            {
                return StatusAction.AccountAction();
            }
        }


        [Description("Người tạo")]
        public string CreateBy { get; set; }
        


        [Description("Ngày tạo")]
        public string CreateDate {
            get
            {
                return CreateDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        
        [Description("Ngày tạo")]
        [JsonIgnore]
        public DateTime CreateDateOn { get; set; }
        
        [Description("Người cập nhật cuối")]
        public string CreateLater { get; set; }
        
        
        [Description("Ngày cập nhật cuối")]
        public string UpdateDateLater { get; set; }
        
        [Description("Nguồn dữ liệu đăng ký")]
        public int SourceAccount { get; set; }
        
        [JsonIgnore]
        public string ConfirmSms { get; set; }
        [JsonIgnore]
        public string ConfirmCode { get; set; }
        
        public int ConfirmStatus { get; set; }

        public string ConfirmStatusName
        {
            get
            {
               return ConfirmStatus.ConfirmStatus();
            }
        }


        public string LinkActivated { get; set; }
        public string ImageFront { get; set; }
        public string ImageBack { get; set; }
        public string ImageFace { get; set; }
        
        [Description("Nội dung xử lý")]
        public string Description { get; set; }
        
        
        
        [Description("Tên chủ tài khoản")]
        public string BankAccName_01 { get; set; } 
        
        [Description("Số tài khoản")]
        public string BankAccNo_01 { get; set; }
        
        [Description(" ngân hàng")]
        public string BankNo_01 { get; set; }
        
        [Description("Ten ngan hang")]
        public string BankName_01 { get; set; }
        
        [Description("Mã Chi nhánh")]
        public string SubBranchNo_01 { get; set; } 

        
        [Description("ten chi nhanh")]
        public string SubBranchName_01 { get; set; } 
        
        [Description("Mã thành phố")]
        public string CityNo_01 { get; set; }
        
        
                
        [Description("Tên chủ tài khoản")]
        public string BankAccName_02 { get; set; } 
        
        [Description("Số tài khoản 02")]
        public string BankAccNo_02 { get; set; }
        
        [Description(" ngân hàng 02")]
        public string BankNo_02  { get; set; }
        
        [Description("Ten ngan hang 02")]
        public string BankName_02  { get; set; }
        
        [Description("Mã Chi nhánh 02")]
        public string SubBranchNo_02 { get; set; } 
        
        [Description(" chi nhanh 02")]
        public string SubBranchName_02  { get; set; } 
        
        [Description("Mã thành phố")]
        public string CityNo_02  { get; set; }
        
        
        [Description("Phương thức giao dịch")]
        public string TradeMethod { get; set; }   
        
        [Description("Phương thức nhận KQGD")]
        public string TradeMethodResult { get; set; }
        
        [Description("Phương thức sao kê")]
        public string TradeReportMethod { get; set; }
        
        
        [Description("Tên người đại diện")]
        public string TvsiManName { get; set; }
        
        [Description("Chức vụ")]
        public string TvsiManPosition { get; set; }
        
        [Description("Giấy ủy quyền số")]
        public string TvsiManNo { get; set; }
        
        [Description("Ngày cấp giấy ủy quyền")]
        public string TvsiManDate {
            get
            {
                return TvsiManDateOn?.ToString("dd/MM/yyyy");
            }
            set
            {
               
            }
        }
        
        [JsonIgnore]
        [Description("Ngày cấp giấy ủy quyền")]
        public DateTime? TvsiManDateOn { get; set; }
        
        [Description("Số CMND")]
        public string TvsiManCardId { get; set; }
        
        [Description("Ngày cấp CMND")]
        public string TvsiManCardDate {
            get
            {
                return TvsiManCardDateOn?.ToString("dd/MM/yyyy");
            }
            set
            {
                
            }
        }
        
        [Description("Ngày cấp CMND")]
        [JsonIgnore]
        public DateTime? TvsiManCardDateOn { get; set; }
        
        [Description("Nơi cấp CMND")]
        public string TvsiManCardIssue { get; set; }
        

        [Description("KH là công dân Hoa Kỳ 0: false, 1: true")]
        public int FatcaKhUsa { get; set; }

        [Description("KH có nơi sinh tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiSinhUsa { get; set; }

        [Description("KH có địa chỉ lưu ký tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiCuTruUsa { get; set; }

        [Description("KH có sdt liên lạc tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaSdtUsa { get; set; }

        [Description("KH có lệnh CK Hoa Kỳ 0: false, 1: true")]
        public int FatcaLenhCkUsa { get; set; }

        [Description("KH có giấy ủy quyền tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaGiayUyQuyenUsa { get; set; }

        [Description("KH có đại chỉ nhận thư hộ tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaDiaChiNhanThuUsa { get; set; }
        

        [Description("Thông tin mở rộng")] 
        public InfoExtend InfoExtend { get; set; } = new InfoExtend();
    }
    
    public class AccountDetailRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public string CustCode { get; set; }
        // Đã có
        [Description("Họ và Tên")]
        [Required]
        public string FullName { get; set; }

        //Đã có
        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        [Required]
        public string CardId { get; set; }

        //Đã có
        [Description("Ngày cấp dd/MM/yyyy")]
        [Required]
        public string IssueDate { get; set; }
        

        //Đã có
        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }

        //Đã có
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        [Required]
        public string Sex { get; set; }
        
        //Đã có
        [Description("Ngày sinh dd/MM/yyyy")] 
        [Required]
        public string Birthday
        {
            get;
            set;
        }

        // Đã có
        [Description("Địa chỉ liên hệ")]
        [Required]
        public string Address_01 { get; set; }
        
        //Đã có
        [Description("Số điện thoại của KH 01")]
        [Required]
        public string Phone_01 { get; set; }


        [Description("Số điện thoại của KH 02")]
        public string Phone_02 { get; set; }


        //Đã có
        [Description("Địa chỉ email")]
        [Required]
        public string Email { get; set; }
        
        
        
        
        
        
        [Description("Danh sách tài khoản ngân hàng List<BankAccountData>")]
        public List<BankAccountData> BankAccountList { get; set; }
        
        
        [Description("Noi mo tai khoan")]
        public string AddressOpenAccount { get; set; }
        

        [Description("Phương thức giao dịch, 1: điện thoại, 2: Internet")]
        public string TradingMethod { get; set; }   
        
        [Description("Phương thức nhận KQGD, 1: Sms , 2: Email, 3: tại quầy")]
        public string TradingResultMethod { get; set; }
        
        [Description("Phương thức sao kê, 1: thư đảm bảo , 2: Email, 3: tại quầy")]
        public string TradingReportMethod { get; set; }
        
        [Description("Trang thai HD, 2: Nợ hồ sơ")]
        public int StatusContact { get; set; }  // Trang thai HD
        
        [Description("Hồ sơ mở hợp đồng 0:HĐ thường, 1:HĐ Margin , 2: Phái sinh")]
        public int OpenContact { get; set; }
        
        [Description("Ma Quoc Tich để là 000")]
        public string NationalityCode { get; set; } 
        
        [Description("Ma Quốc tịch")]
        [Required]
        public string NationalityName { get; set; }

        
        [Description("HĐ Margin")]
        public bool AdditionMargin { get; set; }

        [Description("Hạn mức dư nợ")]
        public string CreditLimit { get; set; } 

        [Description("Thời hạn nợ hợp đồng")]
        public string DebtTerm { get; set; } 
        
        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; } 

        [Description("Hạn mức dư nợ của Chi nhánh")]
        public string CreditLimitBranch { get; set; } 

        [Description("Tổng hạn mức dư nợ còn lại của Chi nhánh")]
        public string TotalCreditLimitBranch { get; set; } 

        [Description("Tên Chi nhánh")]
        public string BranchName { get; set; }
        
        
        [Description("Chủ tài khoản đổi ứng 1")]
        public string AccountContraName_01{get;set;}
        
        [Description("Số tài khoản đối ứng 1")]
        public string AccountContraNo_01 { get; set; } 
        
        [Description("Chủ tài khoản đổi ứng 2")]
        public string AccountContraName_02{get;set;}
        
        [Description("Số tài khoản đối ứng 2")]
        public string AccountContraNo_02 { get; set; }
        
        [Description("HĐ phái sinh")]
        public bool AdditionDerivative { get; set; }
        
        [Description("Tên người đại diện")]
        public string TvsiManName { get; set; }
        
        [Description("Chức vụ")]
        public string TvsiManPosition { get; set; }
        
        [Description("Giấy ủy quyền số")]
        public string TvsiManNo { get; set; }
        
        [Description("Ngày cấp giấy ủy quyền")]
        public string TvsiManDate { get; set; }
        
        [Description("Số CMND")]
        public string TvsiManCardId { get; set; }
        
        [Description("Ngày cấp CMND")]
        public string TvsiManCardDate { get; set; }
        
        [Description("Nơi cấp CMND")]
        public string TvsiManCardIssue { get; set; }

        [Description("Đường dẫn ảnh")]
        public FolderPath FolderPath { get; set; } 
        


        [Description("KH là công dân Hoa Kỳ 0: false, 1: true")]
        public int FatcaKhUsa { get; set; }

        [Description("KH có nơi sinh tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiSinhUsa { get; set; }

        [Description("KH có địa chỉ lưu ký tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiCuTruUsa { get; set; }

        [Description("KH có sdt liên lạc tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaSdtUsa { get; set; }

        [Description("KH có lệnh CK Hoa Kỳ 0: false, 1: true")]
        public int FatcaLenhCkUsa { get; set; }

        [Description("KH có giấy ủy quyền tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaGiayUyQuyenUsa { get; set; }

        [Description("KH có đại chỉ nhận thư hộ tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaDiaChiNhanThuUsa { get; set; }
        
        [Description("Thông tin mở rộng")] 
        public InfoExtend InfoExtend { get; set; }
    }
    public class AccWebEditRequest : BaseRequest
    {
        [Description("ID đăng ký")]
        [Required]
        public int AccOpenId { get; set; }

        [Description("Trạng thái")]
        public string Status { get; set; }

        [Description("User được chỉ định")]
        public string AssignUser { get; set; }

        [Description("Mô tả")]
        [Required]
        public string Description { get; set; }

    }


    public class AccWebDeleteRequest : BaseRequest
    {
        [Description("ID đăng ký")]
        [Required]
        public int AccOpenId { get; set; }
    }
    
   
    
    
}


