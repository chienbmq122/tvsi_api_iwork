﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Contact
{
    public class ContactInfoStaff
    {
        public string SaleID { get; set; }
        public string SaleName { get; set; }
        public int Level { get; set; }
        public string LevelStr {
            get
            {
                switch (Level)
                {
                    case 1:
                        return "GĐ chi nhánh";
                    case 2:
                        return "Trưởng nhóm";
                    default:
                        return "Sale";
                }
            }
        }
        public string Department { get; set; }
    }

    public class ContactinfoStaffRequest : BaseRequest
    {
        public string StaffID { get; set; }
    }

    public class ContactInfoLead
    {
        public int LeadId { get; set; }
        public string LeadName { get; set; }
        public string SaleID { get; set; }
        public string SaleName { get; set; }
        public int LeadSourceID { get; set; }
        public string SourceName {
            get
            {
                switch (LeadSourceID)
                {
                    case 1:
                        return "Hội thảo";
                    case 2:
                        return "Quảng cáo";
                    case 3:
                        return "Người giới thiệu";
                    case 4:
                        return "Sự kiện công ty tổ chức";
                    case 5:
                        return "Qua Website";
                    case 6:
                        return "Khác";
                    default:
                        return "Không xác định";
                }
            }
        }
    }

    public class ContactinfoLeadRequest : BaseRequest
    {
        public int LeadID { get; set; }
    }

    public class ContactInfoCustomer
    {
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string SaleID { get; set; }
        public string SaleName { get; set; }
        public int ProfileType { get; set; }
        public string ProfileTypeName {
            get {
                switch (ProfileType)
                {
                    case 1:
                        return "Cá nhân";
                    case 2:
                        return "Tổ chức";
                    default:
                        return "Khác";
                }
            }
        }
    }
    public class ContactinfoCustRequest : BaseRequest
    {
        public string CustCode { get; set; }
    }
}
