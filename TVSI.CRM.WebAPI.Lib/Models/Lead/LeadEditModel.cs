﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Lead
{
    public class LeadEditRequest : BaseIWRequest
    {
        [Description("Id KH tiềm năng")]
        [Required]
        public string LeadId { get; set; }

        [Description("Họ và tên")]
        [Required]
        public string LeadName { get; set; }

        [Description("Số điện thoại khách hàng")]
        [Required]
        public string LeadMobile { get; set; }
        [Description("Địa chỉ email")]
        public string LeadEmail { get; set; }
        [Description("Địa chỉ liên hệ")]
        public string LeadAddress { get; set; }
        [Description("Loại hình - Cá nhân: 1, Tổ chức: 2")]
        public int LeadProfileType { get; set; }

        [Description("Mô tả về lead")]
        public string LeadDescription { get; set; }

        [Description("User được chỉ định : Thường SaleId nhập vào 4 ký tự số")]
        public string AssignUser { get; set; }
        [Description("Nguồn thông tin qua - Hội thảo: 1, Quảng cáo: 2, Người giới thiệu: 3, Sự kiện công ty tổ chức: 4, Qua website: 5, Khác: 6")]
        public int LeadSourceID { get; set; }

        [Description("Thông tin mở rộng: Truyền vào theo chuỗi - 9|77|78|84|86")]
        [Required]
        public string LeadInfoExtend { get; set; }
    }

    public class LeadUpdateAccount : BaseRequest
    {
        [Description("Id KH tiềm năng")]
        [Required]
        public int LeadId { get; set; }

        [Description("CustCode")]
        [Required]
        public string CustCode { get; set; }
    }
}
