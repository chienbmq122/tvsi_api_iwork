﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class AttributeCommandText : IAttributeCommandText
    {
        #region SQL Command List
        public string GetListAttribute => @"SELECT * FROM Attribute WHERE OrderBy <> 999";

        public string GetListAttributeLead => @"SELECT *  FROM LeadAttribute JOIN Attribute ON Attribute.AttributeID = LeadAttribute.AttributeID WHERE LeadAttribute.LeadID = @leadId";

        public string GetListAttributeCus => @"SELECT *  FROM CustAttribute JOIN Attribute ON Attribute.AttributeID = CustAttribute.AttributeID WHERE CustAttribute.CustCode = @custCode";

        public string CreateAttributeLead => @"INSERT INTO LeadAttribute(AttributeID, LeadID, Value, Status, Priority, CreatedBy, CreatedDate) VALUES (@AttributeID, @LeadID, @Value, @Status, @Priority, @CreatedBy, @CreatedDate)";

        public string CreateAttributeCust => @"INSERT INTO CustAttribute(AttributeID, CustCode, Value, Status, Priority, CreatedBy, CreatedDate) VALUES (@AttributeID, @CustCode, @Value, @Status, @Priority, @CreatedBy, @CreatedDate)";

        public string DeleteAttributeLead => @"DELETE FROM LeadAttribute WHERE AttributeID = @AttributeID AND LeadID = @LeadID";

        public string DeleteAttributeCust => @"DELETE FROM CustAttribute WHERE AttributeID = @AttributeID AND CustCode = @CustCode";

        public string CheckLeadAttribute => @"SELECT *  FROM LeadAttribute JOIN Attribute ON Attribute.AttributeID = LeadAttribute.AttributeID WHERE LeadAttribute.LeadID = @leadId AND LeadAttribute.AttributeID = @attributeID";

        public string CheckCustAttribute => @"SELECT *  FROM CustAttribute JOIN Attribute ON Attribute.AttributeID = CustAttribute.AttributeID WHERE CustAttribute.custCode = @custCode AND CustAttribute.AttributeID = @attributeID";

        public string GetListCategory => @"SELECT Category as ItemID, CategoryName as ItemName, CategoryType as ItemTypeID FROM ExtendCategory ORDER BY CategoryType asc";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
