﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface ISystemCommandText
    {
        /// <summary>
        /// Get user domain 
        /// </summary>
        string GetUserDomain { get; }

        /// <summary>
        /// Get thông tin đăng nhập và quyền của User
        /// </summary>
        string GetUserInfo { get; }

        /// <summary>
        /// Get danh sách thông báo
        /// </summary>
        string GetNotificationList { get; }
        /// <summary>
        /// Get danh sách thông báo chưa đọc
        /// </summary>
        string GetNumOfUnreadNotification { get; }
        /// <summary>
        /// Get danh sách thông báo đã đọc
        /// </summary>
        string SyncNotificationRead { get; }
    }
}
