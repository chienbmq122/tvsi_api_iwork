﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Customer;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface ICustomerServices
    {
        Task<IEnumerable<CustomerListResult>> GetListCust(CustomerListRequest model);
        Task<CustInfoResult> GetInfoCust(CustInfoRequest model);
        Task<IEnumerable<BankAccTransferResult>> GetBankTranfer(BankAccTransferRequest model);
        Task<IEnumerable<CashBalanceInfoResult>> GetCashBalanceInfo(CashBalanceInfoRequest model);
        Task<IEnumerable<AccPorfolioResult>> GetAccPorfolio(AccPorfolioRequest model);
        Task<IEnumerable<TradingInfoResult>> GetTradingInfoList(TradingInfoRequest model);
        Task<IEnumerable<ExCustInfoResult>> GetExCustInfoList();
        Task<IEnumerable<CustActivityResult>> GetActivityCustList(CustActivityRequest model);
        Task<IEnumerable<CustOpportunityResult>> GetOpportunityCustList(CustOpportunityRequest model);
        Task<bool> UpdateCustInfo(CustEditRequest model);
        Task<IEnumerable<ShareHolderListResult>> GetListShareHolder(ShareHolderListRequest model);
        Task<CustomerListSelectResponse> GetListCustSelect(CustomerListSelectRequest model);
        Task<bool> AddNewShareHolderInfo(ShareHolderAddRequest model);
        Task<ShareHolderInfoResult> GetShareHolderInfo(ShareHolderInfoRequest model);

        Task<CustomerInfoResult> GetCustomerInfo(CustomerDetailInfoRequest model);
        Task<bool> ExistsPhonePotential(PhonePotentialInfoRequest model);
        Task<IEnumerable<PhoneInfoListResult>> GetPhoneInfoList(PhoneInfoListRequest model);
    }
}
