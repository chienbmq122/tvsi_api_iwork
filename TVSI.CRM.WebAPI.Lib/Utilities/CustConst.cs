﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public class CustConst
    {
        public const string EXCUST_TYPE_1 = "1"; // Nghề Nghiệp
        public const string EXCUST_TYPE_2 = "2"; // Khả năng tài chính
        public const string EXCUST_TYPE_3 = "3"; // Kiến thức, kinh nghiệm
        public const string EXCUST_TYPE_4 = "4"; // Quan điểm đầu tư
        public const string EXCUST_TYPE_5 = "5"; // Nguồn giới thiệu
        public const string EXCUST_TYPE_6 = "6"; // Nơi làm việc
        public const string EXCUST_TYPE_7 = "7"; // Tổ chức tài chính
        public const string EXCUST_TYPE_8 = "8"; // Kinh nghiệm đầu tư
        public const string EXCUST_TYPE_9 = "9"; // Khả năng rủi ro
        public const string EXCUST_TYPE_10 = "10"; // Sở thích đầu tư
        public const string EXCUST_TYPE_11 = "11"; // Lưu ý khi giao tiếp
        public const string EXCUST_TYPE_12 = "12"; // Mối quan hệ với Sale
        public const string S_SHARE_TYPE = "1"; // Trạng thái cổ đông

    }
}
