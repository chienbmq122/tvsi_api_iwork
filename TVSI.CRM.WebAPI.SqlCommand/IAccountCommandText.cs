﻿namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IAccountCommandText
    {
        
        //SQL Syntax
        
        /// <summary>
        /// Get Customer By SaleID
        /// </summary>
        string GetAccountBySaleId { get; } 
        
                
        /// <summary>
        /// Get Customer By Director
        /// </summary>
        string GetAccountBySaleIdDirector { get; } 
        
                
        /// <summary>
        /// Get Customer By Leader
        /// </summary>
        string GetAccountBySaleIdLeader { get; } 
        
        /// <summary>
        /// Get Customer By SaleID
        /// </summary>
        string GetAccountFillBySaleId { get; }

        /// <summary>
        /// Get Customer Infomation By SaleID
        /// </summary>
        string GetCustInfoBySale { get; }

        /// <summary>
        /// Get Customer Infomation (Validate)
        /// </summary>
        string GetCustInfo { get; }
        
        
        /// <summary>
        /// Get Customer Infomation (Validate)
        /// </summary>
        string GetCustInfoRegister { get; }  
        
        /*/// <summary>
        /// Get Customer Infomation (Validate)
        /// </summary>
        string GetCustInfoRegister { get; }*/
        
        
        /// <summary>
        /// Get Customer Infomation Commondb
        /// </summary>
        string GetCustCode { get; }
        
        /// <summary>
        /// Get Customer full basic Infomation Commondb
        /// </summary>
        string GetDetailBasicInfoCust { get; }
        
        /// <summary>
        /// Get Customer Register service Infomation Commondb
        /// </summary>
        string GetDetailRegisterServiceCust { get; } 
        
        
        /// <summary>
        /// Get Category CRM
        /// </summary>
        string GetCategoryCustExtend { get; }
        
        
        /// <summary>
        /// Get Image CardId Commondb
        /// </summary>
        string GetImageCardId { get; }
        
        string GetBankName { get; }
        string GetSubBranchName { get; }
        string GetProvinceName { get; }
        string GetIssuedByName { get; }
        string GetCardIssue { get; }
        string GetManagementCode { get;}
        string GetUserProfile { get; }
        string SelectInfoAccountTofix { get; }
        string UpdateInfoAccount { get; }
        string SelectInfoNohoso { get; }
        string UpdateNohoso { get; }
        string GetBankList { get; }
        string GetSubBranchList { get; }
        string GetProvinceList { get; }
        string GetAccountInfo { get; }



        //SQL Stored
        string StoredGetAccountFillBySale { get; }
        string StoredCheckSaleExsited { get; }
        string StoredUpdateStatusAccount { get; }
        string StoredInsertAccount { get; }
        string StoredInsertContactFS { get; }
        /// <summary>
        /// Lấy danh sách hồ sơ mở tài khoản từ website
        /// </summary>
        string GetListAccWeb { get; }

        /// <summary>
        /// Update trạng thái chuyển thông tin mở tài khoản cho
        /// </summary>
        string UpdateAccWeb { get; }
        /// <summary>
        /// Query xóa bản ghi mở tài khoản từ website
        /// </summary>
        string DeleteAccWeb { get; }

        /// <summary>
        /// Check tài khoản bảng đăng ký tài khoản commond
        /// </summary>
        string GetRegAccountExistedCommond { get; }
        
        /// <summary>
        /// Check tài khoản bảng tài khoản KH commond
        /// </summary>
        string GetAccountExistedCommond { get; }
        /// <summary>
        /// Check tài khoản bảng tài khoản KH commond
        /// </summary>
        string GetRevertExistedCommond { get; }
        
        /// <summary>
        /// Check tài khoản bảng tài khoản KH commond
        /// </summary>
        string GetAccountExistedBond { get; }
        
        
        /// <summary>
        /// Check tài khoản bảng tài khoản KH 
        /// </summary>
        string GetCustomerCRMWebsite { get; }   
        
        
        /// <summary>
        /// Check tài khoản bảng tài khoản KH 
        /// </summary>
        string GetExtendCust { get; }
        
        
        
    }
}