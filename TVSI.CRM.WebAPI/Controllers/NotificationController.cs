﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Notification;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Notification API
    /// </summary>
    public class NotificationController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(NotificationController));
        private INotificationService _notificationService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="notificationService"></param>
        public NotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        /// <summary>
        /// Danh sách notification của user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("N_GN_GetNotification")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<NotificationResponseModel>))]
        [Note("Danh sách notification của user")]
        public async Task<IHttpActionResult> N_GN_GetNotification(NotificationRequestModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "N_GN_GetNotification");
                SetLanguage(lang);

                var retData = await _notificationService.GetNotification(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<NotificationResponseModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "N_GN_GetNotification");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "N_GN_GetNotification");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
