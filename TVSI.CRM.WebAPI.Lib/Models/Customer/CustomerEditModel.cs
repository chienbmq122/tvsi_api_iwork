﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class CustEditRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public string CustCode { get; set; }

        [Description("User được chỉ định : Thường SaleId nhập vào 4 ký tự số")]
        public string AssignUser { get; set; }

        [Description("Mô tả")]
        [Required]
        public string Description { get; set; }

        [Description("Thông tin mở rộng: Truyền vào theo chuỗi - 11|72|85|98|101")]
        [Required]
        public string CustInfoExtend { get; set; }
    }
}
