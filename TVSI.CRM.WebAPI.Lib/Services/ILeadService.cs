﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Lead;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface ILeadService
    {
        Task<IEnumerable<LeadListResult>> GetLeadList(LeadListRequest model);
        Task<IEnumerable<LeadActivityResult>> GetActivityLeadList(LeadActivityRequest model);
        Task<IEnumerable<LeadOpportunityResult>> GetOpportunityLeadList(LeadOpportunityRequest model);
        Task<bool> AddNewLeadInfo(LeadAddRequest model);
        Task<IEnumerable<ExLeadInfoResult>> GetExLeadInfoList();
        Task<LeadInfoResult> GetLeadInfo(LeadInfoRequest model);
        Task<LeadListUserResponse> GetLeadDropBox(LeadListUserRequest model);
        Task<bool> UpdateLeadInfo(LeadEditRequest model);
        Task<bool> DeleteLeadInfo(LeadDeleteRequest model);
        Task<bool> SwitchLeadToCustomer(LeadUpdateAccount model);
    }
}
