﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Dashboard;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using TVSI.CRM.WebAPI.Lib.Models.Activity;
using System.Collections.Generic;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Dashboard API
    /// </summary>
    public class DashboardController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(DashboardController));

        /// <summary>
        /// 
        /// </summary>
        public IDashboardService _dashboardService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dashboardService"></param>
        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        /// <summary>
        /// API lấy Top 3 Công việc / Sự kiện / Lịch công tác gần nhất sắp diễn ra
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DB_AC_Next3Task")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<IEnumerable<ActivityResult>>))]
        [Note("Màn hình Dashboard")]
        public async Task<IHttpActionResult> DB_AC_Next3Task(DashboardActionRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                SetLanguage(lang);
                var retData = await _dashboardService.Get3TaskNextActivity(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<ActivityResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// API lây thông tin tổng hợp phí ngày hôm nay và tuần hiện tại
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DB_CA_FeeInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<DashboardChartResult>))]
        [Note("Màn hình Dashboard")]
        public async Task<IHttpActionResult> DB_CA_FeeInfo(DashboardChartRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                SetLanguage(lang);
                var retData = await _dashboardService.TradeInfo(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<DashboardChartResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "DB_CA_FeeInfo");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// API lấy thông tin giao dịch ngày hôm nay và tháng hiện tại
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DB_RP_TradeInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<DashboardTradeResult>))]
        [Note("Màn hình Dashboard")]
        public async Task<IHttpActionResult> DB_RP_TradeInfo(DashboardTradeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                SetLanguage(lang);
                var retData = await _dashboardService.TradeTotalInfoHistory(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<DashboardTradeResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "DB_RP_TradeInfo");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// API lấy thông tin tổng hợp Cơ hội / KH tiềm năng / KH / KH active
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DB_RP_SummaryInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<DashboardSummaryResult>))]
        [Note("Màn hình Dashboard")]
        public async Task<IHttpActionResult> DB_RP_SummaryInfo(DashboardSummaryRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                SetLanguage(lang);
                var retData = await _dashboardService.TradeTotalHistory(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<DashboardSummaryResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "DB_RP_SummaryInfo");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// API lấy thông tin giao dịch trong ngày
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DB_RP_TrandingInfoDaily")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<TradingInfoModel>))]
        [Note("Màn hình Dashboard")]
        public async Task<IHttpActionResult> DB_RP_TrandingInfoDaily(DashboardSummaryRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                SetLanguage(lang);
                var retData = await _dashboardService.TradingInfoDaily(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<TradingInfoModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "DB_RP_TrandingInfoDaily");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}