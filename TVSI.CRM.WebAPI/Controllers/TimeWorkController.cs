﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.TimeWork;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using TVSI.CRM.WebAPI.ResourceFile;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// TimeWork API
    /// </summary>
    public class TimeWorkController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(TimeWorkController));
        private ITimeWorkService _timeWorkService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="timeWorkService"></param>
        public TimeWorkController(ITimeWorkService timeWorkService)
        {
            _timeWorkService = timeWorkService;
        }

        /// <summary>
        /// Lịch sử chấm công
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetHisTimeWorkStaffByDay")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetTimeWorkByStaffResult>))]
        [Note("Màn hình lịch sử chấm công")]
        public async Task<IHttpActionResult> TW_GE_GetHisTimeWorkStaffByDay(GetTimeWorkByStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetHisTimeWorkStaffByDay");
                SetLanguage(lang);
                
                //Khong stored
                var retData = await _timeWorkService.GetHisTimeWorkStaffByDay(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetTimeWorkByStaffResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetHisTimeWorkStaffByDay");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetHisTimeWorkStaffByDay");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Lịch sử chấm công tháng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetHisTimeWorkStaffByMonth")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetTimeWorkByStaffByMonthResult>))]
        [Note("Màn hình lịch sử chấm công theo tháng")]
        public async Task<IHttpActionResult> TW_GE_GetHisTimeWorkStaffByMonth(GetTimeWorkByStaffByMonthRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetHisTimeWorkStaffByMonth");
                SetLanguage(lang);
                //Khong stored
                var retData = await _timeWorkService.GetHisTimeWorkStaffByMonth(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetTimeWorkByStaffByMonthResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetHisTimeWorkStaffByMonth");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetHisTimeWorkStaffByMonth");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Thống kê lịch sử châm công theo tháng/tuần
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetTimeWorkStaffReport")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetTimeWorkStaffReportResult>))]
        [Note("Màn hình lịch sử chấm công theo tháng")]
        public async Task<IHttpActionResult> TW_GE_GetTimeWorkStaffReport(GetTimeWorkStaffReportRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetTimeWorkStaffReport");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData = await _timeWorkService.GetTimeWorkStaffReport(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetTimeWorkStaffReportResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetTimeWorkStaffReport");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetTimeWorkStaffReport");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Thống kê danh sách đơn từ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetLeaveApplicationStaffReport")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetLeaveApplicationStaffReportResult>))]
        [Note("Màn hình chấm công | Đơn từ | DANH SÁCH ĐƠN TỪ")]
        public async Task<IHttpActionResult> TW_GE_GetLeaveApplicationStaffReport(
            GetLeaveApplicationStaffReportRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetLeaveApplicationStaffReport");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData = 
                    await _timeWorkService.GetLeaveApplicationStaffReport(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetLeaveApplicationStaffReportResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetLeaveApplicationStaffReport");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetLeaveApplicationStaffReport");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Tạo đơn từ mới
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_CA_CreateApplicationFormStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình chấm công | Đơn từ | Tạo đơn từ mới")]
        public async Task<IHttpActionResult> TW_CA_CreateApplicationFormStaff(CreateApplicationFormStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_CA_CreateApplicationFormStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                // SU DUNG STORED
                var retData = await _timeWorkService.CreateApplicationFormStaff(model);
                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE
                    };
                    LogAnonymousResult(log, retModel, "TW_CA_CreateApplicationFormStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_CA_CreateApplicationFormStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Checkin chấm công GPS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_TK_CheckinTimeKeepingGPSStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình Chấm công Checkin GPS")]
        public async Task<IHttpActionResult> TW_TK_CheckinTimeKeepingGPSStaff(CheckinTimeKeepingGPSStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_TK_CheckinTimeKeepingGPSStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                // SU DUNG STORED
                var retData = await _timeWorkService.CheckinTimeKeepingGPSStaff(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE
                    };
                    LogAnonymousResult(log, retModel, "TW_TK_CheckinTimeKeepingGPSStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_TK_CheckinTimeKeepingGPSStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Checkout chấm công GPS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_TK_CheckoutTimeKeepingGPSStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình Chấm công Checkout GPS")]
        public async Task<IHttpActionResult> TW_TK_CheckoutTimeKeepingGPSStaff(
            CheckoutTimeKeepingGPSStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_TK_CheckoutTimeKeepingGPSStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                // SU DUNG STORED
                var retData = await _timeWorkService.CheckoutTimeKeepingGPSStaff(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE
                    };
                    LogAnonymousResult(log, retModel, "TW_TK_CheckoutTimeKeepingGPSStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_TK_CheckoutTimeKeepingGPSStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Get Checkin chấm công GPS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_TK_GetCheckinTimeKeepingStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình Chấm công Checkin GPS")]
        public async Task<IHttpActionResult> TW_TK_GetCheckinTimeKeepingStaff(
            GetCheckinTimeKeepingGPSStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_TK_GetCheckinTimeKeepingStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored 
                var retData = await _timeWorkService.GetCheckinTimeKeepingGPSStaff(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetCheckinTimeKeepingGPSStaffResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_TK_GetCheckinTimeKeepingStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_TK_GetCheckinTimeKeepingStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }


        /// <summary>
        /// Get Checkout chấm công GPS
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_TK_GetCheckoutTimeKeepingStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetCheckoutTimeKeepingGPSStaffResult>))]
        [Note("Màn hình Chấm công Checkin GPS")]
        public async Task<IHttpActionResult> TW_TK_GetCheckoutTimeKeepingStaff(
            GetCheckoutTimeKeepingGPSStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_TK_GetCheckoutTimeKeepingStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Không sử dụng Stored
                var retData = await _timeWorkService.GetCheckoutTimeKeepingGPSStaff(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetCheckoutTimeKeepingGPSStaffResult>
                    {
                        
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_TK_GetCheckoutTimeKeepingStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_TK_GetCheckoutTimeKeepingStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        /// <summary>
        /// Get lịch sử dấu vân tay
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GF_GetFingerprintingStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetFingerprintingStaffResult>))]
        [Note("Màn hình Chấm công | Vân tay")]
        public async Task<IHttpActionResult> TW_GF_GetFingerprintingStaff(
            GetFingerprintingStaffRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GF_GetFingerprintingStaff");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData = await _timeWorkService.GetFingerprintingStaff(model);

                var results = retData.ToList();
                if (results.Any())
                {
                    var retModel = new SuccessListModel<GetFingerprintingStaffResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = results
                    };
                    LogResult(log, retModel, "TW_GF_GetFingerprintingStaff");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GF_GetFingerprintingStaff");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        
        /// <summary>
        /// Get lịch sử chấm công theo ngày + đơn từ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetHisTWAndLAStaffByDay")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetTWAndLaByStaffResult>))]
        [Note("Màn hình Chấm công | lịch sử | ngày")]
        public async Task<IHttpActionResult> TW_GE_GetHisTWAndLAStaffByDay(
            GetHisTWAndLAStaffByDayRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetHisTWAndLAStaffByDay");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong stored
                var retData = await _timeWorkService.GetHisTWAndLAStaffByDay(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetTWAndLaByStaffResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetHisTWAndLAStaffByDay");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetHisTWAndLAStaffByDay");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }

        /// <summary>
        /// Get Data tồn tại theo ngày
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_CheckDataByDay")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<CheckDataByDateResult>))]
        [Note("Màn hình Chấm công | lịch sử | ngày")]
        public async Task<IHttpActionResult> TW_GE_CheckDataByDay(
            CheckDataByDateRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_CheckDataByDay");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData = await _timeWorkService.CheckDataByDay(model);

                if (retData.Count()> 0)
                {
                    var retModel = new SuccessListModel<CheckDataByDateResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_CheckDataByDay");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_CheckDataByDay");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        /// <summary>
        /// Get Data tồn tại theo ngày
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_CheckDataByMonth")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<checkDataByMonthResult>))]
        [Note("Màn hình Chấm công | lịch sử | ngày")]
        public async Task<IHttpActionResult> TW_GE_CheckDataByMonth(
            CheckDataByMonthRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_CheckDataByMonth");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                //Khong Stored
                var retData = await _timeWorkService.CheckDataByMonth(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<checkDataByMonthResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_CheckDataByMonth");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_CheckDataByMonth");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        
        /// <summary>
        /// Check Detail của đơn từ
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetDetailLeaveApplications")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetDetailLeaveApplicationsResult>))]
        [Note("Màn hình Chấm công | lịch sử | tháng | đơn nghỉ theo ngày")]
        public async Task<IHttpActionResult> TW_GE_GetDetailLeaveApplications(
            GetDetailLeaveApplicationsRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetDetailLeaveApplicaitions");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData = await _timeWorkService.GetDetailLeaveApplications(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetDetailLeaveApplicationsResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetDetailLeaveApplications");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetDetailLeaveApplications");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        /// <summary>
        /// Lấy ra danh sách leader duyệt đơn của thành viên
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetNameLeaderApprovedByUserName")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetNameLeaderApprovedByUserNameResult>))]
        [Note("Màn hình Chấm công | tạo đơn từ mới")]
        public async Task<IHttpActionResult> TW_GE_GetNameLeaderApprovedByUserName(
            BaseRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetNameLeaderApprovedByUserName");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                //Khong Stored
                var retData = await _timeWorkService.GetNameLeaderApprovedByUserName(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetNameLeaderApprovedByUserNameResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetNameLeaderApprovedByUserName");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetNameLeaderApprovedByUserName");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Lấy ra danh sách member cho leader
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetNameMemberApprovedByUserName")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetNameLeaderApprovedByUserNameResult>))]
        [Note("Màn hình Chấm công | tạo đơn từ mới")]
        public async Task<IHttpActionResult> TW_GE_GetNameMemberApprovedByUserName(
            BaseRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetNameMemberApprovedByUserName");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //Khong Stored
                var retData
                    = await _timeWorkService.GetNameMemberApprovedByUserName(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetNameLeaderApprovedByUserNameResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "TW_GE_GetNameMemberApprovedByUserName");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetNameMemberApprovedByUserName");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        } 
        
        
        /// <summary>
        /// Duyệt đơn từ all của Leader
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_LA_LeaveApplicationApprovedByLeaderAll")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Màn hình Duyệt đơn từ | Duyệt all")]
        public async Task<IHttpActionResult> TW_LA_LeaveApplicationApprovedByLeaderAll(
            LeaveApplicationApprovedByLeaderAllRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_LA_LeaveApplicationApprovedByLeaderAll");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                //SU DUNG STORED
                var retData = await _timeWorkService.LeaveApplicationApprovedByLeaderAll(model);
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "TW_LA_LeaveApplicationApprovedByLeaderAll");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_LA_LeaveApplicationApprovedByLeaderAll");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        /// <summary>
        /// Lấy ra danh sách đơn từ của thành viên cho leader
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetLeaveApplicationsMemberByLeader")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetGetLeaveApplicationMemberReportByLeaderResult>))]
        [Note("Màn hình Chấm công | tạo đơn từ mới")]
        public async Task<IHttpActionResult> TW_GE_GetLeaveApplicationsMemberByLeader(
            GetLeaveApplicationsMemberByLeaderRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetLeaveApplicationsMemberByLeader");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                // Khong su dung stored
                var retData = 
                    await _timeWorkService.GetLeaveApplicationsMemberByLeader(model);

                var resData = retData.ToList();
                if (resData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetGetLeaveApplicationMemberReportByLeaderResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = resData
                    };
                    LogResult(log, retModel, "TW_GE_GetLeaveApplicationsMemberByLeader");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetLeaveApplicationsMemberByLeader");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        } 
        /// <summary>
        /// Tìm kiếm danh sách member của Leader
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_GE_GetLAMemberReportByLeader")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetGetLeaveApplicationMemberReportByLeaderResult>))]
        [Note("Màn hình duyệt đơn từ | Tra cứu theo bộ lọc | Leader")]
        public async Task<IHttpActionResult> TW_GE_GetLAMemberReportByLeader(
            GetLAMemberReportByLeaderRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_GE_GetLAMemberReportByLeader");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                // SU DUNG STORED
                var retData
                    = await _timeWorkService.GetLAMemberReportByLeader(model);

                var resData = retData.ToList();
                if (resData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetGetLeaveApplicationMemberReportByLeaderResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = resData
                    };
                    LogResult(log, retModel, "TW_GE_GetLAMemberReportByLeader");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_GE_GetLAMemberReportByLeader");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }  
        
        
        /// <summary>
        /// Chỉnh sửa đơn từ thành viên
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_ED_EditLAMember")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Màn hình chấm công | sửa đơn từ")]
        public async Task<IHttpActionResult> TW_ED_EditLAMember(
            TW_ED_EditLAMemberReportRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_ED_EditLAMember");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                // su dung stored
                var retData
                    = await _timeWorkService.TW_ED_EditLAMember(model);

                
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                       
                    };
                    LogResult(log, retModel, "TW_ED_EditLAMember");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_ED_EditLAMember");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        
        
        /// <summary>
        /// Member sửa/xóa 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("TW_MB_ActionLeaveApplications")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Màn hình chấm công | sửa đơn từ")]
        public async Task<IHttpActionResult> TW_MB_ActionLeaveApplications(
            ActionLeaveApplicationsRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "TW_MB_ActionLeaveApplications");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retData
                    = await _timeWorkService.TW_MB_ActionLeaveApplications(model);
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                       
                    };
                    LogResult(log, retModel, "TW_MB_ActionLeaveApplications");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "TW_MB_ActionLeaveApplications");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        
        
        
        
    }
}