﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Opportunity;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Opportunity API
    /// </summary>
    public class OpportunityController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(OpportunityController));
        private IOpportunityService _opportunityService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="opportunityService"></param>
        public OpportunityController(IOpportunityService opportunityService)
        {
            _opportunityService = opportunityService;
        }

        /// <summary>
        /// Danh sách cơ hội
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_GA_GetListOpportunity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<OpportunityResult>>))]
        [Note("Danh sách cơ hội")]
        public async Task<IHttpActionResult> OP_GA_GetListOpportunity(OpportunityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_GA_GetListOpportunity");
                SetLanguage(lang);

                var retData = await _opportunityService.GetListOpportunity(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<OpportunityResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "OP_GA_GetListOpportunity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_GA_GetListOpportunity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Chi tiết cơ hội
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_GD_GetDetailOpportunity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<OpportunityResult>))]
        [Note("Detail cơ hội")]
        public async Task<IHttpActionResult> OP_GD_GetDetailOpportunity(OpportunityDalete model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_GD_GetDetailOpportunity");
                SetLanguage(lang);

                var retData = await _opportunityService.GetDetailOpportunity(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<OpportunityResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "OP_GD_GetDetailOpportunity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_GD_GetDetailOpportunity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới cơ hội
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_CA_CreateOpportunity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("tạo mới cơ hội")]
        public async Task<IHttpActionResult> OP_GD_CreateOpportunity(OpportunityCreate model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_CA_CreateOpportunity");
                SetLanguage(lang);
                if (!string.IsNullOrEmpty(model.CustCode))
                {
                    var checkCustCode = await _opportunityService.CheckCustCode(model.CustCode, model.AssignUser);
                    if (checkCustCode)
                    {
                        return Ok(new ErrorModel
                        {
                            RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                            ErrorMsg = "Custcode không đúng hoặc không được asign"
                        });
                    }
                }
                var retData = await _opportunityService.CreateOpportunity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_CreateActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_GD_CreateOpportunity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Update Cơ hội
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_UD_UpdateOpportunity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Update cơ hội")]
        public async Task<IHttpActionResult> OP_UD_UpdateOpportunity(OpportunityUpdate model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_UD_UpdateOpportunity");
                SetLanguage(lang);
                if (!string.IsNullOrEmpty(model.CustCode))
                {
                    var checkCustCode = await _opportunityService.CheckCustCode(model.CustCode, model.AssignUser);
                    if (checkCustCode)
                    {
                        return Ok(new ErrorModel
                        {
                            RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                            ErrorMsg = "Custcode không đúng hoặc không được asign"
                        });
                    }
                }
                var retData = await _opportunityService.UpdateOpportunity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "OP_UD_UpdateOpportunity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_UD_UpdateOpportunity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xóa Cơ hội
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_DE_DeleteOpportunity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("xóa cơ hội")]
        public async Task<IHttpActionResult> OP_DE_DeleteOpportunity(OpportunityDalete model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_DE_DeleteOpportunity");
                SetLanguage(lang);

                var retData = await _opportunityService.DeleteOpportunity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "OP_DE_DeleteOpportunity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_DE_DeleteOpportunity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
