﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using NotImplementedException = System.NotImplementedException;

namespace TVSI.CRM.WebAPI.Lib.Models.TimeWork
{
    public class TimeWorkModel : BaseRequest
    {
        
    }

    public class GetTimeWorkByStaffByMonthRequest : BaseRequest
    {
        [Description("Tháng/năm làm việc MM/yyyy")]
        [Required]
        public string MonthWork { get; set; }
    }

    public class GetTimeWorkStaffReportRequest : BaseRequest,IValidatableObject
    {
        [Description("Check theo tuần thì để là: 1, check theo tháng thì để là 2, nếu chọn 1 bắt buộc phải có ToDateWork và FromDateWork, nếu chọn 2 bắt buộc phải có MonthWork")]
        public int Option { get; set; }
        
        [Description("Tháng/năm làm việc MM/yyyy")]
        public string MonthWork { get; set; }
        
        [Description("Từ ngày làm việc dd/MM/yyyy")]
        public string FromDateWork { get; set; }
        
        [Description("Đến ngày làm việc dd/MM/yyyy")]
        public string ToDateWork { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (Option == 1)
            {
                if (string.IsNullOrEmpty(FromDateWork))
                {
                    result.Add(new ValidationResult(ResourceFile.TimeWorkErrors.Validate_FromDate));
                }

                if (string.IsNullOrEmpty(ToDateWork))
                {
                    result.Add(new ValidationResult(ResourceFile.TimeWorkErrors.Validate_ToDate));
                }
            }

            if (Option == 2)
            {
                if (string.IsNullOrEmpty(MonthWork))
                {
                    result.Add(new ValidationResult(ResourceFile.TimeWorkErrors.Validate_MonthWork));
                }
            }
            if (Option != 1 && Option != 2)
            {
                result.Add(new ValidationResult(ResourceFile.TimeWorkErrors.Validate_GetTimeWorkOption));
            }

            return result;
        }
    }

    public class GetLAMemberReportByLeaderRequest : BaseRequest
    {
        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
        
        [Description("Từ ngày làm việc dd/MM/yyyy")]
        public string FromDateWork { get; set; }
        
        [Description("Đến ngày làm việc dd/MM/yyyy")]

        public string ToDateWork { get; set; }

        [Description("Loại đơn")]
        public int? TypeLeave { get; set; }

        
        [Description("UserName của Member cần filter")]
        public string UserNameMember { get; set; }


        [Description("Trạng thái duyệt")]
        public int? ApprovedStatus { get; set; }

    }

    public class GetLeaveApplicationsMemberByLeaderRequest : BaseRequest
    {
        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
    }

    public class LeaveApplicationApprovedByLeaderAllRequest : BaseRequest
    {
        [Description("Id của đơn từ nghỉ phép")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_LeaveId),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public IEnumerable<int> LeaveId {  get; set; }
        
        [Description("Trạng thái duyệt all (1 đã duyệt, -1 từ chối)")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_StatusLeaveNumber),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public int StatusLeaveNumber { get; set; } 
    }

    public class GetLeaveApplicationStaffReportRequest : BaseRequest
    {
        [Description("Thứ tự trang")]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        public int PageSize { get; set; }
        
        [Description("Từ ngày làm việc dd/MM/yyyy")]
        public string FromDateWork { get; set; }
        
        /*[Description("Thành viên muốn filterss")]
        public string UserNameMember { get; set; }*/
        
        [Description("Đến ngày làm việc dd/MM/yyyy")]

        public string ToDateWork { get; set; }

        [Description("Loại đơn")]
        [Range(1,5,ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_TypeLeave),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public int? TypeLeave { get; set; }
        
        [Description("Trạng thái duyệt")]
        public int? ApprovedStatus { get; set; }
    }
    public class GetLeaveApplicationStaffReportResult{
        
        [Description("Id")]
        public int LeaveId { get; set; }
        
        [Description("Tiêu đề")]
        public string Title { get; set;}

        [Description("Loại đơn từ")]
        public string TypeLeave { get; set; }
        
        [Description("Loại đơ từ Number")]
        public int TypeLeaveNum { get; set; }
        
        [Description("Ngày viết đơn dd/MM/yyyy HH:mm:ss")]
        public string DateOfApplication
        {
            get { return DateApplication?.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        
        [Description("Lý do")]
        public string NoteReason { get; set; }

        [Description("Thời gian bắt đầu")]
        public string TimeApplyStart
        {
            get { return TimeStart?.ToString("dd/MM/yyyy HH:mm:ss"); }
        }

        [Description("Thời gian kết thúc")]
        public string TimeApplyEnd
        {
            get { return TimeEnd?.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        
        [Description("Địa điểm")]
        public string Address { get; set; }
        
        [Description("Nghỉ phép chế độ")]
        public int? PolicyLeave { get; set; }

        public string PolicyLeaveName { get; set; }
        
        public int? NumLeave { get; set; }
        
        [Description("Người duyệt")]
        public string Approved { get; set; }
        
        [Description("Trang thai duyet")]
        public int StatusNumber { get; set; }
        
        [Description("tên trạng thái duyệt đơn")]
        public string StatusName { get; set; }
        
        [JsonIgnore]
        public DateTime? DateApplication { get; set; }
        
        [JsonIgnore]
        public DateTime? TimeStart { get; set; }
        
        [JsonIgnore]
        public DateTime? TimeEnd { get; set; }
        
        
        
    }
    public class GetGetLeaveApplicationMemberReportByLeaderResult
    {
        [Description("Id của đơn từ")]
        public int LeaveId { get; set; }
        
        [Description("Tiêu đề")]
        public string Title { get; set;}

        [Description("Ngày viết đơn dd/MM/yyyy HH:mm:ss")]
        public string DateOfApplication
        {
            get { return DateApplication.ToString("dd/MM/yyyy HH:mm:ss"); }
        }
        
        [Description("Người viết đơn")]
        public string Applicants { get; set; }
       
        [Description("Mã nhân viên")]
        public string UserName { get; set; }
        
        [Description("Trang thai duyet")]
        public int StatusNumber { get; set; }
        
        [Description("tên trạng thái duyệt đơn")]
        public string StatusName { get; set; }

        [JsonIgnore]
        public DateTime DateApplication { get; set; }
        
        [JsonIgnore]
        public int TimeWorkType { get; set; }
    }

    public class GetNameLeaderApprovedByUserNameResult
    {
        [Description("Mã nhân viên")]
        public string UserName { get; set; }
        
        [Description("Họ và tên")]
        public string FullName { get; set; }
    }

    public class CreateApplicationFormStaffRequest : BaseRequest,IValidatableObject
    {
        [Description("Mã chi nhánh")]
        [StringLength(5)]
        [Required]
        public string BranchID { get; set; }
        
        [Description("Tiêu đề")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Title),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Title { get; set;}
        
        [Description("Lý do")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Reason),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string NoteReason { get; set; }
        
        [Description("Địa điểm")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Address),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Address { get; set; }
        
        [Description("Người duyệt")]
        /*
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Approved),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        */
        public string Approved { get; set; }
        
        [Description("Chế độ nghỉ phép")]
        public int? PolicyLeave { get; set; }
        
        
        [Description("Loại đơn từ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_TypeLeave),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public int TypeLeave { get; set; }
        
        
        [Description("Từ ngày làm việc dd/MM/yyyy HH:mm:ss")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_FromDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string FromDateTimeWork { get; set; }
        
        
        [Description("Đến ngày làm việc dd/MM/yyyy HH:mm:ss")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_ToDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string ToDateTimeWork { get; set; }
        
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (TypeLeave == 3)
            {
                if (PolicyLeave == null )
                {
                    result.Add(new ValidationResult(ResourceFile.AccountErrors.Validate_PolicyLeave));
                }
            }


            return result;
        }
        

    }

    public class GetTimeWorkStaffReportResult
    {
        [Description("Số giờ làm việc")]
        public string TotalTimeWork { get; set; }
        
        [Description("Số công chuẩn")]
        public string NumWork { get; set; }
        
        [Description("Nghỉ phép")]
        public string Leave { get; set; }
        
        [Description("Nghỉ ko phép")]
        public string WithoutLeave { get; set; }
        
        [Description("Số lần đi muộn")]
        public string NumWorkLate { get; set; }
        
        [Description("Số lần về sớm")]
        public string NumBackEarly { get; set; }
        
        [Description("Số phút đi muộn")]
        public string NumMinuteLate { get; set; }
        
        [Description("Số phút về sớm")]
        public string NumMinuteEarly { get; set; }
        
    }


    public class CheckinTimeKeepingGPSStaffRequest : BaseRequest
    {
        [Description("Thời gian checkin dd/MM/yyy HH:mm")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_TimeCheckin),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string TimeCheckin { get; set; }
        
        
        [Description("Mã chi nhánh")]
        public string BranchCode { get; set; }

        [Description("Kinh độ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Longitude),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Longitude { get; set; }
        
        [Description("Vĩ độ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Latitude),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Latitude { get; set; }
    }
    public class CheckoutTimeKeepingGPSStaffRequest : BaseRequest
    {
        [Description("Thời gian out dd/MM/yyy HH:mm")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_TimeCheckout),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string TimeCheckout { get; set; }

        [Description("Mã chi nhánh")]
        public string BranchCode { get; set; }
        
        [Description("Kinh độ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Longitude),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Longitude { get; set; }
        
        [Description("Vĩ độ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_Latitude),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string Latitude { get; set; }
    }

    public class GetCheckinTimeKeepingGPSStaffRequest : BaseRequest
    {
        [Description("Ngày checkin dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_DateCheckinandout),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string DateCheckin { get; set; }
    }

    public class GetCheckoutTimeKeepingGPSStaffRequest : BaseRequest
    {
        [Description("Ngày checkout dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_DateCheckinandout),
            ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string DateCheckout { get; set; }
    }

    public class GetCheckinTimeKeepingGPSStaffResult
    {
        [Description("Giờ vào")]
        public string TimeCheckin {
            get
            {
              return  TimeCheckinOn.ToString("HH:mm:ss");
            } 
        }
        

        [Description("Địa chỉ checkin")]
        public string Address { get; set; }

        [Description("Kiểu Checkin")]
        public string TypeCheckin { get; set; }
        
        [Description("Kieu check in number")]
        public int TypeCheckinNum { get; set; }
        
        [JsonIgnore]
        public DateTime TimeCheckinOn { get; set; }
    }

    public class GetFingerprintingStaffRequest : BaseRequest
    {
        [Description("Từ ngày làm việc dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_FromDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string FromDateWork { get; set; }
        
        [Description("Đến ngày làm việc dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_ToDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string ToDateWork { get; set; }
    }

    public class GetFingerprintingStaffResult
    {
                
        [Description("Ngày làm việc dd/MM/yyyy")]
        public string DateWork {
            get
            {
                return DateWorkOn.ToString("dd/MM/yyyy");
            } 
        }
        
        [Description("Họ tên")]
        public string FullName { get; set; }
        
        [Description("Mã nhân viên")]
        public string UserName { get; set; }
        
        [Description("số phút đi làm muộn MM")]
        public string TimeLateWork { get; set; }
        
        [Description("Số phút về sớm")]
        public string TimeLeaveEarlyWork { get; set; }
        
        [Description("Giờ bắt đầu")]
        public string TimeWorkStart { get; set; }
        
        [Description("Giờ kết thúc")]
        public string TimeWorkEnd { get; set; }
        
        [JsonIgnore]
        public DateTime DateWorkOn { get; set; }
        
        
    }

    public class GetCheckoutTimeKeepingGPSStaffResult
    {
        [Description("Giờ ra")]
        public string TimeCheckOut {
            get
            {
                return TimeCheckOutOn.ToString("HH:mm:ss");
            }
            
        }
        
        [Description("Địa chỉ checkin")]
        public string Address { get; set; }
        
        [Description("Name Checkout")]
        public string TypeCheckOut { get; set; }
        
        [Description("Kiểu Checkout")]
        public int TypeCheckoutNum { get; set; }
        
        [JsonIgnore]
        public DateTime TimeCheckOutOn { get; set; }
    }
    public class GetTimeWorkByStaffRequest : BaseRequest
    {
        
        [Description("Ngày làm việc dd/MM/yyyy")]
        [Required]
        public string DateWork { get; set; }
    }

    public class GetHisTWAndLAStaffByDayRequest : BaseRequest
    {
        
        /*[Description("Từ ngày làm việc dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_FromDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string FromDateWork { get; set; }
        
        [Description("Đến ngày làm việc dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_ToDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string ToDateWork { get; set; }*/
        
        
        [Description("Ngày làm việc dd/MM/yyyy")]
        [Required]
        public string DateWork { get; set; }
    }

    public class CheckDataByDateRequest : BaseRequest
    {
        [Description("ngày làm việc dd/MM/yyyy")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.TimeWorkErrors.Validate_FromDate),ErrorMessageResourceType = typeof(ResourceFile.TimeWorkErrors))]
        public string DateWork { get; set; }
        
    }

    public class GetDetailLeaveApplicationsRequest : BaseRequest
    {
        [Description("Id của đơn từ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.CommonErrors.E222),ErrorMessageResourceType = typeof(ResourceFile.CommonErrors))]
        public int LeaveId { get; set; }
    }

    public class TW_ED_EditLAMemberReportRequest : BaseRequest,IValidatableObject
    {
        [Description("Id của đơn từ")]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.CommonErrors.E222),ErrorMessageResourceType = typeof(ResourceFile.CommonErrors))]
        public int LeaveId { get; set; }
        
        [Description("Tiêu đề")]
        [Required]
        public string Title { get; set; }
        
       
        [Description("Note")]
        [Required]
        public string NoteReason { get; set; }
        
        [Description("Ngày nghỉ bắt đầu")]
        [Required]
        public string TimeLeaveStart { get; set; }
        
        [Description("Ngày nghỉ kết thúc")]
        [Required]
        public string TimeLeaveEnd { get;set; }

        [Description("Kiểu nghỉ phép ")]
        [Required]
        public int TypeLeave  { get; set; }
        
        [Description("Chế độ nghỉ phép")]
        public int? PolicyLeave { get; set; }
        
        
        [Description("Nguoi duyet")]
        [Required]
        public string Approved  { get; set; }
        
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (TypeLeave == 3)
            {
                if (PolicyLeave == null )
                {
                    result.Add(new ValidationResult(ResourceFile.AccountErrors.Validate_PolicyLeave));
                }
            }
            return result;
        }

    }

    public class ActionLeaveApplicationsRequest : BaseRequest
    {
        public int LeaveId { get; set; }
        public int StatusLeave { get; set; }
    }

    public class GetDetailLeaveApplicationsResult
    {
        public string Title { get; set; }
        public string Applicants { get; set; }
       
        public string UserName { get; set; }
       
        public string Department { get; set; }
       
        public string NoteReason { get; set; }

        public string TimeLeaveStart
        {
            get
            {
                return TimeLeaveStartIn.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public string TimeLeaveEnd
        {
            get
            {
                return TimeLeaveEndOut.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public string Address { get; set; }

        public string DateApplicants
        {
            get
            {
                return DateOfApplicants.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        public string Approved  { get; set; }
        
        public int? PolicyLeave { get; set; }
        public string PolicyLeaveName { get; set; }
        
        public int? NumLeave { get; set; }
       
        public string TypeLeave  { get; set; }
        public int TypeLeaveNum { get; set; }
       
        public string StatusLeave  { get; set; }  
        
        public int StatusLeaveNumber { get; set; }
        
        [JsonIgnore]
        public DateTime TimeLeaveStartIn { get; set; }
        [JsonIgnore]
        public DateTime TimeLeaveEndOut { get; set; }
        [JsonIgnore]
        public DateTime DateOfApplicants { get; set; }
    }

    public class CheckDataByMonthRequest : BaseRequest
    {
        [Description("Từ tháng làm việc MM/yyyy")]
        [Required]
        public string FromMonthWork { get; set; }
        
        [Description("Đến tháng làm việc")]
        [Required]
        public string ToMonthWork { get; set; }
        
    }

    public class DataMontnTw
    {
        public string MonthWork
        {
            get
            {
                return MonthWorkOn.ToString("dd/MM/yyyy");
            }
        }
        
        public DateTime MonthWorkOn { get; set; }
    }
    

    public class CheckDataByDateResult
    {
                
        [Description("Ngày làm việc dd/MM/yyyy")]
        public string DateWork {
            get
            {
                return Date.ToString("dd/MM/yyyy");
            }
            
        }
        
        [Description("Thứ trong tuần")]
        public string OnDayWork { get; set; }
        
        [Description("Data có tồn tại, true/false")]
        public bool IsData { get; set; }
        
        [JsonIgnore]
        public DateTime Date { get; set; }
        
    }

    public class checkDataByMonthResult
    {
        
        [Description("Tháng làm việc")]
        public string MonthWork {
            get
            {
                return MonthWorkOn.ToString("MM/yyyy");
            } }

        [Description("Data có tồn tại, true/false")]
        public bool IsData { get; set; }
        
        [JsonIgnore]
        public DateTime MonthWorkOn { get; set; }
    }

    public class GetHisTimeWorkStaffByMonthResult
    {

        [Description("Dữ liệu của tháng đó")]
        public IEnumerable<GetTimeWorkByStaffByMonthResult> DataMonthWork { get; set; }
        
        
    }

    public class GetTimeWorkByStaffResult
    {
        [Description("giờ bắt đầu")]
        public string TimeStart { get; set; }
        [Description("giờ kết thúc")]
        public string TimeEnd { get; set; }

        [Description("giờ làm việc bắt đầu của nhân viên (HH:mm)")]
        public TimeWorkStart TimeWorkStart { get; set; }
        [Description("giờ làm việc kết thúc của nhân viên (HH:mm)")]
        public TimeWorkEnd TimeWorkEnd { get; set; }
         
    }

    public class GetTWAndLaByStaffResult
    {
        [Description("giờ bắt đầu")]
        public string TimeStart { get; set; }
        [Description("giờ kết thúc")]
        public string TimeEnd { get; set; }
        
        [Description("giờ làm việc bắt đầu của nhân viên (HH:mm)")]
        public TimeWorkStart TimeWorkStart { get; set; }
        public TimeWorkEnd TimeWorkEnd { get; set; }
        public List<LeaveApplications> LeaveApplications { get; set; }
    }

    public class LeaveApplications
    {
        public int CountTypeLeave { get; set; }
        [JsonIgnore]
        public int TypeLeave { get; set; }
        public string NameTypeGroup { get; set; }
        public List<GroupTypeLeave> FormLeave { get; set; }
    }

    public class GetTimeWorkType
    {
        public int CountTypeLeave { get; set; }
        public string NameTypeGroup { get; set; }
    }

    public class GroupTypeLeave
    {
        [JsonIgnore]
        public int LeaveId { get; set; }
        [Description("Tiêu đề")]
        public string Title { get; set;}
        
        [JsonIgnore]
        [Description("Kiểu xin phép")]
        public int TypeLeave { get; set; }
        
        [Description("Lý do")]
        public string NoteReason { get; set; }
        
        [Description("Thời gian dd/MM/yyyy HH:mm")] 
        public string TimeApply {
            get
            {
                return TimeApp.ToString("dd/MM/yyyy HH:mm:ss");
            } 
        }
        
        [Description("Người duyệt")]
        public string Approved { get; set; }

        [Description("Trang thai duyet")]
        public int StatusNumber { get; set; }
        
        [Description("tên trạng thái duyệt đơn")]
        public string StatusName { get; set; }
        
        [JsonIgnore]
        public DateTime TimeApp { get; set; }
        
        public DetailLeaveApplications DetailLeave { get; set; }
    }

    public class DetailLeaveApplications
    {
        [JsonIgnore]
        public int LeaveId { get; set; }
        [Description("Tiêu đề")]
        public string Title { get; set;}
        
        [Description("Mã nhân viên")]
        public string UserName { get; set; }
        
        [Description("Bộ phần của Nhân viên: BackOffice, Sale Trái phiếu, Sale Cổ phiếu")]
        public string UserRole { get; set; }
        
        [Description("Lý do")]
        public string NoteReason { get; set; }

        [Description("thời gian nghỉ bắt đầu")]
        public string TimeLeavestart
        {
            get
            {
                return TimeLeaveStartIn.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }

        [Description("thời gian nghỉ kết thúc")]
        public string TimeLeaveEnd
        {
            get
            {
                return TimeLeaveEndOut.ToString("dd/MM/yyyy HH:mm:ss");  
            }
     
        }

        [Description("Địa điểm")]
        public string Address { get; set; }
        
        [Description("ngày viết đơn dd/MM/yyyy HH:mm")] 
        public string TimeApply {
            get
            {
                return TimeApp.ToString("dd/MM/yyyy HH:mm:ss");
            }

        }
        
        [Description("Người duyệt")]
        public string Approved { get; set; }

        [Description("Loại đơn từ")]
        public string NameTypeLeave { get; set; }
        
        [Description("Trạng thái đơn")]
        public string StatusLeave { get; set; }
        
        [JsonIgnore]
        public DateTime TimeApp { get; set; }
        
        [JsonIgnore]
        public DateTime TimeLeaveStartIn { get; set; }
        [JsonIgnore]
        public DateTime TimeLeaveEndOut { get; set; }
        
    }

    public class GetTimeWorkByStaffByMonthResult
    {


        [Description("Ngày làm việc dd/MM/yyyy")]
        public string DateWork
        {
            get
            {
                return DateWorkOn?.ToString("dd/MM/yyyy");
            }
        }
        
        [Description("Giờ bắt đầu")]
        public string TimeWorkStart {
            get
            {
                return TimeInStart?
                    .ToString("HH:mm:ss");
            }
            
        }

        [Description("Giờ kết thúc")]
        public string TimeWorkEnd
        {
            get
            {
                return TimeOutEnd?.ToString("HH:mm:ss");
            }
        }

        [Description("trả về đơn từ ")]
        public List<TypeLeaveApplications> DetailLeave { get; set; }
        
        
        

        [Description("Tổng số công")]
        public decimal? TimeKeeping { get; set; }

        [JsonIgnore]
        public DateTime? TimeInStart { get; set; }  
        
        [JsonIgnore]
        public DateTime? TimeOutEnd { get; set; }

        [JsonIgnore]
        public DateTime? DateWorkOn { get; set; }
    }

    public class TypeLeaveOther
    {
        [Description("trả về tên của đơn từ (có phép,nghỉ không phép)")]
        public string NameLeaveApplications { get; set; }
        
        [Description("trả về trạng thái duyệt(Chưa duyệt,đã duyệt)")]
        public string NameStatusApproved { get; set; }
        
        [Description("trả về Number của trạng thái")]
        public int StatusNumber { get; set; }
    }

    public class TypeLeaveApplications
    {
        [Description("Id của đơn từ nghỉ phép")]
        public int? LeaveId { get; set; }
        
        [Description("Trạng thái duyệt number")]
        public int StatusNumber { get; set; }

        [Description("tên trạng thái duyệt đơn")]
        public string StatusName { get; set; }
        
        
        [Description("Kiểu phép number")]
        public int TypeLeaveNumber { get; set; }
        
        [Description("Tên Kiểu phép")]
        public string TypeLeaveName { get; set; }

        [JsonIgnore]
        public string DateLeaveApplications
        {
            get
            {
                return DateLeave?.ToString("dd/MM/yyyy");
            }
        }

        [JsonIgnore]
        public DateTime? DateLeave { get; set; }
    }

    public class CheckDataByDay
    {
        public int leaveId { get; set; }
        public string Title { get; set; }
        public string TypeLeave { get; set; }
        public string UserName { get; set; }

        public string DateLeave
        {
            get
            {
                return DateApplication.ToString("dd/MM/yyyy");
            }
        }
        public int StatusNumber { get; set; }
        
        public string StatusName { get; set; }
        
        
        [JsonIgnore]
        public DateTime DateApplication { get; set; }
        
    }

    public class TimeWorkStart
    {
        public string Checkin { get; set; }
        public string TimeToWork { get; set; }
        public string AddressWork { get; set; }
        public int? TypeCheckin { get; set; }
    }

    public class TimeWorkEnd
    {
        public string Checkin { get; set; }
        public string TimeToWork { get; set; }
        public string AddressWork { get; set; }
        public int? TypeCheckin { get; set; }
    }
    
    

 
}