﻿
namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Activity")]
    public partial class Activity
    {
        public long ActivityID { get; set; }

        public int EventType { get; set; }

        [Required]
        [StringLength(250)]
        public string ActivityName { get; set; }

        [StringLength(5)]
        public string BranchID { get; set; }

        [StringLength(50)]
        public string AssignUser { get; set; }

        public int ActivityType { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Status { get; set; }

        public int Priority { get; set; }

        [StringLength(250)]
        public string Location { get; set; }

        public bool Notification { get; set; }
        public bool Recurrence { get; set; }

        [StringLength(1000)]
        public string Description { get; set; }

        public long LeadID { get; set; }

        [StringLength(25)]
        public string CustCode { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public int ConfirmStatus { get; set; }
        public string ConfirmBy { get; set; }

        public DateTime? ConfirmDate { get; set; }
        public string ConfirmCode { get; set; }
        public string RejectCode { get; set; }
        public string RejectReason { get; set; }
        public int? ReasonType { get; set; }
    }
}
