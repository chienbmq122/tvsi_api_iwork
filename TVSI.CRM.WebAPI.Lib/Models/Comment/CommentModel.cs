﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Comment
{
    public class CommentModelRequest : BaseRequest
    {
        [Description("customer code nếu là Lead thì truyền string ''(rỗng) ")]
        public string CustCode { get; set; }
        [Description("LeadId nếu KH là Customer thì truyền ID > 0 nếu là customer truyền -1")]
        public int LeadID { get; set; }
    }

    public class CommentDBModel
    {
        [Description("id comment")]
        public int CommentID { get; set; }

        [Description("id lead")]
        public int LeadID { get; set; }

        [Description("customer code")]
        public string CustCode { get; set; }

        [Description("id comment")]
        public string  Content { get; set; }

        [Description("tài khoản người comment")]
        public string UserName { get; set; }

        [Description("Tên người commnet")]
        public string FullName { get; set; }
        [Description("thời gian")]
        public string CreatedDate { get; set; }

        [Description("Điểm đánh giá (số sao)")]
        public int Rate1 { get; set; }

        public List<ReplyModel> ListReply { get; set; }
    }

    public class CreateCommentRequest : BaseRequest
    {
        [Description("customer code nếu là customer thì truyền string ''(rỗng) ")]
        public string CustCode { get; set; }
        [Description("LeadId nếu KH là lead thì truyền ID > 0 nếu là customer truyền -1")]
        public int LeadID { get; set; }

        [Description("Nội dung")]
        public string Content { get; set; }

        [Description("Full tên của user comment (chỗ này lấy full tên user đang comment ra truyền vào)")]
        public string FullName { get; set; }

        [Description("Số điểm đánh giá max 5 min 1")]
        public int Rate { get; set; }
    }
}
