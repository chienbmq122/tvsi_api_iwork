﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Opportunity
{
    public class OpportunityRequest : BaseRequest
    {
        [Description("Tư khóa k có thì truyền '' hoặc null")]
        public string KeyWord { get; set; }
        [Description("Trạng thái 0: tất cả, 1: đang mở, 2: đang xử lý, 3: đang chờ, 4: đã thắng, 5: thất bại, 6: đã bán hết, 7: đã hủy")]
        public int Status { get; set; }
    }

    public class OpportunityResult
    {
        [Description("ID cơ hội")]
        public int OpportunityID { get; set; }
        [Description("Tên cơ hôi")]
        public string OpportunityName { get; set; }
        [Description("Branch ID")]
        public string BranchID { get; set; }
        [Description("Assign user")]
        public string AssignUser { get; set; }
        [Description("Lead Id")]
        public int LeadID { get; set; }
        [Description("Lead Id")]
        public string LeadName { get; set; }
        [Description("Custormer code")]
        public string CustCode { get; set; }
        [Description("Lợi nhuân dư kiến")]
        public string CustName { get; set; }
        [Description("Lợi nhuân dư kiến")]
        public float ExpectedRevenue { get; set; }
        [Description("Ngày đóng dư kiến")]
        public string ExpectedCloseDate { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public String StatusName {
            get
            {
                switch (Status) {
                    case 1:
                        return "đang mở";
                    case 2:
                        return "đang xử lý";
                    case 3:
                        return "đang chờ";
                    case 4:
                        return "đã thắng";
                    case 5:
                        return "thất bại";
                    case 6:
                        return "đã bán hết";
                    case 7:
                        return "đã hủy";
                    default:
                        return "chưa có";
                }
            }
        }
        [Description("Tạo bởi")]
        public string CreatedBy { get; set; }
        [Description("Ngày tạo")]
        public string CreatedDate { get; set; }
        [Description("Sửa bởi")]
        public string UpdatedBy { get; set; }
        [Description("Ngày sửa")]
        public string UpdatedDate { get; set; }
    }

    public class OpportunityCreate : BaseRequest
    {
        [Description("Tên cơ hội Name")]
        public string OpportunityName { get; set; }
        [Description("Branch ID")]
        public string BranchID { get; set; }
        [Description("Assign user")]
        public string AssignUser { get; set; }
        [Description("Lead Id")]
        public int LeadID { get; set; }
        [Description("Custorme code")]
        public string CustCode { get; set; }
        [Description("Lợi nhuân dư kiến")]
        public float ExpectedRevenue { get; set; }
        [Description("Ngày đóng dư kiến format dd/MM/yyyy")]
        public string ExpectedCloseDate { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
    }

    public class OpportunityUpdate : BaseRequest
    {
        [Description("ID cơ hội")]
        public int OpportunityID { get; set; }
        [Description("Tên cơ hội Name")]
        public string OpportunityName { get; set; }
        [Description("Branch ID")]
        public string BranchID { get; set; }
        [Description("Assign user")]
        public string AssignUser { get; set; }
        [Description("Lead Id")]
        public int LeadID { get; set; }
        [Description("Custorme code")]
        public string CustCode { get; set; }
        [Description("Lợi nhuân dư kiến")]
        public float ExpectedRevenue { get; set; }
        [Description("Ngày đóng dư kiến format dd/MM/yyyy")]
        public string ExpectedCloseDate { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
    }

    public class OpportunityDalete : BaseRequest
    {
        [Description("ID cơ hội")]
        public int OpportunityID { get; set; }
    }
}
