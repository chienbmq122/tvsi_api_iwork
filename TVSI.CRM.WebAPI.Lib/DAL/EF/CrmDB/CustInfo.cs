﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{
    [Table("CustInfo")]
    public class CustInfo
    {

        [Key]
        [StringLength(25)]
        public string CustCode { get; set; }
        public long? LeadID { get; set; }
        [Required]
        [StringLength(250)]
        public string CustName { get; set; }
        [StringLength(15)]
        public string Mobile { get; set; }
        [StringLength(150)]
        public string Email { get; set; }
        [StringLength(300)]
        public string Address { get; set; }
        public int ProfileType { get; set; }
        public int Status { get; set; }
        [StringLength(5)]
        public string BranchID { get; set; }
        [StringLength(50)]
        public string AssignUser { get; set; }
        [StringLength(50)]
        public string Picture { get; set; }
        public int? VipType { get; set; }
        [StringLength(50)]
        public string VipDesc { get; set; }
        public DateTime? OpenAccDate { get; set; }
        [StringLength(800)]
        public string Description { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string SaleID_BB { get; set; }
    }
}
