﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IDashboardCommandText
    {
        /// <summary>
        /// Get Sum Trade Info
        /// </summary>
        string SumTradeInfo { get; }

        /// <summary>
        /// Get Sum Fee daily
        /// </summary>
        string SumFeeDaily { get; }

        /// <summary>
        /// Get Sum Fee last week
        /// </summary>
        string SumFeeLastWeek { get; }

        /// <summary>
        /// Get history trande daily
        /// </summary>
        string HistoryTrandeDaily { get; }

        /// Total Amount Last Day
        /// </summary>
        string TotalAmountLastDay { get; }

        /// <summary>
        /// Total Amount last month
        /// </summary>
        string TotalLastMonth { get; }

        /// <summary>
        /// Total Amount Month Before
        /// </summary>
        string TotalAmountMonthBefore { get; }

        /// <summary>
        /// Count account
        /// </summary>
        string CountAccount { get; }

        /// <summary>
        /// Order Info
        /// </summary>
        string OrderInfo { get; }

        /// <summary>
        /// Deal Info
        /// </summary>
        string DealInfo { get; }
    }
}
