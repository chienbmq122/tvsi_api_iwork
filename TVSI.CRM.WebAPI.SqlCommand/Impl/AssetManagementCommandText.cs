﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class AssetManagementCommandText : IAssetManagementCommandText
    {
        #region SQL Command List
        public string GetListTypeAsset => @"SELECT TVSI_TYPE_ASSET.type_assetid as TypeAssetId,name_type as NameType,method as Type 
                                ,(SELECT Sum(quantity) FROM TVSI_ASSET WHERE TVSI_ASSET.type_assetid = TVSI_TYPE_ASSET.type_assetid) as TotalAssetValid 
                                ,(SELECT Count(TVSI_MANAGEMENT_ASSET.assetid) FROM TVSI_MANAGEMENT_ASSET 
                                LEFT JOIN TVSI_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.type_assetid = TVSI_TYPE_ASSET.type_assetid AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL) as TotalAssetUsed 
                                FROM TVSI_TYPE_ASSET WHERE is_active = 1";

        public string GetListStateAsset => @"SELECT state_assetid as StateAssetId, name_state as NameState, type_state as TypeState, color as Color FROM TVSI_STATE_ASSET WHERE is_active = 1";

        public string GetListAsset => @"SELECT a.assetid as AssetId,a.date_buy_asset as DateBuyAsset,a.date_out_warranty as DateOutWarranty,a.detail as Detail,a.serial_number_asset as SerialNumberAsset,a.money as Money,
                                a.quantity as Quantity, a.state_assetid as StateAssetId,a.type_assetid as TypeAssetId,a.name_asset as NameAsset,b.date_receive_asset as DateReceiveAsset, b.date_response_asset as DateResponseAsset, 
                                b.management_state as Status,b.management_assetid as ManagementAssetId, b.userid as UserId,b.departmentid as DepartmentId, a.created_date as CreatedDate FROM TVSI_ASSET AS a 
                                LEFT JOIN TVSI_MANAGEMENT_ASSET b ON a.assetid = b.assetid AND b.date_response_asset IS NULL WHERE a.is_active = 1 and a.type_assetid = @typeId 
                                ORDER BY a.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetListAssetSelect => @"SELECT a.assetid as AssetId,a.serial_number_asset as SerialNumberAsset, a.quantity as Quantity, a.name_asset as NameAsset  
                                FROM TVSI_ASSET AS a WHERE a.is_active = 1 and a.type_assetid = @typeId";

        public string CountListAsset => @"SELECT COUNT(1) FROM TVSI_ASSET WHERE TVSI_ASSET.is_active = 1 AND TVSI_ASSET.type_assetid = @typeId";

        public string GetListAssetConfirm => @"SELECT TVSI_ASSET.assetid as AssetId, TVSI_ASSET.type_assetid as TypeAssetId, TVSI_ASSET.state_assetid as StateAssetId, TVSI_ASSET.money as Money, 
                                TVSI_ASSET.serial_number_asset as SerialNumberAsset, TVSI_MANAGEMENT_ASSET.quantity as Quantity,TVSI_ASSET.barcode_assetid as BarcodeAssetId, 
                                TVSI_ASSET.name_asset as NameAsset, TVSI_ASSET.detail as Detail, TVSI_ASSET.date_buy_asset as DateBuyAsset, TVSI_ASSET.date_out_warranty as DateOutWarranty, 
                                TVSI_ASSET.created_date as CreatedDate, TVSI_ASSET.updated_date as UpdatedDate, TVSI_ASSET.deleted_date as DeletedDate, TVSI_MANAGEMENT_ASSET.management_assetid as ManagementAssetId, 
                                TVSI_MANAGEMENT_ASSET.management_state as Status, TVSI_MANAGEMENT_ASSET.userid as UserId, TVSI_MANAGEMENT_ASSET.departmentid as DepartmentId 
                                FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.management_state = 3 
                                ORDER BY TVSI_ASSET.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetListAssetInIds => @"SELECT a.assetid as AssetId,a.date_buy_asset as DateBuyAsset,a.date_out_warranty as DateOutWarranty,a.detail as Detail,a.serial_number_asset as SerialNumberAsset,a.money as Money,
                                a.quantity as Quantity, a.state_assetid as StateAssetId,a.type_assetid as TypeAssetId,a.name_asset as NameAsse FROM TVSI_ASSET AS a 
                                WHERE a.is_active = 1 AND a.assetId IN {ids}";

        public string CountListAssetConfirm => @"SELECT COUNT(1) FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.management_state = 3";

        public string CreateAsset => @"INSERT INTO TVSI_ASSET(type_assetid, state_assetid, money, quantity, name_asset, serial_number_asset, date_out_warranty, date_buy_asset, detail, is_active, 
                                created_date, created_by) VALUES(@type, @state, @money, @quantity, @name, @serial_number, @date_out_warranty, @date_buy_asset, @detail, 1, CURRENT_TIMESTAMP, @userId)";

        public string UpdateAsset => @"UPDATE TVSI_ASSET SET type_assetid = @type_assetid, state_assetid = @state_assetid, money = @money, quantity = @quantity,
                            name_asset = @name_asset, serial_number_asset = @serial_number_asset, date_out_warranty = @date_out_warranty, date_buy_asset = @date_buy_asset,
                            detail = @detail, updated_by = @updatedBy,updated_date = CURRENT_TIMESTAMP WHERE assetid = @assetid AND is_active = 1";

        public string DeleteAsset => @"UPDATE TVSI_ASSET SET is_active = 0, deleted_date = CURRENT_TIMESTAMP, deleted_by = @userId WHERE assetid = @assetid";

        public string GetDetailAsset => @"SELECT a.assetid as AssetId,a.date_buy_asset as DateBuyAsset,a.date_out_warranty as DateOutWarranty,a.detail as Detail,a.serial_number_asset as SerialNumberAsset,a.money as Money,
                                a.quantity as Quantity, a.state_assetid as StateAssetId,a.type_assetid as TypeAssetId,a.name_asset as NameAsset,b.date_receive_asset as DateReceiveAsset, b.date_response_asset as DateResponseAsset, 
                                b.management_state as Status,b.management_assetid as ManagementAssetId, b.userid as UserId,b.departmentid as DepartmentId, a.created_date as CreatedDate FROM TVSI_ASSET AS a 
                                LEFT JOIN TVSI_MANAGEMENT_ASSET b ON a.assetid = b.assetid AND b.date_response_asset IS NULL WHERE a.is_active = 1 AND a.assetId = @assetId";

        public string FilterAsset => @"SELECT TVSI_ASSET.assetid as AssetId, TVSI_ASSET.type_assetid as TypeAssetId, TVSI_ASSET.state_assetid as StateAssetId, TVSI_ASSET.money as Money, 
                                TVSI_ASSET.serial_number_asset as SerialNumberAsset, TVSI_MANAGEMENT_ASSET.quantity as Quantity,TVSI_ASSET.barcode_assetid as BarcodeAssetId, 
                                TVSI_ASSET.name_asset as NameAsset, TVSI_ASSET.detail as Detail, TVSI_ASSET.date_buy_asset as DateBuyAsset, TVSI_ASSET.date_out_warranty as DateOutWarranty, 
                                TVSI_ASSET.created_date as CreatedDate, TVSI_ASSET.updated_date as UpdatedDate, TVSI_ASSET.deleted_date as DeletedDate, TVSI_MANAGEMENT_ASSET.management_assetid as ManagementAssetId, 
                                TVSI_MANAGEMENT_ASSET.management_state as Status, TVSI_MANAGEMENT_ASSET.userid as UserId, TVSI_MANAGEMENT_ASSET.departmentid as DepartmentId 
                                FROM TVSI_ASSET LEFT JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL 
                                WHERE TVSI_ASSET.is_active = 1 ";
        public string FilterCount => @"SELECT Count(1) FROM TVSI_ASSET WHERE TVSI_ASSET.is_active = 1 ";

        public string GetListAssetUser => @"SELECT TVSI_ASSET.assetid as AssetId, TVSI_ASSET.type_assetid as TypeAssetId, TVSI_ASSET.state_assetid as StateAssetId, TVSI_ASSET.money as Money, 
                                TVSI_ASSET.serial_number_asset as SerialNumberAsset, TVSI_MANAGEMENT_ASSET.quantity as Quantity,TVSI_ASSET.barcode_assetid as BarcodeAssetId, 
                                TVSI_ASSET.name_asset as NameAsset, TVSI_ASSET.detail as Detail, TVSI_ASSET.date_buy_asset as DateBuyAsset, TVSI_ASSET.date_out_warranty as DateOutWarranty, 
                                TVSI_ASSET.created_date as CreatedDate, TVSI_ASSET.updated_date as UpdatedDate, TVSI_ASSET.deleted_date as DeletedDate, TVSI_MANAGEMENT_ASSET.management_assetid as ManagementAssetId, 
                                TVSI_MANAGEMENT_ASSET.management_state as Status, TVSI_MANAGEMENT_ASSET.userid as UserId, TVSI_MANAGEMENT_ASSET.date_receive_asset as DateReceiveAsset,
                                TVSI_MANAGEMENT_ASSET.date_response_asset as DateResponseAsset, TVSI_MANAGEMENT_ASSET.departmentid as DepartmentId 
                                FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.userid = @userid AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL 
                                ORDER BY TVSI_ASSET.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string CountListAssetUser => @"SELECT COUNT(1) FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.userid = @userid AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL";

        public string GetHistoryAssetUser => @"SELECT TVSI_ASSET.assetid as AssetId, TVSI_ASSET.type_assetid as TypeAssetId, TVSI_ASSET.state_assetid as StateAssetId, TVSI_ASSET.money as Money, 
                                TVSI_ASSET.serial_number_asset as SerialNumberAsset, TVSI_MANAGEMENT_ASSET.quantity as Quantity,TVSI_ASSET.barcode_assetid as BarcodeAssetId, 
                                TVSI_ASSET.name_asset as NameAsset, TVSI_ASSET.detail as Detail, TVSI_ASSET.date_buy_asset as DateBuyAsset, TVSI_ASSET.date_out_warranty as DateOutWarranty, 
                                TVSI_ASSET.created_date as CreatedDate, TVSI_ASSET.updated_date as UpdatedDate, TVSI_ASSET.deleted_date as DeletedDate, TVSI_MANAGEMENT_ASSET.management_assetid as ManagementAssetId, 
                                TVSI_MANAGEMENT_ASSET.management_state as Status 
                                FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.userid = @userid 
                                ORDER BY TVSI_ASSET.created_date DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string CountHistoryAssetUser => @"SELECT COUNT(1) FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 AND TVSI_MANAGEMENT_ASSET.userid = @userid AND TVSI_MANAGEMENT_ASSET.date_response_asset IS NULL";

        public string GetListAssetChild => @"SELECT c.assetid as AssetId,c.name_asset as NameAsset, c.detail as Detail,c.serial_number_asset as SerialNumberAsset,c.date_buy_asset as DateBuyAsset,
                                c.state_assetid as StateAssetId, c.date_out_warranty as DateOutWarranty,c.type_assetid as TypeAssetId, f.created_date as CreatedDate,f.updated_date as UpdatedDate,
                                f.deleted_date as DeletedDate, f.relationship_id as RelationshipId, f.quantity as Quantity 
                                FROM TVSI_ASSET as c RIGHT JOIN TVSI_ASSETRELATIONSHIP as f ON f.child_assetid = c.assetid WHERE f.parent_assetid = @assetId AND c.is_active = 1 AND f.deleted_date IS NULL";

        public string AsignAssetChild => @"INSERT INTO TVSI_ASSETRELATIONSHIP(parent_assetid,child_assetid,quantity,is_active,created_date,created_by)
                                VALUES(@assetParentId, @assetChildId, @quantity, 1, CURRENT_TIMESTAMP, @userId)";

        public string RemoveAssetChild => @"UPDATE TVSI_ASSETRELATIONSHIP SET deleted_date = CURRENT_TIMESTAMP, deleted_by = @userId WHERE relationship_id = @relationshipId";

        public string GetHistoryAsignAsset => @"SELECT c.assetid as AssetId,c.name_asset as NameAsset, c.detail as Detail,c.serial_number_asset as SerialNumberAsset,c.date_buy_asset as DateBuyAsset,
                                c.state_assetid as StateAssetId, c.date_out_warranty as DateOutWarranty,c.type_assetid as TypeAssetId, f.created_date as CreatedDate,f.updated_date as UpdatedDate,
                                f.deleted_date as DeletedDate, f.relationship_id as RelationshipId, f.quantity as Quantity 
                                FROM TVSI_ASSET as c INNER JOIN TVSI_ASSETRELATIONSHIP as f ON f.child_assetid = c.assetid WHERE f.parent_assetid = @assetId AND c.is_active = 1";

        public string CreateManagementAsset => @"INSERT INTO TVSI_MANAGEMENT_ASSET(assetid, userid, date_receive_asset, management_state, departmentid, is_active, created_date, created_by, quantity) 
                                VALUES(@assetid, @receiveUserid, @dateReceiveAsset, @managementState, @departmentid, 1, CURRENT_TIMESTAMP, @userId, @quantity)";

        public string UpdateManagementAsset => @"UPDATE TVSI_MANAGEMENT_ASSET SET updated_date = CURRENT_TIMESTAMP, ";

        public string GetHistoryManagementAsset => @"SELECT a.management_assetid as ManagementAssetId,a.assetid as AssetId,a.date_receive_asset as DateReceiveAsset,COALESCE(a.date_response_asset,'') DateResponseAsset,
                                a.management_state as Status, a.userid as UserId, a.departmentid as DepartmentId FROM TVSI_MANAGEMENT_ASSET as a WHERE a.assetid = @assetId";

        public string FilterManagementAsset => @"SELECT TVSI_ASSET.assetid as AssetId, TVSI_ASSET.type_assetid as TypeAssetId, TVSI_ASSET.state_assetid as StateAssetId, TVSI_ASSET.money as Money, 
                                TVSI_ASSET.serial_number_asset as SerialNumberAsset, TVSI_MANAGEMENT_ASSET.quantity as Quantity,TVSI_ASSET.barcode_assetid as BarcodeAssetId, 
                                TVSI_ASSET.name_asset as NameAsset, TVSI_ASSET.detail as Detail, TVSI_ASSET.date_buy_asset as DateBuyAsset, TVSI_ASSET.date_out_warranty as DateOutWarranty, 
                                TVSI_ASSET.created_date as CreatedDate, TVSI_ASSET.updated_date as UpdatedDate, TVSI_ASSET.deleted_date as DeletedDate, TVSI_MANAGEMENT_ASSET.management_assetid as ManagementAssetId, 
                                TVSI_MANAGEMENT_ASSET.management_state as Status 
                                FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid 
                                WHERE TVSI_ASSET.is_active = 1 ";

        public string FilterCountManagement => @"SELECT Count(1) FROM TVSI_ASSET INNER JOIN TVSI_MANAGEMENT_ASSET ON TVSI_ASSET.assetid = TVSI_MANAGEMENT_ASSET.assetid WHERE TVSI_ASSET.is_active = 1 ";

        public string GetListInspection => @"with temptable AS (select departmentid, max(date_inspection) as date_inspection from TVSI_INSPECTION_ASSET group by departmentid) 
                                select a.inspectionid as InspectionId, a.date_inspection as DateInspection, a.departmentid as DepartmentId, a.asset_checked_inspection as AssetCheckedInspection, 
                                a.asset_not_checked_inspection as AssetNotCheckedInspection, a.created_date as CreatedDate, a.created_by as CreatedBy, a.updated_date as UpdatedDate, a.updated_by as UpdatedBy 
                                from TVSI_INSPECTION_ASSET a join temptable b ON a.departmentid = b.departmentid AND a.date_inspection = b.date_inspection order by a.departmentid";

        public string GetInspectionDetail => @"SELECT TOP(1) a.inspectionid as InspectionId, a.date_inspection as DateInspection, a.departmentid as DepartmentId, a.asset_checked_inspection as AssetCheckedInspection, 
                                a.asset_not_checked_inspection as AssetNotCheckedInspection, a.created_date as CreatedDate, a.created_by as CreatedBy, a.updated_date as UpdatedDate, a.updated_by as UpdatedBy 
                                FROM TVSI_INSPECTION_ASSET as a WHERE a.inspectionid = @InspectionId ORDER BY a.date_inspection DESC";

        public string GetHistoryInspection => @"SELECT a.inspectionid as InspectionId, a.date_inspection as DateInspection, a.departmentid as DepartmentId, a.asset_checked_inspection as AssetCheckedInspection, 
                                a.asset_not_checked_inspection as AssetNotCheckedInspection, a.created_date as CreatedDate, a.created_by as CreatedBy, a.updated_date as UpdatedDate, a.updated_by as UpdatedBy 
                                FROM TVSI_INSPECTION_ASSET as a WHERE a.departmentid = @DepartmentId ORDER BY a.date_inspection DESC";

        public string CreateInspectionAsset => @"INSERT INTO TVSI_INSPECTION_ASSET(date_inspection, departmentid, asset_checked_inspection, asset_not_checked_inspection, created_date, created_by) 
                                VALUES(@dateInspection,@departmentid,@assetCheckedInspection,@assetNotCheckedInspection,CURRENT_TIMESTAMP,@userId)";

        public string UpdateInspectionAsset => @"UPDATE TVSI_INSPECTION_ASSET SET asset_checked_inspection = @assetCheckedInspection, asset_not_checked_inspection = @assetCheckedInspection, updated_date = CURRENT_TIMESTAMP,
                                updated_by = @userId WHERE inspectionid = @InspectionId";

        public string DeleteInspectionAsset => @"UPDATE TVSI_INSPECTION_ASSET SET is_active = 0, deleted_date = CURRENT_TIMESTAMP, deleted_by = @userId WHERE inspectionid = @inspectionId";

        public string GetListDepartment => @"SELECT danh_muc_phong_ban_id as DepartmentId, ma_phong_ban as DepartmentCode, ten_phong_ban as DepartmentName FROM TVSI_DANH_MUC_PHONG_BAN WHERE trang_thai <> 99";

        public string GetListUserFromDepartment => @"SELECT TVSI_THONG_TIN_TRUY_CAP.ten_dang_nhap as UserName, TVSI_NHAN_SU.ho_va_ten as FullName, TVSI_NHAN_SU.danh_muc_phong_ban_id as DepartmentId 
                                FROM TVSI_NHAN_SU INNER JOIN TVSI_THONG_TIN_TRUY_CAP ON TVSI_NHAN_SU.nhan_su_id = TVSI_THONG_TIN_TRUY_CAP.nhan_su_id WHERE TVSI_NHAN_SU.danh_muc_phong_ban_id = @DepartmentId AND TVSI_NHAN_SU.trang_thai = 1";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
