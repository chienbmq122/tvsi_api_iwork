﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Category;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Models.System;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class AttributeService : BaseService, IAttributeService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private IAttributeCommandText _commandText;
        public AttributeService(IAttributeCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<IEnumerable<AtributeModel>> GetListAttribute()
        {
            var sql = _commandText.GetListAttribute;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryAsync<AtributeModel>(sql, new { });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<LeadAttrinbuteViewModel>> GetLeadAttribute(LeadAttributeRequest model)
        {
            var sql = _commandText.GetListAttributeLead;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryAsync<LeadAttrinbuteViewModel>(sql, new { leadId = model.LeadID });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<CustomerAttributeModel>> GetCustAttribute(CustomerAttributeRequest model)
        {
            var sql = _commandText.GetListAttributeCus;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryAsync<CustomerAttributeModel>(sql, new { custCode = model.CustCode });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateLeadAttribute(LeadAttrinbuteCreate model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateAttributeLead, new
                    {
                        AttributeID = model.AttributeID,
                        LeadID = model.LeadID,
                        Value = model.Value,
                        Status = 1,
                        Priority = model.Priority,
                        CreatedBy = model.UserName,
                        CreatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex);
                throw;
            }
        }

        public async Task<bool> CreateCustAttribute(CustomerAttrinbuteCreate model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateAttributeCust, new
                    {
                        AttributeID = model.AttributeID,
                        CustCode = model.CustCode,
                        Value = model.Value,
                        Status = 1,
                        Priority = model.Priority,
                        CreatedBy = model.UserName,
                        CreatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex);
                throw;
            }
        }

        public async Task<bool> DeleteLeadAttribute(LeadAttrinbuteDelete model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.DeleteAttributeLead, new{ AttributeID = model.AttributeID, LeadID = model.LeadID });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex);
                throw;
            }
        }

        public async Task<bool> DeleteCustAttribute(CustomerAttrinbuteCreate model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.DeleteAttributeCust, new { AttributeID = model.AttributeID, CustCode = model.CustCode });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex);
                throw;
            }
        }

        public async Task<bool> CheckLeadAttributeExit(LeadAttrinbuteCreate model)
        {
            var sql = _commandText.CheckLeadAttribute;
            try
            {
                var data = await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<LeadAttrinbuteViewModel>(sql, new { leadId = model.LeadID, attributeID = model.AttributeID });
                });
                if(data != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CheckCustAttributeExit(CustomerAttrinbuteCreate model)
        {
            var sql = _commandText.CheckCustAttribute;
            try
            {
                var data = await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<CustomerAttributeModel>(sql, new { custCode = model.CustCode, attributeID = model.AttributeID });
                });
                if (data != null)
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<CategoryResult>> GetListCategory(CategoryRequset model)
        {
            var sql = _commandText.GetListCategory;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    return await conn.QueryAsync<CategoryResult>(sql, new {});
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAttribute:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
