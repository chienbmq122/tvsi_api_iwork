﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Services.Impl;
using TVSI.CRM.WebAPI.SqlCommand;
using TVSI.CRM.WebAPI.SqlCommand.Impl;
using Unity;

namespace TVSI.CRM.WebAPI
{
    /// <summary>
    /// Config Web API
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Đăng ký cấu hình
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            var container = new UnityContainer();

            // Regist Sql Command Text
            container.RegisterType<ISystemCommandText, SystemCommandText>();
            container.RegisterType<ILeadCommandText, LeadCommandText>();
            container.RegisterType<ITimeWorkCommandText,TimeWorkCommandText>();
            container.RegisterType<IActivityCommandText, ActivityCommandText>();
            container.RegisterType<ICustCommandText, CustCommandText>();
            container.RegisterType<IOpportunityCommandText, OpportunityCommandText>();
            container.RegisterType<IAttributeCommandText, AttributeCommandText>();
            container.RegisterType<IDashboardCommandText, DashboardCommandText> ();
            container.RegisterType<IAccountCommandText, AccountCommandText> ();
            container.RegisterType<ICommentText, CommentText>();
            container.RegisterType<IContactCommandText, ContactCommandText>();
            container.RegisterType<IAssetManagementCommandText, AssetManagementCommandText>();
            container.RegisterType<INotificationCommandText, NotificationCommandText>();


            // Regist Service
            container.RegisterType<ISystemService, SystemService>();
            container.RegisterType<IDashboardService, DashboardService>();
            container.RegisterType<ILeadService, LeadService>();
            container.RegisterType<ITimeWorkService, TimeWorkService>();
            container.RegisterType<IActivityService, ActivityService>();
            container.RegisterType<ICustomerServices, CustomerService>();
            container.RegisterType<IBondService, BondService>();
            container.RegisterType<IOpportunityService, OpportunityService>();
            container.RegisterType<IAttributeService, AttributeService>();
            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<ICommentService, CommentService>();
            container.RegisterType<IContactService, ContactService>();
            container.RegisterType<IAssetService, AssetService>();
            container.RegisterType<INotificationService, NotificationService>();

            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
