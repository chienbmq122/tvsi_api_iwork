﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Notification;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface INotificationService
    {
        Task<NotificationResponseModel> GetNotification(NotificationRequestModel model);
    }
}
