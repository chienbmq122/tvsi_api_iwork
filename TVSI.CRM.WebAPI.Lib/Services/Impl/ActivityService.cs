﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Activity;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class ActivityService : BaseService, IActivityService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private IActivityCommandText _commandText;
        public ActivityService(IActivityCommandText commandText)
        {
            _commandText = commandText;
        }
        public async Task<List<ActivityResult>> GetActivity(ActivityRequest model)
        {
            //var start = (model.PageIndex - 1) * model.PageSize;
            string dateTime = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            // var sqlCount = _commandText.GetCountActivity;
            var sql = _commandText.GetListActivity;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    List<ActivityResult> resultData = new List<ActivityResult>();
                    if (model.UserRole.Contains("BO"))
                    {
                        string sqlBo = "SELECT * FROM BOActivity WHERE BOActivity.StartDate <= @dateStart AND BOActivity.EndDate >= @dateEnd AND BOActivity.Status <> 99 AND BOActivity.AssignUser = @userAssign Order by BOActivity.StartDate DESC";
                        IEnumerable<BOActivityDatabase> result = await conn.QueryAsync<BOActivityDatabase>(sqlBo,
                            new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00" , userAssign = model.UserName});
                        foreach (BOActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.BOActivityID;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        // sqlCount.Replace("Activity", "CFActivity");
                        sql = sql.Replace("Activity.ReasonType,", "");
                        sql = sql.Replace("FROM Activity", "FROM CFActivity");
                        sql = sql.Replace("Activity.", "CFActivity.");
                        IEnumerable<CFActivityDatabase> result = await conn.QueryAsync<CFActivityDatabase>(sql,
                        new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", userAssign = model.UserName });
                        foreach (CFActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else
                    {
                        IEnumerable<ActivityDatabase> result = await conn.QueryAsync<ActivityDatabase>(sql,
                        new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", userAssign = model.UserName });
                        foreach (ActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            _activityResult.ReasonType = data.ReasonType;
                            resultData.Add(_activityResult);
                        }
                    }
                    // int activityCount = await conn.QueryFirstOrDefaultAsync<int>(sqlCount, new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", userAssign = model.UserName });
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<ActivityResult>> GetActivityByEventType(ActivityRequestByEventType model)
        {
            // var start = (model.PageIndex - 1) * model.PageSize;
            string dateTime = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            //var sqlCount = _commandText.GetCountActivityByEvenType;
            var sql = _commandText.GetListActivityByEvenType;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    ActivityResultData dataResponse = new ActivityResultData();
                    List<ActivityResult> resultData = new List<ActivityResult>();
                    if (model.UserRole.Contains("BO"))
                    {
                        // sqlCount = sqlCount.Replace("Activity", "BOActivity");
                        //sql.Replace("Activity", "BOActivity");
                        string sqlBo = "SELECT * FROM BOActivity WHERE StartDate <= @dateStart AND EndDate >= @dateEnd AND Status <> 99 AND EventType = @eventType AND AssignUser = @userAssign Order by StartDate DESC";
                        IEnumerable<BOActivityDatabase> result = await conn.QueryAsync<BOActivityDatabase>(sqlBo,
                            new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", eventType = model.EventType, userAssign = model.UserName });
                        foreach (BOActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.BOActivityID;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        //sqlCount = sqlCount.Replace("Activity", "CFActivity");
                        sql = sql.Replace("Activity.ReasonType,", "");
                        sql = sql.Replace("FROM Activity", "FROM CFActivity");
                        sql = sql.Replace("Activity.", "CFActivity.");
                        IEnumerable<CFActivityDatabase> result = await conn.QueryAsync<CFActivityDatabase>(sql,
                        new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", eventType = model.EventType, userAssign = model.UserName });
                        foreach (CFActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else
                    {
                        IEnumerable<ActivityDatabase> result = await conn.QueryAsync<ActivityDatabase>(sql,
                        new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", eventType = model.EventType, userAssign = model.UserName });
                        foreach (ActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    //int activityCount = await conn.QueryFirstOrDefaultAsync<int>(sqlCount, new { dateStart = dateTime + " 23:59:59", dateEnd = dateTime + " 00:00:00", eventType = model.EventType, userAssign = model.UserName });
                    //dataResponse.totalItem = activityCount;
                    //dataResponse.Items = resultData;
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ActivityResultData> GetActivityByFiltter(FilterActivity model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            string fromTime = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            string toTime = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
            var sqlCount = "SELECT count(*) FROM Activity WHERE StartDate  >= '" + fromTime + " 00:00:00' and EndDate <= '" + toTime + " 23:59:59'";
            var sql = "SELECT Activity.ActivityID, Activity.EventType, Activity.ActivityName, Activity.BranchID, Activity.AssignUser, Activity.ActivityType, Activity.StartDate, Activity.EndDate, ";
            sql += "Activity.Status, Activity.Priority, Activity.Location, Activity.Recurrence, Activity.Notification, Activity.Description, Activity.LeadID, Activity.CustCode, Activity.ConfirmStatus, ";
            sql += "Activity.ConfirmBy, Activity.ConfirmDate, Activity.ConfirmCode, Activity.RejectCode, Activity.RejectReason FROM Activity WHERE Activity.StartDate  >= '" + fromTime + " 00:00:00' and Activity.EndDate <= '" + toTime + " 23:59:59'";
            if (!String.IsNullOrEmpty(model.Priority.ToString()) && model.Priority != 0)
            {
                sql = sql + " and Activity.Priority = " + model.Priority;
                sqlCount = sqlCount + " and Activity.Priority = " + model.Priority;
            }
            if (!String.IsNullOrEmpty(model.UserName))
            {
                sql = sql + " and Activity.AssignUser = '" + model.UserName + "'";
                sqlCount = sqlCount + " and Activity.AssignUser = '" + model.UserName + "'";
            }
            if (!String.IsNullOrEmpty(model.Status.ToString()) && model.Status != 0)
            {
                sql = sql + " and Activity.Status = " + model.Status;
                sqlCount = sqlCount + " and Activity.Status = " + model.Status;
            }
            if (!String.IsNullOrEmpty(model.EventType.ToString()) && model.EventType != 0)
            {
                sql = sql + " and Activity.EventType = " + model.EventType;
                sqlCount = sqlCount + " and Activity.EventType = " + model.EventType;
            }
            sql = sql + " Order by Activity.StartDate DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    ActivityResultData dataResponse = new ActivityResultData();
                    List<ActivityResult> resultData = new List<ActivityResult>();
                    if (model.UserRole.Contains("BO"))
                    {
                        sqlCount = sqlCount.Replace("Activity", "BOActivity");
                        sql = sql.Replace("Activity", "BOActivity");
                        IEnumerable<BOActivityDatabase> result = await conn.QueryAsync<BOActivityDatabase>(sql,
                            new { Start = start, PageSize = model.PageSize });
                        foreach (BOActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.BOActivityID;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sqlCount = sqlCount.Replace("Activity", "CFActivity");
                        sql = sql.Replace("Activity.", "CFActivity.");
                        sql = sql.Replace(" FROM Activity", ", Lead.LeadName, CustInfo.CustName FROM CFActivity ");
                        sql = sql.Replace("WHERE", "LEFT JOIN Lead ON Lead.LeadID = CFActivity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = CFActivity.CustCode WHERE ");
                        log.Error(sql);
                        IEnumerable <CFActivityDatabase> result = await conn.QueryAsync<CFActivityDatabase>(sql,
                        new { Start = start, PageSize = model.PageSize });
                        foreach (CFActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else
                    {
                        sql = sql.Replace(" FROM Activity", ", Lead.LeadName, CustInfo.CustName, Activity.ReasonType FROM Activity ");
                        sql = sql.Replace("WHERE", "LEFT JOIN Lead ON Lead.LeadID = Activity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Activity.CustCode WHERE ");
                        IEnumerable<ActivityDatabase> result = await conn.QueryAsync<ActivityDatabase>(sql,
                        new { Start = start, PageSize = model.PageSize });
                        foreach (ActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    int activityCount = await conn.QueryFirstOrDefaultAsync<int>(sqlCount, new { });
                    dataResponse.totalItem = activityCount;
                    dataResponse.Items = resultData;
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<ActivityResult> GetDetailActivity(DeleteActivityRequest model)
        {
            var sql = _commandText.GetDetailActivity;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    ActivityResult dataResponse = new ActivityResult();
                    if (model.UserRole.Contains("BO"))
                    {
                        string sqlBo = "SELECT * FROM BOActivity WHERE BOActivity.BOActivityID = @activityId AND BOActivity.Status <> 99";
                        BOActivityDatabase result = await conn.QueryFirstOrDefaultAsync<BOActivityDatabase>(sqlBo,
                            new { activityId = model.ActivityId });

                        dataResponse.ActivityId = result.BOActivityID;
                        dataResponse.Title = result.ActivityName;
                        dataResponse.EventType = result.EventType;
                        dataResponse.Type = result.ActivityType;
                        dataResponse.StartDate = result.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.EndDate = result.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.Location = result.Location;
                        dataResponse.Priority = result.Priority;
                        dataResponse.Status = result.Status;
                        dataResponse.Assigned = result.AssignUser;
                        dataResponse.Detail = result.Description;
                        dataResponse.ConfirmBy = result.ConfirmBy;
                        dataResponse.ConfirmCode = result.ConfirmCode;
                        dataResponse.ConfirmDate = result.ConfirmDate;
                        dataResponse.ConfirmStatus = result.ConfirmStatus;
                        dataResponse.RejectCode = result.RejectCode;
                        dataResponse.RejectReason = result.RejectReason;
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql = sql.Replace("Activity.ReasonType,", "");
                        sql = sql.Replace("Activity", "CFActivity");
                        CFActivityDatabase result = await conn.QueryFirstOrDefaultAsync<CFActivityDatabase>(sql,
                            new { activityId = model.ActivityId });

                        dataResponse.ActivityId = result.ActivityID;
                        dataResponse.Title = result.ActivityName;
                        dataResponse.EventType = result.EventType;
                        dataResponse.LeadId = result.LeadID;
                        dataResponse.LeadName = result.LeadName;
                        dataResponse.CustCode = result.CustCode;
                        dataResponse.CustName = result.CustName;
                        dataResponse.Type = result.ActivityType;
                        dataResponse.StartDate = result.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.EndDate = result.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.Location = result.Location;
                        dataResponse.Priority = result.Priority;
                        dataResponse.Status = result.Status;
                        dataResponse.Assigned = result.AssignUser;
                        dataResponse.Detail = result.Description;
                        dataResponse.ConfirmBy = result.ConfirmBy;
                        dataResponse.ConfirmCode = result.ConfirmCode;
                        dataResponse.ConfirmDate = result.ConfirmDate;
                        dataResponse.ConfirmStatus = result.ConfirmStatus;
                        dataResponse.RejectCode = result.RejectCode;
                        dataResponse.RejectReason = result.RejectReason;
                    }
                    else
                    {
                        ActivityDatabase result = await conn.QueryFirstOrDefaultAsync<ActivityDatabase>(sql,
                            new { activityId = model.ActivityId });

                        dataResponse.ActivityId = result.ActivityID;
                        dataResponse.Title = result.ActivityName;
                        dataResponse.EventType = result.EventType;
                        dataResponse.LeadId = result.LeadID;
                        dataResponse.LeadName = result.LeadName;
                        dataResponse.CustCode = result.CustCode;
                        dataResponse.CustName = result.CustName;
                        dataResponse.Type = result.ActivityType;
                        dataResponse.StartDate = result.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.EndDate = result.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                        dataResponse.Location = result.Location;
                        dataResponse.Priority = result.Priority;
                        dataResponse.Status = result.Status;
                        dataResponse.Assigned = result.AssignUser;
                        dataResponse.Detail = result.Description;
                        dataResponse.ConfirmBy = result.ConfirmBy;
                        dataResponse.ConfirmCode = result.ConfirmCode;
                        dataResponse.ConfirmDate = result.ConfirmDate;
                        dataResponse.ConfirmStatus = result.ConfirmStatus;
                        dataResponse.RejectCode = result.RejectCode;
                        dataResponse.RejectReason = result.RejectReason;
                        dataResponse.ReasonType = result.ReasonType;
                    }
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<IEnumerable<ActivityTypeResult>> GetActivityType()
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    IEnumerable<ActivityTypeResult> resultData = await conn.QueryAsync<ActivityTypeResult>(_commandText.GetListActivityType, new {});
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> CreateActivity(CreateActivityRequest model)
        {
            try
            {
                string startTime = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                string endTime = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                return await WithCrmDbConnection(async conn =>
                {
                    if (model.UserRole.Contains("BO"))
                    {
                        await conn.ExecuteAsync(_commandText.PostBOActivity, new
                        {
                            eventType = model.EventType,
                            activityName = model.Title,
                            assignUser = model.Assigned,
                            activityType = model.Type,
                            startDate =  startTime + ".000",
                            endDate = endTime + ".000",
                            status = model.Status,
                            priority = model.Priority,
                            location = model.Location,
                            description = model.Detail,
                            createdBy = model.UserName,
                            createdDate = DateTime.Now,
                        });
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        await conn.ExecuteAsync(_commandText.PostCFActivity, new
                        {
                            eventType = model.EventType,
                            activityName = model.Title,
                            assignUser = model.Assigned,
                            tvsiUserName = model.Assigned,
                            directContact = model.Approved,
                            activityType = model.Type,
                            startDate = startTime + ".000",
                            endDate = endTime + ".000",
                            status = model.Status,
                            recurrence = 0,
                            notification = 1,
                            leadID = model.LeadId,
                            custCode = model.Custcode,
                            priority = model.Priority,
                            location = model.Location,
                            description = model.Detail,
                            createdBy = model.UserName,
                            createdDate = DateTime.Now
                        });
                    }
                    else
                    {
                        await conn.ExecuteAsync(_commandText.PostActivity, new
                        {
                            eventType = model.EventType,
                            activityName = model.Title,
                            assignUser = model.Assigned,
                            notification = 1,
                            activityType = model.Type,
                            startDate = startTime + ".000",
                            endDate = endTime + ".000",
                            status = model.Status,
                            recurrence = 0,
                            leadID = model.LeadId,
                            custCode = model.Custcode,
                            priority = model.Priority,
                            location = model.Location,
                            description = model.Detail,
                            createdBy = model.UserName,
                            createdDate = DateTime.Now,
                            ReasonType = model.ReasonType,
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("SyncNotificationRead:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> UpdateActivity(UpdateActivityRequest model)
        {
            try
            {
                string startTime = DateTime.ParseExact(model.StartDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                string endTime = DateTime.ParseExact(model.EndDate, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
                return await WithCrmDbConnection(async conn =>
                {
                    var sql = _commandText.GetDetailActivity;
                    if (model.UserRole.Contains("BO"))
                    {
                        sql.Replace("Activity", "BOActivity");
                        sql.Replace("ActivityID", "BOActivityID");
                        BOActivityDatabase result = await conn.QueryFirstOrDefaultAsync<BOActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.PutBOActivity, new
                        {
                            eventType = !String.IsNullOrEmpty(model.EventType.ToString()) ? model.EventType : result.EventType,
                            activityName = !String.IsNullOrEmpty(model.Title) ? model.Title : result.ActivityName,
                            assignUser = !String.IsNullOrEmpty(model.Assigned)? model.Assigned : result.AssignUser,
                            activityType = !String.IsNullOrEmpty(model.Type.ToString()) ? model.Type : result.ActivityType,
                            branchID = 0,
                            startDate = !String.IsNullOrEmpty(model.StartDate) ? startTime + ".000" : result.StartDate.ToString(),
                            endDate = !String.IsNullOrEmpty(model.EndDate) ? endTime + ".000" : result.EndDate.ToString(),
                            status = !String.IsNullOrEmpty(model.Status.ToString())? model.Status : result.Status,
                            priority = !String.IsNullOrEmpty(model.Priority.ToString()) ? model.Priority : result.Priority,
                            location = !String.IsNullOrEmpty(model.Location) ? model.Location : result.Location,
                            description = !String.IsNullOrEmpty(model.Detail) ? model.Detail : result.Description,
                            updatedBy = model.UserName,
                            updatedDate = DateTime.Now,
                            activityID = model.ActivityId
                        });
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql.Replace("Activity", "CFActivity");
                        CFActivityDatabase result = await conn.QueryFirstOrDefaultAsync<CFActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.PutCFActivity, new
                        {
                            eventType = !String.IsNullOrEmpty(model.EventType.ToString()) ? model.EventType : result.EventType,
                            activityName = !String.IsNullOrEmpty(model.Title) ? model.Title : result.ActivityName,
                            assignUser = !String.IsNullOrEmpty(model.Assigned) ? model.Assigned : result.AssignUser,
                            tvsiUserName = !String.IsNullOrEmpty(model.Assigned) ? model.Assigned : result.TvsiUserName,
                            directContact = !String.IsNullOrEmpty(model.Assigned) ? model.Assigned : result.DirectContact,
                            activityType = !String.IsNullOrEmpty(model.Type.ToString()) ? model.Type : result.ActivityType,
                            startDate = !String.IsNullOrEmpty(model.StartDate) ? startTime + ".000" : result.StartDate.ToString(),
                            endDate = !String.IsNullOrEmpty(model.EndDate) ? endTime + ".000" : result.EndDate.ToString(),
                            status = !String.IsNullOrEmpty(model.Status.ToString()) ? model.Status : result.Status,
                            recurrence = 0,
                            branchID = 0,
                            notification = 1,
                            leadID = !String.IsNullOrEmpty(model.LeadId.ToString()) ? model.LeadId : result.LeadID,
                            custCode = !String.IsNullOrEmpty(model.Custcode) ? model.Custcode : result.CustCode,
                            priority = !String.IsNullOrEmpty(model.Priority.ToString()) ? model.Priority : result.Priority,
                            location = !String.IsNullOrEmpty(model.Location) ? model.Location : result.Location,
                            description = !String.IsNullOrEmpty(model.Detail) ? model.Detail : result.Description,
                            updatedBy = model.UserName,
                            updatedDate = DateTime.Now,
                            activityID = model.ActivityId
                        });
                    }
                    else
                    {
                        ActivityDatabase result = await conn.QueryFirstOrDefaultAsync<ActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.PutActivity, new
                        {
                            eventType = !String.IsNullOrEmpty(model.EventType.ToString()) ? model.EventType : result.EventType,
                            activityName = !String.IsNullOrEmpty(model.Title) ? model.Title : result.ActivityName,
                            assignUser = !String.IsNullOrEmpty(model.Assigned) ? model.Assigned : result.AssignUser,
                            notification = 1,
                            activityType = !String.IsNullOrEmpty(model.Type.ToString()) ? model.Type : result.ActivityType,
                            startDate = !String.IsNullOrEmpty(model.StartDate) ? startTime + ".000" : result.StartDate.ToString(),
                            endDate = !String.IsNullOrEmpty(model.EndDate) ? endTime + ".000" : result.EndDate.ToString(),
                            status = !String.IsNullOrEmpty(model.Status.ToString()) ? model.Status : result.Status,
                            recurrence = 0,
                            leadID = !String.IsNullOrEmpty(model.LeadId.ToString()) ? model.LeadId : result.LeadID,
                            custCode = !String.IsNullOrEmpty(model.Custcode) ? model.Custcode : result.CustCode,
                            priority = !String.IsNullOrEmpty(model.Priority.ToString()) ? model.Priority : result.Priority,
                            location = !String.IsNullOrEmpty(model.Location) ? model.Location : result.Location,
                            description = !String.IsNullOrEmpty(model.Detail) ? model.Detail : result.Description,
                            updatedBy = model.UserName,
                            updatedDate = DateTime.Now,
                            activityID = model.ActivityId,
                            ReasonType = model.ReasonType,
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateActivity:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> DeleteActivity(DeleteActivityRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    //ActivityResult detailActivityData = await GetDetailActivity(requertDataGetDetailActivity);
                    var sql = _commandText.GetDetailActivity;
                    if (model.UserRole.Contains("BO"))
                    {
                        sql.Replace("Activity", "BOActivity");
                        sql.Replace("ActivityID", "BOActivityID");
                        BOActivityDatabase result = await conn.QueryFirstOrDefaultAsync<BOActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.DeleteActivity, new
                        {
                            activityID = model.ActivityId
                        });
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql.Replace("Activity", "CFActivity");
                        CFActivityDatabase result = await conn.QueryFirstOrDefaultAsync<CFActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.DeleteActivity, new
                        {
                            activityID = model.ActivityId
                        });
                    }
                    else
                    {
                        ActivityDatabase result = await conn.QueryFirstOrDefaultAsync<ActivityDatabase>(sql,
                            new { activityId = model.ActivityId });
                        await conn.ExecuteAsync(_commandText.DeleteActivity, new
                        {
                            activityID = model.ActivityId
                        });
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateActivity:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> UpdateOrDeleteListActivity(UpdateListActivity model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var sql = _commandText.UpdateListActivity;
                    if (model.UserRole.Contains("BO"))
                    {
                        sql.Replace("Activity", "BOActivity");
                        sql.Replace("ActivityID", "BOActivityID");
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql.Replace("Activity", "CFActivity");
                    }
                    await conn.ExecuteAsync(sql, new
                    {
                        status = model.Status,
                        activityIDs = model.Ids
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateActivity:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> ConfirmOrRejectActivity(ConfirmOrRejectRequest model)
        {
            string sql = "";
            foreach (int id in model.Ids)
            {
                var code = ActivityConst.EncriptMd5(model.UserName + ActivityConst.RandomNumber(6));
                if (code.Length > 50)
                    code = code.Substring(0, 50);
                if(model.Status == 1)
                    sql = sql + "UPDATE Activity SET ConfirmCode = '" + code + "', ConfirmDate = '" + DateTime.Now + "' WHERE ActivityID = " + id + ";";
                else
                    sql = sql + "UPDATE Activity SET RejectCode = '" + code + "' WHERE ActivityID = " + id + ";";
            }
            if(model.Status != 1 && model.Status != 2)
            {
                return false;
            }
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    log.Error("UpdateActivity:" + sql);
                    if (model.UserRole.Contains("BO"))
                    {
                        sql.Replace("Activity", "BOActivity");
                        sql.Replace("ActivityID", "BOActivityID");
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql.Replace("Activity", "CFActivity");
                    }
                    await conn.ExecuteAsync(sql, new {});
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateActivity:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<ActivityDataMonthCheckResponse>> CheckDataByMonth(ActivityDataMonthRequest model)
        {
            DateTime today = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            int days = DateTime.DaysInMonth(today.Year, today.Month);
            string sql = _commandText.CheckDataByMonth;
            if (model.UserRole.Contains("BO"))
            {
                sql = sql.Replace("ActivityID", "BOActivityID");
                sql = sql.Replace("FROM Activity", "FROM BOActivity");
            }
            else if (model.UserRole.Contains("CF"))
            {
                sql = sql.Replace("FROM Activity", "FROM CFActivity");
            }
            IEnumerable<ActivityDataFromQuery> dataQuery = await WithCrmDbConnection(async conn =>
            {
                return await conn.QueryAsync<ActivityDataFromQuery>(sql, new { userName = model.UserName });
            });
            List<ActivityDataMonthCheckResponse> data = new List<ActivityDataMonthCheckResponse>();
            for(var i = 1; i <= days; i++)
            {
                ActivityDataMonthCheckResponse dayCheck = new ActivityDataMonthCheckResponse();
                string day = new DateTime(today.Year, today.Month, i).ToString("dd/MM/yyyy");
                var dataCheck = dataQuery.FirstOrDefault(e => e.DateWork.Equals(day));
                bool hasWork = false;
                if(dataCheck != null)
                {
                    hasWork = true;
                }
                dayCheck.DateWork = day;
                dayCheck.HasData = hasWork;
                data.Add(dayCheck);
            }
            return data;
        }
        public async Task<List<ActivityDataMonthResponse>> GetHisTimeWorkStaffByMonth(ActivityDataMonthRequest model)
        {
            List<ActivityDataMonthResponse> data = new List<ActivityDataMonthResponse>();
            string sql = _commandText.GetHisTimeWorkStaffByMonth;
            DateTime today = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            int days = DateTime.DaysInMonth(today.Year, today.Month);
            if (model.UserRole.Contains("BO"))
            {
                sql = sql.Replace("ActivityID", "BOActivityID");
                sql = sql.Replace("FROM Activity", "FROM BOActivity");
            }
            else if (model.UserRole.Contains("CF"))
            {
                sql = sql.Replace("FROM Activity", "FROM CFActivity");
            }
            IEnumerable<ActivityDataFromQuery> dataQueryEvent = await WithCrmDbConnection(async conn =>
            {
                return await conn.QueryAsync<ActivityDataFromQuery>(sql, new { userName = model.UserName, type = 1 });
            });
            IEnumerable<ActivityDataFromQuery> dataQueryWork = await WithCrmDbConnection(async conn =>
            {
                return await conn.QueryAsync<ActivityDataFromQuery>(sql, new { userName = model.UserName, type = 2 });
            });
            for (var i = 1; i <= days; i++)
            {
                ActivityDataMonthResponse dayCheck = new ActivityDataMonthResponse();
                string day = new DateTime(today.Year, today.Month, i).ToString("dd/MM/yyyy");
                int hasEvent = 0;
                int hasWork = 0;
                var dataCheckEnvent = dataQueryEvent.FirstOrDefault(e => e.DateWork.Equals(day));
                if (dataCheckEnvent != null)
                {
                    hasEvent = dataCheckEnvent.CountActivity;
                }
                var dataCheckWork = dataQueryWork.FirstOrDefault(e => e.DateWork.Equals(day));
                if (dataCheckWork != null)
                {
                    hasWork = dataCheckWork.CountActivity;
                }
                
                dayCheck.EventCount = hasEvent;
                dayCheck.WorkCount = hasWork;
                dayCheck.DateWork = day;
                data.Add(dayCheck);
            }
            return data;
        }

        public async Task<bool> CheckCustCode(string custCode, string assignUser)
        {
            var sql = @"SELECT CustCode FROM CustInfo WHERE CustCode = @custCode AND Status <> 99 AND AssignUser = @assignUser";
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    string result = await conn.QueryFirstOrDefaultAsync<string>(sql,
                           new { custCode = custCode, assignUser = assignUser });
                    if (string.IsNullOrEmpty(result))
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                log.Error("CheckCustCode:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
