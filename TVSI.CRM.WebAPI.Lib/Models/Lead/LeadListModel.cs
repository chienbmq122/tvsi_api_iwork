﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.Models.TimeWork;

namespace TVSI.CRM.WebAPI.Lib.Models.Lead
{
    public class LeadListRequest : BaseRequest
    {
        [Description("Từ khóa tìm kiếm : Họ tên, mã khách hàng")]
        public string keySearch { get; set; }

        [Description("Trạng thái - 0:Thêm mới, 1:Đã được Assign, 2:Đã được chuyển thành Khách hàng, -1:Đã đóng, 99:Đã xóa")]
        [Required]
        public int Status { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }


    public class LeadListResult
    {
        [Description("Ngày tạo")]
      
        public string CreatedDate { get; set; }

        [Description("ID khách hàng")]
        public long LeadID { get; set; }

        [Description("Họ và tên")]
        public string LeadName { get; set; }
        [Description("Số điện thoại")]
        public string Mobile { get; set; }
        [Description("Email")]
        public string Email { get; set; }
        [Description("SaleId")]
        public string AssignUser { get; set; }

        [Description("ID Cá nhân/Tổ Chức")]
        public int? ProfileType { get; set; }
        [Description("Cá nhân/Tổ Chức")]
        public string ProfileName => ResourceFile.SystemModule.ResourceManager.GetString("ProfileType_" + ProfileType);

        [Description("Trạng thái - 0:Thêm mới, 1:Đã được Assign, 2:Đã được chuyển thành Khách hàng, -1:Đã đóng, 99:Đã xóa")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.SystemModule.ResourceManager.GetString("LeadStatus_" + Status);
        [Description("ID nguồn giới thiệu")]
        public int LeadSourceID { get; set; }

        [Description("Nguồn giới thiệu")]
        //public string SourceName => ResourceFile.SystemModule.ResourceManager.GetString("LeadSourceId_" + LeadSourceID);
        public string SourceName{ get; set; }
        public string MobileStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Mobile))
                {
                    return Mobile.Length >= 3 ? Mobile.Remove(Mobile.Length - 3) + "***" : "***";
                }

                return Mobile;
            }
        }

        public string EmailStr
        {
            get
            {
                if (!string.IsNullOrEmpty(Email))
                {
                    return Email.Length >= 3 ? "***" + Email.Substring(3) : "***";
                }

                return Email;
            }
        }
    }

    public class LeadDeleteRequest : BaseRequest
    {

        [Description("ID KH tiềm năng")]
        [Required]
        public int LeadId { get; set; }

        [Description("SaleId")]
        public string AssignUser { get; set; }
    }

    public class LeadListUserRequest : BaseRequest
    {
        [Description("Tên lead")]
        [Required]
        public string LeadName { get; set; }
        [Description("Trang")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi 1 trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class LeadListUserRessult
    {
        [Description("ID khách hàng")]
        public long LeadID { get; set; }
        [Description("Họ và tên")]
        public string LeadName { get; set; }
    }

    public class LeadListUserResponse
    {
        [Description("Tổng số lead")]
        public int totalItem { get; set; }
        [Description("Danh sách leads")]
        public IEnumerable<LeadListUserRessult> Items { get; set; }
    }
}
