﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Bond;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Bond API
    /// </summary>
    public class BondController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(BondController));
        private IBondService _bondService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bondService"></param>
        public BondController(IBondService bondService)
        {
            _bondService = bondService;
        }

        #region Danh sách lệnh đặt
        /// <summary>
        /// Lấy danh sách lệnh đặt
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OL_BondRequestOrderList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondRequestOrderListResult>))]
        [Note("MH Mua bán TP > Danh sách lệnh yêu cầu", true)]
        public async Task<IHttpActionResult> BM_OL_BondRequestOrderList(BondRequestOrderListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var data = await _bondService.GetBondRequestOrderList(model);

                var retData = new SuccessListModel<BondRequestOrderListResult> {RetData = data};

                LogResult(log, retData);

                return Ok(retData);

            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy chi tiết lệnh đặt
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OL_BondRequestOrderDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondRequestOrderDetailResult>))]
        [Note("MH Mua bán TP > Chi tiết lệnh", true)]
        public async Task<IHttpActionResult> BM_OL_BondRequestOrderDetail(BondRequestOrderDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var data = await _bondService.GetBondRequestOrderDetail(model.RequestOrderID);

                var retData = new SuccessModel<BondRequestOrderDetailResult> {RetData = data};
                LogResult(log, retData);

                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
        #endregion

        /// <summary>
        /// Lấy danh sách nhóm Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BB_BondGroupList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondGroupResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BB_BondGroupList(BondGroupRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);
                var data = await _bondService.GetBondGroupList();

                var retData = new SuccessListModel<BondGroupResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách Trái phiếu theo nhóm Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BB_BondCodeList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondCodeResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BB_BondCodeList(BondCodeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);
                var data = await _bondService.GetBondCodeListByGroupCode(model.GroupCode);

                var retData = new SuccessListModel<BondCodeResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách nhóm hoa hồng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BB_BondNavConfigList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondNavConfigResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BB_BondNavConfigList(BondNavConfigRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);
                var data = await _bondService.GetBondNavConfigList();

                var retData = new SuccessListModel<BondNavConfigResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách phương thức thanh toán
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BB_BondPaymentMethodList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondPaymentMethodResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BB_BondPaymentMethodList(BondPaymentMethodRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);
                var data = await _bondService.GetBondPaymentMethodList();

                var retData = new SuccessListModel<BondPaymentMethodResult> { RetData = data };
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách khách hàng và danh sách cộng tác viên theo Sale
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_CI_BondCustInfoBySale")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondCustInfoResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_CI_BondCustInfoBySale(BondCustInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var data = await _bondService.GetCustInfo(model.UserName);

                var retData = new SuccessModel<BondCustInfoResult> {RetData = data};
                LogResult(log, retData);
                return Ok(new SuccessModel<BondCustInfoResult> { RetData = data });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy chi tiết thông tin khách hàng (khi chọn khách hàng)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_CI_BondCustInfoDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondCustInfoDetailResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_CI_BondCustInfoDetail(BondCustInfoDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var saleId = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";
                var data = await _bondService.GetCustInfoDetail(model.CustCode, saleId);

                var retData = new SuccessModel<BondCustInfoDetailResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy chi tiết thông tin Trái phiếu (Khi select mã trái phiếu)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BI_BondInfoDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondInfoResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BI_BondInfoDetail(BondInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var data = await _bondService.GetBondInfo(model.BondCode, model.CustCode);

                var retData = new SuccessModel<BondInfoResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy chi tiết thông tin lãi suất của Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_BI_BondRateInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondRateResult>))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_BI_BondRateInfo(BondRateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                SetLanguage(lang);
                LogRequest(log, model);

                var data = await _bondService.GetBondRate(model);

                var retData = new SuccessModel<BondRateResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// Đặt lệnh mua Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_BondBuyOrder")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_OD_BondBuyOrder(BondBuyOrderRequest model, 
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.InsertBuyOrderRequest(model);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Cập nhật lệnh mua Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_BondBuyOrderUpdate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Mua bán TP > Đặt lệnh mua", true)]
        public async Task<IHttpActionResult> BM_OD_BondBuyOrderUpdate(BondBuyOrderUpdateRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.UpdateBuyOrderRequest(model);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách thông tin lệnh bán trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_SellContractTempList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<BondSellContractTempListResult>))]
        [Note("MH Mua bán TP > Đặt lệnh bán", true)]
        public async Task<IHttpActionResult> BM_OD_SellContractTempList(BondSellContractTempListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.GetSellContractTempList(model);

                var retData = new SuccessListModel<BondSellContractTempListResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy chi tiết thông tin lệnh bán trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_SellContractTempDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BondSellContractTempDetailResult>))]
        [Note("MH Mua bán TP > Đặt lệnh bán", true)]
        public async Task<IHttpActionResult> BM_OD_SellContractTempDetail(BondSellContractTempDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var saleId = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";
                var data = await _bondService.GetSellContractTempDetail(model.ContractTempID, saleId);

                var retData = new SuccessModel<BondSellContractTempDetailResult> {RetData = data};
                LogResult(log, retData);
                return Ok(retData);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Đặt lệnh bán Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_BondSellOrder")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Mua bán TP > Đặt lệnh bán", true)]
        public async Task<IHttpActionResult> BM_OD_BondSellOrder(BondSellOrderRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.InsertSellOrderRequest(model);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Cập nhật lệnh bán Trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_BondSellOrderUpdate")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Mua bán TP > Đặt lệnh bán", true)]
        public async Task<IHttpActionResult> BM_OD_BondSellOrderUpdate(BondSellOrderUpdateRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.UpdateSellOrderRequest(model);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Xóa lệnh Mua/Bán trái phiếu
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_BondOrderDelete")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("MH Mua bán TP > Danh sách lệnh > Hủy lệnh đặt", true)]
        public async Task<IHttpActionResult> BM_OD_BondOrderDelete(BondOrderDeleteRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var data = await _bondService.DeleteOrderRequest(model.RequestOrderID, model.UserName);

                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy giá bán tạm tính 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BM_OD_SellPriceInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SellPriceResult>))]
        [Note("MH Mua bán TP > Đặt lệnh bán", true)]
        public async Task<IHttpActionResult> BM_OD_SellPriceInfo(SellPriceRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model);
                var sellDate = DateTime.ParseExact(model.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                var data = await _bondService.GetSellPrice(model.ContractNo, sellDate, model.Volume);

                var retData = new SuccessModel<SellPriceResult> { RetData = data };
                LogResult(log, retData);
                return Ok(retData);
                
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
    }
}
