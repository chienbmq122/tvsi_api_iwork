﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.System;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface ISystemService
    {
        Task<UserLoginResult> Login(string userName, string password);

        Task<NotificationResult> GetNotificationList(int pageIndex, int pageSize,
           string userName, string lang);

        Task<int> SyncNotificationRead(List<long> idList, string userName);
    }
}
