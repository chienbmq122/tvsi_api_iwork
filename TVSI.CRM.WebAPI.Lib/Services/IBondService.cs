﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Bond;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IBondService
    {
        Task<IEnumerable<BondRequestOrderListResult>> GetBondRequestOrderList(BondRequestOrderListRequest request);
        Task<BondRequestOrderDetailResult> GetBondRequestOrderDetail(long id);

        Task<IEnumerable<BondGroupResult>> GetBondGroupList();

        Task<IEnumerable<BondCodeResult>> GetBondCodeListByGroupCode(string groupCode);

        Task<IEnumerable<BondNavConfigResult>> GetBondNavConfigList();
        Task<List<BondPaymentMethodResult>> GetBondPaymentMethodList();

        Task<BondCustInfoResult> GetCustInfo(string userName);

        Task<BondCustInfoDetailResult> GetCustInfoDetail(string custCode, string saleID);

        Task<BondInfoResult> GetBondInfo(string bondCode, string custCode);
        Task<BondRateResult> GetBondRate(BondRateRequest model);

        Task<int> InsertBuyOrderRequest(BondBuyOrderRequest model);

        Task<int> UpdateBuyOrderRequest(BondBuyOrderUpdateRequest model);

        #region Bán Trái phiếu

        Task<IEnumerable<BondSellContractTempListResult>> GetSellContractTempList(BondSellContractTempListRequest model);

        Task<BondSellContractTempDetailResult> GetSellContractTempDetail(long contractTempID, string saleID);

        Task<int> InsertSellOrderRequest(BondSellOrderRequest model);
        Task<int> UpdateSellOrderRequest(BondSellOrderUpdateRequest model);

        #endregion

        Task<int> DeleteOrderRequest(long requestOrderID, string userName);
        Task<SellPriceResult> GetSellPrice(string contractNo, DateTime sellDate, int volume);
    }
}
