﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IContactCommandText
    {
        /// <summary>
        /// get list contact staff
        /// </summary>
        string GetListUserStaf { get; }

        /// <summary>
        /// get count contact
        /// </summary>
        string GetCountContact { get; }

        /// <summary>
        /// get list Role of staff
        /// </summary>
        string GetListRoleStaf { get; }

        string GetListContacCustomer { get; }
        string GetCountContacCustomer { get; }
        string GetInfoStaff { get; }
        string GetInfoLead { get; }
        string GetInfoCust { get; }
    }
}
