﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using log4net;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB;
using TVSI.CRM.WebAPI.Lib.ENum;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Account;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.Lib.Utilities.ConfigUtils;
using TVSI.CRM.WebAPI.SqlCommand;


namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class AccountService : BaseService, IAccountService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(AccountService));
        private IAccountCommandText _accountCommand;

        public AccountService(IAccountCommandText accountCommand)
        {
            _accountCommand = accountCommand;
        }

        public async Task<bool> CreateAccount(CreateAccountRequest model)
        {
            try
            {
                if (model.BankAccountList.Count() > 0)
                {
                    for (int i = 0; i < model.BankAccountList.Count(); i++)
                    {
                        model.BankAccountList[i].BankName =
                                await GetBankName(model.BankAccountList[i].BankNo);
                            model.BankAccountList[i].SubBranchName = await GetSubBranchName(
                                model.BankAccountList[i].BankNo,
                                model.BankAccountList[i].SubBranchNo);
                    }
                }
                return await InsertOpenAccDocument(model);
            }
            catch (Exception ex)
            {
                log.Error("CreateAccount:CreateAccount=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetAccountBySaleResult>> AC_GE_GetListAccountBySale(GetAccountBySaleRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var saleList = new List<string>();
                var branchList = new List<string>();
                var dataListRes = new List<GetAccountBySaleResult>();
                var systemID = await GetSystemID(model.UserName);
                if (systemID != null)
                {
                    if (systemID.Level == 2)
                    {
                        saleList.Add(model.UserName);
                        if (systemID != null)
                        {
                            var saleListRes = await GetSaleList(systemID.SystemID);
                            if (saleList != null)
                            {
                                foreach (var saleName in saleListRes)
                                {
                                    if (!saleList.Contains(saleName))
                                    {
                                        saleList.Add(saleName);
                                    }
                                }
                            }
                        }
                    }
                    else if (systemID.Level == 1)
                    {
                        var branchListRes = await GetBranchList(systemID.BranchID);
                        if (branchListRes != null)
                        {
                            foreach (var data in branchListRes)
                            {
                                branchList.Add(data);
                            }

                            saleList.Add(systemID.UserName);
                            var saleListResponse = await GetSaleList(systemID.SystemID);
                            if (saleListResponse != null)
                            {
                                foreach (var saleName in saleListResponse)
                                {
                                    if (!saleList.Contains(saleName))
                                        saleList.Add(saleName);
                                }
                            }
                        }
                    }

                    if (model.UserName != null)
                    {

                        switch (systemID.Level)
                        {
                            
                                    
                            case 1:
                                dataListRes = (List<GetAccountBySaleResult>) await WithCommonDBConnection(async conn =>
                                    await conn.QueryAsync<GetAccountBySaleResult>(
                                        _accountCommand.GetAccountBySaleIdDirector, new
                                        {
                                            @branch_id = branchList,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        }));
                                break;
                            case 2:
                                dataListRes = (List<GetAccountBySaleResult>) await WithCommonDBConnection(async conn =>
                                    await conn.QueryAsync<GetAccountBySaleResult>(
                                        _accountCommand.GetAccountBySaleIdLeader, new
                                        {
                                            @UserName = saleList,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        }));
                                break;
                            case 0:
                                dataListRes = (List<GetAccountBySaleResult>) await WithCommonDBConnection(async conn =>
                                    await conn.QueryAsync<GetAccountBySaleResult>(_accountCommand.GetAccountBySaleId,
                                        new
                                        {
                                            @UserName = model.UserName,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        }));
                                break;
                        }
                    }
                }

                return dataListRes;
            }
            catch (Exception ex)
            {
                log.Error("OP_GE_GetListAccountBySale:OP_GE_GetListAccountBySale=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetAccountBySaleResult>> AC_GE_GetListAccountFillBySale(
            GetAccountFillBySaleRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var fromDateWork = model.FromDate != null
                    ? DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var toDateWork = model.ToDate != null
                    ? DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var sql = _accountCommand.GetAccountInfo;
                sql = sql.Replace("{cond1}", fromDateWork != null ? "and CAST(ngay_tao as date) >= cast(@FromDate as date)" : "");
                sql = sql.Replace("{cond2}", toDateWork != null ? " and CAST(ngay_tao as date) <= cast(@ToDate as date)" : "");
                sql = sql.Replace("{cond3}",  !string.IsNullOrEmpty(model.ProfileStatus) ? "and trang_thai_ho_so = @ProfileStatus" : "");
                sql = sql.Replace("{cond4}", model.SourceAccount != null ? "and nguon_du_lieu = @SourceAccount" : "");
                sql = sql.Replace("{cond5}", !string.IsNullOrEmpty(model.Activated)  ? "and trang_thai_tk = @Activated" : "");
                sql = sql.Replace("{cond6}", !string.IsNullOrEmpty(model.KeySearch) ? "and ( ma_khach_hang LIKE @KeySearch or ho_ten_kh LIKE @KeySearch )" : "");
                var data = await WithCommonDBConnection(async conn => await conn.QueryAsync<GetAccountBySaleResult>(sql, new
                {
                    UserName = model.UserName,
                    FromDate = fromDateWork,
                    ToDate = toDateWork,
                    ProfileStatus = model.ProfileStatus,
                    SourceAccount = model.SourceAccount,
                    Activated = model.Activated,
                    KeySearch = "%" + model.KeySearch + "%",
                    Start = start,
                    PageSize = model.PageSize
                    
                }));
                return data;
            }
            catch (Exception ex)
            {
                log.Error("Loi Method AC_GE_GetListAccountFillBySale = " + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> OP_CA_CheckAccountNumExisted(string accountNo)
        {
            try
            {
     
                return await WithCommonDBConnection(async conn =>
                {
                    var checkRegCommon = await conn.QueryFirstOrDefaultAsync<string>(_accountCommand.GetRegAccountExistedCommond, new
                    {
                        @UserName = accountNo
                    });
                    var checkAccountCommon = await conn.QueryFirstOrDefaultAsync<string>(_accountCommand.GetAccountExistedCommond, new
                    {
                        @UserName = accountNo
                    });
                    var revertAccountCommon = await conn.QueryFirstOrDefaultAsync<string>(_accountCommand.GetRevertExistedCommond, new
                    {
                        @UserName = accountNo
                    });
                    var vevertBond =
                        await WithBondDbConnection(async bond => await bond.QueryFirstOrDefaultAsync<string>(_accountCommand.GetAccountExistedBond, new
                    {
                        @UserName = accountNo
                    }));
                    var extendCust = await WithCrmDbConnection(async crm => await crm.QueryFirstOrDefaultAsync<string>(
                        _accountCommand.GetExtendCust, new
                        {
                            @CustCode = accountNo
                        }));
                    return string.IsNullOrEmpty(checkRegCommon) && string.IsNullOrEmpty(checkAccountCommon) && string.IsNullOrEmpty(revertAccountCommon) 
                           && string.IsNullOrEmpty(vevertBond) && string.IsNullOrEmpty(extendCust);
                    
                });

            }
            catch (Exception ex)
            {
                log.Error("OP_CA_CheckAccountNumExisted:OP_CA_CheckAccountNumExisted=" + accountNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CheckCardIdExsted(string cardID)
        {
            try
            {
                var sqlCma =
                    "SELECT so_cmnd_passport FROM TVSI_TAI_KHOAN_KHACH_HANG WHERE trang_thai = 1 AND so_cmnd_passport = @so_cmnd";
                var sqlCrm = "SELECT * FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd=@so_cmnd AND trang_thai_tk <> 99";
                var sqlCmb = "SELECT so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN WHERE so_cmnd = @so_cmnd";

                var check = true;

                await WithCommonDBConnection(async conn =>
                {
                    var dataFirst = await conn.QueryFirstOrDefaultAsync<string>(sqlCma, new
                    {
                        so_cmnd = cardID
                    });
                    var dataSecond = await conn.QueryFirstOrDefaultAsync<string>(sqlCmb, new
                    {
                        so_cmnd = cardID
                    });
                    if (!string.IsNullOrEmpty(dataFirst) || !string.IsNullOrEmpty(dataSecond))
                        check = false;
                });
                await WithCrmDbConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(sqlCrm, new
                    {
                        so_cmnd = cardID
                    });
                    if (!string.IsNullOrEmpty(data))
                        check = false;
                });
                return check;
            }
            catch (Exception ex)
            {
                log.Error("Loi Method CheckCardIdExsted =" + cardID + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetAccountDetailBySaleResult> OP_CA_GetAccountDetailBySale(
            GetAccountDetailBySaleRequest model)
        {
            try
            {
              return await WithCommonDBConnection(async conn =>
                {
                    var retData = new GetAccountDetailBySaleResult();
                    
                    // lấy thông tin tài khoản bảng DANG_KY_TAI_KHOAN_KHACH_HANG
                    var dataBasic = await conn.QueryFirstOrDefaultAsync<GetAccountDetailBasicBySaleResult>(
                        _accountCommand.GetDetailBasicInfoCust, new
                        {
                            @CustId = model.CustId,
                            @UserName = model.UserName
                        });
                    var sqlOpenContract = "SELECT han_muc_du_no CreditLimit,thoi_gian_ghi_no DebtTerm,ty_le_ky_quy DepositRate FROM TVSI_FS_THONG_TIN_NO_HOP_DONG WHERE ho_so_mo_tk_id = @CustID";
                    var openContract = await conn.QueryFirstOrDefaultAsync<GetAccountDetailBasicBySaleResult>(
                        sqlOpenContract, new
                        {
                            CustID = model.CustId
                        });
                    if (dataBasic != null)
                    {
                        retData.AccountDetailBasic = dataBasic;
                        if (openContract != null)
                        {
                            retData.AccountDetailBasic.CreditLimit = openContract.CreditLimit;
                            retData.AccountDetailBasic.DepositRate = openContract.DepositRate;
                            retData.AccountDetailBasic.DebtTerm = openContract.DebtTerm;
                        }
                        var dataRegService = await conn.QueryFirstOrDefaultAsync<GetAccountDetailRegisterServiceResult>(
                            _accountCommand.GetDetailRegisterServiceCust, new
                            {
                                @CustId = model.CustId,
                                @UserName = model.UserName
                            });
                        if (dataRegService != null)
                        {
                            retData.AccountDetailRegisterService = dataRegService;
                        }
                        
                        // nguon du lieu = 2 lay anh crm
                        if (dataBasic.SourceData == 2)
                        {
                            var dataImageCard = await conn.QueryFirstOrDefaultAsync<ImageDataCommondb>(
                                _accountCommand.GetImageCardId,
                                new
                                {
                                    @Id = model.CustId
                                });
                            if (dataImageCard != null)
                            {
                                var resizeFront = await Resize2Max50Kbytes(dataImageCard.FileDataFront);
                                var resizeBack = await Resize2Max50Kbytes(dataImageCard.FileDataBack);

                                if (resizeFront != null)
                                    retData.AccountDetailBasic.ImageFront = Convert.ToBase64String(resizeFront);
                                if (resizeBack != null)
                                    retData.AccountDetailBasic.ImageBack = Convert.ToBase64String(resizeBack);
                            }
                            // nguon du lieu ekyc = 4 lay anh website
                        }else if (dataBasic.SourceData == 4)
                        {
                            var url = ConfigurationManager.AppSettings["TVSI_WEBSITE"];
                           var urlFront =  url + "EkycCustomer/" + dataBasic.CrmId + "/" + dataBasic.CrmId + "_Front.jpg";
                           var urlBack =  url + "EkycCustomer/" + dataBasic.CrmId + "/" + dataBasic.CrmId + "_Back.jpg";
                            var imageFrontByte = AccountUtis.GetImageUrl(urlFront);
                            var imageBackByte = AccountUtis.GetImageUrl(urlBack);
                            var resizeFront = await Resize2Max50Kbytes(imageFrontByte);
                            var resizeBack = await Resize2Max50Kbytes(imageBackByte);
                            if (imageFrontByte != null)
                                retData.AccountDetailBasic.ImageFront = Convert.ToBase64String(resizeFront);
                            if (imageBackByte != null)
                                retData.AccountDetailBasic.ImageBack =  Convert.ToBase64String(resizeBack);
                        }
                        // nguon du lieu = 6 lay anh ekyc mobile
                        else if (dataBasic.SourceData == 6)
                        {
                          var data =  await GetImageCardEKYC(dataBasic.CardId);
                          if (data != null)
                          {
                              if (data.RetData.Front_Image != null)
                                  retData.AccountDetailBasic.ImageFront = data.RetData.Front_Image.Replace("data:image/jpg;base64,","");
                              
                              if (data.RetData.Back_Image != null)
                                  retData.AccountDetailBasic.ImageBack = data.RetData.Back_Image.Replace("data:image/jpg;base64,","");
                          }
                        }
                        var dataExtend = await WithCrmDbConnection(async crm =>
                            await crm.QueryAsync<ExtendCategories>(_accountCommand.GetCategoryCustExtend, new
                            {
                                CustCode = dataBasic.CustCode,
                            }));
                        //extend custcode
                        foreach (var extend in dataExtend)
                        {
                            if (extend.CategoryType == 1)
                            {
                                retData.AccountDetailExtend.OccupationStr
                                    = extend.CategoryName;
                                retData.AccountDetailExtend.Occupation 
                                    = extend.Category;
                            }

                            if (extend.CategoryType == 2)
                            {
                                retData.AccountDetailExtend.FinancialResourcesStr
                                    = extend.CategoryName;
                                
                                retData.AccountDetailExtend.FinancialResources
                                    = extend.Category;
                            }

                            if (extend.CategoryType == 3)
                            {
                                retData.AccountDetailExtend.KnowledExpStr
                                    = extend.CategoryName;
                                
                                retData.AccountDetailExtend.KnowledExpID
                                    = extend.Category;
                            } 
                            if (extend.CategoryType == 4)
                            {
                                retData.AccountDetailExtend.OpinionInvestStr
                                    = extend.CategoryName;
                                
                                retData.AccountDetailExtend.OpinionInvestID
                                    = extend.Category;
                            }
                            if (extend.CategoryType == 5)
                            {
                                retData.AccountDetailExtend.SourceStr
                                    = extend.CategoryName;
                                
                                retData.AccountDetailExtend.SourceID
                                    = extend.Category;
                            }

                            if (extend.CategoryType == 6)
                            {
                                retData.AccountDetailExtend.WorkplaceStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.Workplace =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 7)
                            {
                                retData.AccountDetailExtend.JoinedBondSinceStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.JoinedBondSince =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 8)
                            {
                                retData.AccountDetailExtend.InvestmentExperienceStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.InvestmentExperience =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 9)
                            {
                                retData.AccountDetailExtend.RiskAcceptanceStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.RiskAcceptance =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 10)
                            {
                                retData.AccountDetailExtend.HobbyInvestmentStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.HobbyInvestment =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 11)
                            {
                                retData.AccountDetailExtend.NoteTalkingStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.NoteTalking =
                                    extend.Category;
                            }

                            if (extend.CategoryType == 12)
                            {
                                retData.AccountDetailExtend.RelationshipSaleStr =
                                    extend.CategoryName;
                                retData.AccountDetailExtend.RelationshipSale =
                                    extend.Category;
                            }
                        }
                        return retData;
                    }
                    return null;
                });
            }
            catch (Exception ex)
            {
                log.Error("OP_CA_GetAccountDetailBySale:OP_CA_GetAccountDetailBySale=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> OP_DE_DeleteOpenAccount(GetAccountDetailBySaleRequest model)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var parameter = new DynamicParameters();
                            parameter.Add("@Id", model.CustId);
                            parameter.Add("@UserName", model.UserName);
                            parameter.Add("@statusAccount", ENumCommon.OpenAccountStatus.HUY_999);
                            parameter.Add("@StatusProfile", ENumCommon.OpenAccountStatus.NVSS_TU_CHOI_199);
                            parameter.Add("@UserNameUpdate", model.UserName);
                            var query = await conn.ExecuteAsync(_accountCommand.StoredUpdateStatusAccount, parameter,
                                commandType: CommandType.StoredProcedure, transaction: trans);
                            trans.Commit();
                            return query;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("OP_DE_DeleteOpenAccount:OP_DE_DeleteOpenAccount=" + model + ":" +
                                      ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                }) > 0;
            }
            catch (Exception ex)
            {
                log.Error("OP_DE_DeleteOpenAccount:OP_DE_DeleteOpenAccount=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<GetImageResult> GetImageCardEKYC(string cardID)
        {
            try
            {
                var url = ConfigurationManager.AppSettings["TVSI_API_EKYC"];
                var hostAPI = url + "api/Ekyc/CI_CA_GetImageByCardID?src=W&lang=VI";
                log.Error(string.Format(" URL API - {0}",hostAPI));
                if (!string.IsNullOrEmpty(cardID))
                {
                    using (var stringContent = new StringContent("{ \"CardId\": \"" + cardID + "\" }",
                       Encoding.UTF8, "application/json"))
                    {
                        using (var client = new HttpClient())
                        {
                            try
                            {
                                var response = await client.PostAsync(hostAPI, stringContent);
                                var data = await response.Content.ReadAsStringAsync();
                                var result = JsonConvert.DeserializeObject<GetImageResult>(data);
                                if (result.RetCode.Equals("000"))
                                    return result;
                                return null;
                            }
                            catch (Exception ex)
                            {
                                log.Error("GetImageCardID-> call api:  "+
                                          ex.Message);
                                log.Error(ex.InnerException);
                                throw;
                            }
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetImageCardID: "+
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetCardIssue>> OP_GE_GetListCardIssue(BaseRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                    await conn.QueryAsync<GetCardIssue>(_accountCommand.GetCardIssue));
            }
            catch (Exception ex)
            {
                log.Error("OP_GE_GetListCardIssue:OP_GE_GetListCardIssue=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> InsertOpenAccDocument(CreateAccountRequest model)
        {
            try
            {
                var addMargin = model.OpenContact.ToString().Contains("1") ? AdditionContractConst.Margin : (int?) null;
                var addDerivative = model.OpenContact.ToString().Contains("2") ? AdditionContractConst.Derivative : (int?) null;
                var addService = addMargin + addDerivative.ToString();
                var description = "HĐ thường";
                if (addService.Contains(AdditionContractConst.Margin.ToString()))
                    description += ", Margin";

                if (addService.Contains(AdditionContractConst.Derivative.ToString()))
                    description += ", Phái sinh";

                var branchID = OpenAccConst.BRANCH_ID_DEFAULT;
                var branchName = OpenAccConst.BRANCH_NAME_DEFAULT;

                var branchInfo = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<ManagementCodeList>(_accountCommand.GetManagementCode, new
                    {
                        @saleID = model.SaleID.Substring(0, 4)
                    }));
                if (branchInfo != null)
                {
                    branchID = branchInfo.CodeBranch;
                    branchName = branchInfo.NameDepartment;
                }

                var statusService = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var transferType = string.Empty;
                var statusServiceBank01 = (int)ServiceStatusConst.KO_DANG_KY;
                var statusServiceBank02 = (int)ServiceStatusConst.KO_DANG_KY;
                var statusServiceBank03 = (int)ServiceStatusConst.KO_DANG_KY;
                var statusServiceBank04 = (int)ServiceStatusConst.KO_DANG_KY;
                for (int i = 0; i < model.BankAccountList.Count(); i++)
                {
                    if (i == 0)
                        statusServiceBank01 = (int)ServiceStatusConst.CHO_XU_LY;
                    else if (i == 1)
                        statusServiceBank02 = (int)ServiceStatusConst.CHO_XU_LY;
                    else if (i == 2)
                        statusServiceBank03 = (int)ServiceStatusConst.CHO_XU_LY;
                    else if (i == 3)
                        statusServiceBank04 = (int)ServiceStatusConst.CHO_XU_LY;

                }

                if (model.BankAccountList.FirstOrDefault(bank => !string.IsNullOrEmpty(bank.BankAccName)) != null)
                {
                    statusService = ServiceStatusConst.CHO_XU_LY.GetHashCode();
                    transferType += TransferMoneyConst.Account.GetHashCode();
                }

                var statusReciprocal_01 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var statusReciprocal_02 = ServiceStatusConst.KO_DANG_KY.GetHashCode();

                var statusServiceChannelInfo = (int) ServiceStatusConst.KO_DANG_KY;
                if (model.RegistChannelInfo)
                {
                    statusServiceChannelInfo = (int) ServiceStatusConst.CHO_XU_LY;
                    statusService = (int) ServiceStatusConst.CHO_XU_LY;
                }

                var statusProfile = DocumentStatusConst.TAO_MOI_000;
                var statusAccount = ENumCommon.OpenAccountStatus.CHO_XU_LY_000;
                if (model.StatusContact != 1)
                {
                    statusProfile = DocumentStatusConst.NO_HO_SO_101;
                    statusAccount = ENumCommon.OpenAccountStatus.BU_MAN_XU_LY_001;
                }
                else
                {
                    statusAccount = ENumCommon.OpenAccountStatus.CHO_XU_LY_000;
                }
                
                var insertExtend = await InsertExtendCustCreateAccount(model);
                if (insertExtend)
                    return await StoredInsertAccountCommond(model, addService, transferType, description,
                    statusAccount, statusProfile, statusService, statusServiceBank01, statusServiceBank02,statusServiceBank03,
                    statusServiceBank04, statusReciprocal_01, statusReciprocal_02
                    , statusServiceChannelInfo, branchID, branchName);
                

                return false;
            }
            catch (Exception ex)
            {
                log.Error("InsertOpenAccDocument:InsertOpenAccDocument=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetBankName(string bankNo)
        {
            try
            {
                return await WithInnoDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_accountCommand.GetBankName, new
                    {
                        @bankNo = bankNo
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankName:GetBankName=" + bankNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetSubBranchName(string bankNo, string branchNo)
        {
            try
            {
                return await WithInnoDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<string>(_accountCommand.GetSubBranchName, new
                    {
                        @bankNo = bankNo,
                        @branchNo = branchNo
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetSubBranchName:GetSubBranchName=" + bankNo + " - " + branchNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetProvinceName(string provinceId)
        {
            try
            {
                return await WithInnoDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstAsync<string>(_accountCommand.GetProvinceName, new
                    {
                        provinceID = provinceId
                    });
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetProvinceName:GetProvinceName=" + provinceId + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<BankInfo>> GetListBank()
        {
            try
            {
                var data = await WithInnoDBConnection(async conn =>
                {
                    return await conn.QueryAsync<BankInfo>(_accountCommand.GetBankList, new { });
                });
                return data.ToList();
            }
            catch (Exception ex)
            {
                log.Error("GetListBank" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<SubBranchInfo>> GetListBranch(BranchRequest model)
        {
            try
            {
                var data = await WithInnoDBConnection(async conn =>
                {
                    return await conn.QueryAsync<SubBranchInfo>(_accountCommand.GetSubBranchList, new { bankNo = model.BankNo });
                });
                return data.ToList();
            }
            catch (Exception ex)
            {
                log.Error("GetListBranch" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<List<ProvinceInfo>> GetListProvince()
        {
            try
            {
                var data = await WithInnoDBConnection(async conn =>
                {
                    return await conn.QueryAsync<ProvinceInfo>(_accountCommand.GetProvinceList, new { });
                });
                return data.ToList();
            }
            catch (Exception ex)
            {
                log.Error("GetListProvince" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetAccountStatusResult>> GetAccountStatus()
        {
            try
            {
                var data = new List<GetAccountStatusResult>();
                
                data.Add(new GetAccountStatusResult() {
                    Name = "Chờ xử lý",
                    Value = "000"
                });  
                /*data.Add(new GetAccountStatusResult() {
                    Name = "Chờ BU_MAN duyệt",
                    Value = "001"
                });  */
                /*data.Add(new GetAccountStatusResult() {
                    Name = "Chờ FS duyệt",
                    Value = "002"
                });  
                data.Add(new GetAccountStatusResult() {
                    Name = "BU_MAN từ chối",
                    Value = "099"
                });*/
                data.Add(new GetAccountStatusResult() {
                    Name = "Đang xử lý",
                    Value = "101"
                });  
                data.Add(new GetAccountStatusResult() {
                    Name = "Kích hoạt",
                    Value = "102"
                }); 
                data.Add(new GetAccountStatusResult() {
                    Name = "Hoàn thành",
                    Value = "200"
                });
                /*data.Add(new GetAccountStatusResult() {
                    Name = "FS từ chối",
                    Value = "399"
                });*/
                data.Add(new GetAccountStatusResult() {
                    Name = "Từ chối",
                    Value = "199"
                });
                /*data.Add(new GetAccountStatusResult() {
                    Name = "Từ chối",
                    Value = "299"
                }); */
                data.Add(new GetAccountStatusResult() {
                    Name = "Đã hủy",
                    Value = "999"
                });
                return data;
            }
            catch (Exception ex)
            {
                log.Error("GetAccountStatus" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetAccountStatusResult>> ProfileStatus()
        {
            try
            {
                var data = new List<GetAccountStatusResult>();
                data.Add(new GetAccountStatusResult() {
                    Name = "Chờ xử lý",
                    Value = "000"
                }); 
                data.Add(new GetAccountStatusResult() {
                    Name = "Chờ duyệt",
                    Value = "100"
                }); 
                data.Add(new GetAccountStatusResult() {
                    Name = "Nợ hồ sơ",
                    Value = "101"
                });
                data.Add(new GetAccountStatusResult() {
                    Name = "Không hồ sơ",
                    Value = "102"
                });
                data.Add(new GetAccountStatusResult() {
                    Name = "Hoàn thành",
                    Value = "200"
                }); 
                data.Add(new GetAccountStatusResult() {
                    Name = "Từ chối",
                    Value = "199"
                });
                return data;
            }
            catch (Exception ex)
            {
                log.Error("GetAccountStatusProfileStatus" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> InsertCalllog(CalllogRequest model)
        {
            try
            {
                var customerName = "";
                if (!string.IsNullOrEmpty(model.CustCode))
                {
                    var sql = "  SELECT CUSTOMERNAME FROM CUSTOMER WHERE CUSTOMERID = @CustCode";
                    customerName = await WithInnoDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                        sql, new
                        {
                            CustCode = model.CustCode
                        }));
                }
                return await WithCommonDBConnection(async conn =>
                {
                    var parameters = new DynamicParameters();
                    parameters.Add("@CalllogID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                    parameters.Add("@CallID", model.CallId);
                    parameters.Add("@CustomerID", model.CustCode);
                    parameters.Add("@CustomerName", customerName);
                    parameters.Add("@SaleID", model.SaleID);
                    parameters.Add("@Phone", model.Phone);
                    parameters.Add("@CreateBy", model.UserName);
                    var result = await conn.ExecuteAsync("TVSI_sINSERT_CALLLOG_TWORK", parameters,
                        commandType: CommandType.StoredProcedure);
                    return result > 0;
                });
            }
            catch (Exception ex)
            {
                log.Error("InsertCalllog erros - " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<ProvinceResult>> GetProvinces()
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var sql = "SELECT * FROM Province WHERE status = 1";
                    return await conn.QueryAsync<ProvinceResult>(sql);
                });
            }
            catch (Exception ex)
            {
                log.Error("GetProvinces erros - " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<DistrictResult>> GetDistricts(string provinceCode)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var sql = "SELECT * FROM District WHERE status = 1 And ProvinceCode = @ProvinceCode";
                    return await conn.QueryAsync<DistrictResult>(sql, new
                    {
                        ProvinceCode = provinceCode
                    });
                });            }
            catch (Exception ex)
            {
                log.Error("GetDistricts erros - " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<WardResult>> GetWards(string districtCode)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var sql = "SELECT * FROM Ward  WHERE status = 1 AND DistrictCode = @DistrictCode";
                    return await conn.QueryAsync<WardResult>(sql, new
                    {
                        DistrictCode = districtCode
                    });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetWards erros - " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        

        public async Task<string> GetIssuedByName(int id)
        {
            try
            {
                return await WithCrmDbConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _accountCommand.GetIssuedByName,
                    new
                    {
                        @id = id
                    }));
            }
            catch (Exception ex)
            {
                log.Error("GetIssuedByName:GetIssuedByName=" + id + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<SaleProfileModel> GE_FO_GetProfileUser(BaseRequest model)
        {
            try
            {
                var dataInfo = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<SaleProfileModel>(_accountCommand.GetUserProfile, new
                    {
                        @UserName = model.UserName
                    }));
                var pathImage = AccountUtis.GetImageAvatarUser(model.UserName);
                dataInfo.NameAvatar = pathImage;
                return dataInfo;
            }
            catch (Exception ex)
            {
                log.Error("GE_FO_GetProfileUser:GE_FO_GetProfileUser=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<bool> ChangeImageUser(ChangeImageUser model)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                log.Error("ChangeImageUser:ChangeImageUser=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<ManagementCodeList> GetManagementCode(string saleID)
        {
            try
            {
                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetManagementCode:GetManagementCode=" + saleID + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<AccountValidation>> AccountRegisterValidation(CreateAccountRequest model)
        {
            try
            {
                var retErrorList = new List<AccountValidation>();
                return retErrorList;
            }
            catch (Exception ex)
            {
                log.Error("AccountRegisterValidation:AccountRegisterValidation=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /*public async Task<string> ValidateCustCode(AccountNoRequest model)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    var uri = host + "api/Account/ValidateCustCode_02?custcode=" + model.AccountNo + "&custType=" +
                              null;
                    var response = httpClient.GetAsync(uri).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        return "-1";
                    }

                    return await response.Content.ReadAsStringAsync();
                }
            }
            catch (Exception ex)
            {
                log.Error("ValidateCustCode:ValidateCustCode=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }*/


        public async Task<string> GetCustCode(string accountNo)
        {
            try
            {
                var data = await WithCommonDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _accountCommand.GetCustCode, new
                    {
                        @CustCode = accountNo
                    }));
                return data;
            }
            catch (Exception ex)
            {
                log.Error("GetCustCode:GetCustCode=" + accountNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<string> Base64Decode(string base64EncodedData)
        {
            throw new NotImplementedException();
        }


        public async Task<bool> StoredInsertAccountCommond(CreateAccountRequest model, string addService,
            string transferType, string description, string statusAccount, string statusProfile, int statusService
            , int statusServiceBank01, int statusServiceBank02,int statusServiceBank03,int statusServiceBank04, int statusReciprocal_01, int statusReciprocal_02,
            int statusServiceChannelInfo
            , string branchID, string branchName)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    try
                    {
                        var sourceCRM = SourceInputTypeConst.CRM.GetHashCode();
                        var id = 0;
                        var dateBirth = DateTime.ParseExact(model.Birthday, "dd/MM/yyyy",
                            CultureInfo.CurrentCulture);
                        var issueDate = DateTime.ParseExact(model.IssueDate, "dd/MM/yyyy",
                            CultureInfo.CurrentCulture);
                        var powerofAttorneyDate = !string.IsNullOrEmpty(model.PowerofAttorneyDate)
                            ? DateTime.ParseExact(model.PowerofAttorneyDate, "dd/MM/yyyy",
                                CultureInfo.CurrentCulture)
                            : (DateTime?) null;
                        var tVSIIssueDate = !string.IsNullOrEmpty(model.TVSIIssueDate)
                            ? DateTime.ParseExact(model.TVSIIssueDate, "dd/MM/yyyy",
                                CultureInfo.CurrentCulture)
                            : (DateTime?) null;
                        var parameter = new DynamicParameters();
                        parameter.Add("@ma_khach_hang", model.CustCode);
                        parameter.Add("@so_tai_khoan", AccountCommonDb.Account044C + model.CustCode);
                        parameter.Add("@loai_ho_so", DocumentTypeConst.OPEN_DOCUMENT.GetHashCode());
                        parameter.Add("@loai_hinh_tai_khoan",
                            ENumCommon.CustTypeConst.INDIVIDUAL_DOMESTIC_INVEST.GetHashCode());
                        parameter.Add("@ho_ten_kh",
                            CultureInfo.CurrentCulture.TextInfo.ToTitleCase(model.FullName.ToLower()));
                        parameter.Add("@ngay_sinh", dateBirth);
                        parameter.Add("@gioi_tinh", model.Sex);
                        parameter.Add("@ma_quoc_tich", model.NationalityCode);
                        parameter.Add("@quoc_tich", model.NationalityName);
                        parameter.Add("@so_cmnd", model.CardId);
                        parameter.Add("@ngay_cap_cmnd", issueDate);
                        parameter.Add("@noi_cap_cmnd", model.CardIssuer);
                        parameter.Add("@dia_chi_lien_he", model.Address_01);
                        parameter.Add("@hop_dong_chinh", 0);
                        parameter.Add("@hop_dong_bo_sung", addService);
                        parameter.Add("@phuong_thuc_gd", model.TradingMethod);
                        parameter.Add("@phuong_thuc_nhan_kqgd", model.TradingResultMethod);
                        parameter.Add("@phuong_thuc_sao_ke", model.TradingReportMethod);
                        parameter.Add("@dv_chuyen_tien", transferType);
                        parameter.Add("@so_dien_thoai_01", model.Phone_01);
                        parameter.Add("@so_dien_thoai_02", model.Phone_02);
                        parameter.Add("@email", model.Email);
                        if (model.BankAccountList.Count() > 0)
                        {
                            for (int i = 0; i < model.BankAccountList.Count(); i++)
                            {
                                if (i < 4)
                                {
                                    parameter.Add("@chu_tai_khoan_nh_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].BankAccName);
                                    parameter.Add("@so_tai_khoan_nh_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].BankAccNo);
                                    parameter.Add("@ma_ngan_hang_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].BankNo);
                                    parameter.Add("@ngan_hang_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].BankName);
                                    parameter.Add("@chi_nhanh_nh_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].SubBranchName);
                                    parameter.Add("@ma_chi_nhanh_0" + (i + 1).ToString(),
                                        model.BankAccountList[i].SubBranchNo);
                                }
                            }
                        }

                        parameter.Add("@chu_tk_doi_ung_01", null);
                        parameter.Add("@so_tk_doi_ung_01", null);
                        parameter.Add("@chu_tk_doi_ung_02", null);
                        parameter.Add("@so_tk_doi_ung_02", null);
                        parameter.Add("@kenh_trao_doi", 1);
                        parameter.Add("@phuong_thuc_kenh_td", null);
                        parameter.Add("@trang_thai_tk", statusAccount);
                        parameter.Add("@trang_thai_ho_so", statusProfile);
                        parameter.Add("@trang_thai_dv", statusService);
                        parameter.Add("@trang_thai_dv_ngan_hang", statusServiceBank01);
                        parameter.Add("@trang_thai_dv_ngan_hang_02", statusServiceBank02);
                        parameter.Add("@trang_thai_dv_ngan_hang_03", statusServiceBank03);
                        parameter.Add("@trang_thai_dv_ngan_hang_04", statusServiceBank04);
                        parameter.Add("@trang_thai_dv_doi_ung", statusReciprocal_01);
                        parameter.Add("@trang_thai_dv_doi_ung_02", statusReciprocal_02);
                        parameter.Add("@trang_thai_dv_kenh_tt", statusServiceChannelInfo);
                        parameter.Add("@mo_ta", description);
                        parameter.Add("@sale_id", model.SaleID);
                        parameter.Add("@branch_id", branchID);
                        parameter.Add("@branch_name", branchName);
                        parameter.Add("@nguon_du_lieu", sourceCRM);
                        parameter.Add("@tvsi_noi_mo_tk", model.AddressOpenAccount);
                        parameter.Add("@tvsi_nguoi_dai_dien", model.RepresentTvsi);
                        parameter.Add("@tvsi_chuc_vu", model.RollTvsi);
                        parameter.Add("@tvsi_giay_uq", model.PowerofAttorneyNum);
                        parameter.Add("@tvsi_ngay_uq", powerofAttorneyDate);
                        parameter.Add("@tvsi_so_cmnd", model.TVSICardId);
                        parameter.Add("@tvsi_noi_cap_cmnd", model.TVSICardIssuer);
                        parameter.Add("@tvsi_ngay_cap_cmnd", tVSIIssueDate);
                            
                        parameter.Add("@fatca_kh_usa", model.FatcaInfo.FatcaKhUsa);
                        parameter.Add("@@fatca_sdt_usa", model.FatcaInfo.FatcaSdtUsa);
                        parameter.Add("@fatca_noi_sinh_usa", model.FatcaInfo.FatcaNoiSinhUsa);
                        parameter.Add("@fatca_noi_cu_tru_usa",model.FatcaInfo.FatcaNoiCuTruUsa);
                        parameter.Add("@fatca_lenh_ck_usa", model.FatcaInfo.FatcaLenhCkUsa);
                        parameter.Add("@fatca_giay_uy_quyen_usa",model.FatcaInfo.FatcaGiayUyQuyenUsa);
                        parameter.Add("@fatca_dia_chi_nhan_thu_usa",model.FatcaInfo.FatcaDiaChiNhanThuUsa);

                        parameter.Add("@nguoi_tao", model.UserName);
                        parameter.Add("@ngay_tao", DateTime.Now);
                        parameter.Add("@ho_so_mo_tk_id", dbType: DbType.Int32,
                            direction: ParameterDirection.Output);
                        var result = await conn.ExecuteAsync(_accountCommand.StoredInsertAccount, parameter,
                            commandType: CommandType.StoredProcedure);
                        id = parameter.Get<int>("@ho_so_mo_tk_id");
                        if (id > 0)
                        {
                            if (model.StatusContact == 2 && model.OpenContact == 1)
                            {
                                bool retSaveFs = await WithCommonDBConnection(async fsconn =>
                                {
                                    var param = new DynamicParameters();
                                    param.Add("@ho_so_mo_tk_id", id);
                                    param.Add("@han_muc_du_no",
                                        model.CreditLimit != null
                                            ? float.Parse(model.CreditLimit.Replace(",", ""))
                                            : 0);
                                    param.Add("@thoi_gian_ghi_no", int.Parse(model.DebtTerm));
                                    param.Add("@ty_le_ky_quy",
                                        model.DepositRate != null
                                            ? float.Parse(model.DepositRate.Replace(",", ""))
                                            : 0);
                                    param.Add("@fs_phe_duyet", "");
                                    param.Add("@hop_dong_no_id", dbType: DbType.Int32,
                                        direction: ParameterDirection.Output);
                                    var resultFs = await conn.ExecuteAsync(_accountCommand.StoredInsertContactFS,
                                        param, commandType: CommandType.StoredProcedure);
                                    return resultFs > 0;
                                });
                            }

                            if (model.FolderPath != null)
                            {
                                if (model.FolderPath.ImageFront != null && model.FolderPath.ImageBack != null)
                                {
                                    var frontBinary = Convert.FromBase64String(model.FolderPath.ImageFront);
                                    var backBinary = Convert.FromBase64String(model.FolderPath.ImageBack);
                                        
                                    var paramFile = new DynamicParameters();
                                    paramFile.Add("@ho_so_mo_tk_id",id);
                                    paramFile.Add("@file_name_front",id +".jpg");
                                    paramFile.Add("@file_name_back",id + "_ms.jpg");
                                    paramFile.Add("@file_front",frontBinary);
                                    paramFile.Add("@file_back",backBinary);
                                    paramFile.Add("@ma_khach_hang",model.CustCode);
                                    paramFile.Add("@nguoi_tao",model.UserName);
                                    await conn.ExecuteAsync("TVSI_sDANG_KY_MO_TAI_KHOAN_FILES_INSERT_IWORK", paramFile,
                                        commandType: CommandType.StoredProcedure);
                                }
                            }
                        }
                        return result;
                    }
                    catch (Exception ex)
                    {
                        log.Error("InsertOpenAccDocument:InsertOpenAccDocument=" + model + ":" +
                                  ex.Message);
                        log.Error(ex.InnerException);
                        throw;
                    }
                }) > 0;
            }
            catch (Exception ex)
            {
                log.Error("StoredInsertAccountCommond:StoredInsertAccountCommond=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> ValidateAccount(string accountNo, int custType)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var custInfo = await conn.QueryFirstOrDefaultAsync<TvsiTaiKhoanKhachHang>(
                        _accountCommand.GetCustInfo, new
                        {
                            ma_khach_hang = accountNo
                        });
                    if (custInfo != null)
                    {
                        if (custInfo.loai_khach_hang != custType)
                            return (int) ENumCommon.ValidateAccountConst.ACCOUNT_EXIST_DIF_CUSTYPE;
                    }

                    var existOpenContractInfo =
                        await conn.QueryFirstOrDefaultAsync<TvsiDangKyMoTk>(_accountCommand.GetCustInfoRegister,
                            new {ma_khach_hang = accountNo});
                    if (existOpenContractInfo != null && existOpenContractInfo.trang_thai_tk !=
                                                      ENumCommon.OpenAccountStatus.NVSS_TU_CHOI_199
                                                      && existOpenContractInfo.trang_thai_tk !=
                                                      ENumCommon.OpenAccountStatus.KSVSS_TU_CHOI_299)
                    {
                        return (int) ENumCommon.ValidateAccountConst.OPEN_CONTRACT_EXSIT;
                    }

                    return 0;
                });
            }
            catch (Exception ex)
            {
                log.Error("ValidateAccount:ValidateAccount=" + accountNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> ValidateCustCode(string accountNo)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var sql = @"select ma_khach_hang from
	                        (select ma_khach_hang from TVSI_TAI_KHOAN_KHACH_HANG
		                        union
	                         select ma_khach_hang from TVSI_TAI_KHOAN_PHONG_TOA
                                union
                             select CustCode as ma_khach_hang from TVSI_REVERT_ACCOUNT) a
	                         where ma_khach_hang = @ma_khach_hang";
                    var data = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        ma_khach_hang = accountNo
                    });
                    if (!string.IsNullOrEmpty(data))
                    {
                        sql = @"select so_tai_khoan from TVSI_TAI_KHOAN_GIAO_DICH
                            where left(so_tai_khoan, 6) = @ma_khach_hang and trang_thai = 1";
                        var accountList = await conn.QueryAsync<string>(sql, new { ma_khach_Hang = accountNo });
                        if (accountList.Count() > 0)
                        {
                            if (accountList.Contains(accountNo + "6") && accountList.Contains(accountNo + "8"))
                            {
                                // Truong hop da mo ca hop dong Margin + Phai Sinh
                                return ((int)ENumCommon.ValidateAccountConst.ACCOUNT_EXIST_MARGIN_AND_DERIVATIVE);
                            }
                            if (accountList.Contains(accountNo + "6"))
                            {
                                // Truong hop da mo hop dong Margin
                                return (int)ENumCommon.ValidateAccountConst.ACCOUNT_EXIST_MARGIN;
                            }
                            if (accountList.Contains(accountNo + "8"))
                            {
                                // Truong hop da mo hop dong phai sinh
                                return (int) ENumCommon.ValidateAccountConst.ACCOUNT_EXIST_DERIVATIVE;
                            }

                            // Truong hop moi mo TK duoi 1
                            if (accountList.Contains(accountNo + "1"))
                                return (int)ENumCommon.ValidateAccountConst.ACCOUNT_EXIST_NOT_MARGIN;
                        }
                        else
                        {
                            return (int) ENumCommon.ValidateAccountConst.CUSTOMER_EXIST_BUT_ACCOUNT_NOT_EXIST;
                        }

                    }
                    return (int) ENumCommon.ValidateAccountConst.ACCOUNT_NOT_EXIST;
                });
            }
            catch (Exception ex)
            {
                log.Error("ValidateCustCode:ValidateCustCode=" + accountNo + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<AccountInfoReponse> GetAccountInfo(DetailAccountRequest model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAccountInfo(AccountInfoReponse model)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAccountCommond(AccountInfoReponse model, string addService, string transferType,
            string description,
            string statusAccount, string statusProfile, int statusService, int statusServiceBank01,
            int statusServiceBank02,
            int statusReciprocal_01, int statusReciprocal_02, int statusServiceChannelInfo, string branchID,
            string branchName)
        {
            throw new NotImplementedException();
        }
        

        public async Task<bool> CheckAccountPermission(int level,string saleID )
        {
            try
            {

                return false;
            }
            catch (Exception ex)
            {
                log.Error("CheckAccountPermission:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Lấy danh sách tài khoản thường mở từ website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AccWebListResult>> GetListAccWeb(AccWebListRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<AccWebListResult>();
                    var sql = _accountCommand.GetListAccWeb;
                    sql = sql.Replace("{cond1}", !string.IsNullOrEmpty(model.Status) ? " and a.trang_thai_tk = @Status" : "");
                    sql = sql.Replace("{cond2}", !string.IsNullOrEmpty(model.keySearch) ? " and (a.ho_ten like  @keySearch  or a.so_dien_thoai_01 like @keySearch)" : "");
                    
                    var result = 
                        await conn.QueryAsync<TVSI_DANG_KY_MO_TAI_KHOAN>(sql,
                        new {
                            Start = start,
                            PageSize = model.PageSize,
                            Status = model.Status, 
                            keySearch = "%" + model.keySearch + "%", 
                            SaleId = model.UserName });
                    var sqlMap = "SELECT ma_khach_hang, trang_thai_tk, trang_thai_ho_so,so_cmnd FROM TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd in @cardIDs";
                    var listCard = await WithCommonDBConnection(async con => await con.QueryAsync<TVSI_DANG_KY_MO_TAI_KHOAN>(
                        sqlMap, new
                        {
                            cardIDs = result.Select( x => x.so_cmnd)
                        }));

                    foreach (var data in result)
                    {
                        var statuseKYCStr = string.Empty;
                        if (data.ConfirmSms != null)
                        {
                            if (data.ConfirmStatus != 0)
                            {
                                statuseKYCStr = "Đã kích hoạt";
                            }
                            else
                            {
                                statuseKYCStr = "Chưa kích hoạt";
                            }
                        }
                        
                        var _custResult = new AccWebListResult
                        {
                            CreateDate = data.ngay_tao.ToString("dd/MM/yyyy"),
                            ProfileId = data.dang_kyid,
                            ProfileType = data.loai_hinh_tk,
                            CustName = data.ho_ten,
                            Mobile = data.so_dien_thoai_01,
                            Status = data.trang_thai_tk,
                            AssignUser = data.sale_id,
                            CreateBy = data.sale_id,
                            UpdateBy = data.nguoi_cap_nhat,
                            Description = data.noi_dung_xu_ly,
                            HandledBy = data.sale_assign,
                            ConfirmSms = data.ConfirmSms,
                            CustomerType = !string.IsNullOrEmpty(data.ConfirmSms) ? "KH eKYC" : " ",
                            StatusEKYC = data.ConfirmStatus,
                            StatusEKYCStr = statuseKYCStr,
                            StatusAction = data.trang_thai_tt,
                            StatusProfile = data.trang_thai_ho_so,
                            CustCode = data.ma_khach_hang,
                            CardID = data.so_cmnd,
                            SourceAccount = data.nguon_du_lieu,
                            Email = data.email
                        };
                        if (listCard.Select(x => x.so_cmnd) != null)
                        {
                            var dataInfo = listCard.FirstOrDefault(x => x.so_cmnd.Equals(data.so_cmnd));
                            if (dataInfo != null)
                            {
                                _custResult.Status = dataInfo.trang_thai_tk;
                                _custResult.CustCode = dataInfo.ma_khach_hang;
                                _custResult.StatusProfile = dataInfo.trang_thai_ho_so;
                            }
                        }
                        resultData.Add(_custResult);
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListAccWeb:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> PushAccountWebsite(AccountDetailRequest model)
        {
            try
            {
                var id = 0;
                var additionMargin = model.OpenContact == 1 ? AdditionContractConst.Margin.ToString() : "";
                var additionDerivative = model.OpenContact == 2 ? AdditionContractConst.Derivative.ToString() : "";
                var additionService = additionMargin + additionDerivative;
                
                var description = "HĐ thường";
                if (model.OpenContact == 1)
                {
                    if (additionService.Contains(AdditionContractConst.Margin.ToString()))
                        description += ", Margin";
                }
                else if (model.OpenContact == 2)
                {
                    if (additionService.Contains(AdditionContractConst.Derivative.ToString()))
                        description += ", Phái sinh";
                }



                var branchID = OpenAccConst.BRANCH_ID_DEFAULT;
                var branchName = OpenAccConst.BRANCH_NAME_DEFAULT;
                
                var sql = "select ma_chi_nhanh,ten_bo_phan from [dbo].[TVSI_DANH_MUC_MA_QUAN_LY] where ma_quan_ly = @SaleID";
                var branchInfo = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<CodeManager>(sql, new
                    {
                        @SaleID = model.SaleID
                    }));
                if (branchInfo != null)
                {
                    branchID = branchInfo.ma_chi_nhanh; 
                    branchName = branchInfo.ten_bo_phan;
                }
                var trang_thai_dv = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var transferType = "";

                var trang_thai_dv_ngan_hang_01 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var trang_thai_dv_ngan_hang_02 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var trang_thai_dv_ngan_hang_03 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var trang_thai_dv_ngan_hang_04 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                if (AccountUtis.IsRegistBankTransferService(model))
                {
                    trang_thai_dv_ngan_hang_01 = AccountUtis.BankServiceStatus(model, 1);
                    trang_thai_dv_ngan_hang_02 = AccountUtis.BankServiceStatus(model, 2);
                    trang_thai_dv_ngan_hang_03 = AccountUtis.BankServiceStatus(model, 3);
                    trang_thai_dv_ngan_hang_04 = AccountUtis.BankServiceStatus(model, 4);
                    trang_thai_dv = ServiceStatusConst.CHO_XU_LY.GetHashCode();
                    transferType += TransferMoneyConst.Bank.GetHashCode();
                }
                
                var trang_thai_dv_doi_ung_01 = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var trang_thai_dv_doi_ung_02 = ServiceStatusConst.KO_DANG_KY.GetHashCode();

                if (AccountUtis.IsRegistAccountTransferService(model))
                {
                    trang_thai_dv_doi_ung_01 = AccountUtis.AccountTransferService(model, 1);
                    trang_thai_dv_doi_ung_02 =  AccountUtis.AccountTransferService(model, 2);
                    trang_thai_dv = ServiceStatusConst.CHO_XU_LY.GetHashCode();
                    transferType += TransferMoneyConst.Account.GetHashCode();
                }
                var trang_thai_dv_kenh_tt = ServiceStatusConst.KO_DANG_KY.GetHashCode();
                var trang_thai_ho_so = DocumentStatusConst.TAO_MOI_000;
                var trang_thai_tai_khoan = string.Empty;
                if (model.StatusContact != 1)
                {
                    trang_thai_ho_so = DocumentStatusConst.NO_HO_SO_101;                 
                    trang_thai_tai_khoan = ENumCommon.OpenAccountStatus.BU_MAN_XU_LY_001;
                }
                else
                {
                    trang_thai_tai_khoan = ENumCommon.OpenAccountStatus.CHO_XU_LY_000;
                }
                
                var dateBirth = DateTime.ParseExact(model.Birthday, "dd/MM/yyyy",
                    CultureInfo.CurrentCulture);
                var issueDate = DateTime.ParseExact(model.IssueDate, "dd/MM/yyyy",
                    CultureInfo.CurrentCulture);
                var saveaccount =  await WithCommonDBConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var param = new DynamicParameters();
                            param.Add("@ma_khach_hang", model.CustCode);
                            param.Add("@so_tai_khoan", AccountCommonDb.Account044C + model.CustCode);
                            param.Add("@loai_ho_so", (int) DocumentTypeConst.OPEN_DOCUMENT);
                            param.Add("@loai_hinh_tai_khoan",
                                (int) ENumCommon.CustTypeConst.INDIVIDUAL_DOMESTIC_INVEST);
                            param.Add("@ho_ten_kh",
                                CultureInfo.CurrentCulture.TextInfo.ToTitleCase(model.FullName.ToLower()));
                            param.Add("@ngay_sinh", dateBirth);
                            param.Add("@gioi_tinh", model.Sex);
                            param.Add("@ma_quoc_tich", model.NationalityCode);
                            param.Add("@quoc_tich", model.NationalityName);
                            param.Add("@so_cmnd", model.CardId);
                            param.Add("@ngay_cap_cmnd", issueDate);
                            param.Add("@noi_cap_cmnd", model.CardIssuer);
                            param.Add("@dia_chi_lien_he", model.Address_01);
                            param.Add("@hop_dong_chinh", 0);
                            param.Add("@hop_dong_bo_sung", additionService);
                            param.Add("@phuong_thuc_gd", model.TradingMethod);
                            param.Add("@phuong_thuc_nhan_kqgd", model.TradingResultMethod);
                            param.Add("@phuong_thuc_sao_ke", model.TradingReportMethod);
                            param.Add("@dv_chuyen_tien", transferType);
                            param.Add("@so_dien_thoai_01", model.Phone_01);
                            param.Add("@so_dien_thoai_02", model.Phone_02);
                            param.Add("@email", model.Email);
                            if (model.BankAccountList.Count() > 0)
                            {
                                for (int i = 0; i < model.BankAccountList.Count(); i++)
                                {
                                    if (i < 4)
                                    {
                                        param.Add("@chu_tai_khoan_nh_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].BankName);
                                        param.Add("@so_tai_khoan_nh_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].BankAccName);
                                        param.Add("@ma_ngan_hang_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].BankNo);
                                        param.Add("@ngan_hang_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].BankName);
                                        param.Add("@chi_nhanh_nh_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].SubBranchName);
                                        param.Add("@ma_chi_nhanh_0" + (i + 1).ToString(),
                                            model.BankAccountList[i].SubBranchNo);
                                    }
                                }
                            }

                            param.Add("@chu_tk_doi_ung_01", model.AccountContraName_01);
                            param.Add("@so_tk_doi_ung_01", model.AccountContraNo_01);
                            param.Add("@chu_tk_doi_ung_02", model.AccountContraName_02);
                            param.Add("@so_tk_doi_ung_02", model.AccountContraNo_02);
                            param.Add("@kenh_trao_doi", 1);
                            param.Add("@phuong_thuc_kenh_td", null);
                            param.Add("@trang_thai_tk", trang_thai_tai_khoan);
                            param.Add("@trang_thai_ho_so", trang_thai_ho_so);
                            param.Add("@trang_thai_dv", trang_thai_dv);
                            param.Add("@trang_thai_dv_ngan_hang", trang_thai_dv_ngan_hang_01);
                            param.Add("@trang_thai_dv_ngan_hang_02", trang_thai_dv_ngan_hang_02);
                            param.Add("@@trang_thai_dv_ngan_hang_03", trang_thai_dv_ngan_hang_03);
                            param.Add("@@trang_thai_dv_ngan_hang_04", trang_thai_dv_ngan_hang_04);
                            param.Add("@trang_thai_dv_doi_ung", trang_thai_dv_doi_ung_01);
                            param.Add("@trang_thai_dv_doi_ung_02", trang_thai_dv_doi_ung_02);
                            param.Add("@trang_thai_dv_kenh_tt", trang_thai_dv_kenh_tt);
                            param.Add("@mo_ta", description);
                            param.Add("@sale_id", model.SaleID);
                            param.Add("@branch_id", branchID);
                            param.Add("@branch_name", branchName);
                            param.Add("@nguon_du_lieu", SourceInputTypeConst.CRM.GetHashCode());
                            param.Add("@tvsi_noi_mo_tk", model.AddressOpenAccount);
                            param.Add("@tvsi_nguoi_dai_dien", model.TvsiManName);
                            param.Add("@tvsi_chuc_vu", model.TvsiManPosition);
                            param.Add("@tvsi_giay_uq", model.TvsiManNo);
                            param.Add("@tvsi_ngay_uq",
                                !string.IsNullOrEmpty(model.TvsiManDate)
                                    ? DateTime.ParseExact(model.TvsiManDate, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                                    : (DateTime?) null);
                            param.Add("@tvsi_so_cmnd", model.TvsiManCardId);
                            param.Add("@tvsi_noi_cap_cmnd", model.TvsiManCardIssue);
                            param.Add("@tvsi_ngay_cap_cmnd", !string.IsNullOrEmpty(model.TvsiManCardDate)
                                ? DateTime.ParseExact(model.TvsiManCardDate, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                                : (DateTime?) null);
                            param.Add("@fatca_kh_usa", model.FatcaKhUsa);
                            param.Add("@fatca_noi_sinh_usa",model.FatcaNoiSinhUsa);     
                            param.Add("@fatca_sdt_usa",model.FatcaSdtUsa);
                            param.Add("@fatca_noi_cu_tru_usa",model.FatcaNoiCuTruUsa);
                            param.Add("@fatca_lenh_ck_usa",model.FatcaLenhCkUsa);
                            param.Add("@fatca_giay_uy_quyen_usa",model.FatcaGiayUyQuyenUsa);
                            param.Add("@fatca_dia_chi_nhan_thu_usa",model.FatcaDiaChiNhanThuUsa);
                            param.Add("@nguoi_tao", model.UserName);
                            param.Add("@ngay_tao", DateTime.Now);
                            param.Add("@ho_so_mo_tk_id", dbType: DbType.Int32,
                                direction: ParameterDirection.Output);
                            var result = await conn.ExecuteAsync(_accountCommand.StoredInsertAccount, param,
                                commandType: CommandType.StoredProcedure,
                                transaction: trans);
                            id = param.Get<int>("@ho_so_mo_tk_id");
                            trans.Commit();
                            if (model.FolderPath != null)
                            {
                                if (model.FolderPath.ImageFront != null && model.FolderPath.ImageBack != null)
                                {
                                    var frontBinary = Convert.FromBase64String(model.FolderPath.ImageFront);
                                    var backBinary = Convert.FromBase64String(model.FolderPath.ImageBack);
                                        
                                    var paramFile = new DynamicParameters();
                                    paramFile.Add("@ho_so_mo_tk_id",id);
                                    paramFile.Add("@file_name_front",id +".jpg");
                                    paramFile.Add("@file_name_back",id + "_ms.jpg");
                                    paramFile.Add("@file_front",frontBinary);
                                    paramFile.Add("@file_back",backBinary);
                                    paramFile.Add("@ma_khach_hang",model.CustCode);
                                    paramFile.Add("@nguoi_tao",model.UserName);
                                    await conn.ExecuteAsync("TVSI_sDANG_KY_MO_TAI_KHOAN_FILES_INSERT_IWORK", paramFile,
                                        commandType: CommandType.StoredProcedure);
                                }
                            }

                            return id > 0;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("PushAccountWebsite:" + ex.Message);
                            throw;
                        }
                    }


                });
                var saveDateExtend = await InsertExtendCust(model);
                return saveaccount;
            }
            catch (Exception ex)
            {
                log.Error("PushAccountWebsite:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        

        public async Task<bool> InsertExtendCust(AccountDetailRequest model)
        {
            try
            {
                var id = 0;
                var strExend = string.Empty;

                if (model.UserRole.Equals("CRM_SALE") || model.UserRole.Equals("CRM_BB"))
                {
                    strExend = model.InfoExtend.Occupation + "|" 
                                                            + model.InfoExtend.Workplace + "|"
                               + model.InfoExtend.FinancialResources + 
                                                            "|" + model.InfoExtend.InvestmentExperience
                               + "|" + model.InfoExtend.JoinedBondSince + 
                                                            "|" + model.InfoExtend.RiskAcceptance
                               + "|" + model.InfoExtend.HobbyInvestment + 
                                                            "|" + model.InfoExtend.NoteTalking
                               + "|" + model.InfoExtend.RelationshipSale;
                }
                else
                {
                    strExend = model.InfoExtend.Occupation + "|" + model.InfoExtend.FinancialResources + "|"
                               + model.InfoExtend.KnowledExpID + "|" + model.InfoExtend.OpinionInvestID + "|" +
                               model.InfoExtend.SourceID;
                
                }

                return await WithCrmDbConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var arrList = strExend.Split('|');
                            foreach (var extend in arrList)
                            {
                                if (!string.IsNullOrEmpty(extend.Trim()))
                                {
                                    var obj = new
                                    {
                                        Category = int.Parse(extend).ToString()
                                    };
                                    var parameter = new DynamicParameters();
                                    parameter.Add("@UserName",model.UserName);
                                    parameter.Add("@CustCode",model.CustCode);
                                    parameter.Add("@Category",obj.Category);
                                    parameter.Add("@check", dbType: DbType.Int32,direction: ParameterDirection.Output);
                                    await conn.ExecuteAsync("TVSI_sINSERT_CATEGORY_CUST_EXTEND_IWORK", parameter,
                                        commandType: CommandType.StoredProcedure,
                                        transaction: trans);
                                    id = parameter.Get<int>("@check");
                                }
                            }
                            trans.Commit();
                            return id > 0;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("InsertExtendCust:" + ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("InsertExtendCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> InsertExtendCustCreateAccount(CreateAccountRequest model)
        {
            try
            {
                var id = 0;
                var strExend = string.Empty;

                if (model.UserRole.Equals("CRM_BB"))
                {
                    strExend = model.InfoExtend.Occupation + "|" + model.InfoExtend.Workplace + "|"
                               + model.InfoExtend.FinancialResources + "|" + model.InfoExtend.InvestmentExperience
                               + "|" + model.InfoExtend.JoinedBondSince + "|" + model.InfoExtend.RiskAcceptance
                               + "|" + model.InfoExtend.HobbyInvestment + "|" + model.InfoExtend.NoteTalking
                               + "|" + model.InfoExtend.RelationshipSale;
                }
                else if(model.UserRole.Equals("CRM_SALE"))
                {
                    strExend = model.InfoExtend.Occupation + "|" + model.InfoExtend.FinancialResources + "|"
                               + model.InfoExtend.KnowledExpID + "|" + model.InfoExtend.OpinionInvestID + "|" +
                               model.InfoExtend.SourceID;
                }
                else
                {
                    strExend = model.InfoExtend.Occupation + "|" + model.InfoExtend.FinancialResources + "|"
                               + model.InfoExtend.KnowledExpID + "|" + model.InfoExtend.OpinionInvestID + "|" +
                               model.InfoExtend.SourceID;
                }


                return await WithCrmDbConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var arrList = strExend.Split('|');
                            foreach (var extend in arrList)
                            {
                                if (!string.IsNullOrEmpty(extend.Trim()))
                                {
                                    var obj = new
                                    {
                                        Category = int.Parse(extend).ToString()
                                    };
                                    var parameter = new DynamicParameters();
                                    parameter.Add("@UserName",model.UserName);
                                    parameter.Add("@CustCode",model.CustCode);
                                    parameter.Add("@Category",obj.Category);
                                    parameter.Add("@check", dbType: DbType.Int32,direction: ParameterDirection.Output);
                                    await conn.ExecuteAsync("TVSI_sINSERT_CATEGORY_CUST_EXTEND_IWORK", parameter,
                                        commandType: CommandType.StoredProcedure,
                                        transaction: trans);
                                    id = parameter.Get<int>("@check");
                                }
                            }
                            trans.Commit();
                            return id > 0;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("InsertExtendCust:" + ex.Message);
                            log.Error(ex.InnerException);
                            return false;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("InsertExtendCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<AccountDetailResult> RQ_GE_DetailAccountWebsite(AccountDetailWebsiteRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var retData = await conn.QueryFirstOrDefaultAsync<AccountDetailResult>(
                        _accountCommand.GetCustomerCRMWebsite, new
                        {
                            @UserName = model.UserName,
                            @Id = model.ProfileId
                        });
                    if (retData != null)
                    {
                        if (retData.ConfirmStatus == 0)
                        {
                            var urlWeb = ConfigUtilities.GetAppsettingValue("TVSI_WEBSITE");
                            if (retData.SourceAccount == 4)
                                retData.LinkActivated = 
                                    urlWeb + "user/confirm-ekyc-open-account?code="+retData.ConfirmCode+"&sms="+retData.ConfirmSms+"";
                            if (retData.SourceAccount == 6)
                                retData.LinkActivated = 
                                    urlWeb + "user/confirm-ekyc-open-account?codem="+retData.ConfirmCode+"&sms="+retData.ConfirmSms+"";
                        }
                        var sql =
                            "select trang_thai_tk StatusNumber from TVSI_DANG_KY_MO_TAI_KHOAN where so_cmnd = @cardID";
                        var data = await WithCommonDBConnection(async con => await con.QueryFirstOrDefaultAsync<string>(
                            sql, new
                            {
                                cardID = retData.CardId
                            }));
                        if (data != null)
                            retData.StatusNumber = data;
                        
                        var manSql =
                            "select * from TVSI_DANG_KY_MO_TAI_KHOAN_CAU_HINH where loai_cau_hinh = 'nguoi_dai_dien'";
                        var getAllMan = await WithCommonDBConnection(async connq
                            => await connq.QueryAsync<GetFullManinfo>(manSql));
                        var getMan = new GetFullManinfo();
                        var manName = string.Empty;
                        var postionName = string.Empty;
                        var manNo = string.Empty;
                        var manDate = string.Empty;
                        var manCardId = string.Empty;
                        var manCardIssuse = string.Empty;
                        var manCardDate = string.Empty;
                        foreach (var item in getAllMan)
                        {
                            if (!string.IsNullOrEmpty(item.ma_chi_nhanh) &&
                                item.ma_chi_nhanh.Contains(model.BranchID))
                            {
                                getMan = item;
                                break;
                            }
                        }

                        if (getMan == null)
                        {
                            getMan = getAllMan.FirstOrDefault(x => x.ma_chi_nhanh == "All");
                        }
                        
                        if (getMan != null)
                        {
                            var arrPara = getMan.gia_tri.Split('|');
                            manName = arrPara[0];
                            postionName = arrPara[1];
                            manNo = arrPara[2];
                            manDate = arrPara[3];
                            manCardId = arrPara[4];
                            manCardDate = arrPara[5];
                            manCardIssuse = arrPara[6];
                        }

                        retData.TvsiManName = retData.TvsiManName ?? manName;
                        retData.TvsiManPosition = retData.TvsiManPosition ?? postionName;
                        retData.TvsiManNo = retData.TvsiManNo ?? manNo;
                        retData.TvsiManDate = retData.TvsiManDate ?? manDate;
                        retData.TvsiManCardId = retData.TvsiManCardId ?? manCardId;
                        retData.TvsiManCardDate = retData.TvsiManCardDate ?? manCardDate;
                        retData.TvsiManCardIssue = retData.TvsiManCardIssue ?? manCardIssuse;

                        if (retData.SourceAccount == 4)
                        {
                            var url = ConfigUtilities.GetAppsettingValue("TVSI_WEBSITE");
                            
                            var urlFront =  url + "EkycCustomer/" + model.ProfileId + "/" + model.ProfileId + "_Front.jpg";
                            var urlBack =  url + "EkycCustomer/" + model.ProfileId + "/" + model.ProfileId + "_Back.jpg";
                            var urlFace =  url + "EkycCustomer/" + model.ProfileId + "/" + model.ProfileId + "_Face.jpg";
                            
                            var imageFrontByte = AccountUtis.GetImageUrl(urlFront);
                            var imageBackByte = AccountUtis.GetImageUrl(urlBack);
                            var imageFaceByte = AccountUtis.GetImageUrl(urlFace);
                            
                            if (imageFrontByte != null && imageBackByte != null && imageFaceByte != null)
                            {
                                var resizeFront = await Resize2Max50Kbytes(imageFrontByte);
                                var resizeBack = await Resize2Max50Kbytes(imageBackByte);
                                var resizeFace = await Resize2Max50Kbytes(imageFaceByte);
                            
                                if (!string.IsNullOrEmpty(urlFront))
                                    retData.ImageFront =  Convert.ToBase64String(resizeFront);
                                if (!string.IsNullOrEmpty(urlBack))
                                    retData.ImageBack =  Convert.ToBase64String(resizeBack);
                                if (!string.IsNullOrEmpty(urlFace))
                                    retData.ImageFace =  Convert.ToBase64String(resizeFace);
                            }

                        }
                        if (retData.SourceAccount == 6)
                        {
                            var dataImage =  await GetImageCardEKYC(retData.CardId);
                            if (dataImage != null)
                            {
                                if (dataImage.RetData.Front_Image != null)
                                    retData.ImageFront = dataImage.RetData.Front_Image.Replace("data:image/jpg;base64,","");
                                if (dataImage.RetData.Back_Image != null)
                                    retData.ImageBack = dataImage.RetData.Back_Image.Replace("data:image/jpg;base64,","");
                                if(dataImage.RetData.Face_Image != null)
                                    retData.ImageFace = dataImage.RetData.Face_Image.Replace("data:image/jpg;base64,","");
                            }
                        }
                        
                        return retData;
                    }

                    return null;
                });
            }
            catch (Exception ex)
            {
                log.Error("RQ_GE_DetailAccountWebsite:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Update thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public async Task<bool> UpdateAccWebInfo(AccWebEditRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var retData = await conn.ExecuteAsync(_accountCommand.UpdateAccWeb, new
                    {
                        sale_assign = model.AssignUser,
                        noi_dung_xu_ly = model.Description,
                        trang_thai_tk = model.Status,
                        nguoi_cap_nhat = model.UserName,
                        ngay_cap_nhat = DateTime.Now,
                        dang_kyid = model.AccOpenId
                    });

                    
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateAccWebInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Xóa thông tin danh sách mở tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public async Task<bool> DeleteAccWebInfo(AccWebDeleteRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var retData = await conn.ExecuteAsync(_accountCommand.DeleteAccWeb, new
                    {
                        nguoi_cap_nhat = model.UserName,
                        ngay_cap_nhat = DateTime.Now,
                        dang_kyid = model.AccOpenId
                    });


                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("DeleteAccWebInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetManInfoResult> GetManInfo(GetManInfoRequest model)
        {
            try
            {
                var sql = "SELECT gia_tri FROM [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN_CAU_HINH] Where loai_cau_hinh = 'nguoi_dai_dien' and trang_thai = 1 and ma_cau_hinh = @ManCode";
                return await WithCommonDBConnection(async conn =>
                {
                    var value = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                    {
                        @ManCode = model.ManCode
                    });
                    var result = value.Split('|');
                    return new GetManInfoResult()
                    {
                        ManCode = model.ManCode,
                        ManName = result[0],
                        ManPosition = result[1],
                        ManNo = result[2],
                        ManDate = result[3],
                        ManCardID = result[4],
                        ManCardDate = result[5],
                        ManCardIssue = result[6]
                    };
                });

            }
            catch (Exception ex)
            {
                log.Error("GetManInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetManInfoListResult>> GetManInfoList(BaseRequest model)
        {
            try
            {
                var manList = new List<GetManInfoListResult>();
                var sql =
                    "SELECT ma_cau_hinh, gia_tri FROM TVSI_DANG_KY_MO_TAI_KHOAN_CAU_HINH where loai_cau_hinh = 'nguoi_dai_dien' and trang_thai = 1 ";
                return await WithCommonDBConnection(async conn =>
                {
                    var data = await conn.QueryAsync<GetManInfoSql>(sql);
                    foreach (var rsql in data)
                    {
                        var value = rsql.gia_tri.Split('|');
                        var manName = value[0];
                        manList.Add(new GetManInfoListResult()
                        {
                            ManCode = rsql.ma_cau_hinh,
                            ManName = manName
                        });
                    }
                    return manList;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetManInfoList: " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<byte[]> Resize2Max50Kbytes(byte[] byteImageIn)
        {
            try
            {
                byte[] currentByteImageArray = byteImageIn;
                double scale = 1f;

                if (byteImageIn.Length <= 0)
                {
                    return null;
                }

                MemoryStream inputMemoryStream = new MemoryStream(byteImageIn);
                Image fullsizeImage = Image.FromStream(inputMemoryStream);

                while (currentByteImageArray.Length > 50000)
                {
                    Bitmap fullSizeBitmap = new Bitmap(fullsizeImage, 
                        new Size((int)(fullsizeImage.Width * scale), (int)(fullsizeImage.Height * scale)));
                    MemoryStream resultStream = new MemoryStream();

                    fullSizeBitmap.Save(resultStream, fullsizeImage.RawFormat);

                    currentByteImageArray = resultStream.ToArray();
                    resultStream.Dispose();
                    resultStream.Close();

                    scale -= 0.05f;
                }

                return currentByteImageArray;
            }
            catch (Exception ex)
            {
                log.Error("Resize2Max50Kbytes: " + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
