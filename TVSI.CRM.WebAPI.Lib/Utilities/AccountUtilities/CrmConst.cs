﻿using System.ComponentModel;

namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public class CrmConst
    {
        //Thông tin lưu log
        public const string CRM_SYSTEM = "CRM";
        public const string CRM_VIEW = "View Customer";
        public const string CRM_EXPORT = "Export Report";
        //----------------------//

        public const string ROLE_SALE = "Sale";
        public const string ROLE_ADMIN = "Admin";
        public const string ROLE_CF = "CF";
        public const string ROLE_HR = "HR";
        public const string ROLE_BO = "BO";

        public const string ATTRIBUTE_NAME_EMAIL = "email";
        public const string ATTRIBUTE_NAME_COMPANY = "noi_lam_viec";
        public const string ATTRIBUTE_NAME_JOB = "nghe_nghiep";
        public const string ATTRIBUTE_NAME_ADDRESS = "dia_chi";

        public const string COLUMN_DEPARTMENT_CODE = "DepartmentCode";
        public const string COLUMN_QUESTION_CODE = "QuestionCode";
        public const string COLUMN_IS_UPDATE = "IsUpdate";
        public const string COLUMN_IS_ACTIVE = "IsActive";
        public const string COLUMN_QUESTIONCONTENT = "QuestionContent";
        public const string COLUMN_ANSWERTRUE = "AnswerTrue";
        public const string CouponType_IP = "IP";
        public const string CouponType_CP = "CP";
        public enum CouponContractStatus
        {
            [Description("Chờ trả lãi")]
            Queue = 0,
            [Description("Đã trả lãi")]
            Complete = 1,
            [Description("Hủy trả lãi")]
            Deleted = 99
        }
        public enum DefaultStatus
        {
            New = 0,
            Active = 1,
            Deleted = 99,
            InActive = 2,
            Rejected = -1,
        }

        public enum LeadStatus
        {
            New = 0,
            Assigned = 1,
            Account = 2,
            Closed = -1,
            Deleted = 99
        }

        public enum CFLeadStatus
        {           
            New = 0,            // Tạo mới
            Negotiating = 1,    // Đang đàm phán
            MetOneTime = 2,     // Gặp mặt 1 lần
            ProposalSent = 3,   // Đã gửi Proposal
            ConvertAccount = 4, // Đã convert thành Khách hàng
            Deleted = 99        // Đã xoá
        }

        public enum OpportunityStatus
        {
            Open = 1,
            InProgess = 2,
            OnHold = 3,
            Won = 4,
            Lost = 5,
            OutSold = 6,
            Cancelled = 7,
            Deleted = 99
        }
        public enum ShareHolderConst
        {
            sharetype_1 = 1,     // Cổ đông lớn
            sharetype_2 = 2,     // Cổ đông nội bộ
            sharetype_3 = 3,    // Người liên quan đến cổ đông lớn
            sharetype_4 = 4 // Sắp thành cổ đông lớn
        }
        public enum ActivityTaskStatus
        {
            NotStarted = 1,
            InProgress = 2,
            Completed = 3,
            Pending = 4,
            Canceled = 5
        }

        public enum ActivityEventStatus
        {
            Planned = 11,
            Held = 12,
            OnHeld = 13,
            Completed = 14
        }


        public enum TimeWorkStatus
        {
            Started = 0,
            BU_Progress = 1,
            BD_Progress = 2,
            Rejected = -1,
            Deleted = 99,
        }

        public enum BondRequestOrderStatus
        {
            New = 0,
            Processing = 1,
            Withdraw = 11,
            Approved = 2,
            Rejected = -1
        }

        public enum TimeWorkType
        {
            Timekeeping = 1,
            Overtime = 2,
            Vacation = 3
        }


        public enum BondOrderType
        {
            BUY = 1,
            SELL = 2
        }

        public enum BondPaymentMethodType
        {
            STOCK = 0,
            BANK = 1
        }

        public enum BondFeeType
        {

            FEE_1 = 1,
            FEE_2 = 2,
            FEE_3 = 3,
            FEE_4 = 4,
            FEE_5 = 5,
            FEE_6 = 6,
            FEE_7 = 7,
            FEE_8 = 8,
            FEE_9 = 9,
            FEE_10 = 10,
            FEE_11 = 11
        }

        public enum VacationType
        {
            VacationOne = 1,
            VacationTwo = 2,
            VacationThree = 3,
            VacationFour = 4,
            VacationFive = 5,
            VacationSix = 6,
            VacationSeven = 7
        }

        public enum AttributeGroup
        {
            BASIC_INFO = 1,
            INVEST_INFO = 2,
            SERVICE_INFO = 3,
            EXTEND_INFO = 4
        }

        public enum AttributePriority
        {
            PUBLIC = 1,
            PRIVATE = 2
        }

        public enum AttributeLevel
        {
            LEVEL_1 = 1,        // Luôn hiển thị trên phần chi tiết của Lead, Account để sale có thể nhập bất cứ lúc nào
            LEVEL_99 = 99       // Ko hiển thị trên phần chi tiết của Lead, Account (dùng cho trường hợp private)
        }

        public enum AttributeType
        {
            TEXTBOX = 1,
            TEXTAREA = 2,
            DATEBOX = 3,
            LISTBOX = 4
        }

        public enum EventType
        {
            EVENT = 1,
            TASK = 2
        }


        public enum Priority
        {
            High = 1,
            Medium = 2,
            Low = 3
        }

        public enum ActivityType
        {
            Appointment = 1,
            Call_Email = 2,
            Late_Early = 3,
            Other = 4,
            Absent = 5

        }

        public enum SaleLevel
        {
            Sale = 0,       // Sale
            Leader = 2,     // Trưởng nhóm
            Director = 1    // GĐ chi nhánh
        }


        public enum CFOppServiceStatus
        {
            New = 0,            // Tạo mới
            Negotiating = 1,    // Đang đàm phán
            MetOneTime = 2,     // Gặp mặt 1 lần
            ProposalSent = 3,   // Đã gửi Proposal
            ConvertAccount = 4, // Đã convert thành Khách hàng
            Deleted = 99        // Đã xoá
        }


        public enum CFContractStatus
        {
            Pending = 0,            // Đang chờ xử lý
            PaymentOne = 1,    // Thanh toán lần 1
            PaymentTwo = 2,     // Thanh toán lần 2
            PaymentN = 3,   // Thanh toán lần N
            Invoice = 4, // Xuất hóa đơn
            Liquidation = 5, // Thanh lý HĐ
            Cancel = 99        // Đã xoá
        }

        public enum CFContractType
        {
            Contract = 0,            // Hợp đồng
            Records = 1,    // Biên bản ghi nhớ
           
        }

        public enum CFContractService
        {
            CoPhanHoa = 1,            // Cổ phần hóa
            ThoaiVon = 2,    // Thoái vốn
            NiemYet = 3,     // Niêm yết
            Upcom = 4,   // Upcom
            MASell = 5, // M&A bên bán
            MABuy = 6, // M&A bên mua
            PhatHanh = 7,        // Phát hành cổ phiếu
            TraiPhieu = 8, // Trái phiếu
            Other = 9 // Khác
        }

        public enum ExamUserStatus
        {
            New = 0,
            Edit = 1,
            Finish = 2,
        }

        public enum UserType
        {
            Sale = 0,     // Chuyen vien tu van
            BackOffice = 1       // Kiem soat vien va other user
        }

        public enum SystemLogType
        {
            Login = 1,
            Account = 2,
            Activity = 3,
            SendEmail = 4
        }

        public enum SystemLogEventType
        {
            Info = 1,
            Warning = 2,
            Error = 3,
            
        }

        public class ScreenName
        {
            public const string LOGIN_SCREEN = "Login";
            public const string SALE_ACCOUNT_DETAIL = "SaleAccountDetail";

            public const string ACTIVITY_DETAIL = "SaleActivityDetail";
            public const string ACTIVITY_ADD = "ActivityAdd";
            public const string TIMEWORK_ADD = "TimeWorkAdd";
        }

        public enum OpenAccWebTVSIStatus
        {
            CHO_XU_LY_000 = 000,
            DANG_XU_LY_100 = 100,
            DA_KICK_HOAT_200 = 200,
            HOAN_THANH_300 = 300,
            NVSS_TU_CHOI_199 = 199,
            KSVSS_TU_CHOI_299 = 299,
            HUY_999 = 999,
        }
		
		public enum OpenAccWebTVSISex
        {
            Male = 1,
            Female = 2
        }

        public enum ValidateAccountConst
        {
            ACCOUNT_NOT_EXIST = 0,
            ACCOUNT_EXIST_NOT_MARGIN = 1,
            ACCOUNT_EXIST_MARGIN = 2,
            ACCOUNT_EXIST_DIF_CUSTYPE = 3
        }

        public class OpenAccountStatus
        {
            public const string CHO_XU_LY_000 = "000";
            public const string BU_MAN_XU_LY_001 = "001";
            public const string BU_MAN_TU_CHOI_099 = "099";
            public const string DANG_XU_LY_100 = "100";
            public const string DA_KICK_HOAT_200 = "200";
            public const string HOAN_THANH_300 = "300";
            public const string NVSS_TU_CHOI_199 = "199";
            public const string KSVSS_TU_CHOI_299 = "299";
            public const string HUY_999 = "999";
        }
        public enum CustTypeConst
        {
            INDIVIDUAL_DOMESTIC_INVEST = 1,     // CN trong nuoc
            ORGANIZATION_DOMESTIC_INVEST = 2,   // TC trong nuoc
            INDIVIDUAL_FOREIGN_INVEST = 3,       // CN nuoc ngoai
            ORGANIZATION_FOREIGN_INVEST = 4     // TC nuoc ngoai
        }

        public class DocumentStatusConst
        {
            public const string TAO_MOI_000 = "000";
            public const string CHO_XU_LY_100 = "100";
            public const string HOAN_THANH_200 = "200";
            public const string TU_CHOI_199 = "199";
            public const string NO_HO_SO_101 = "101";
        }

        public enum DocumentTypeConst
        {
            OPEN_DOCUMENT = 1,
            ADDITION_DOCUMENT = 2
        }

        public enum ServiceStatusConst
        {
            KO_TRANG_THAI = -1,
            CHO_XU_LY = 0,
            CHO_HO_XAC_NHAN = 1,
            CHO_KH_XAC_NHAN = 2
        }

        public enum SourceInputTypeConst
        {
            EMS = 1,
            CRM = 2,
            WEBSITE = 3
        }
    }
}