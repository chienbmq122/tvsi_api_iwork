﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Bond
{
    public class BondSellContractTempDetailRequest : BaseRequest
    {
        public long ContractTempID { get; set; }
    }

    public class BondSellContractTempDetailResult
    {
        public long ContractTempID { get; set; }
        public DateTime TradeDate { get; set; }
        public string ContractNo { get; set; }
        public string AccountNo { get; set; }
        public string CustName { get; set; }
        public string BondCode { get; set; }
        public int Volume { get; set; }
        public decimal Amount { get; set; }
        public string SaleID { get; set; }
        public string ReferID { get; set; }
        public int PaymentMethod { get; set; }
        public decimal Tax { get; set; }
        public int? CustBankID { get; set; }
        public int CustType { get; set; }
        public string BankAccountInfo { get; set; }
        public decimal CurNav { get; set; }
    }

    public class BondSellContractTempListRequest : BaseRequest
    {
        public string SearchKey { get; set; } 
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }

        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class BondSellContractTempListResult
    {
        public long ContractTempID { get; set; }
        public DateTime TradeDate { get; set; }
        public string ContractNo { get; set; }
        public string AccountNo { get; set; }
        public string CustName { get; set; }
        public string BondCode { get; set; }
        public int Volume { get; set; }
        public decimal Amount { get; set; }
        public string SaleID { get; set; }
    }

    public class BondSellOrderRequest : BaseRequest
    {
        public long ContractTempID { get; set; }
        public string Note { get; set; }
        public string Side { get; set; }
        public string SellDate { get; set; }
        public int SellVolume { get; set; }
    }

    public class BondSellOrderUpdateRequest : BaseRequest
    {
        public long RequestOrderID { get; set; }
        public string Note { get; set; }
        public string Side { get; set; }

        public string SellDate { get; set; }
        public int SellVolume { get; set; }
    }

    public class BondContractTempInfo
    {
        public string AccountNo { get; set; }
        public string CustName { get; set; }
        public string ContractNo { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime? DealDate { get; set; }
        public string BondCode { get; set; }
        public int Volume { get; set; }
        public int PaymentMethod { get; set; }
        public int? CustBankID { get; set; }
        public string SaleID { get; set; }
        public string ReferID { get; set; }

    }

    public class BondSellContractInfo
    {
        public string AccountNo { get; set; }
        public string Contractno { get; set; }
        public string BondCode { get; set; }
        public DateTime TradeDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public DateTime CouponDate { get; set; }
        public decimal Rate { get; set; }
        public int RollBack { get; set; }
        public int DebitType { get; set; }
        public long Volume { get; set; }
    }
}
