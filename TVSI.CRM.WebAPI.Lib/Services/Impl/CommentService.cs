﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Comment;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class CommentService : BaseService, ICommentService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private ICommentText _commandText;
        public CommentService(ICommentText commandText)
        {
            _commandText = commandText;
        }

        public async Task<List<CommentDBModel>> GetListComment(CommentModelRequest model)
        {
            try
            {
                //List<CommentDBModel> listComments = new List<CommentDBModel>();
                return await WithCrmDbConnection(async conn =>
                {
                    IEnumerable<CommentDBModel> data;
                    if(model.LeadID == -1)
                    {
                        data = await conn.QueryAsync<CommentDBModel>(_commandText.GetListCommentCust, new { CustCode = model.CustCode });
                    }
                    else
                    {
                        data = await conn.QueryAsync<CommentDBModel>(_commandText.GetListCommentLead, new { LeadID = model.LeadID });
                    }
                    List<int> commentIds = new List<int>();
                    foreach (CommentDBModel comment in data)
                    {
                        commentIds.Add(comment.CommentID);
                    }
                    IEnumerable<ReplyModel> ListReply = await conn.QueryAsync<ReplyModel>(_commandText.GetListReply, new { CommentIDs = commentIds });
                    foreach (CommentDBModel comment in data)
                    {
                        comment.ListReply = ListReply.Where(reply => reply.CommentID == comment.CommentID).ToList();
                    }
                    return data.ToList();
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListComment:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> CreateComent(CreateCommentRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateComment, new
                    {
                        LeadID = model.LeadID,
                        CustCode = model.CustCode,
                        Content = model.Content,
                        UserName = model.UserName,
                        FullName = model.FullName,
                        Rate = model.Rate,
                        CreatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateComent:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> CreateReply(CreateReplyRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateReply, new
                    {
                        CommentID = model.CommentID,
                        Reply = model.Reply,
                        UserName = model.UserName,
                        FullName = model.FullName,
                        CreatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateReply:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
