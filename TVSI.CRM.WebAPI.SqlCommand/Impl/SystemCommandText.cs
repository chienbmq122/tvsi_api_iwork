﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class SystemCommandText : ISystemCommandText
    {
        #region SQL Command List

        /// <summary>
        /// Get User Domain
        /// </summary>
        public string GetUserDomain
            => @"SELECT user_domain FROM TVSI_THONG_TIN_TRUY_CAP_CHI_TIET WHERE id_he_thong = @userId AND trang_thai=1";


        /// <summary>
        /// Get thông tin đăng nhập và quyền của User
        /// </summary>
        public string GetUserInfo => @"SELECT A.ten_dang_nhap AS UserName, B.ho_va_ten AS FullName,
                 B.ma_nhan_vien_quan_ly AS SaleID, B.cap_quan_ly AS Level,
                 A.cap_do_he_thong_id as SystemID, C.ma_chi_nhanh as BranchID,
                 A.nhan_su_id AS UserID, C.ma_cap_do_he_thong AS SystemCode
          FROM TVSI_THONG_TIN_TRUY_CAP A 
          INNER JOIN TVSI_NHAN_SU B ON A.nhan_su_id = B.nhan_su_id 
          LEFT JOIN TVSI_CAP_DO_HE_THONG C ON A.cap_do_he_thong_id = C.cap_do_he_thong_id 
          WHERE A.ten_dang_nhap = @ten_dang_nhap AND a.trang_thai = 1";


        /// <summary>
        /// Get thông tin thông báo
        /// </summary>
        public string GetNotificationList => @"SELECT MessageID MessageId, MessageType NotificationType, TradeDate TradeDateDT, 
                                                Title, Message, IsRead
                                                FROM TVSI_THONG_BAO_CRM
                                                WHERE UserName = @UserName 
                                                ORDER BY MessageID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        /// <summary>
        /// Get thông tin thông báo chưa đọc
        /// </summary>
        public string GetNumOfUnreadNotification => @"SELECT count(*) FROM TVSI_THONG_BAO_CRM WHERE UserName = @UserName AND IsRead = 0";


        /// <summary>
        /// Get thông tin thông báo đã đọc
        /// </summary>
        public string SyncNotificationRead => @"UPDATE TVSI_THONG_BAO_CRM SET IsRead = 1, UpdatedBy = @UpdatedBy, UpdatedDate = @UpdatedDate 
                                                WHERE UserName = @UserName AND IsRead = 0 AND MessageID IN @MessageIDList";
        #endregion


        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
