﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.AssetManagement
{
    public class AssetModel
    {
        [Description("id thiết bị")]
        public int AssetId { get; set; }
        [Description("Id asset type")]
        public int TypeAssetId { get; set; }
        [Description("Tình trạng tài sản")]
        public int StateAssetId { get; set; }
        [Description("Giá tiền")]
        public int Money { get; set; }
        [Description("Mã serial")]
        public string SerialNumberAsset { get; set; }
        [Description("Số lượng trong kho")]
        public int Quantity { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        [Description("Ngày nhập")]
        public string DateBuyAsset { get; set; }
        [Description("Hạn bảo hành")]
        public string DateOutWarranty { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Id mượn trả")]
        public int ManagementAssetId { get; set; }
        [Description("Ngày tạo")]
        public string CreatedDate { get; set; }
        [Description("Ngày cập nhật")]
        public string UpdatedDate { get; set; }
    }
    public class AssetListResponse
    {
        [Description("Tổng số thiết bị")]
        public int TotalItem { get; set; }
        [Description("Danh sách thiết bị")]
        public List<AssetModel> Items { get; set; }
    }

    public class AssetListCommonResponse
    {
        [Description("Tổng số thiết bị")]
        public int TotalItem { get; set; }
        [Description("Danh sách thiết bị")]
        public List<AssetDetailModel> Items { get; set; }
    }

    public class AssetListRequest : BaseRequest
    {
        [Description("Số trang")]
        public int PageIndex { get; set; }
        [Description("Số item 1 trang")]
        public int PageSize { get; set; }
        [Description("Loai tài sản")]
        public int TypeAssetId { get; set; }
    }

    public class AssetListConfirmRequest : BaseRequest
    {
        [Description("Số trang")]
        public int PageIndex { get; set; }
        [Description("Số item 1 trang")]
        public int PageSize { get; set; }
    }

    public class AssetDetailModel
    {
        [Description("id thiết bị")]
        public int AssetId { get; set; }
        [Description("Id asset type")]
        public int TypeAssetId { get; set; }
        [Description("Tình trạng tài sản")]
        public int StateAssetId { get; set; }
        [Description("Giá tiền")]
        public int Money { get; set; }
        [Description("Mã serial")]
        public string SerialNumberAsset { get; set; }
        [Description("Số lượng trong kho")]
        public int Quantity { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        [Description("Ngày nhập")]
        public string DateBuyAsset { get; set; }
        [Description("Hạn bảo hành")]
        public string DateOutWarranty { get; set; }
        [Description("Ngày mượn")]
        public string DateReceiveAsset { get; set; }
        [Description("Ngày trả")]
        public string DateResponseAsset { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Id mượn trả")]
        public int ManagementAssetId { get; set; }
        [Description("Người mượn")]
        public string UserId { get; set; }
        [Description("Thuộc phòng")]
        public int DepartmentId { get; set; }
        [Description("Ngày tạo")]
        public string CreatedDate { get; set; }
        [Description("Ngày cập nhật")]
        public string UpdatedDate { get; set; }
    }

    public class CreateAsset : BaseRequest
    {
        [Description("ID loại thiết bị")]
        public int TypeAssetId { get; set; }
        [Description("id tình trạng thiết bị")]
        public int StateAssetId { get; set; }
        [Description("Số lượng")]
        public int quantity { get; set; }
        [Description("Giá nhập")]
        public int Price { get; set; }
        [Description("Số serial")]
        public string SerialNumber { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Hạn bảo hành format YYYY-MM-DD")]
        public string DateOutWarranty { get; set; }
        [Description("Ngày nhập format YYYY-MM-DD")]
        public string DateBuyAsset { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
    }

    public class UpdateAsset : BaseRequest
    {
        [Description("Id thiết bị")]
        public int AssetId { get; set; }
        [Description("ID loại thiết bị")]
        public int TypeAssetId { get; set; }
        [Description("id tình trạng thiết bị")]
        public int StateAssetId { get; set; }
        [Description("Số lượng")]
        public int quantity { get; set; }
        [Description("Giá nhập")]
        public int Price { get; set; }
        [Description("Số serial")]
        public string SerialNumber { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Hạn bảo hành format YYYY-MM-DD")]
        public string DateOutWarranty { get; set; }
        [Description("Ngày nhập format YYYY-MM-DD")]
        public string DateBuyAsset { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
    }

    public class DeleteAsset : BaseRequest
    {
        [Description("Id thiết bị")]
        public int AssetId { get; set; }
    }

    public class FilteAsset : BaseRequest
    {
        [Description("Mã serial")]
        public string SerialNumberAsset { get; set; }
        [Description("Tên thiết bị")]
        public string NameAsset { get; set; }
        [Description("Ngày nhập")]
        public string DateBuyAsset { get; set; }
        [Description("Id asset type")]
        public int TypeAssetId { get; set; }
        [Description("Tình trạng tài sản")]
        public int StateAssetId { get; set; }
        [Description("Số trang")]
        public int PageIndex { get; set; }
        [Description("Số item 1 trang")]
        public int PageSize { get; set; }
    }
}
