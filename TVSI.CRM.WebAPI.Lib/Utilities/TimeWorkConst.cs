﻿namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public class TimeWorkConst
    {
        public const string TimeStart = "08:00";
        public const string TimeEnd = "17:00";
        public const string AddressHO = "08:00 - 17:30 Tầng 8, Tòa R3, Royal City, Nguyễn Trãi, Thanh Xuân, Hà Nội (BK: 100m) - Pico Plaza, Nguyễn Trãi, Thanh Xuân (BK: 500m)";
        public const string EmailMan = "@tvsi.com.vn";
    }
}