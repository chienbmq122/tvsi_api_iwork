﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Infrastructure
{
    public class ConnectionFactory
    {
        public static string EmsDbConnectionString => ConfigurationManager.ConnectionStrings["EmsDB_Connection"].ConnectionString;
        public static string CrmDbConnectionString => ConfigurationManager.ConnectionStrings["CrmDB_Connection"].ConnectionString;
        public static string InnoConnectionString => ConfigurationManager.ConnectionStrings["InnoDB_Connection"].ConnectionString;
        public static string TimeWorkConnectionString => ConfigurationManager.ConnectionStrings["TimeWorkDB_Connection"].ConnectionString;
        public static string CommonDbConnection => ConfigurationManager.ConnectionStrings["CommonDB_Connection"].ConnectionString;
        public static string TbmDbConnection => ConfigurationManager.ConnectionStrings["TbmDB_Connection"].ConnectionString;
        public static string BondDbConnection => ConfigurationManager.ConnectionStrings["BondDbContext_Connection"].ConnectionString;
        public static string AssetDbConnection => ConfigurationManager.ConnectionStrings["AssetDB_Connection"].ConnectionString;
        public static string SystemNotificationConnection => ConfigurationManager.ConnectionStrings["SystemNotificationDB_Connection"].ConnectionString;
    }
}
