﻿namespace TVSI.CRM.WebAPI.Lib.Models.TimeWork
{
    public class TimeWorkGetName
    {
        public string FullName { get; set; }
    }

    public class TimeWorkGetDepartment
    {
        public string Department { get; set; }
    }
}