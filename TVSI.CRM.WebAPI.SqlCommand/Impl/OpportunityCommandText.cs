﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class OpportunityCommandText : IOpportunityCommandText
    {
        #region SQL Command List
        public string GetListOpportunity => @"SELECT Opportunity.OpportunityID, Opportunity.OpportunityName, Opportunity.BranchID, Opportunity.AssignUser, Opportunity.LeadID, Opportunity.CustCode, 
                                                    Opportunity.ExpectedRevenue, Opportunity.ExpectedCloseDate, Opportunity.Description, Opportunity.Status ,CustInfo.CustName, Lead.LeadName 
                            FROM Opportunity LEFT JOIN Lead ON Lead.LeadID = Opportunity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Opportunity.CustCode WHERE Opportunity.Status <> 99 ";

        public string GetDetailOpportunity => @"SELECT Opportunity.OpportunityID, Opportunity.OpportunityName, Opportunity.BranchID, Opportunity.AssignUser, Opportunity.LeadID, Opportunity.CustCode, 
                                                    Opportunity.ExpectedRevenue, Opportunity.ExpectedCloseDate, Opportunity.Description, Opportunity.Status ,CustInfo.CustName, Lead.LeadName 
                            FROM Opportunity LEFT JOIN Lead ON Lead.LeadID = Opportunity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Opportunity.CustCode WHERE Opportunity.Status <> 99 AND Opportunity.OpportunityID = @opportunityID AND Opportunity.AssignUser = @assignUser";

        public string CreateOpportunity => @"INSERT INTO Opportunity(OpportunityName, BranchID, AssignUser, LeadID, CustCode, ExpectedRevenue, ExpectedCloseDate, Description, Status, CreatedBy, CreatedDate) 
                                            VALUES (@OpportunityName, @BranchID, @AssignUser, @LeadID, @CustCode, @ExpectedRevenue, @ExpectedCloseDate, @Description, @Status, @CreatedBy, @CreatedDate)";

        public string UpdateOpportunity => @"UPDATE Opportunity SET OpportunityName = @OpportunityName, BranchID = @BranchID, AssignUser = @AssignUser, LeadID = @LeadID, 
                                            CustCode = @CustCode, ExpectedRevenue = @ExpectedRevenue, ExpectedCloseDate = @ExpectedCloseDate, Description = @Description, Status = @Status , 
                                            UpdatedBy = @UpdatedBy, UpdatedDate = @UpdatedDate 
                                            WHERE Opportunity.OpportunityID = @opportunityID";

        public string DeleteOpportunity => @"UPDATE Opportunity SET Status = 99, UpdatedBy = @UpdatedBy, UpdatedDate = @UpdatedDate WHERE OpportunityID = @opportunityID;";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
