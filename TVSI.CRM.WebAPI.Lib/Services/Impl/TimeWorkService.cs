﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.TimeWork;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.Lib.Utilities.TimeWorkUtils;
using TVSI.CRM.WebAPI.ResourceFile;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class TimeWorkService : BaseService, ITimeWorkService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(TimeWorkService));
        private ITimeWorkCommandText _timeWorkCommandText;

        public TimeWorkService(ITimeWorkCommandText timeWorkCommandText)
        {
            _timeWorkCommandText = timeWorkCommandText;
        }


        public async Task<GetTimeWorkByStaffResult> GetHisTimeWorkStaffByDay(GetTimeWorkByStaffRequest model)
        {
            try
            {
                var dateWork = model.DateWork != null
                    ? DateTime.ParseExact(model.DateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;

                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTwdb = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    var dataTwCRM = await WithCrmDbConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDateGPSCRM,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
            
                    if (dataTwdb != null || dataTwCRM != null)
                    {
                        var dates = new List<SaleWorkTimeModel>();
                        if (dataTwdb != null)
                        {
                            dates.Add(dataTwdb);
                        }

                        if (dataTwCRM != null)
                        {
                            dates.Add(dataTwCRM);
                        }

                        var min =  dates.Min(x => x.TimeInStart);
                        var max = dates.Max(x => x.TimeOutEnd);

                        var addressCheckin = string.Empty;
                        var addressCheckout = string.Empty;
                        if (dataTwdb != null)
                        {
                            addressCheckin = dataTwdb.BranchCodeStart;
                        }

                        if (dataTwdb != null)
                        {
                            addressCheckout = dataTwdb.BranchCodeEnd;
                        }

                        if (dataTwCRM != null)
                        {
                            addressCheckin = dataTwCRM.BranchCodeStart;
                        }
                        if (dataTwCRM != null)
                        {
                            addressCheckout = dataTwCRM.BranchCodeEnd;
                        }
                        
                        if (!string.IsNullOrEmpty(addressCheckin))
                        {
                            if (addressCheckin.Equals("01"))
                            {
                                addressCheckin = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckin.Equals("02"))
                            {
                                addressCheckin = "Hồ Chí Minh";
                            }   
                        }

                        if (!string.IsNullOrEmpty(addressCheckout))
                        {
                            if (addressCheckout.Equals("01"))
                            {
                                addressCheckout = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckout.Equals("02"))
                            {
                                addressCheckout = "Hồ Chí Minh";
                            }   
                        }

                        var resData = new GetTimeWorkByStaffResult();
                        resData.TimeWorkStart = new TimeWorkStart();
                        resData.TimeWorkEnd = new TimeWorkEnd();

                        resData.TimeStart = TimeWorkConst.TimeStart;
                        resData.TimeEnd = TimeWorkConst.TimeEnd;

                        var minTime = dates.FirstOrDefault(x => x.TimeInStart == min);
                        var maxTime = dates.FirstOrDefault(x => x.TimeOutEnd == max && x.TimeOutEnd != min);
                        resData.TimeWorkStart.Checkin = minTime?.TypeKeeping;
                        resData.TimeWorkStart.AddressWork = addressCheckin;
                        resData.TimeWorkStart.TimeToWork = minTime?.TimeIn;
                        resData.TimeWorkStart.TypeCheckin = minTime?.TypeKeepingNum;
                        
                        resData.TimeWorkEnd.Checkin = maxTime?.TypeKeeping; 
                        resData.TimeWorkEnd.AddressWork = addressCheckout;
                        resData.TimeWorkEnd.TimeToWork = maxTime?.TimeOut;
                        resData.TimeWorkEnd.TypeCheckin = maxTime?.TypeKeepingNum;

                        return resData;
                    }

                    return null;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetHisTimeWorkStaffByDay:GetHisTimeWorkStaffByDay = " + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetTimeWorkByStaffByMonthResult>> GetHisTimeWorkStaffByMonth(
            GetTimeWorkByStaffByMonthRequest model)
        {
            try
            {
                var monthWork = model.MonthWork != null
                    ? DateTime.ParseExact(model.MonthWork, "MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;

                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataResList = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryAsync<GetTimeWorkByStaffByMonthResult>(
                            _timeWorkCommandText.GetTimeKeepingByMonth, new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @MonthWork = monthWork
                            })
                    );
                    var dataResListGPS =
                        await WithCrmDbConnection(async conn => await conn.QueryAsync<GetTimeWorkByStaffByMonthResult>(
                            _timeWorkCommandText.GetTimeKeepingByMonthGPS, new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @MonthWork = monthWork
                            }));
                    var dataMonthLeave = await
                        WithCrmDbConnection(async conn => await conn.QueryAsync<TypeLeaveApplications>(
                            _timeWorkCommandText.GetLeaveApplicationsByMonth, new
                            {
                                @UserName = model.UserName,
                                @MonthWork = monthWork
                            }));
                    var days = DateTime.DaysInMonth(monthWork.Year, monthWork.Month);
                    var datesMinMax = new List<GetTimeWorkByStaffByMonthResult>(); 
                    datesMinMax.AddRange(dataResList);
                    datesMinMax.AddRange(dataResListGPS);
                    var getTimeWorkB = dataResList.ToList();
                    
                    var retList = new List<GetTimeWorkByStaffByMonthResult>();
                    if (datesMinMax.Count() > 0)
                    {
                        for (int i = 1; i <= days; i++)
                        {
                            var retObject = new GetTimeWorkByStaffByMonthResult();
                            retObject.DetailLeave = new List<TypeLeaveApplications>();
                            var date = new DateTime(monthWork.Year, monthWork.Month, i); // get so ngay trong thang
                            retObject.DateWorkOn = date;
                            foreach (var dataMonth in dataMonthLeave) // get don nghi phep
                            {
                                if (date.Date == dataMonth.DateLeave?.Date)
                                {
                                    var retObjecttype = new TypeLeaveApplications();
                                    retObjecttype.LeaveId = dataMonth.LeaveId;
                                    retObjecttype.StatusNumber = dataMonth.StatusNumber;
                                    retObjecttype.StatusName = dataMonth.StatusName;
                                    retObjecttype.TypeLeaveNumber = dataMonth.TypeLeaveNumber;
                                    retObjecttype.TypeLeaveName = dataMonth.TypeLeaveName;
                                    retObjecttype.DateLeave = dataMonth.DateLeave;
                                    retObject.DetailLeave.Add(retObjecttype);
                                }
                            }

                            var dates = new List<GetTimeWorkByStaffByMonthResult>();

                            foreach (var dataRet in datesMinMax)
                            {
                                if (date.Date == dataRet.DateWorkOn?.Date)
                                {
                                    dates.Add(dataRet);
                                }
                            }

                            foreach (var dataRet in datesMinMax)
                            {
                                if (date.Date == dataRet.DateWorkOn?.Date)
                                {
                                    var min = dates.Min(x => x.TimeInStart);
                                    var max = dates.Max(x => x.TimeOutEnd);
                                    var timeKeeping = dates.Max(x => x.TimeKeeping);
                                    var minTime =
                                        dates.FirstOrDefault(x => x.TimeInStart == min && x.DateWorkOn == date);
                                    var maxTime = dates.FirstOrDefault(x =>
                                        x.TimeOutEnd == max && x.TimeOutEnd != min && x.DateWorkOn == date);
                                    var maxTimeKeeping = dates.FirstOrDefault(x => x.DateWorkOn == date.Date);
                                    retObject.TimeInStart = minTime?.TimeInStart;
                                    retObject.TimeOutEnd = maxTime?.TimeOutEnd;
                                    retObject.TimeKeeping = maxTimeKeeping?.TimeKeeping;
                                }
                            }
                            /*
                            foreach (var dataRet in getTimeWorkB) // get cham cong 
                            {
                                if (date.Date == dataRet.DateWorkOn?.Date)
                                {
                                    retObject.TimeInStart = dataRet.TimeInStart;
                                    retObject.TimeOutEnd = dataRet.TimeOutEnd;
                                    retObject.TimeKeeping = dataRet.TimeKeeping;
                                }
                            }
    
                            foreach (var dataRet in dataResListGPS)
                            {
                                if (date.Date == dataRet.DateWorkOn?.Date)
                                {
                                    retObject.TimeInStart = dataRet.TimeInStart;
                                    retObject.TimeOutEnd = dataRet.TimeOutEnd;
                                    retObject.TimeKeeping = dataRet.TimeKeeping;
                                }
                            }*/

                            retList.Add(retObject);
                        }
                    }
                    
                    return retList;
                }

                return new List<GetTimeWorkByStaffByMonthResult>();
            }
            catch (Exception ex)
            {
                log.Error("GetHisTimeWorkStaffByMonth:GetHisTimeWorkStaffByMonth=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetTimeWorkStaffReportResult> GetTimeWorkStaffReport(GetTimeWorkStaffReportRequest model)
        {
            try
            {
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    if (model.Option == 2) // Get Data by Month
                    {
                        var monthWork = model.MonthWork != null
                            ? DateTime.ParseExact(model.MonthWork, "MM/yyyy", CultureInfo.CurrentCulture)
                            : DateTime.MinValue;
                        var dataTw = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByMonth,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @Month = monthWork
                                }));
                        var timeLateByMonthWork = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkLateByMonth,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @Month = monthWork
                                }));
                        var timeBackEarlyWork = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkBackEarlyByMonth,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @Month = monthWork
                                }));
                        var getMinuteLate =
                            await WithTimeWorkDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetMinuteLateByMonth, new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @Month = monthWork
                                }));
                        var getMinuteEarlyWork =
                            await WithTimeWorkDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetMinuteEarlyWorkByMonth, new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @Month = monthWork
                                }));

                        var saleWorkTimeModels = dataTw.ToList();
                        var leaveApplicationApproved = await WithCrmDbConnection(async conn =>
                            await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetLeaveApplicationsApproved,
                                new
                                {
                                    @username = model.UserName
                                }));
                        var withoutLeave = await WithCrmDbConnection(async conn =>
                            await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetWithoutLeave, new
                                {
                                    @username = model.UserName
                                }));

                        if (saleWorkTimeModels.Count() > 0)
                        {
                            var resData = new GetTimeWorkStaffReportResult();
                            var countDateWork = saleWorkTimeModels.Count() * 8;
                            resData.TotalTimeWork = countDateWork.ToString();
                            resData.NumWork = saleWorkTimeModels.Count().ToString();
                            resData.Leave = leaveApplicationApproved;
                            resData.WithoutLeave = withoutLeave;
                            resData.NumWorkLate = timeLateByMonthWork.Count().ToString();
                            resData.NumBackEarly = timeBackEarlyWork.Count().ToString();
                            resData.NumMinuteLate = getMinuteLate;
                            resData.NumMinuteEarly = getMinuteEarlyWork.Replace("-", "");
                            return resData;
                        }
                    }

                    if (model.Option == 1) // Get Data by Weekly
                    {
                        var fromDateWork = model.FromDateWork != null
                            ? DateTime.ParseExact(model.FromDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                            : DateTime.MinValue;
                        var toDateWork = model.ToDateWork != null
                            ? DateTime.ParseExact(model.ToDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                            : DateTime.MinValue;

                        var dataTw = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkFromDateAndToDate,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @FromDate = fromDateWork,
                                    @ToDate = toDateWork
                                }));
                        var timeLateWork = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkLate,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @FromDate = fromDateWork,
                                    @ToDate = toDateWork
                                }));
                        var timeBackEarlyWork = await WithTimeWorkDBConnection(async conn =>
                            await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkBackEarly,
                                new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @FromDate = fromDateWork,
                                    @ToDate = toDateWork
                                }));
                        var getMinuteLate =
                            await WithTimeWorkDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetMinuteLate, new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @FromDate = fromDateWork,
                                    @ToDate = toDateWork
                                }));
                        var getMinuteEarlyWork =
                            await WithTimeWorkDBConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetMinuteEarlyWork, new
                                {
                                    @CardNumber = cardNumber.EmployeeNo,
                                    @FromDate = fromDateWork,
                                    @ToDate = toDateWork
                                }));

                        var saleWorkTimeModels = dataTw.ToList();
                        var leaveApplicationApproved = await WithCrmDbConnection(async conn =>
                            await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetLeaveApplicationsApproved,
                                new
                                {
                                    @username = model.UserName
                                }));
                        var withoutLeave = await WithCrmDbConnection(async conn =>
                            await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetWithoutLeave, new
                                {
                                    @username = model.UserName
                                }));

                        if (saleWorkTimeModels.Count() > 0)
                        {
                            var resData = new GetTimeWorkStaffReportResult();
                            var countDateWork = saleWorkTimeModels.Count() * 8;
                            resData.TotalTimeWork = countDateWork.ToString();
                            resData.NumWork = saleWorkTimeModels.Count().ToString();
                            resData.Leave = leaveApplicationApproved;
                            resData.WithoutLeave = withoutLeave;
                            resData.NumWorkLate = timeLateWork.Count().ToString();
                            resData.NumBackEarly = timeBackEarlyWork.Count().ToString();
                            resData.NumMinuteLate = getMinuteLate;
                            resData.NumMinuteEarly = getMinuteEarlyWork.Replace("-", "");
                            return resData;
                        }
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetTimeWorkStaffReport:GetTimeWorkStaffReport=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<IEnumerable<GetLeaveApplicationStaffReportResult>> GetLeaveApplicationStaffReport(
            GetLeaveApplicationStaffReportRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var fromDateWork = model.FromDateWork != null
                    ? DateTime.ParseExact(model.FromDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var toDateWork = model.ToDateWork != null
                    ? DateTime.ParseExact(model.ToDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;
                var typeLeave = model.TypeLeave;
                var approvedStatus = model.ApprovedStatus;

                var sql = _timeWorkCommandText.GetLeaveApplicationsStaff;
                sql = sql.Replace("{cond1}",
                    !string.IsNullOrEmpty(model.FromDateWork) ? " and CAST(CreatedDate AS DATE) >= cast(@FromDate as date) " : "");
                sql = sql.Replace("{cond2}",
                    !string.IsNullOrEmpty(model.ToDateWork) ? " and CAST(CreatedDate AS DATE) <= cast(@ToDate as date) " : "");
                sql = sql.Replace("{cond3}",
                    model.TypeLeave != null ? " and TimeWorkType = @TypeLeave " : "");
                
                sql = sql.Replace("{cond4}",
                    model.ApprovedStatus != null ? " and ConfirmStatus = @ApprovedStatus " : "");
               

                return await WithCrmDbConnection(async conn =>
                    await conn.QueryAsync<GetLeaveApplicationStaffReportResult>(
                        sql,
                        new
                        {
                            Start = start,
                            @PageSize = model.PageSize,
                            @UserName = model.UserName,
                            @FromDate = fromDateWork,
                            @ToDate = toDateWork,
                            @TypeLeave = typeLeave,
                            @ApprovedStatus = approvedStatus
                        }));
            }
            catch (Exception ex)
            {
                log.Error("GetLeaveApplicationStaffReport:GetLeaveApplicationStaffReportRequest=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CreateApplicationFormStaff(CreateApplicationFormStaffRequest model)
        {
            try
            {
                var fromDateWork = model.FromDateTimeWork != null
                    ? DateTime.ParseExact(model.FromDateTimeWork, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                
                var toDateWork = model.ToDateTimeWork != null
                    ? DateTime.ParseExact(model.ToDateTimeWork, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var numLeave = (toDateWork - fromDateWork)?.Days;
                var confirmCode = TimeWorkUtils.EncriptMd5(model.UserName + TimeWorkUtils.RandomNumber(6));
                if (confirmCode.Length > 50)
                    confirmCode = confirmCode.Substring(0, 50);

                var rejectCode = TimeWorkUtils.EncriptMd5(model.UserName + TimeWorkUtils.RandomNumber(6));
                if (rejectCode.Length > 50)
                    rejectCode = rejectCode.Substring(0, 50);

                return await WithCrmDbConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var param = new DynamicParameters();
                            param.Add("@BranchId", model.BranchID);
                            param.Add("@Title", model.Title);
                            param.Add("@UserName", model.UserName);
                            param.Add("@NoteReason", model.NoteReason);
                            param.Add("@Address", model.Address);
                            param.Add("@Appoved", model.Approved);
                            param.Add("@TypeLeave", model.TypeLeave);
                            param.Add("@PolicyLeave",model.PolicyLeave);
                            param.Add("@NumLeave",numLeave);
                            param.Add("@FromDate", fromDateWork);
                            param.Add("@ToDate", toDateWork);
                            param.Add("@ConfirmCode", confirmCode);
                            param.Add("@RejectCode", rejectCode);
                            param.Add("@TimeWorkID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                            await conn.ExecuteAsync(_timeWorkCommandText.StoredRegisLeaveApplications
                                , param, commandType: CommandType.StoredProcedure, transaction: trans
                            );
                            trans.Commit();
                            return param.Get<int>("@TimeWorkID") > 0;

                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("CreateApplicationFormStaff:WithCrmDbConnection=" + model + ":" +
                                      ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("CreateApplicationFormStaff:CreateApplicationFormStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CheckinTimeKeepingGPSStaff(CheckinTimeKeepingGPSStaffRequest model)
        {
            try
            {
                var dateCheckin = DateTime.ParseExact(model.TimeCheckin, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    return await WithCrmDbConnection(async conn =>
                    {
                        using (var trans = conn.BeginTransaction())
                        {
                            try
                            {
                            
                                var paramerter = new DynamicParameters();
                                paramerter.Add("@TimeWorkID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                                paramerter.Add("@UserName", model.UserName);
                                paramerter.Add("@CardNumber", cardNumber.EmployeeNo);
                                paramerter.Add("@BranchCode", model.BranchCode);
                                paramerter.Add("@DayCheckin",dateCheckin.Date);
                                paramerter.Add("@TimeCheckin",dateCheckin);
                                paramerter.Add("@Longitude",model.Longitude);
                                paramerter.Add("@Latitude",model.Latitude);
                                var result = await conn.ExecuteAsync(_timeWorkCommandText.StoredInsertCheckinTimeKeepingGPS,
                                    paramerter, commandType: CommandType.StoredProcedure, transaction: trans);
                                trans.Commit();
                                return result > 0;
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                log.Error("CheckinTimeKeepingGPSStaff:CheckinTimeKeepingGPSStaff=" + model + ":" +
                                          ex.Message);
                                log.Error(ex.InnerException);
                                throw;
                            }
                        }
                    });   
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Error("CheckinTimeKeepingGPSStaff:CheckinTimeKeepingGPSStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> CheckoutTimeKeepingGPSStaff(CheckoutTimeKeepingGPSStaffRequest model)
        {
            try
            {
                var dateCheckin = DateTime.ParseExact(model.TimeCheckout, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    return await WithCrmDbConnection(async conn =>
                    {
                        using (var trans = conn.BeginTransaction())
                        {
                            try
                            {
                                var paramerter = new DynamicParameters();
                                paramerter.Add("@TimeWorkID", dbType: DbType.Int32, direction: ParameterDirection.Output);
                                paramerter.Add("@UserName", model.UserName);
                                paramerter.Add("@BranchCode", model.BranchCode);
                                paramerter.Add("@CardNumber", cardNumber.EmployeeNo);
                                paramerter.Add("@DayCheckin",dateCheckin.Date);
                                paramerter.Add("@TimeCheckin",dateCheckin);
                                paramerter.Add("@Longitude",model.Longitude);
                                paramerter.Add("@Latitude",model.Latitude);
                                var result = await conn.ExecuteAsync(_timeWorkCommandText.StoredInsertCheckinTimeKeepingGPS,
                                    paramerter, commandType: CommandType.StoredProcedure, transaction: trans);
                                trans.Commit();
                                return result > 0;
                            }
                            catch (Exception ex)
                            {
                                log.Error("CheckoutTimeKeepingGPSStaff:WithCrmDbConnection=" + model + ":" +
                                          ex.Message);
                                log.Error(ex.InnerException);
                                throw;
                            } 
                        }
                    });   
                }

                return false;
            }
            catch (Exception ex)
            {
                log.Error("CheckoutTimeKeepingGPSStaff:CheckoutTimeKeepingGPSStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetCheckinTimeKeepingGPSStaffResult> GetCheckinTimeKeepingGPSStaff(
            GetCheckinTimeKeepingGPSStaffRequest model)
        {
            try
            {
                var dateWork = model.DateCheckin != null ? 
                    DateTime.ParseExact(model.DateCheckin, "dd/MM/yyyy", CultureInfo.CurrentCulture) 
                    : (DateTime?) null ;
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTwdb = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    var dataTwCRM = await WithCrmDbConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDateGPSCRM,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    if (dataTwdb != null || dataTwCRM != null)
                    {
                        var dates = new List<SaleWorkTimeModel>();
                        if (dataTwdb != null)
                        {
                            dates.Add(dataTwdb);
                        }

                        if (dataTwCRM != null)
                        {
                            dates.Add(dataTwCRM);
                        }

                        var min =  dates.Min(x => x.TimeInStart);
                        var max = dates.Max(x => x.TimeOutEnd);

                        var addressCheckin = string.Empty;
                        var addressCheckout = string.Empty;
                        if (dataTwdb != null)
                        {
                            addressCheckin = dataTwdb.BranchCodeStart;
                        }

                        if (dataTwdb != null)
                        {
                            addressCheckout = dataTwdb.BranchCodeEnd;
                        }

                        if (dataTwCRM != null)
                        {
                            addressCheckin = dataTwCRM.BranchCodeStart;
                        }
                        if (dataTwCRM != null)
                        {
                            addressCheckout = dataTwCRM.BranchCodeEnd;
                        }
                        
                        if (!string.IsNullOrEmpty(addressCheckin))
                        {
                            if (addressCheckin.Equals("01"))
                            {
                                addressCheckin = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckin.Equals("02"))
                            {
                                addressCheckin = "Hồ Chí Minh";
                            }   
                        }

                        if (!string.IsNullOrEmpty(addressCheckout))
                        {
                            if (addressCheckout.Equals("01"))
                            {
                                addressCheckout = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckout.Equals("02"))
                            {
                                addressCheckout = "Hồ Chí Minh";
                            }   
                        }

                        var resData = new GetCheckinTimeKeepingGPSStaffResult();
                        
                        var minTime = dates.FirstOrDefault(x => x.TimeInStart == min);
                        if (minTime != null)
                        {
                            resData.TimeCheckinOn = minTime.TimeInStart;
                            resData.Address = addressCheckout;
                            resData.TypeCheckinNum = minTime.TypeKeepingNum;
                            resData.TypeCheckin = minTime.TypeKeeping;
                            return resData;   
                        }
                    }
                    
                }
                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetCheckinTimeKeepingGPSStaff:GetCheckinTimeKeepingGPSStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetCheckoutTimeKeepingGPSStaffResult> GetCheckoutTimeKeepingGPSStaff(
            GetCheckoutTimeKeepingGPSStaffRequest model)
        {
            try
            {
                var dateWork = model.DateCheckout != null ? 
                    DateTime.ParseExact(model.DateCheckout, "dd/MM/yyyy", CultureInfo.CurrentCulture) 
                    : (DateTime?) null ;
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTwdb = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    var dataTwCRM = await WithCrmDbConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDateGPSCRM,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    if (dataTwdb != null || dataTwCRM != null)
                    {
                        var dates = new List<SaleWorkTimeModel>();
                        if (dataTwdb != null)
                        {
                            dates.Add(dataTwdb);
                        }

                        if (dataTwCRM != null)
                        {
                            dates.Add(dataTwCRM);
                        }

                        var min =  dates.Min(x => x.TimeInStart);
                        var max = dates.Max(x => x.TimeOutEnd);

                        var addressCheckin = string.Empty;
                        var addressCheckout = string.Empty;
                        if (dataTwdb != null)
                        {
                            addressCheckin = dataTwdb.BranchCodeStart;
                        }

                        if (dataTwdb != null)
                        {
                            addressCheckout = dataTwdb.BranchCodeEnd;
                        }

                        if (dataTwCRM != null)
                        {
                            addressCheckin = dataTwCRM.BranchCodeStart;
                        }
                        if (dataTwCRM != null)
                        {
                            addressCheckout = dataTwCRM.BranchCodeEnd;
                        }
                        
                        if (!string.IsNullOrEmpty(addressCheckin))
                        {
                            if (addressCheckin.Equals("01"))
                            {
                                addressCheckin = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckin.Equals("02"))
                            {
                                addressCheckin = "Hồ Chí Minh";
                            }   
                        }

                        if (!string.IsNullOrEmpty(addressCheckout))
                        {
                            if (addressCheckout.Equals("01"))
                            {
                                addressCheckout = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckout.Equals("02"))
                            {
                                addressCheckout = "Hồ Chí Minh";
                            }   
                        }

                        var resData = new GetCheckoutTimeKeepingGPSStaffResult();
                        var maxTime = dates.FirstOrDefault(x => x.TimeOutEnd == max && x.TimeOutEnd != min);
                        if (maxTime != null)
                        {
                            resData.TimeCheckOutOn = maxTime.TimeOutEnd;
                            resData.Address = addressCheckout;
                            resData.TypeCheckoutNum = maxTime.TypeKeepingNum;
                            resData.TypeCheckOut = maxTime.TypeKeeping;
                            return resData;   
                        }
                    }
                    
                }
                return null;
                
            }
            catch (Exception ex)
            {
                log.Error("GetCheckoutTimeKeepingGPSStaff:GetCheckoutTimeKeepingGPSStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

   

        public async Task<IEnumerable<GetFingerprintingStaffResult>> GetFingerprintingStaff(
            GetFingerprintingStaffRequest model)
        {
            try
            {
                var fromDateWork = model.FromDateWork != null
                    ? DateTime.ParseExact(model.FromDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;
                var toDateWork = model.ToDateWork != null
                    ? DateTime.ParseExact(model.ToDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;

                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTw = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkFromDateAndToDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @FromDate = fromDateWork,
                                @ToDate = toDateWork
                            }));
                    var saleWorkTimeModels = dataTw.ToList();
                    if (saleWorkTimeModels.Count() > 0)
                    {
                        var resDataList = new List<GetFingerprintingStaffResult>();
                        foreach (var saleWorkTimeModel in saleWorkTimeModels)
                        {
                            var resData = new GetFingerprintingStaffResult();
                            resData.FullName = saleWorkTimeModel.HoTenUnicode;
                            resData.DateWorkOn = saleWorkTimeModel.DateWorkOn;
                            resData.UserName = model.UserName;
                            if (!string.IsNullOrEmpty(saleWorkTimeModel.TimeIn) &&
                                !string.IsNullOrEmpty(saleWorkTimeModel.TimeOut))
                            {
                                DateTime today = DateTime.Today;
                                DateTime start = new DateTime(today.Year, today.Month, today.Day, 08, 00, 00);
                                DateTime end = new DateTime(today.Year, today.Month, today.Day, 17, 00, 00);
                                resData.TimeWorkStart = saleWorkTimeModel.TimeIn;
                                resData.TimeWorkEnd = saleWorkTimeModel.TimeOut;

                                DateTime timeIn = Convert.ToDateTime(saleWorkTimeModel.TimeIn);
                                DateTime timeOut = Convert.ToDateTime(saleWorkTimeModel.TimeOut);
                                TimeSpan timeLateWork = timeIn.Subtract(start);
                                TimeSpan timeLeaveEarlyWork = timeOut.Subtract(end);

                                var lateWork = timeLateWork.TotalMinutes > 0 ? timeLateWork.TotalMinutes : 0;
                                var leaveEarlyWork = timeLeaveEarlyWork.TotalMinutes < 0
                                    ? timeLeaveEarlyWork.TotalMinutes
                                    : 0;

                                resData.TimeLateWork = lateWork.ToString(CultureInfo.InvariantCulture).Replace("-", "");
                                resData.TimeLeaveEarlyWork = leaveEarlyWork.ToString(CultureInfo.InvariantCulture)
                                    .Replace("-", "");
                                resDataList.Add(resData);
                            }
                        }

                        return resDataList;
                    }
                }

                return new List<GetFingerprintingStaffResult>();
            }
            catch (Exception ex)
            {
                log.Error("GetFingerprintingStaff:GetFingerprintingStaff=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetTWAndLaByStaffResult> GetHisTWAndLAStaffByDay(GetHisTWAndLAStaffByDayRequest model)
        {
            try
            {
                var dateWork = model.DateWork != null
                    ? DateTime.ParseExact(model.DateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTwdb = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    var dataTwCRM = await WithCrmDbConnection(async conn =>
                        await conn.QueryFirstOrDefaultAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkByDateGPSCRM,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @Date = dateWork
                            }));
                    
                    var resData = new GetTWAndLaByStaffResult();
                    if (dataTwdb != null || dataTwCRM != null)
                    {
                        var dates = new List<SaleWorkTimeModel>();
                        if (dataTwdb != null)
                        {
                            dates.Add(dataTwdb);
                        }

                        if (dataTwCRM != null)
                        {
                            dates.Add(dataTwCRM);
                        }
                        var min =  dates.Min(x => x.TimeInStart);
                        var max = dates.Max(x => x.TimeOutEnd);
                        
                        var addressCheckin = string.Empty;
                        var addressCheckout = string.Empty;
                        if (dataTwdb != null)
                        {
                            addressCheckin = dataTwdb.BranchCodeStart;
                        }

                        if (dataTwdb != null)
                        {
                            addressCheckout = dataTwdb.BranchCodeEnd;
                        }

                        if (dataTwCRM != null)
                        {
                            addressCheckin = dataTwCRM.BranchCodeStart;
                        }
                        if (dataTwCRM != null)
                        {
                            addressCheckout = dataTwCRM.BranchCodeEnd;
                        }
                        if (!string.IsNullOrEmpty(addressCheckin))
                        {
                            if (addressCheckin.Equals("01"))
                            {
                                addressCheckin = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckin.Equals("02"))
                            {
                                addressCheckin = "Hồ Chí Minh";
                            }   
                        }

                        if (!string.IsNullOrEmpty(addressCheckout))
                        {
                            if (addressCheckout.Equals("01"))
                            {
                                addressCheckout = "79 LÝ THƯỜNG KIỆT,HOÀN KIẾM,HÀ NỘI";
                            }else if (addressCheckout.Equals("02"))
                            {
                                addressCheckout = "Hồ Chí Minh";
                            }   
                        }
                        resData.TimeWorkStart = new TimeWorkStart();
                        resData.TimeWorkEnd = new TimeWorkEnd();

                        resData.TimeStart = TimeWorkConst.TimeStart;
                        resData.TimeEnd = TimeWorkConst.TimeEnd;

                        var minTime = dates.FirstOrDefault(x => x.TimeInStart == min);
                        var maxTime = dates.FirstOrDefault(x => x.TimeOutEnd == max && x.TimeOutEnd != min);
                        resData.TimeWorkStart.Checkin = minTime?.TypeKeeping;
                        resData.TimeWorkStart.AddressWork = addressCheckin;
                        resData.TimeWorkStart.TimeToWork = minTime?.TimeIn;
                        resData.TimeWorkStart.TypeCheckin = minTime?.TypeKeepingNum;
                        
                        resData.TimeWorkEnd.Checkin =maxTime?.TypeKeeping; 
                        resData.TimeWorkEnd.AddressWork = addressCheckout;
                        resData.TimeWorkEnd.TimeToWork = maxTime?.TimeOut;
                        resData.TimeWorkEnd.TypeCheckin = maxTime?.TypeKeepingNum;
                    }

                    resData.LeaveApplications = new List<LeaveApplications>();

                    var timeWorkRes = await
                        WithCrmDbConnection(async conn =>
                            await conn.QueryAsync<LeaveApplications>(_timeWorkCommandText.GetTimeWorkTypeByDay, new
                            {
                                @UserName = model.UserName,
                                @DateWork = dateWork
                            }));
                    if (timeWorkRes.Count() > 0)
                    {
                        var leaveApplicationsEnumerable = timeWorkRes.ToList();
                        if (leaveApplicationsEnumerable.Count() > 0)
                        {
                            resData.LeaveApplications.AddRange(leaveApplicationsEnumerable);
                            if (resData.LeaveApplications.Count() > 0)
                            {
                                foreach (var listFormLeave in resData.LeaveApplications)
                                {
                                    listFormLeave.FormLeave = new List<GroupTypeLeave>();
                                    var typeLeave = await WithCrmDbConnection(async conn =>
                                        await conn.QueryAsync<GroupTypeLeave>(
                                            _timeWorkCommandText.GetHisTWAndLAStaffByDay,
                                            new
                                            {
                                                @UserName = model.UserName,
                                                @DateWork = dateWork,
                                                @TypeLeave = listFormLeave.TypeLeave
                                            }));
                                    if (typeLeave.Count() > 0)
                                    {
                                        foreach (var type in typeLeave)
                                        {
                                            if (type.TypeLeave == listFormLeave.TypeLeave)
                                            {
                                                listFormLeave.FormLeave.Add(type);
                                            }

                                            if (listFormLeave.FormLeave.Count() > 0)
                                            {
                                                foreach (var listForm in listFormLeave.FormLeave)
                                                {
                                                    var dataDetail = await WithCrmDbConnection(async conn => await
                                                        conn.QueryFirstOrDefaultAsync<DetailLeaveApplications>
                                                        (_timeWorkCommandText.GetIdTimeWorkLeaveApp, new
                                                        {
                                                            @UserName = model.UserName,
                                                            @DateWork = dateWork,
                                                            @Id = listForm.LeaveId
                                                        }));
                                                    if (dataDetail != null)
                                                    {
                                                        if (listForm.LeaveId == dataDetail.LeaveId)
                                                        {
                                                            listForm.DetailLeave = new DetailLeaveApplications()
                                                            {
                                                                Title = dataDetail.Title,
                                                                UserName = dataDetail.UserName,
                                                                UserRole = dataDetail.UserRole,
                                                                NoteReason = dataDetail.NoteReason,
                                                                TimeLeaveStartIn = dataDetail.TimeLeaveStartIn,
                                                                TimeLeaveEndOut = dataDetail.TimeLeaveEndOut,
                                                                Address = dataDetail.Address,
                                                                TimeApp = dataDetail.TimeApp,
                                                                Approved = dataDetail.Approved,
                                                                NameTypeLeave = dataDetail.NameTypeLeave,
                                                                StatusLeave = dataDetail.StatusLeave
                                                            };
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return resData;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetHisTWAndLAStaffByDay:GetHisTWAndLAStaffByDay=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<CheckDataByDateResult>> CheckDataByDay(CheckDataByDateRequest model)
        {
            try
            {
                var dateWorkStaff = model.DateWork != null
                    ? DateTime.ParseExact(model.DateWork, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    : DateTime.MinValue;

                var dateRange = TimeWorkUtils.GetRange(TimeWorkUtils.FrequencyType.Weekly,dateWorkStaff);
                var fDate = Convert.ToDateTime(dateRange[0]).ToString("dd/MM/yyyy");
                var tDate = Convert.ToDateTime(dateRange[1]).ToString("dd/MM/yyyy");
                var fromDateWork = DateTime.ParseExact(fDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                var toDateWork = DateTime.ParseExact(tDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {

                    var dataTimeWorkDb = await WithTimeWorkDBConnection(async conn =>
                        await conn.QueryAsync<SaleWorkTimeModel>(_timeWorkCommandText.GetTimeWorkFromDateAndToDate,
                            new
                            {
                                @CardNumber = cardNumber.EmployeeNo,
                                @FromDate = fromDateWork,
                                @ToDate = toDateWork
                            }));
                    var dataCRMLeave = await WithCrmDbConnection(async conn =>
                        await conn.QueryAsync<CheckDataByDay>(
                        _timeWorkCommandText.GetLeaveCrmByDay, new
                        {
                            @UserName = model.UserName,
                            @FromDate = fromDateWork,
                            @ToDate = toDateWork,
                        }));
                    
                    var resList = new List<CheckDataByDateResult>();
                    for (DateTime i = fromDateWork.Date; i <= toDateWork.Date;i = i.AddDays(1))
                    {
                        var dataCheck = new CheckDataByDateResult();
                        var dateWork = new DateTime(i.Year, i.Month, i.Day);
                        dataCheck.Date = dateWork;
                        dataCheck.OnDayWork = TimeWorkUtils.CompareDayOfWeekVn(dateWork);
                        if (dataTimeWorkDb.Count() > 0)
                        {
                            foreach (var data in dataTimeWorkDb)
                            {
                                log.Info("CheckDataByDay -> forloop" + data.DateWorkOn);
                                if (dateWork.Date == data.DateWorkOn.Date)
                                {
                                    dataCheck.IsData = true;
                                }
                            }

                            foreach (var dataCrm in dataCRMLeave)
                            {
                                if (dateWork.Date == dataCrm.DateApplication.Date)
                                {
                                    dataCheck.IsData = true;
                                }
                            }
                        }
                        resList.Add(dataCheck);
                    }
                        
                    return resList;   
                    
                }

                return new List<CheckDataByDateResult>();
            }
            catch (Exception ex)
            {
                log.Error("CheckDataByDay:CheckDataByDay=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<GetDetailLeaveApplicationsResult> GetDetailLeaveApplications(
            GetDetailLeaveApplicationsRequest model)
        {
            try
            {
                var nameStaff = await WithEmsDbConnection(async conn =>
                    {
                        var userDomain = await conn.QueryFirstOrDefaultAsync<string>(
                            _timeWorkCommandText.GetUserDomainByUserName, new
                            {
                                @username = model.UserName
                            });
                        return await conn.QueryFirstOrDefaultAsync<string>(
                            _timeWorkCommandText.GetNameStaffEmsDbtttcct, new
                            {
                                @UserName = model.UserName,
                                @UserDomain = userDomain
                            });
                    }
                );
                if (nameStaff != null)
                {
                    var department = await WithEmsDbConnection(async conn =>
                        {
                            return await conn.QueryFirstOrDefaultAsync<string>(
                                _timeWorkCommandText.GetDepartmentStaffEmsDb, new
                                {
                                    @username = model.UserName
                                });
                        }
                    );
                    if (department != null)
                    {
                        var resData = await WithCrmDbConnection(async conn =>
                        {
                            return await conn.QueryFirstOrDefaultAsync<GetDetailLeaveApplicationsResult>(
                                _timeWorkCommandText.GetLeaveApplications, new
                                {
                                    @username = model.UserName,
                                    @leaveid = model.LeaveId
                                });
                        });
                        if (resData != null)
                        {
                            resData.Department = department;
                            resData.Applicants = nameStaff;
                            return resData;
                        }

                        return null;
                    }
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("GetDetailLeaveApplications:GetDetailLeaveApplications=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> TW_ED_EditLAMember(TW_ED_EditLAMemberReportRequest model)
        {
            try
            {
                var startDate = DateTime.ParseExact(model.TimeLeaveStart, "dd/MM/yyyy HH:mm:ss",
                    CultureInfo.CurrentCulture);
                var endDate = DateTime.ParseExact(model.TimeLeaveEnd, "dd/MM/yyyy HH:mm:ss",
                    CultureInfo.CurrentCulture);
                var numLeavee = (endDate - startDate).Days;
                return await WithCrmDbConnection(async conn =>
                {
       
                        using (var trans = conn.BeginTransaction())
                        {
                            try
                            {
                                var parameter = new DynamicParameters();
                                parameter.Add("@TimeWorkID", model.LeaveId);
                                parameter.Add("@UserName", model.UserName);
                                parameter.Add("@Title", model.Title);
                                parameter.Add("@Description", model.NoteReason);
                                parameter.Add("@DateStart", startDate);
                                parameter.Add("@DateEnd", endDate);
                                parameter.Add("@PolicyLeave", model.PolicyLeave);
                                parameter.Add("@NumLeave", numLeavee);
                                parameter.Add("@TypeLeave", model.TypeLeave);
                                parameter.Add("@Approved", model.Approved);
                                var save = await conn.ExecuteAsync(_timeWorkCommandText.StoredUpdateLAMember, parameter,
                                    commandType: CommandType.StoredProcedure, transaction: trans);
                                trans.Commit();
                                return save > 0;
                            }
                            catch (Exception ex)
                            {
                                trans.Rollback();
                                log.Error("TW_ED_EditLAMemberReport:TW_ED_EditLAMemberReport=" + model + ":" +
                                          ex.Message);
                                log.Error(ex.InnerException);
                                throw;
                            }
                            finally
                            {
                                conn.Close();
                            }

                        }
                });
            }
            catch (Exception ex)
            {
                log.Error("TW_ED_EditLAMemberReport:TW_ED_EditLAMemberReport=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> TW_MB_ActionLeaveApplications(ActionLeaveApplicationsRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    using (var trans = conn.BeginTransaction())
                    {
                        try
                        {
                            var paramerter = new DynamicParameters();
                            paramerter.Add("@UserName", model.UserName);
                            paramerter.Add("@LeaveId", model.LeaveId);
                            paramerter.Add("@StatusLeave", model.StatusLeave);
                            var result = await conn.ExecuteAsync(_timeWorkCommandText.StoredActionsLeaveApplications,
                                paramerter,
                                commandType: CommandType.StoredProcedure, transaction: trans);
                            trans.Commit();
                            return result > 0;
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            log.Error("TW_MB_ActionLeaveApplications:WithCrmDbConnection=" + model + ":" +
                                      ex.Message);
                            log.Error(ex.InnerException);
                            throw;
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                log.Error("TW_MB_ActionLeaveApplications:TW_MB_ActionLeaveApplications=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<checkDataByMonthResult>> CheckDataByMonth(CheckDataByMonthRequest model)
        {
            try
            {
                var fromMonthWork = model.FromMonthWork != null
                    ? DateTime.ParseExact(model.FromMonthWork, "MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;
                var toMonthWork = model.ToMonthWork != null
                    ? DateTime.ParseExact(model.ToMonthWork, "MM/yyyy", CultureInfo.CurrentCulture)
                    : DateTime.MinValue;
                var cardNumber = await WithEmsDbConnection(async conn =>
                    await conn.QueryFirstOrDefaultAsync<GetEmployee>(_timeWorkCommandText.GetCardNumberEmsDb, new
                    {
                        @username = model.UserName
                    }));
                if (cardNumber != null)
                {
                    var dataTw =
                        await WithTimeWorkDBConnection(async conn => await conn.QueryAsync<DateTime>(
                        _timeWorkCommandText.GetTimeKeepingFromMonthToMonthTW, new
                        {
                            @CardNumber = cardNumber.EmployeeNo,
                            @FromMonth = fromMonthWork,
                            @ToMonth = toMonthWork
                        }));
                    
                    var dataLeaveCrm = await WithCrmDbConnection(async conn => await conn.QueryAsync<DateTime>(
                        _timeWorkCommandText.GetTimeWorkLeaveByMonthCrm, new
                        {
                            @UserName = model.UserName,
                            @FromMonth = fromMonthWork,
                            @ToMonth = toMonthWork
                        }
                    ));
                    var resList = new List<checkDataByMonthResult>();

                    for (DateTime a = fromMonthWork; a <= toMonthWork; a = a.AddMonths(1))
                    {
                        var dataCheck = new checkDataByMonthResult();
                        var monthWork = new DateTime(a.Year, a.Month, a.Day);
                        dataCheck.MonthWorkOn = monthWork;
                        
                        if (dataTw.Count() > 0)
                        {
                            foreach (var date in dataTw)
                            {
                                if (monthWork.Month == date.Month && monthWork.Year == date.Year)
                                {
                                    dataCheck.IsData = true;
                                }
                            }
                        }

                        foreach (var date in dataLeaveCrm)
                        {
                            if (monthWork.Month == date.Month && monthWork.Year == date.Year)
                            {
                                dataCheck.IsData = true;
                            }
                        }
                        resList.Add(dataCheck);
                    }

                    return resList;
                }

                return null;
            }
            catch (Exception ex)
            {
                log.Error("CheckDataByMonth:CheckDataByMonth=" + model + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetNameLeaderApprovedByUserNameResult>> GetNameLeaderApprovedByUserName(
            BaseRequest model)
        {
            try
            {
                var userDomain = await WithEmsDbConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _timeWorkCommandText.GetUserDomainByUserName, new
                    {
                        @username = model.UserName
                    }));
                if (!string.IsNullOrEmpty(userDomain))
                {
                    var resList = new List<GetNameLeaderApprovedByUserNameResult>();
                    var data = await WithEmsDbConnection(async conn =>
                        await conn.QueryAsync<GetNameLeaderApprovedByUserNameResult>(
                            _timeWorkCommandText.GetNameLeaderApprovedByUserName,
                            new
                            {
                                @UserDomain = userDomain,
                                @UserName = model.UserName
                            }));
                    if (data.Count() > 0)
                    {
                        foreach (var list in data)
                        {
                            list.FullName = await GetNameStaff(list.UserName);
                            resList.Add(list);
                        }
                    }

                    return resList;
                }

                return new List<GetNameLeaderApprovedByUserNameResult>();
            }
            catch (Exception ex)
            {
                log.Error("GetNameLeaderApprovedByUserName:GetNameLeaderApprovedByUserName=" + model +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetNameLeaderApprovedByUserNameResult>> GetNameMemberApprovedByUserName(
            BaseRequest model)
        {
            try
            {
                var userDomain = await WithEmsDbConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _timeWorkCommandText.GetUserDomainByUserName, new
                    {
                        @username = model.UserName
                    }));
                if (!string.IsNullOrEmpty(userDomain))
                {
                    var emailMan = userDomain += TimeWorkConst.EmailMan;
                    var resList = new List<GetNameLeaderApprovedByUserNameResult>();

                    var data = await WithEmsDbConnection(async conn =>
                        await conn.QueryAsync<GetNameLeaderApprovedByUserNameResult>(
                            _timeWorkCommandText.GetNameMemberApprovedByUserName,
                            new
                            {
                                @EmailMan = emailMan,
                            }));
                    if (data.Count() > 0)
                    {
                        foreach (var list in data)
                        {
                            list.FullName = await GetNameStaff(list.UserName);
                            resList.Add(list);
                        }
                    }

                    return resList;
                }

                return new List<GetNameLeaderApprovedByUserNameResult>();
            }
            catch (Exception ex)
            {
                log.Error("GetNameMemberApprovedByUserName:GetNameMemberApprovedByUserName=" + model +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetGetLeaveApplicationMemberReportByLeaderResult>>
            GetLeaveApplicationsMemberByLeader(GetLeaveApplicationsMemberByLeaderRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var checkUserDomain = await WithEmsDbConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(
                    _timeWorkCommandText.GetUserDomainByUserName, new
                    {
                        @username = model.UserName
                    }));
                if (!string.IsNullOrEmpty(checkUserDomain))
                {
                    var userDomain = await WithEmsDbConnection(async conn =>
                    {
                        return await conn.QueryFirstOrDefaultAsync<string>(
                            _timeWorkCommandText.GetUserDomain, new
                            {
                                @UserName = model.UserName,
                                @UserDomain = checkUserDomain
                            });
                    });

                    if (!string.IsNullOrEmpty(userDomain))
                    {
                        userDomain += TimeWorkConst.EmailMan;
                        var listRes = new List<GetGetLeaveApplicationMemberReportByLeaderResult>();
                        var toList = await WithCrmDbConnection(async conn =>
                        {
                            var dataRes = new List<GetGetLeaveApplicationMemberReportByLeaderResult>();
                            var branchID = await GetSystemID(model.UserName);
                            if (model.UserLevel == 1)
                            {
                                var listBranch = await GetBranchList(branchID.BranchID);
                                var dataList = await
                                    conn.QueryAsync<GetGetLeaveApplicationMemberReportByLeaderResult>(
                                        _timeWorkCommandText.GetLeaveApplicationsMemberByLeaderBD, new
                                        {
                                            @listID = listBranch,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        });
                                var dataListNotVType2 = await
                                    conn.QueryAsync<GetGetLeaveApplicationMemberReportByLeaderResult>(
                                        _timeWorkCommandText.GetLeaveApplicationsMemberByLeaderBDNotCtype2, new
                                        {
                                            @listID = listBranch,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        });
                                dataRes.AddRange(dataList);
                                dataRes.AddRange(dataListNotVType2);
                            }else if (model.UserLevel == 2)
                            {
                                var subSaleList = await GetSubSaleList(branchID.SystemID);
                                
                            }
                            else
                            {
                                var dataList = (List<GetGetLeaveApplicationMemberReportByLeaderResult>) await
                                    conn.QueryAsync<GetGetLeaveApplicationMemberReportByLeaderResult>(
                                        _timeWorkCommandText.GetLeaveApplicationsMemberByLeader, new
                                        {
                                            @EmailMan = userDomain,
                                            @Start = start,
                                            @PageSize = model.PageSize
                                        });
                                dataRes.AddRange(dataList);
                            }

                            return dataRes;
                        });
                        if (toList.Count() > 0)
                        {
                            foreach (var list in toList)
                            {
                                list.Applicants = await GetNameStaff(list.UserName);
                                listRes.Add(list);
                            }
                        }

                        return listRes;
                    }
                }

                return new List<GetGetLeaveApplicationMemberReportByLeaderResult>();
            }
            catch (Exception ex)
            {
                log.Error("GetLeaveApplicationsMemberByLeader:GetLeaveApplicationsMemberByLeader=" + model +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<GetGetLeaveApplicationMemberReportByLeaderResult>> GetLAMemberReportByLeader(
            GetLAMemberReportByLeaderRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                var fromDateWork = model.FromDateWork != null
                    ? DateTime.ParseExact(model.FromDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var toDateWork = model.ToDateWork != null
                    ? DateTime.ParseExact(model.ToDateWork, "dd/MM/yyyy", CultureInfo.CurrentCulture)
                    : (DateTime?) null;
                var typeLeave = model.TypeLeave;
                var approvedStatus = model.ApprovedStatus;
                
                var resList = new List<GetGetLeaveApplicationMemberReportByLeaderResult>();

                var sql = _timeWorkCommandText.FillterLAMemberReportByLeader;

                sql = sql.Replace("{condition1}",
                    !string.IsNullOrEmpty(model.UserNameMember)
                        ? "AND AssignUser = @UserNameMember ' ELSE N' and AssignUser in ( select id_he_thong as UserName from [TVSI_EMSDB].[dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] where email_man = @EmailMan and trang_thai = 1  GROUP BY id_he_thong )"
                        : "");
                sql = sql.Replace("{condition2}", fromDateWork != null ? " and CreatedDate >= cast(@FromDate as date)" : "");
                sql = sql.Replace("{condition3}", toDateWork != null ? "and CreatedDate <= cast(@ToDate as date)" : " ");
                sql = sql.Replace("{condition4}",model.TypeLeave != null ? "  and TimeWorkType = @TypeLeave " : "");
                sql = sql.Replace("{condition5}",
                    model.ApprovedStatus != null ? " and ConfirmStatus = @ApprovedStatus" : "");
                var dataList = await WithCrmDbConnection(async conn =>
                {
                    var emailMan = model.UserName += TimeWorkConst.EmailMan;
                    return await conn.QueryAsync<GetGetLeaveApplicationMemberReportByLeaderResult>(
                        sql,new
                        {
                            UserNameMember = model.UserNameMember,
                            EmailMan = emailMan,
                            FromDate = fromDateWork,
                            ToDate = toDateWork,
                            TypeLeave = typeLeave,
                            ApprovedStatus = approvedStatus,
                            Start = start,
                            PageSize = model.PageSize
                        });
                });
                foreach (var listUser in dataList)
                {
                    listUser.Applicants = await GetNameStaff(listUser.UserName);
                    resList.Add(listUser);
                }

                return resList;
            }
            catch (Exception ex)
            {
                log.Error("GetLAMemberReportByLeader:GetLAMemberReportByLeader=" + model +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<bool> LeaveApplicationApprovedByLeaderAll(LeaveApplicationApprovedByLeaderAllRequest model)
        {
            try
            {
                var leaveId = model.LeaveId;
                var enumerable = leaveId.ToList();
                if (enumerable.Count() > 0)
                {
                   return  await WithCrmDbConnection(async conn =>
                        {
                                foreach (var leave in enumerable)
                                {
                                    
                                    /*var sql =
                                        " select ConfirmStatus from [TVSI_CRM].[dbo].[TimeWorks] where TimeWorkType = 2 and ConfirmStatus = 1 and TimeWorkID = @TimeWorkID";
                                    var getStatus = await conn.QueryFirstOrDefaultAsync<string>(sql, new
                                    {
                                        @TimeWorkID = leave
                                    });
                                    var status = !string.IsNullOrEmpty(getStatus) ? 2 : 1;*/

                                    var parameters = new DynamicParameters();
                                    parameters.Add("@UserName", model.UserName);
                                    parameters.Add("@StatusLeave", model.StatusLeaveNumber);
                                    parameters.Add("@LeaveId", leave);
                                    parameters.Add("@check", dbType: DbType.Int32,
                                        direction: ParameterDirection.Output);
                                    await conn.ExecuteAsync(
                                        _timeWorkCommandText.StoredApprovedLeaveApplicationsByLeader,
                                        parameters, commandType: CommandType.StoredProcedure);
                                }
                                return true;
                 
                        });
                }
                return false;
            }
            catch (Exception ex)
            {
                log.Error("LeaveApplicationApprovedByLeaderAll:LeaveApplicationApprovedByLeaderAll=" + model +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public Task<string> GetNumBackEarly(string timeIn, string timeOut)
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                log.Error("GetNumBackEarly:GetNumBackEarly=" + timeIn + ":" + timeOut + ":" +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<string> GetNameStaff(string userName)
        {
            try
            {
                return await WithEmsDbConnection(async conn =>
                {
                    var userDomain = await conn.QueryFirstOrDefaultAsync<string>(
                        _timeWorkCommandText.GetUserDomainByUserName, new
                        {
                            @username = userName
                        });
                    return await conn.QueryFirstOrDefaultAsync<string>(
                        _timeWorkCommandText.GetNameStaffEmsDbtttcct, new
                        {
                            @UserName = userName,
                            @userDomain = userDomain
                        });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetNameStaff:GetNameStaff=" + userName + " " +
                          ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}