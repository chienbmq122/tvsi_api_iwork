﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Lead API
    /// </summary>
    public class LeadController  : BaseController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(LeadController));
        
        /// <summary>
        /// 
        /// </summary>
        public ILeadService _leadService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="leadService"></param>
        public LeadController(ILeadService leadService)
        {
            _leadService = leadService;
        }

        /// <summary>
        /// Lấy danh sách lead
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_AC_LeadList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<LeadListResult>))]
        [Note("KH tiềm năng > Danh sách khách hàng", true)]
        public async Task<IHttpActionResult> LD_AC_LeadList(LeadListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "LD_AC_LeadList");
                SetLanguage(lang);

                var retData = await _leadService.GetLeadList(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<LeadListResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "LD_AC_LeadList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        
        }



        /// <summary>
        /// Lấy danh sách hoạt động theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_LA_LeadActivityInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<LeadActivityResult>))]
        [Note("KH tiềm năng > Thông tin chi tiết > Lịch sử hoạt động", true)]
        public async Task<IHttpActionResult> CT_LA_LeadActivityInfoList(LeadActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_LA_LeadActivityInfoList");
                SetLanguage(lang);

                var retData = await _leadService.GetActivityLeadList(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<LeadActivityResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_LA_LeadActivityInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogRequest(log, model, "CT_LA_LeadActivityInfoList" + ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }



        /// <summary>
        /// Lấy danh sách cơ hội theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_LO_LeadOpportunityInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<LeadOpportunityResult>))]
        [Note("KH tiềm năng > Thông tin chi tiết > Lịch sử hoạt động", true)]
        public async Task<IHttpActionResult> CT_LO_LeadOpportunityInfoList(LeadOpportunityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_LO_LeadOpportunityInfoList");
                SetLanguage(lang);
                var retData = await _leadService.GetOpportunityLeadList(model);
                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<LeadOpportunityResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_LO_LeadOpportunityInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogRequest(log, model, "CT_LO_LeadOpportunityInfoList" + ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Thêm mới lead
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_AC_LeadAdd")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("KH tiềm năng > Thêm mới KH tiềm năng", true)]
        public async Task<IHttpActionResult> LD_AC_LeadAdd(LeadAddRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
               
                LogRequestAnonymous(log, model, "LD_AC_LeadAdd");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retData = await _leadService.AddNewLeadInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "LD_AC_LeadAdd");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogExceptionAnonymous(log, model, ex, "LD_AC_LeadAdd");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Danh sách thuộc tính mở rộng của KH tiềm năng
        /// </summary>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("LD_EL_ExLeadInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ExLeadInfoResult>))]
        [Note("DS thuộc tính mở rộng của KH tiềm năng", true)]
        public async Task<IHttpActionResult> LD_EL_ExLeadInfoList(string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequestAnonymous(log, null, "LD_EL_ExLeadInfoList");
                SetLanguage(lang);

                var retData = await _leadService.GetExLeadInfoList();

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<ExLeadInfoResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "LD_EL_ExLeadInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, null, ex, "LD_EL_ExLeadInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy thông tin cơ bản KH tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_GL_GetLeadInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<LeadInfoResult>))]
        [Note("Lấy chi tiết khách hàng theo LeadId")]
        public async Task<IHttpActionResult> LD_GL_GetLeadInfo(LeadInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "LD_GL_GetLeadInfo");
                SetLanguage(lang);

                var retData = await _leadService.GetLeadInfo(model);
                
                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var dataRet = new SuccessModel<LeadInfoResult> { RetData = retData };
                LogResult(log, dataRet, "LD_GL_GetLeadInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "LD_GL_GetLeadInfo");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lấy danh sách lead cho select
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_GS_GetLeadListSelect")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<LeadListUserResponse>))]
        [Note("Lấy danh sách lead cho select")]
        public async Task<IHttpActionResult> LD_GS_GetLeadListSelect(LeadListUserRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "LD_GS_GetLeadListSelect");
                SetLanguage(lang);

                var retData = await _leadService.GetLeadDropBox(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var dataRet = new SuccessModel<LeadListUserResponse> { RetData = retData };
                LogResult(log, dataRet, "LD_GS_GetLeadListSelect");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "LD_GS_GetLeadListSelect");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Update thông tin KH tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_LE_LeadEdit")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("KH tiềm năng > Update KH tiềm năng", true)]
        public async Task<IHttpActionResult> LD_LE_LeadEdit(LeadEditRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {

                LogRequestAnonymous(log, model, "LD_LE_LeadEdit");
                SetLanguage(lang);
                var retData = await _leadService.UpdateLeadInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "LD_LE_LeadEdit");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogExceptionAnonymous(log, model, ex, "LD_LE_LeadEdit");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
        

        /// <summary>
        /// Xóa thông tin KH tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_LE_LeadDelete")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("KH tiềm năng > Xóa thông tin KH tiềm năng", true)]
        public async Task<IHttpActionResult> LD_LE_LeadDelete(LeadDeleteRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {

                LogRequest(log, model, "LD_LE_LeadDelete");
                SetLanguage(lang);
                var retData = await _leadService.DeleteLeadInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "LD_LE_LeadDelete");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "LD_LE_LeadDelete");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// chuyển lead thành account
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("LD_LE_SwitchLeadToCustomer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("chuyển lead thành account", true)]
        public async Task<IHttpActionResult> LD_LE_SwitchLeadToCustomer(LeadUpdateAccount model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {

                LogRequest(log, model, "LD_LE_SwitchLeadToCustomer");
                SetLanguage(lang);
                var retData = await _leadService.SwitchLeadToCustomer(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "LD_LE_SwitchLeadToCustomer");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "LD_LE_SwitchLeadToCustomer");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
    }
}