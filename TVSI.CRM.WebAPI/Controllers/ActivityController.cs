﻿using log4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.CRM.WebAPI.Areas.HelpPage;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Activity;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using TVSI.CRM.WebAPI.Lib.Utilities.EnumExtension;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Activity API
    /// </summary>
    public class ActivityController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(ActivityController));
        private IActivityService _activityService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="activityService"></param>
        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService;
        }

        /// <summary>
        /// Danh sách các hoạt động
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetListActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<ActivityResult>>))]
        [Note("Danh sách hoạt động")]
        public async Task<IHttpActionResult> A_GA_GetListActivity(ActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetListActivity");
                SetLanguage(lang);

                var retData = await _activityService.GetActivity(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<ActivityResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetListActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách các hoạt động theo eventType
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("A_GA_GetListActivityByEventType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<ActivityResult>>))]
        [Note("Danh sách các hoạt động theo eventType")]
        public async Task<IHttpActionResult> A_GA_GetListActivityByEventType(ActivityRequestByEventType model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "A_GA_GetListActivityByEventType");
                SetLanguage(lang);

                var retData = await _activityService.GetActivityByEventType(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<ActivityResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "A_GA_GetListActivityByEventType");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "A_GA_GetListActivityByEventType");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách các hoạt động theo filter
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("A_GA_GetListActivityByFilter")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ActivityResultData>))]
        [Note("Danh sách các hoạt động theo filter")]
        public async Task<IHttpActionResult> A_GA_GetListActivityByFilter(FilterActivity model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "A_GA_GetListActivityByFilter");
                SetLanguage(lang);

                var retData = await _activityService.GetActivityByFiltter(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ActivityResultData>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "A_GA_GetListActivityByFilter");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "A_GA_GetListActivityByFilter");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Lấy chi tiết hoạt động
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetDetailActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ActivityResult>))]
        [Note("Detail hoạt động theo activity id")]
        public async Task<IHttpActionResult> AC_GA_GetDetailActivity(DeleteActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetDetailActivity");
                SetLanguage(lang);

                var retData = await _activityService.GetDetailActivity(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ActivityResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetDetailActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetDetailActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách activity type
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetListActivityType")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<ActivityTypeResult>>))]
        [Note("Danh sách activity type")]
        public async Task<IHttpActionResult> A_GA_GetListActivityType(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetListActivityType");
                SetLanguage(lang);

                var retData = await _activityService.GetActivityType();

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<ActivityTypeResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListActivityType");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetListActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới hoạt động
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_CreateActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới hoạt động")]
        public async Task<IHttpActionResult> A_CA_CreateActivity(CreateActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_CreateActivity");
                SetLanguage(lang);

                if (!string.IsNullOrEmpty(model.Custcode))
                {
                    var checkCustCode = await _activityService.CheckCustCode(model.Custcode, model.Assigned);
                    if (checkCustCode)
                    {
                        return Ok(new ErrorModel
                        {
                            RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                            ErrorMsg = "Custcode không đúng hoặc không được assigner"
                        });
                    }
                }
                var retData = await _activityService.CreateActivity(model);
                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_CreateActivity");

                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_CreateActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Update hoạt động
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_UA_UpdateActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Update hoạt động")]
        public async Task<IHttpActionResult> A_UA_UpdateActivity(UpdateActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_UA_UpdateActivity");
                SetLanguage(lang);
                if (!string.IsNullOrEmpty(model.Custcode))
                {
                    var checkCustCode = await _activityService.CheckCustCode(model.Custcode, model.Assigned);
                    if (checkCustCode)
                    {
                        return Ok(new ErrorModel
                        {
                            RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                            ErrorMsg = "Custcode không đúng hoặc không được asign"
                        });
                    }
                }
                var retData = await _activityService.UpdateActivity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_UA_UpdateActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_UpdateActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }


        /// <summary>
        /// Xoá hoạt động
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_DA_DeleteActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Xoá hoạt động")]
        public async Task<IHttpActionResult> A_DA_DeleteActivity(DeleteActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_DA_DeleteActivity");
                SetLanguage(lang);

                var retData = await _activityService.DeleteActivity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_DA_DeleteActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_DA_DeleteActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Update danh sách Activity
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("A_DA_UpdateListActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Update danh sách Activity")]
        public async Task<IHttpActionResult> A_DA_UpdateListActivity(UpdateListActivity model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "A_DA_UpdateListActivity");
                SetLanguage(lang);

                var retData = await _activityService.UpdateOrDeleteListActivity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "A_DA_UpdateListActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "A_DA_UpdateListActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// confirm or reject danh sách Activity
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("A_DA_ConfirmOrRejectListActivity")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("confirm or reject danh sách Activity")]
        public async Task<IHttpActionResult> A_DA_ConfirmOrRejectListActivity(ConfirmOrRejectRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "A_DA_ConfirmOrRejectListActivity");
                SetLanguage(lang);

                var retData = await _activityService.ConfirmOrRejectActivity(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "A_DA_ConfirmOrRejectListActivity");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "A_DA_ConfirmOrRejectListActivity");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách check activity theo tháng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_CheckDataByMonth")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<ActivityDataMonthCheckResponse>>))]
        [Note("Danh sách check activity theo tháng")]
        public async Task<IHttpActionResult> AC_GA_CheckDataByMonth(ActivityDataMonthRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_CheckDataByMonth");
                SetLanguage(lang);

                var retData = await _activityService.CheckDataByMonth(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<ActivityDataMonthCheckResponse>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_CheckDataByMonth");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_CheckDataByMonth");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách số lượng công việc sự kiện theo tháng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetHisTimeWorkByMonth")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<ActivityDataMonthResponse>>))]
        [Note("Danh sách số lượng công việc sự kiện theo tháng")]
        public async Task<IHttpActionResult> AC_GA_GetHisTimeWorkByMonth(ActivityDataMonthRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetHisTimeWorkByMonth");
                SetLanguage(lang);

                var retData = await _activityService.GetHisTimeWorkStaffByMonth(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<ActivityDataMonthResponse>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetHisTimeWorkByMonth");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetHisTimeWorkByMonth");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
