﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Bond
{
    public class BondRequestOrderListRequest : BaseRequest
    {
        [Description("Từ ngày")]
        public string FromDate { get; set; }

        [Description("Đến ngày")]
        public string ToDate { get; set; }
        [Description("Trạng thái: -1: All")]
        public int Status { get; set; }
        [Description("Từ khóa")]
        public string SearchKey { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }

        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class BondRequestOrderListResult
    {
        [Description("ID của bảng")]
        public long RequestOrderID { get; set; }

        public DateTime CreatedDate { get; set; }
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        
        [Description("Mua/Bán")]
        public string Side { get; set; }

        public string SideName
        {
            get
            {
                return ResourceFile.BondModule.ResourceManager.GetString("Side_" + Side);
            }
        }

        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }

        [Description("Tên khách hàng")]
        public string AccountName { get; set; }
        [Description("Mã Trái phiếu")]
        public string BondCode { get; set; }
        [Description("Khối lượng")]
        public int Volume { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName {

            get
            {
                if (Status < 0)
                {
                    return ResourceFile.BondModule.ResourceManager.GetString("OrderStatus_Negative_" + Math.Abs(Status));
                }

                return ResourceFile.BondModule.ResourceManager.GetString("OrderStatus_" + Status);
            }
            
        }



        [Description("ID CTV")]
        public string ReferID { get; set; }
        [Description("Sale ID")]
        public string SaleID { get; set; }
        [Description("Cờ đánh dấu HĐ đến ngày bán đúng hạn hay ko")]
        public int IsSellMaturity { get; set; }
    }

    public class BondRequestOrderDetailRequest : BaseRequest
    {
        [Description("ID của Request")]
        public long RequestOrderID { get; set; }
    }

    public class BondRequestOrderDetailResult
    {
        public long RequestOrderID { get; set; }
        public string Side { get; set; }

        public string SideName
        {
            get { return ResourceFile.BondModule.ResourceManager.GetString("Side_" + Side); }
        }

        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string Address { get; set; }
        public string ContractNo { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? CouponDate { get; set; }
        public string BondCode { get; set; }
        public int? Volume { get; set; }
        public decimal? Rate { get; set; }
        public int? PaymentMethod { get; set; }

        public string PaymentMethodName
        {
            get
            {
                return ResourceFile.BondModule.ResourceManager.GetString("PaymentMethod_" + PaymentMethod);
            }
        }

        public string BankAccountName { get; set; }
        public int? NavConfigID { get; set; }
        public string SaleID { get; set; }
        public string ReferID { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }

        public string StatusName
        {
            get
            {
                if (Status >= 0)
                return ResourceFile.BondModule.ResourceManager.GetString("OrderStatus_" + Status);

                if (Status == -1)
                    return ResourceFile.BondModule.OrderStatus_Negative_1;

                return Status + "";

            }
        }

        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CustBankID { get; set; }

        public BondInfo BondInfo { get; set; }
        public int CustType { get; set; }
        public string NavName { get; set; }

        public string FirstTradeDate { get; set; }
        public decimal TotalNav { get; set; }

        public decimal IRRCompany { get; set; }
        public decimal RateBasic { get; set; }
        public decimal NavContractActive { get; set; }
        public decimal NavContractProcess { get; set; }
        public decimal NavContractQueue { get; set; }
    }

    public class BondInfo
    {
        public int BondID { get; set; }
        public string BondCode { get; set; }
        public string BondName { get; set; }
        public int Term { get; set; }
        public decimal CouponRate { get; set; }
        public string Issuer { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal ParValue { get; set; }

        public DateTime IssueDate { get; set; }
        public string IssuerGroup { get; set; }
        public int BondType { get; set; }

        public string ProfesMsg { get; set; }
    }

    public class BankInfo
    {
        public string BankName { get; set; }
        public string SubBranchName { get; set; }
        public string BankAccountName { get; set; }
        public string BankAccount { get; set; }
    }

    public class BondOrderDeleteRequest : BaseRequest
    {
        public long RequestOrderID { get; set; }
    }

    public class SellPriceRequest : BaseRequest
    {
        [Description("Số hợp đồng")]
        public string ContractNo { get; set; }
        [Description("Khối lượng")]
        public int Volume { get; set; }

        [Description("Ngày bán")]
        public string SellDate { get; set; }
    }

    public class SellPriceResult
    {
        public decimal SellPrice { get; set; }
        public decimal SellAmount { get; set; }
    }
}
