﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Bond
{
    public class BondSourceShareModel
    {
        public int SharedID { get; set; }
        public string IssuerCode { get; set; }
        public int? TeamId { get; set; }
        public DateTime? TradeDate { get; set; }
        public long StartVolume { get; set; }
        public long Volume { get; set; }
        public int Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public byte[] RowVersion { get; set; }
    }

    public class BondRequestOrderDetail
    {
        public long RequestOrderID { get; set; }
        public string Side { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string Address { get; set; }
        public string ContractNo { get; set; }
        public DateTime? TradeDate { get; set; }
        public DateTime? CouponDate { get; set; }
        public string BondCode { get; set; }
        public int? Volume { get; set; }
        public decimal? Rate { get; set; }
        public int? PaymentMethod { get; set; }
        public string BankAccountName { get; set; }
        public int? NavConfigID { get; set; }
        public string SaleID { get; set; }
        public string ReferID { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? CustBankID { get; set; }

        public BondInfo BondInfo { get; set; }
        public int CustType { get; set; }
        public string NavName { get; set; }

        public string FirstTradeDate { get; set; }
        public decimal TotalNav { get; set; }

        public decimal IRRCompany { get; set; }
        public decimal RateBasic { get; set; }
        public decimal NavContractActive { get; set; }
        public decimal NavContractProcess { get; set; }
        public decimal NavContractQueue { get; set; }
    }
}
