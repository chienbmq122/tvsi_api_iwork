﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Contact;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    public class ContactController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(OpportunityController));
        private IContactService _contactService;

        /// <summary>
        /// Controller Comment
        /// </summary>
        /// <param name="contactService"></param>
        public ContactController(IContactService contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// Danh sách contact staff
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CS_GC_GetListContactStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContactStaffResponse>))]
        [Note("Danh sách contact staff")]
        public async Task<IHttpActionResult> CS_GC_GetListContactStaff(ContactStaffRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CS_GC_GetListContactStaff");
                SetLanguage(lang);

                var retData = await _contactService.GetListContactStaff(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ContactStaffResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CS_GC_GetListContactStaff");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CS_GC_GetListContactStaff");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách contact customer
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetListContactCustomer")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContactCustomerResponse>))]
        [Note("Danh sách contact staff")]
        public async Task<IHttpActionResult> CC_GC_GetListContactCustomer(ContactStaffRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetListContactCustomer");
                SetLanguage(lang);

                var retData = await _contactService.GetListContactCust(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ContactCustomerResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetListContactCustomer");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetListContactCustomer");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách contact staff info
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetInfoContactStaff")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContactInfoStaff>))]
        [Note("contact staff info")]
        public async Task<IHttpActionResult> CC_GC_GetInfoContactStaff(ContactinfoStaffRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetInfoContactStaff");
                SetLanguage(lang);

                var retData = await _contactService.GetInfoContactStaff(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ContactInfoStaff>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetInfoContactStaff");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetInfoContactStaff");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách contact staff info
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetInfoContactLead")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContactInfoLead>))]
        [Note("contact staff info")]
        public async Task<IHttpActionResult> CC_GC_GetInfoContactLead(ContactinfoLeadRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetInfoContactLead");
                SetLanguage(lang);

                var retData = await _contactService.GetInfoContactLead(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ContactInfoLead>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetInfoContactLead");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetInfoContactLead");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách contact staff info
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetInfoContactCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ContactInfoCustomer>))]
        [Note("contact staff info")]
        public async Task<IHttpActionResult> CC_GC_GetInfoContactCust(ContactinfoCustRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetInfoContactCust");
                SetLanguage(lang);

                var retData = await _contactService.GetInfoContactCust(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<ContactInfoCustomer>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetInfoContactCust");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetInfoContactCust");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
