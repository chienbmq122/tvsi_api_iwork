﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface ICustCommandText
    {
        /// <summary>
        /// Get list customer 
        /// </summary>
        string GetListCust { get; }
        /// <summary>
        /// Get info customer 
        /// </summary>
        string GetInfoCust { get; }
        /// <summary>
        /// Lấy số điện thoại đăng ký SMS, CC
        /// </summary>
        string GetPhoneCust { get; }
        /// <summary>
        /// Lấy danh sách tài khoản ngân hàng đăng ký chuyển tiền
        /// </summary>
        string GetBankTransfer { get; }
        //string GetCountCust { get; }
        /// <summary>
        /// Lấy danh mục đầu tư
        /// </summary>
        string GetAccPorfolio { get; }
        /// <summary>
        /// Danh sách lệnh đặt
        /// </summary>
        string GetOrderInfo { get; }
        /// <summary>
        /// Danh sách lệnh khớp
        /// </summary>
        string GetDealInfo { get; }
        /// <summary>
        /// Danh sách thuộc tính mở rộng khách hàng
        /// </summary>
        string GetExCustInfo { get; }
        /// <summary>
        /// Danh sách hoạt động
        /// </summary>
        string GetActivityInfo { get; }
        /// <summary>
        /// Danh sách cơ hội
        /// </summary>
        string GetOpportunityInfo { get; }
        /// <summary>
        /// Update thông tin khách hàng 
        /// </summary>
        string UpdateCustInfo { get; }
        /// <summary>
        /// Xóa thông tin thông tin mở rộng
        /// </summary>
        string DeleteExCustInfo { get; }
        /// <summary>
        /// Thêm mới thông tin mở rộng theo KH
        /// </summary>
        string PostCustExtInfo { get; }
        /// <summary>
        /// Lấy danh sách cổ đông lớn
        /// </summary>
        string GetListShareHolder { get; }

        /// <summary>
        /// Danh sách khách hàng select
        /// </summary>
        string GetListCustSelect { get; }

        /// <summary>
        /// Đếm danh sách khách hàng select
        /// </summary>
        string GetListCustSelectCount { get; }
        /// <summary>
        /// Thêm mới cổ đông lớn
        /// </summary>
        string PostShareHolderInfo { get; }
        /// <summary>
        /// View chi tiết thông tin cổ đông
        /// </summary>
        string ViewShareHolderInfo { get; }
        
        /// <summary>
        /// thông tin chi tiết khách hàng
        /// </summary>
        string GetCustomerInfo { get; }
        
        /// <summary>
        /// thông tin đầu tư khách hàng
        /// </summary>
        string GetInvestmentInfo { get; }  
        
        /// <summary>
        /// thông tin đầu tư khách hàng
        /// </summary>
        string GetCustomerInfoSupport { get; }

        /// <summary>
        /// thông tin mở rộng khách hàng
        /// </summary>
        string GetListAttributeCus { get; }


    }
}
