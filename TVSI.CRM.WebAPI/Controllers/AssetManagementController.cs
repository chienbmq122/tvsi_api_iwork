﻿using log4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.AssetManagement;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// AssetManagement API
    /// </summary>
    public class AssetManagementController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(ActivityController));
        private IAssetService _assetService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="assetService"></param>
        public AssetManagementController(IAssetService assetService)
        {
            _assetService = assetService;
        }

        /// <summary>
        /// Lấy quyền user
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RU_GS_GetRoleUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<RoleUserAsset>))]
        [Note("Lấy quyền user")]
        public async Task<IHttpActionResult> RU_GS_GetRoleUser(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RU_GS_GetRoleUser");
                SetLanguage(lang);

                var retData = await _assetService.GetRoleUser(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<RoleUserAsset>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "RU_GS_GetRoleUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RU_GS_GetRoleUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách trạng thái
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AM_GS_GetListStateAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetState>>))]
        [Note("Danh sách trạng thái")]
        public async Task<IHttpActionResult> AM_GS_GetListStateAsset(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AM_GS_GetListStateAsset");
                SetLanguage(lang);

                var retData = await _assetService.GetListStateAsset();

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetState>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AM_GS_GetListStateAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AM_GS_GetListStateAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách phòng ban
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("DE_GL_GetListDepartment")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<DepartmentManagementAsset>>))]
        [Note("Danh sách phòng ban")]
        public async Task<IHttpActionResult> DE_GL_GetListDepartment(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "DE_GL_GetListDepartment");
                SetLanguage(lang);

                var retData = await _assetService.GetListDepartment();

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<DepartmentManagementAsset>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "DE_GL_GetListDepartment");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "DE_GL_GetListDepartment");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách user phòng ban
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("US_GL_GetListUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<UserManagementAsset>>))]
        [Note("Danh sách user phòng ban")]
        public async Task<IHttpActionResult> US_GL_GetListUser(UserManagementAssetRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "US_GL_GetListUser");
                SetLanguage(lang);

                var retData = await _assetService.GetListUser(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<UserManagementAsset>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "US_GL_GetListUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "US_GL_GetListUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách loại tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GT_GetListTypeAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetType>>))]
        [Note("Danh sách loại tài sản")]
        public async Task<IHttpActionResult> AT_GT_GetListTypeAsset(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GT_GetListTypeAsset");
                SetLanguage(lang);

                var retData = await _assetService.GetListTypeAsset();

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetType>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GT_GetListTypeAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GT_GetListTypeAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách tài sản theo loại
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GA_GetListAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListCommonResponse>))]
        [Note("Danh sách tài sản theo loại")]
        public async Task<IHttpActionResult> AT_GA_GetListAsset(AssetListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GA_GetListAsset");
                SetLanguage(lang);

                var retData = await _assetService.GetListAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListCommonResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GA_GetListAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GA_GetListAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách tài sản chờ thu hồi
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetListAssetConfirm")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListResponse>))]
        [Note("Danh sách tài sản chờ thu hồi")]
        public async Task<IHttpActionResult> AC_GA_GetListAssetConfirm(AssetListConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetListAssetConfirm");
                SetLanguage(lang);

                var retData = await _assetService.GetListConfirmAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListAssetConfirm");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetListAssetConfirm");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_CreateAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới thiết bị")]
        public async Task<IHttpActionResult> AC_CA_CreateAsset(CreateAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_CreateAsset");
                SetLanguage(lang);

                var retData = await _assetService.CreateAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_CreateAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_CreateAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Sửa thông tin thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_UpdateAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sửa thông tin thiết bị")]
        public async Task<IHttpActionResult> AC_CA_UpdateAsset(UpdateAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_UpdateAsset");
                SetLanguage(lang);

                var retData = await _assetService.UpdateAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_UpdateAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_UpdateAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xóa thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_DeleteAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Xóa thiết bị")]
        public async Task<IHttpActionResult> AC_CA_DeleteAsset(DeleteAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_DeleteAsset");
                SetLanguage(lang);

                var retData = await _assetService.DeleteAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_DeleteAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_DeleteAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Chi tiết thông tin thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetDetailAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetDetailModel>))]
        [Note("Chi tiết thông tin thiết bị")]
        public async Task<IHttpActionResult> AC_GA_GetDetailAsset(DeleteAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetDetailAsset");
                SetLanguage(lang);

                var retData = await _assetService.GetDetailAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetDetailModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetDetailAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetDetailAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lọc tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_FilteAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListCommonResponse>))]
        [Note("Lọc tài sản")]
        public async Task<IHttpActionResult> AC_GA_FilteAsset(FilteAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_FilteAsset");
                SetLanguage(lang);

                var retData = await _assetService.FilterAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListCommonResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_FilteAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_FilteAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách tài sản cá nhân
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AU_GA_GetListAssetUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListCommonResponse>))]
        [Note("Danh sách tài sản cá nhân")]
        public async Task<IHttpActionResult> AU_GA_GetListAssetUser(AssetListConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AU_GA_GetListAssetUser");
                SetLanguage(lang);

                var retData = await _assetService.GetListAssetUser(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListCommonResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AU_GA_GetListAssetUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AU_GA_GetListAssetUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách lịch sử tài sản cá nhân
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AU_GH_GetHistoryAssetUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListResponse>))]
        [Note("Danh sách lịch sử tài sản cá nhân")]
        public async Task<IHttpActionResult> AU_GH_GetHistoryAssetUser(AssetListConfirmRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AU_GH_GetHistoryAssetUser");
                SetLanguage(lang);

                var retData = await _assetService.GetHistoryAssetUser(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AU_GH_GetHistoryAssetUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AU_GH_GetHistoryAssetUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// danh sách thiết bị đi kèm select
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetListAssetChildSeclect")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetChildModelSelect>>))]
        [Note("danh sách thiết bị đi kèm select")]
        public async Task<IHttpActionResult> AC_GA_GetListAssetChild(AssetChildModelSelectReq model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetListAssetChildSeclect");
                SetLanguage(lang);

                var retData = await _assetService.GetListAssetChildSeclect(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetChildModelSelect>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListAssetChildSeclect");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetListAssetChildSeclect");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// thông tin thiết bị bị kèm
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetListAssetChild")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetChildModel>>))]
        [Note("thông tin thiết bị bị kèm")]
        public async Task<IHttpActionResult> AC_GA_GetListAssetChild(RequestAssetChild model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetListAssetChild");
                SetLanguage(lang);

                var retData = await _assetService.GetListAssetChild(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetChildModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetListAssetChild");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetListAssetChild");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Gán tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_AsignAssetChild")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Gán tài sản")]
        public async Task<IHttpActionResult> AC_CA_AsignAssetChild(AginAssetChild model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_AsignAssetChild");
                SetLanguage(lang);

                var retData = await _assetService.AsignAssetChild(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_AsignAssetChild");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_AsignAssetChild");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Gỡ tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_RemoveAssetChild")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Gỡ tài sản")]
        public async Task<IHttpActionResult> AC_CA_RemoveAssetChild(RemoveAssetChild model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_RemoveAssetChild");
                SetLanguage(lang);

                var retData = await _assetService.RemoveAssetChild(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_RemoveAssetChild");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_RemoveAssetChild");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// thông tin lịch sử mượn trả thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_GetHistoryReceiveAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetManagement>>))]
        [Note("thông tin lịch sử mượn trả thiết bị")]
        public async Task<IHttpActionResult> AC_GA_GetHistoryReceiveAsset(DeleteAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_GetHistoryReceiveAsset");
                SetLanguage(lang);

                var retData = await _assetService.GetHistoryReceiveAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetManagement>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_GetHistoryReceiveAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_GetHistoryReceiveAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới mượn thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_CreateManagement")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới mượn thiết bị")]
        public async Task<IHttpActionResult> AC_CA_CreateManagement(RequestCreateManagement model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_CreateManagement");
                SetLanguage(lang);

                var retData = await _assetService.CreateManagement(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_CreateManagement");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_CreateManagement");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Sửa thông tin mượn thiết bị
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_CA_UpdateManagement")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sửa thông tin mượn thiết bị")]
        public async Task<IHttpActionResult> AC_CA_UpdateManagement(RequestUpdateManagement model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_CA_UpdateManagement");
                SetLanguage(lang);

                var retData = await _assetService.UpdateManagement(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AC_CA_UpdateManagement");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_CA_UpdateManagement");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lọc mượn trả tài sản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GA_FilterManagementAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetListResponse>))]
        [Note("Lọc mượn trả tài sản")]
        public async Task<IHttpActionResult> AC_GA_FilterManagementAsset(FliterManagementAsset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GA_FilterManagementAsset");
                SetLanguage(lang);

                var retData = await _assetService.FilterManagementAsset(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetListResponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GA_FilterManagementAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GA_FilterManagementAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Chi tiết kiểm kê
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_GA_GetInspectionDetail")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AssetInspectionDetailModel>))]
        [Note("Chi tiết kiểm kê")]
        public async Task<IHttpActionResult> IA_GA_GetInspectionDetail(InspectionDeleteRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_GA_GetInspectionDetail");
                SetLanguage(lang);

                var retData = await _assetService.GetInspectionDetail(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AssetInspectionDetailModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "IA_GA_GetInspectionDetail");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_GA_GetInspectionDetail");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lịch sử kiểm kê mới nhất theo phòng ban
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_GA_GetListInspection")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetInspectionModel>>))]
        [Note("Lịch sử kiểm kê mới nhất theo phòng ban")]
        public async Task<IHttpActionResult> IA_GA_GetListInspection(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_GA_GetListInspection");
                SetLanguage(lang);

                var retData = await _assetService.GetListInspection();

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetInspectionModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "IA_GA_GetListInspection");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_GA_GetListInspection");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Lịch sử kiểm kê
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_GA_GetHistoryInspection")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<AssetInspectionModel>>))]
        [Note("Lịch sử kiểm kê")]
        public async Task<IHttpActionResult> IA_GA_GetHistoryInspection(RequestAssetInspectionModel model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_GA_GetHistoryInspection");
                SetLanguage(lang);

                var retData = await _assetService.GetHistoryInspection(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<List<AssetInspectionModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "IA_GA_GetHistoryInspection");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_GA_GetHistoryInspection");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới kiểm kê
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_CA_CreateInspectionAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới kiểm kê")]
        public async Task<IHttpActionResult> IA_CA_CreateInspectionAsset(InspectionCreateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_CA_CreateInspectionAsset");
                SetLanguage(lang);

                var retData = await _assetService.CreateInspectionAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "IA_CA_CreateInspectionAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_CA_CreateInspectionAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Sửa thông tin kiểm kê
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_CA_UpdateInspectionAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Sửa thông tin kiểm kê")]
        public async Task<IHttpActionResult> IA_CA_UpdateInspectionAsset(InspectionUpdateRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_CA_UpdateInspectionAsset");
                SetLanguage(lang);

                var retData = await _assetService.UpdateInspectionAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "IA_CA_UpdateInspectionAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_CA_UpdateInspectionAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Xóa thông tin kiểm kê
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("IA_CA_DeleteInspectionAsset")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Xóa thông tin kiểm kê")]
        public async Task<IHttpActionResult> IA_CA_DeleteInspectionAsset(InspectionDeleteRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "IA_CA_DeleteInspectionAsset");
                SetLanguage(lang);

                var retData = await _assetService.DeleteInspectionAsset(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "IA_CA_DeleteInspectionAsset");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "IA_CA_DeleteInspectionAsset");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
