﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Lead
{
    public class LeadAttrinbuteViewModel
    {
        public int AttributeID { get; set; }
        public int LeadID { get; set; }
        public string AttributeName { get; set; }
        public string AttributeLabel { get; set; }
        public string Value { get; set; }
        public int AttributeGroupID { get; set; }
        public int AttributeType { get; set; }
        public int Level { get; set; }
        public int? Priority { get; set; }
        // public string CreatedBy { get; set; }
    }

    public class LeadAttributeRequest : BaseRequest
    {
        [Description("LeadID")]
        [Required]
        public int LeadID { get; set; }
    }

    public class LeadAttrinbuteCreate : BaseRequest
    {
        [Description("AttributeID")]
        [Required]
        public int AttributeID { get; set; }
        [Description("LeadID")]
        [Required]
        public int LeadID { get; set; }
        [Description("Giá tri của thuộc tính")]
        [Required]
        public string Value { get; set; }
        [Description("Đô ưu tiên")]
        [Required]
        public int Priority { get; set; }
    }

    public class LeadAttrinbuteDelete : BaseRequest
    {
        [Description("AttributeID")]
        [Required]
        public int AttributeID { get; set; }
        [Description("LeadID")]
        [Required]
        public int LeadID { get; set; }
    }
}
