﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Activity
{
    public class ActivityRequest : BaseRequest
    {
        [Description("Ngày")]
        [Required]
        public string DateIndex { get; set; }
    }
    public class ActivityRequestByEventType : BaseRequest
    {
        [Description("Loại event: 0 = get all, 1 = sự kiện, 2 = công việc")]
        [Required]
        public int EventType { get; set; }
        [Description("Ngày")]
        [Required]
        public string DateIndex { get; set; }
    } 

    public class ActivityTypeResult
    {
        [Description("Id activity type")]
        public int ActivityTypeID { get; set; }
        [Description("Tên trạng thái")]
        public string ActivityTypeName { get; set; }
    }

    public class CreateActivityRequest : BaseRequest
    {
        [Description("Đối tác")]
        public int LeadId { get; set; }
        [Description("Code khách hàng")]
        public string Custcode { get; set; }
        [Description("Tiêu đề hoạt động")]
        public string Title { get; set; }
        [Description("Phân loại hoạt động")]
        public int Type { get; set; }
        [Description("Phân loại event")]
        public int EventType { get; set; }
        [Description("Ngày bắt đầu format dd/MM/yyyy HH:mm:ss")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc format dd/MM/yyyy HH:mm:ss")]
        public string EndDate { get; set; }
        [Description("Địa điểm")]
        public string Location { get; set; }
        [Description("Mức độ ưu tiên hoạt động")]
        public int Priority { get; set; }
        [Description("Tình trạng")]
        public int Status { get; set; }
        [Description("Người duyệt (CF Only) Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Approved { get; set; }
        [Description("Assigned Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Assigned { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        [Description("Lý do")]
        public int ReasonType { get; set; }
    }

    public class ActivityResult
    {
        [Description("ID activity")]
        public int ActivityId { get; set; }
        [Description("Đối tác")]
        public int LeadId { get; set; }
        [Description("Tên đối tác")]
        public string LeadName { get; set; }
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        [Description("Tên khách hàng")]
        public string CustName { get; set; }
        [Description("Tiêu đề hoạt động")] 
        public string Title { get; set; }
        [Description("Phân loại hoạt động")]
        public int Type { get; set; }
        [Description("Phân loại hoạt động name")]
        public string TypeName
        {
            get
            {
                switch (Type)
                {
                    case 1:
                        return "Hẹn trực tiếp";
                    case 2:
                        return "Gọi điện thoại";
                    case 3:
                        return "Gửi Email";
                    case 5:
                        return "Nghỉ phép";
                    case 4:
                    default:
                        return "Khác";
                }
            }
        }
        [Description("Phân loại event")]
        public int EventType { get; set; }
        [Description("Phân loại eventtype name")]
        public string EventTypeName {
            get { return ResourceFile.ActivityEventType.ResourceManager.GetString("EventType_" + Priority); }
        }
        [Description("Ngày bắt đầu")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc")]
        public string EndDate { get; set; }
        [Description("Địa điểm")]
        public string Location { get; set; }
        [Description("Mức độ ưu tiên hoạt động")]
        public int Priority { get; set; }
        [Description("Tên mức độ ưu tiên")]
        public string PriorityName
        {
            get { return ResourceFile.ActivityPriority.ResourceManager.GetString("Priority_" + Priority); }
        }
        [Description("Tình trạng")]
        public int Status { get; set; }
        [Description("Tên tình trạng")]
        public string StatusName {
            get { return ResourceFile.ActivityStatus.ResourceManager.GetString("Status_" + Status); }
        }
        [Description("Người duyệt Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Approved { get; set; }
        [Description("Assigned Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Assigned { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        public int ConfirmStatus { get; set; }
        public string ConfirmBy { get; set; }
        public string ConfirmDate { get; set; }
        public string ConfirmCode { get; set; }
        public string RejectCode { get; set; }
        public string RejectReason { get; set; }
        public int ReasonType { get; set; }
    }

    public class ActivityResultData
    {
        [Description("Tổng số item")]
        public int totalItem { get; set; }
        [Description("Danh sách items")]
        public IEnumerable<ActivityResult> Items { get; set; }
    }

    public class FilterActivity : BaseRequest
    {
        [Description("FROM DATE")]
        [Required]
        public string FromDate { get; set; }
        [Description("TO DATE")]
        [Required]
        public string ToDate { get; set; }
        [Description("Loại event: 0 = get all, 1 = sự kiện, 2 = công việc")]
        public int EventType { get; set; }
        [Description("người phụ trách")]
        public string Assigned { get; set; }
        [Description("Mức độ ưu tiên")]
        public int Priority { get; set; }
        [Description("Status")]
        public int Status { get; set; }
        [Description("Trang")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi 1 trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class UpdateListActivity : BaseRequest
    {
        [Description("Danh sách activityid")]
        [Required]
        public IEnumerable<int> Ids { get; set; }
        [Description("Status nếu update nhiều thì truyền trạng thái, nếu xóa nhiều thì truyền 99")]
        [Required]
        public int Status { get; set; }
    }

    public class UpdateActivityRequest : BaseRequest
    {
        [Description("ID hoạt động")]
        public int ActivityId { get; set; }
        [Description("Đối tác")]
        public int LeadId { get; set; }
        [Description("Code khách hàng")]
        public string Custcode { get; set; }
        [Description("Tiêu đề hoạt động")]
        public string Title { get; set; }
        [Description("Phân loại hoạt động")]
        public int Type { get; set; }
        [Description("Phân loại event")]
        public int EventType { get; set; }
        [Description("Ngày bắt đầu")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc")]
        public string EndDate { get; set; }
        [Description("Địa điểm")]
        public string Location { get; set; }
        [Description("Mức độ ưu tiên hoạt động")]
        public int Priority { get; set; }
        [Description("Tình trạng")]
        public int Status { get; set; }
        [Description("Người duyệt Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Approved { get; set; }
        [Description("Assigned Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string Assigned { get; set; }
        [Description("Mô tả")]
        public string Detail { get; set; }
        [Description("Lý do")]
        public int ReasonType { get; set; }
    }

    public class DeleteActivityRequest : BaseRequest
    {
        [Description("ID hoạt động")]
        [Required]
        public int ActivityId { get; set; }
    }

    public class ActivityDatabase
    {
        public int ActivityID { get; set; }
        public int EventType { get; set; }
        public string ActivityName { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public int ActivityType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public string Location { get; set; }
        public int Recurrence { get; set; }
        public int Notification { get; set; }
        public string Description { get; set; }
        public int LeadID { get; set; }
        public string LeadName { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string  UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public int ConfirmStatus { get; set; }
        public string ConfirmBy { get; set; }
        public string ConfirmDate { get; set; }
        public string ConfirmCode { get; set; }
        public string RejectCode { get; set; }
        public string RejectReason { get; set; }
        public int ReasonType { get; set; }
    }

    public class CFActivityDatabase
    {
        [Description("ID activity")]
        public int ActivityID { get; set; }
        public int EventType { get; set; }
        public string ActivityName { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public string TvsiUserName { get; set; }
        public string DirectContact { get; set; }
        public int ActivityType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public string Location { get; set; }
        public int Recurrence { get; set; }
        public int Notification { get; set; }
        public string Description { get; set; }
        public int LeadID { get; set; }
        public string LeadName { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public int ConfirmStatus { get; set; }
        public string ConfirmBy { get; set; }
        public string ConfirmDate { get; set; }
        public string ConfirmCode { get; set; }
        public string RejectCode { get; set; }
        public string RejectReason { get; set; }

    }

    public class BOActivityDatabase
    {
        public int BOActivityID { get; set; }
        public int EventType { get; set; }
        public string ActivityName { get; set; }
        public int BranchID { get; set; }
        public string AssignUser { get; set; }
        public int ActivityType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Status { get; set; }
        public int Priority { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedDate { get; set; }
        public int ConfirmStatus { get; set; }
        public string ConfirmBy { get; set; }
        public string ConfirmDate { get; set; }
        public string ConfirmCode { get; set; }
        public string RejectCode { get; set; }
        public string RejectReason { get; set; }
    }

    public class ConfirmOrRejectRequest : BaseRequest
    {
        [Description("Danh sách activityid")]
        [Required]
        public IEnumerable<int> Ids { get; set; }
        [Description("Confirm = 1, reject = 2")]
        [Required]
        public int Status { get; set; }
    }
}
