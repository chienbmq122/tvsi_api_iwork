﻿namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class AccountCommandText : IAccountCommandText
    {
        public string GetAccountBySaleId => @"
		select ho_so_mo_tk_id CustId,ho_ten_kh as FullName, ma_khach_hang as CodeCust, 
		  case 
		   when trang_thai_ho_so = 000 then N'Chờ xử lý'
		   when trang_thai_ho_so = 100 then N'Chờ duyệt'
		   when trang_thai_ho_so = 101 then N'Nợ hồ sơ'
		   when trang_thai_ho_so = 200 then N'Hoàn thành'
		   when trang_thai_ho_so = 199 or trang_thai_ho_so = 299 then N'Từ chối' end as ProfileStatusName,
		   trang_thai_ho_so as ProfileStatusNum,
		   case
		    when loai_ho_so = 1 then N'Thêm mới'
			when loai_ho_so = 2 then N'Bổ sung'
			end as TypeProfile,
			mo_ta as TypeContract,
			ngay_tao as CreateDateOn,
			case 
			when trang_thai_tk = 000 then N'Chờ xử lý'
			when trang_thai_tk = 001 then N'Chờ BU_MAN duyệt'
			when trang_thai_tk = 002 then N'Chờ FS duyệt'
			when trang_thai_tk = 099 then N'BU_MAN từ chối'
			when trang_thai_tk = 100 then N'Đang xử lý'
			when trang_thai_tk = 102 then N'Kích hoạt(KSV SS)'
			when trang_thai_tk = 200 then N'Hoàn thành'
			when trang_thai_tk = 399 then N'FS từ chối'
			when trang_thai_tk = 199 or trang_thai_tk = 299 then N'Từ chối'
			when trang_thai_tk = 999 then N'Đã hủy' end as  ActivatedName,
			trang_thai_tk as ActivatedNum
		    from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where nguoi_tao = @UserName 
		    ORDER BY ho_so_mo_tk_id DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string GetAccountBySaleIdDirector => @"
		select ho_so_mo_tk_id CustId,ho_ten_kh as FullName, ma_khach_hang as CodeCust, 
		  case 
		   when trang_thai_ho_so = 000 then N'Chờ xử lý'
		   when trang_thai_ho_so = 100 then N'Chờ duyệt'
		   when trang_thai_ho_so = 101 then N'Nợ hồ sơ'
		   when trang_thai_ho_so = 200 then N'Hoàn thành'
		   when trang_thai_ho_so = 199 or trang_thai_ho_so = 299 then N'Từ chối' end as ProfileStatusName,
		   trang_thai_ho_so as ProfileStatusNum,
		   case
		    when loai_ho_so = 1 then N'Thêm mới'
			when loai_ho_so = 2 then N'Bổ sung'
			end as TypeProfile,
			mo_ta as TypeContract,
			ngay_tao as CreateDateOn,
			case 
			when trang_thai_tk = 000 then N'Chờ xử lý'
			when trang_thai_tk = 001 then N'Chờ BU_MAN duyệt'
			when trang_thai_tk = 002 then N'Chờ FS duyệt'
			when trang_thai_tk = 099 then N'BU_MAN từ chối'
			when trang_thai_tk = 100 then N'Đang xử lý'
			when trang_thai_tk = 102 then N'Kích hoạt(KSV SS)'
			when trang_thai_tk = 200 then N'Hoàn thành'
			when trang_thai_tk = 399 then N'FS từ chối'
			when trang_thai_tk = 199 or trang_thai_tk = 299 then N'Từ chối'
			when trang_thai_tk = 999 then N'Đã hủy' end as  ActivatedName,
			trang_thai_tk as ActivatedNum
		    from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where branch_id in @branch_id
		     ORDER BY ho_so_mo_tk_id DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        
        public string GetAccountBySaleIdLeader => @"
		select ho_so_mo_tk_id CustId,ho_ten_kh as FullName, ma_khach_hang as CodeCust, 
		  case 
		   when trang_thai_ho_so = 000 then N'Chờ xử lý'
		   when trang_thai_ho_so = 100 then N'Chờ duyệt'
		   when trang_thai_ho_so = 101 then N'Nợ hồ sơ'
		   when trang_thai_ho_so = 200 then N'Hoàn thành'
		   when trang_thai_ho_so = 199 or trang_thai_ho_so = 299 then N'Từ chối' end as ProfileStatusName,
		   trang_thai_ho_so as ProfileStatusNum,
		   case
		    when loai_ho_so = 1 then N'Thêm mới'
			when loai_ho_so = 2 then N'Bổ sung'
			end as TypeProfile,
			mo_ta as TypeContract,
			ngay_tao as CreateDateOn,
			case 
			when trang_thai_tk = 000 then N'Chờ xử lý'
			when trang_thai_tk = 001 then N'Chờ BU_MAN duyệt'
			when trang_thai_tk = 002 then N'Chờ FS duyệt'
			when trang_thai_tk = 099 then N'BU_MAN từ chối'
			when trang_thai_tk = 100 then N'Đang xử lý'
			when trang_thai_tk = 102 then N'Kích hoạt(KSV SS)'
			when trang_thai_tk = 200 then N'Hoàn thành'
			when trang_thai_tk = 399 then N'FS từ chối'
			when trang_thai_tk = 199 or trang_thai_tk = 299 then N'Từ chối'
			when trang_thai_tk = 999 then N'Đã hủy' end as  ActivatedName,
			trang_thai_tk as ActivatedNum
		    from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where nguoi_tao in @UserName
		    ORDER BY ho_so_mo_tk_id DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetAccountFillBySaleId => @"  select ho_ten_kh as FullName, ma_khach_hang as CodeCust, 
		  case 
		   when trang_thai_ho_so = 000 then N'Chờ xử lý'
		   when trang_thai_ho_so = 100 then N'Chờ duyệt'
		   when trang_thai_ho_so = 101 then N'Nợ hồ sơ'
		   when trang_thai_ho_so = 200 then N'Hoàn thành'
		   when trang_thai_ho_so = 199 or trang_thai_ho_so = 299 then N'Từ chối' end as ProfileStatusName,
		   trang_thai_ho_so as ProfileStatusNum,
		   case
		    when loai_ho_so = 1 then N'Thêm mới'
			when loai_ho_so = 2 then N'Bổ sung'
			end as TypeProfile,
			mo_ta as TypeContract,
			ngay_tao as CreateDateOn,
			case 
			when trang_thai_tk = 000 then N'Chờ xử lý'
			when trang_thai_tk = 001 then N'Chờ BU_MAN duyệt'
			when trang_thai_tk = 002 then N'Chờ FS duyệt'
			when trang_thai_tk = 099 then N'BU_MAN từ chối'
			when trang_thai_tk = 100 then N'Đang xử lý'
			when trang_thai_tk = 102 then N'Kích hoạt(KSV SS)'
			when trang_thai_tk = 200 then N'Hoàn thành'
			when trang_thai_tk = 399 then N'FS từ chối'
			when trang_thai_tk = 199 or trang_thai_tk = 299 then N'Từ chối'
			when trang_thai_tk = 999 then N'Đã hủy' end as  ActivatedName,
			trang_thai_tk as ActivatedNum
		    from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where nguoi_tao = @UserName and CAST(ngay_tao as date) >= CAST(@FromDate AS DATE)
			and CAST(ngay_tao as date) <= CAST(@ToDate AS DATE) and trang_thai_ho_so = @ProfileStatus and trang_thai_tk = @Activated 
		    ORDER BY ho_so_mo_tk_id DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetCustInfoBySale => @"
SELECT CustCode, AssignUser,  BranchID FROM  [dbo].[CustInfo] 
where Status = @Status and CustCode == @AccountNo";

        public string GetCustInfo => @"select ma_khach_hang, loai_khach_hang 
                    from TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @ma_khach_hang";

        public string GetCustInfoRegister => @"select loai_ho_so, ma_khach_hang, trang_thai_tk from TVSI_DANG_KY_MO_TAI_KHOAN
                        where ma_khach_hang = @ma_khach_hang and trang_thai_tk != '999'";

        public string GetCustCode =>
            @" select ma_khach_hang from TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @CustCode and trang_thai = 1";

        public string GetDetailBasicInfoCust =>
            @"SELECT ma_khach_hang as CustCode, ho_ten_kh as FullName, ngay_sinh as BirthdayOn,
        case when  gioi_tinh = 1 then N'Nam'
            when  gioi_tinh = 2 then N'Nữ'
            when  gioi_tinh = 3 then N'Không xác định' else N'không tìm thấy' end as Gender,so_cmnd as CardId,ngay_cap_cmnd as IssueDateOn
             ,noi_cap_cmnd as CardIssuer, quoc_tich as Nationality,dia_chi_lien_he as Address01, nguoi_tao as  SaleId, file_upload as ImageFront1
             ,file_upload_backside as ImageBack2, 
			 case 
			 when hop_dong_bo_sung = '' or hop_dong_bo_sung IS NULL or  hop_dong_bo_sung = 0 then N'Hợp đồng thường'
			   when hop_dong_bo_sung = 1  then N'HĐ thường, HĐ margin'
			   when hop_dong_bo_sung = 2  then N'HĐ thường, HĐ phái sinh'
			   when hop_dong_bo_sung = 12 then N'HĐ thường, HĐ margin,HĐ phái sinh'
			     else N'Không tìm thấy' end as OpenContactName,
			  case 
			  when trang_thai_ho_so  = '000' then N'Chờ xử lý'
			   when trang_thai_ho_so  = '100' then N'Chờ duyệt'
			    when trang_thai_ho_so  = '101' then N'Nợ hồ sơ'
				when trang_thai_ho_so  = '200' then N'Hoàn thành'
				when trang_thai_ho_so  = '199' or trang_thai_ho_so = '299' then N'Từ chối' else N'Không tìm thấy' end as StatusContactName,
				tvsi_noi_mo_tk as AddressOpenAccount,
				tvsi_nguoi_dai_dien as RepresentTvsi,
				tvsi_chuc_vu as RollTvsi,
				tvsi_giay_uq as PowerofAttorneyNum,
				tvsi_ngay_uq as AuthorizationDateOn,
                tvsi_so_cmnd as TVSICardId,
                tvsi_ngay_cap_cmnd as TVSIIssueDateOn,
                tvsi_noi_cap_cmnd as TVSICardIssuer,
                fatca_kh_usa as CustomerUSA,
	            fatca_noi_sinh_usa as PlaceOfBirthUSA,
	            fatca_noi_cu_tru_usa as DepositoryUSA,
	            fatca_sdt_usa as CustomerPhoneNumberUSA,
	            fatca_lenh_ck_usa as CustomerTransferBankUSA,
	            fatca_giay_uy_quyen_usa as CustomerAuthorizationUSA,
	            fatca_dia_chi_nhan_thu_usa as CustomerReceiveMailUSA,
                nguon_du_lieu as SourceData,
                crm_id as CrmId,
                case when hop_dong_bo_sung = '' or hop_dong_bo_sung IS NULL then  0
				 when hop_dong_bo_sung = 1 then 1
				 when hop_dong_bo_sung = 2 then 2  end as  OpenContact           
			      FROM 
             [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where ho_so_mo_tk_id = @CustId AND nguoi_tao = @UserName ";

        public string GetDetailRegisterServiceCust => @" select 
  case 
  when phuong_thuc_gd = 1 then N'Qua điện thoại'
  when phuong_thuc_gd = 2 then N'Qua Internet'
  when phuong_thuc_gd = 12 then N'Qua điện thoại và Internet'
  when phuong_thuc_gd = 21 then N'Qua Internet và điện thoại'
      else N'Không tìm thấy' end as TradingMethod,
  case
   when phuong_thuc_nhan_kqgd = 1 then N'Qua SMS'
  when phuong_thuc_nhan_kqgd = 2 then N'Qua Email'
  when phuong_thuc_nhan_kqgd = 3 then N'Tại Quầy'
  when phuong_thuc_nhan_kqgd = 12 then N'Qua SMS và Email'
  when phuong_thuc_nhan_kqgd = 21 then N'Qua Email và SMS'
  when phuong_thuc_nhan_kqgd = 123 then N'Qua SMS và Email và Quầy'
      else N'Không tìm thấy' end as TradingResultMethod,
  case
   when phuong_thuc_sao_ke = 1 then N'Thư đảm bảo'
   when phuong_thuc_sao_ke = 2 then N'Email'
   when phuong_thuc_sao_ke = 3 then N'Tại quầy'
   when phuong_thuc_sao_ke = 12 then N'Thư đảm bảo và Email'
   when phuong_thuc_sao_ke = 21 then N'Email và Thư đảm bảo'
	when phuong_thuc_sao_ke = 123 then N'Qua thư đảm bảo và Email và Quầy'
     
   else N'Không tìm thấy' end as TradingReportMethod, 
   so_dien_thoai_01 as Phone01,
   so_dien_thoai_02 as Phone02,
   email as Email,
   case
    when dv_chuyen_tien = 1 then N'Sang TK NH'
	when dv_chuyen_tien = 2 then N'Chuyển tiền nội bộ'
	when dv_chuyen_tien = 3 then N'TK nước ngoài'
	when dv_chuyen_tien = 4 then N'Chuyển tiền Mobile'
	when dv_chuyen_tien = 5 then N'Chuyển tiền ItradeHome'
    when dv_chuyen_tien = 12 then N'Chuyển tài ngân hàng và nội bộ' else N'Không tìm thấy' end as TransferCashService,
        
	chu_tai_khoan_nh_01 as BankAccName_01,
	so_tai_khoan_nh_01 as BankAccNo_01,
	ngan_hang_01 as BankName_01,
	chi_nhanh_nh_01 as SubBranchName_01,
        
	chu_tai_khoan_nh_02 as BankAccName_02,
	so_tai_khoan_nh_02 as BankAccNo_02,
	ngan_hang_02 as BankName_02,
	chi_nhanh_nh_02 as SubBranchName_02,
    
    chu_tai_khoan_nh_03 as BankAccName_03,
	so_tai_khoan_nh_03 as BankAccNo_03,
	ngan_hang_03 as BankName_03,
	chi_nhanh_nh_03 as SubBranchName_03,
        
    chu_tai_khoan_nh_04 as BankAccName_04,
	so_tai_khoan_nh_04 as BankAccNo_04,
	ngan_hang_04 as BankName_04,
	chi_nhanh_nh_04 as SubBranchName_04,
    chu_tk_doi_ung_01 as AccountContraName_01,
    so_tk_doi_ung_01  as AccountContraNo_01,
    chu_tk_doi_ung_02 as AccountContraName_02,
    so_tk_doi_ung_02 as AccountContraNo_02
    from TVSI_DANG_KY_MO_TAI_KHOAN where ho_so_mo_tk_id = @CustId AND nguoi_tao = @UserName";
        
        public string GetCategoryCustExtend => @"  select b.CategoryType, b.CategoryName, b.Category from [dbo].[ExtendCategoryCust] a Inner join [dbo].[ExtendCust] b ON a.Category = b.Category where a.CustCode = @CustCode and b.Status = 1";
        
        
        /*
        public string GetCategoryCustExtend => @"  select b.CategoryType, b.CategoryName from [dbo].[ExtendCategoryCust] where CustCode = @CustCode and b.Status = 1";
        */
        

        public string GetImageCardId => @" select ho_so_mo_tk_id ProfileId, file_name NameFront, file_data FileDataFront ,file_name_backside NameBack,file_data_backside FileDataBack from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN_FILES] where ho_so_mo_tk_id = @Id";
        public string GetBankName => @"SELECT A.ShortName as ShortName FROM BANKLIST A WHERE A.BankNo = @bankNo";
        public string GetSubBranchName =>  @"SELECT A.ShortBranchName as ShortBranchName FROM BRANCHNOLIST A WHERE BankNo = @bankNo AND A.BranchNo=@branchNo";
        public string GetProvinceName => @"SELECT A.ProvinceName FROM PROVINCES A where A.ProvinceID=@provinceID";
        public string GetIssuedByName => @"SELECT CategoryName FROM [dbo].[ExtendCust] WHERE Category = @id ";
        public string GetCardIssue => @"SELECT Category id, CategoryName CardIssueName FROM [dbo].[ExtendCust] where CategoryType = 13";

        public string GetManagementCode => @"SELECT ma_chi_nhanh AS CodeBranch, ten_bo_phan as NameDepartment FROM [dbo].[TVSI_DANH_MUC_MA_QUAN_LY] where ma_quan_ly = @saleID";
        public string GetUserProfile => @"SELECT A.ma_nhan_vien_quan_ly as SaleID, A.ho_va_ten as SaleName, 
                                    A.cap_quan_ly as Level, A.gioi_tinh as Sex, A.ngay_sinh as BirthDay,
                                    A.dia_chi as Address, C.ten_phong_ban as Department,
                                    A.dien_thoai_lien_he as Mobile, A.dia_chi_email as Email
                            FROM TVSI_THONG_TIN_TRUY_CAP T 
                                INNER JOIN TVSI_NHAN_SU A ON T.nhan_su_id = A.nhan_su_id 
                                INNER JOIN TVSI_DANH_MUC_PHONG_BAN C on A.danh_muc_phong_ban_id = C.danh_muc_phong_ban_id 
                            WHERE T.ten_dang_nhap = @userName";

        public string GetAccountInfo => @"select ho_so_mo_tk_id as CustId,
			  ho_ten_kh as FullName, ma_khach_hang as CustCode, 
		  case 
		   when trang_thai_ho_so = 000 then N'Chờ xử lý'
		   when trang_thai_ho_so = 100 then N'Chờ duyệt'
		   when trang_thai_ho_so = 101 then N'Nợ hồ sơ'
		   when trang_thai_ho_so = 200 then N'Hoàn thành'
		   when trang_thai_ho_so = 199 or trang_thai_ho_so = 299 then N'Từ chối' end as ProfileStatusName,
		   trang_thai_ho_so as ProfileStatusNum,
		   case
		    when loai_ho_so = 1 then N'Thêm mới'
			when loai_ho_so = 2 then N'Bổ sung'
			end as TypeProfile,
			mo_ta as TypeContract,
			ngay_tao as CreateDateOn,
			case 
			when trang_thai_tk = 000 then N'Chờ xử lý'
			when trang_thai_tk = 001 then N'Chờ BU_MAN duyệt'
			when trang_thai_tk = 002 then N'Chờ FS duyệt'
			when trang_thai_tk = 099 then N'BU_MAN từ chối'
			when trang_thai_tk = 100 then N'Đang xử lý'
			when trang_thai_tk = 102 then N'Kích hoạt(KSV SS)'
			when trang_thai_tk = 200 then N'Hoàn thành'
			when trang_thai_tk = 399 then N'FS từ chối'
			when trang_thai_tk = 199 or trang_thai_tk = 299 then N'Từ chối'
			when trang_thai_tk = 999 then N'Đã hủy' end as  ActivatedName,
			trang_thai_tk as ActivatedNum ,
            case 
			when nguon_du_lieu = 1 then N'Mở tài khoản EMS'
			when nguon_du_lieu = 2 then N'Mở tài khoản CRM'
			when nguon_du_lieu = 3 then N'Mở tài khoản Website'
			when nguon_du_lieu = 4 then N'Mở tài khoản eKYC Website'
			when nguon_du_lieu = 6 then N'ở tài khoản eKYC Mobile' else N'Không xác định' end as SourceAccountName,
			nguon_du_lieu as SourceAccountNum
		    from TVSI_DANG_KY_MO_TAI_KHOAN where nguoi_tao = @UserName {cond1} {cond2} {cond3} {cond4} {cond5} {cond6}
            ORDER BY ho_so_mo_tk_id DESC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY ";



        // Stored SQL
        public string StoredCheckSaleExsited => @"TVSI_sGET_ID_SALE";
        public string StoredUpdateStatusAccount => @"TVSI_sUPDATE_TRANG_THAI_DANG_KY_MO_TAI_KHOAN";
        public string StoredInsertAccount => @"TVSI_sINSERT_DANG_KY_MO_TAI_KHOAN_IWORK";
        public string StoredInsertContactFS => @"TVSI_sINSERT_FS_THONG_TIN_NO_HD_IWORK";
        public string StoredGetAccountFillBySale => @"TVSI_sGET_ACCOUNT_FILLTER_BY_SALE";

        public string SelectInfoAccountTofix => @"SELECT * FROM TVSI_DANG_KY_MO_TAI_KHOAN LEFT JOIN TVSI_FS_THONG_TIN_NO_HOP_DONG 
                                                ON TVSI_FS_THONG_TIN_NO_HOP_DONG.ho_so_mo_tk_id = TVSI_DANG_KY_MO_TAI_KHOAN.ho_so_mo_tk_id 
                                                WHERE TVSI_DANG_KY_MO_TAI_KHOAN.ma_khach_hang = @custCode AND TVSI_DANG_KY_MO_TAI_KHOAN.trang_thai_tk = '000'";

        public string UpdateInfoAccount => @"UPDATE TVSI_DANG_KY_MO_TAI_KHOAN SET so_tai_khoan = @so_tai_khoan, loai_ho_so = @loai_ho_so, loai_hinh_tk = @loai_hinh_tk, ho_ten_kh = @ho_ten_kh,
                        ngay_sinh = @ngay_sinh, gioi_tinh = @gioi_tinh, ma_quoc_tich = @ma_quoc_tich, quoc_tich = @quoc_tich, so_cmnd = @so_cmnd, ngay_cap_cmnd = @ngay_cap_cmnd, 
                        noi_cap_cmnd = @noi_cap_cmnd, dia_chi_lien_he = @dia_chi_lien_he, hop_dong_chinh = @hop_dong_chinh, hop_dong_bo_sung = @hop_dong_bo_sung, phuong_thuc_gd = @phuong_thuc_gd,
                        phuong_thuc_nhan_kqgd = @phuong_thuc_nhan_kqgd, phuong_thuc_sao_ke = @phuong_thuc_sao_ke, dv_chuyen_tien = @dv_chuyen_tien, so_dien_thoai_01 = @so_dien_thoai_01,
                        so_dien_thoai_02 = @so_dien_thoai_02, email = @email, chu_tai_khoan_nh_01 = @chu_tai_khoan_nh_01, so_tai_khoan_nh_01 = @so_tai_khoan_nh_01, ma_ngan_hang_01 = @ma_ngan_hang_01,
                        ngan_hang_01 = @ngan_hang_01, chi_nhanh_nh_01 = @chi_nhanh_nh_01, ma_chi_nhanh_01 = @ma_chi_nhanh_01, tinh_tp_nh_01 = @tinh_tp_nh_01, chu_tai_khoan_nh_02 = @chu_tai_khoan_nh_02,
                        so_tai_khoan_nh_02 = @so_tai_khoan_nh_02, ngan_hang_02 = @ngan_hang_02, ma_ngan_hang_02 = @ma_ngan_hang_02, chi_nhanh_nh_02 = @chi_nhanh_nh_02, ma_chi_nhanh_02 = @ma_chi_nhanh_02,
                        tinh_tp_nh_02 = @tinh_tp_nh_02, chu_tai_khoan_nh_03 = @chu_tai_khoan_nh_03, so_tai_khoan_nh_03 = @so_tai_khoan_nh_03, ngan_hang_03 = @ngan_hang_03, ma_ngan_hang_03 = @ma_ngan_hang_03,
                        chi_nhanh_nh_03 = @chi_nhanh_nh_03, ma_chi_nhanh_03 = @ma_chi_nhanh_03, tinh_tp_nh_03 = @tinh_tp_nh_03, chu_tai_khoan_nh_04 = @chu_tai_khoan_nh_04, so_tai_khoan_nh_04 = @so_tai_khoan_nh_04,
                        ngan_hang_04 = @ngan_hang_04, ma_ngan_hang_04 = @ma_ngan_hang_04, chi_nhanh_nh_04 = @chi_nhanh_nh_04, ma_chi_nhanh_04 = @ma_chi_nhanh_04, tinh_tp_nh_04 = @tinh_tp_nh_04,

                        chu_tk_doi_ung_01 = @chu_tk_doi_ung_01, so_tk_doi_ung_01 = @so_tk_doi_ung_01, chu_tk_doi_ung_02 = @chu_tk_doi_ung_02, so_tk_doi_ung_02 = @so_tk_doi_ung_02, kenh_trao_doi = @kenh_trao_doi,
                        phuong_thuc_kenh_td = @phuong_thuc_kenh_td, trang_thai_tk = @trang_thai_tk, trang_thai_ho_so = @trang_thai_ho_so, trang_thai_dv = @trang_thai_dv, trang_thai_dv_ngan_hang = @trang_thai_dv_ngan_hang,
                        trang_thai_dv_ngan_hang_02 = @trang_thai_dv_ngan_hang_02, trang_thai_dv_doi_ung = @trang_thai_dv_doi_ung, trang_thai_dv_doi_ung_02 = @trang_thai_dv_doi_ung_02, trang_thai_dv_kenh_tt = @trang_thai_dv_kenh_tt,
                        mo_ta = @mo_ta, sale_id = @sale_id, branch_id = @branch_id, branch_name = @branch_name, nguon_du_lieu = @nguon_du_lieu, tvsi_noi_mo_tk = @tvsi_noi_mo_tk, tvsi_nguoi_dai_dien = @tvsi_nguoi_dai_dien,
                        tvsi_chuc_vu = @tvsi_chuc_vu, tvsi_giay_uq = @tvsi_giay_uq, tvsi_ngay_uq = @tvsi_ngay_uq, tvsi_so_cmnd = @tvsi_so_cmnd, tvsi_noi_cap_cmnd = @tvsi_noi_cap_cmnd, tvsi_ngay_cap_cmnd = @tvsi_ngay_cap_cmnd,
                        fatca_kh_usa = @fatca_kh_usa, fatca_noi_sinh_usa = @fatca_noi_sinh_usa, fatca_noi_cu_tru_usa = @fatca_noi_cu_tru_usa, fatca_sdt_usa = @fatca_sdt_usa, fatca_lenh_ck_usa = @fatca_lenh_ck_usa,
                        fatca_giay_uy_quyen_usa = @fatca_giay_uy_quyen_usa, fatca_dia_chi_nhan_thu_usa = @fatca_dia_chi_nhan_thu_usa, nguoi_cap_nhat = @nguoi_cap_nhat, ngay_cap_nhat = @ngay_cap_nhat 
                                            WHERE ma_khach_hang = @custCode AND trang_thai_tk = '000'";
        public string SelectInfoNohoso => @"SELECT hop_dong_no_id FROM TVSI_FS_THONG_TIN_NO_HOP_DONG WHERE ho_so_mo_tk_id = @ho_so_mo_tk_id";
        public string UpdateNohoso => @"UPDATE TVSI_FS_THONG_TIN_NO_HOP_DONG SET han_muc_du_no = @han_muc_du_no, thoi_gian_ghi_no = @thoi_gian_ghi_no, ty_le_ky_quy = @ty_le_ky_quy WHERE ho_so_mo_tk_id = @ho_so_mo_tk_id";
        public string GetBankList => @"SELECT A.BankNo as BankNo, A.ShortName as ShortName FROM BANKLIST A WHERE STATUS = 1";

        public string GetProvinceList => @"SELECT A.ProvinceID, A.ProvinceName FROM PROVINCES A";

        public string GetSubBranchList => @"SELECT A.BranchNo as BranchNo, A.ShortBranchName as ShortBranchName FROM BRANCHNOLIST A WHERE BankNo = @bankNo AND Status in (1,3)";
        /// <summary>
        /// Query lấy danh sách mở tài khoản từ website
        /// </summary>
        public string GetListAccWeb => @"
SELECT a.dang_kyid, a.ngay_tao, a.loai_hinh_tk, a.ho_ten, a.so_dien_thoai_01, a.trang_thai_tk trang_thai_tt, a.sale_id,
                        a.noi_dung_xu_ly,a.sale_assign,a.nguoi_cap_nhat,a.ConfirmStatus,a.ConfirmCode,a.ConfirmSms,a.so_cmnd,a.nguon_du_lieu,a.email Email
                        FROM TVSI_DANG_KY_MO_TAI_KHOAN a
                        WHERE trang_thai_tk <> 999 and a.sale_id = @SaleId {cond1} {cond2}  
                        Order by ngay_tao DESC 
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        /// <summary>
        /// Update trạng thái hồ sơ mở tài khoản từ website
        /// </summary>
        public string UpdateAccWeb => @"UPDATE TVSI_DANG_KY_MO_TAI_KHOAN SET 
                                                        sale_assign = @sale_assign,
                                                        noi_dung_xu_ly = @noi_dung_xu_ly,
                                                        trang_thai_tk = @trang_thai_tk,
                                                        nguoi_cap_nhat = @nguoi_cap_nhat,
                                                        ngay_cap_nhat = @ngay_cap_nhat
                                                        WHERE dang_kyid = @dang_kyid AND trang_thai_tk <> 999";
        /// <summary>
        /// Xóa bản ghi danh sách mở tài khoản từ website
        /// </summary>
        public string DeleteAccWeb => @"UPDATE TVSI_DANG_KY_MO_TAI_KHOAN SET 
                                                        nguoi_cap_nhat = @nguoi_cap_nhat,
                                                        ngay_cap_nhat = @ngay_cap_nhat,
                                                        trang_thai_tk = 999
                                                        WHERE dang_kyid = @dang_kyid";
        public string GetRegAccountExistedCommond =>  @"select ma_khach_hang from [dbo].[TVSI_DANG_KY_MO_TAI_KHOAN] where ma_khach_hang = @UserName";
        public string GetAccountExistedCommond =>
	        @"SELECT ma_khach_hang FROM [dbo].[TVSI_TAI_KHOAN_KHACH_HANG] where ma_khach_hang = @UserName";

        public string GetRevertExistedCommond => @"select CustCode from [dbo].[TVSI_REVERT_ACCOUNT] where CustCode = @UserName";

        public string GetAccountExistedBond =>
	        @"SELECT CustCode from ReservedCustInfo where CustCode = @UserName ";
        public string GetCustomerCRMWebsite => @"
            SELECT ho_ten FullName, so_cmnd  CardId,sale_assign HandledBy, ngay_cap_cmnd IssueDateOn,noi_cap_cmnd CardIssuer, trang_thai_tk StatusAction,
                   case when gioi_tinh = 1 then N'Nam' when gioi_tinh = 2 then N'Nữ' 
                  else N'Không xác định' end Sex ,ngay_sinh BirthdayOn, dia_chi Address_01, so_dien_thoai_01 Phone_01, 
                  so_dien_thoai_02 Phone_02, email Email, sale_id SaleID,  
                    trang_thai_tk StatusName, trang_thai_tk StatusNumber,
                   nguon_du_Lieu SourceAccount,
                   yeu_cau NoteRequest,
                   ConfirmStatus ConfirmStatus,
                   ConfirmSms ConfirmSms,
                   ConfirmCode ConfirmCode,
                   nguoi_cap_nhat CreateBy,
                   ngay_tao CreateDateOn, 
	               sale_id CreateLater, ngay_cap_nhat UpdateDateLater, noi_dung_xu_ly Description, 
	               chu_tai_khoan_nh_01 BankAccName_01, 
                   so_tai_khoan_nh_01 BankAccNo_01,
                   ma_ngan_hang_01 BankNo_01,
	                  ngan_hang_01 BankName_01,
                   ma_chi_nhanh_01 SubBranchNo_01,
                   chi_nhanh_nh_01 SubBranchName_01,
                   tinh_tp_nh_01 CityNo_01,
	  	                 chu_tai_khoan_nh_02 BankAccName_02,
                   so_tai_khoan_nh_02 BankAccNo_02,
                   ma_ngan_hang_02 BankNo_02,
	                  ngan_hang_02 BankName_02, 
                      ma_chi_nhanh_02 SubBranchNo_02,
                   chi_nhanh_nh_02 SubBranchName_02,
                   tinh_tp_nh_02 CityNo_02,
	                  case when phuong_thuc_nhan_kqgd =  1 then N'Qua SMS' when phuong_thuc_nhan_kqgd = 2 then N'Qua Email'
	                  when phuong_thuc_nhan_kqgd = 12 then N'Qua SMS và Email' end TradeMethodResult,
	                  case when phuong_thuc_gd = 1 then N'Qua SMS' when phuong_thuc_gd = 2 then N'Qua Email'
	                   when phuong_thuc_gd = 12 then N'Qua SMS và Email' end TradeMethod,
	                   case when phuong_thuc_sao_ke = 2 then N'Qua Email' end TradeReportMethod,
	                   fatca_kh_usa FatcaKhUsa,
	                   fatca_noi_sinh_usa FatcaNoiSinhUsa,
	                   fatca_noi_cu_tru_usa FatcaNoiCuTruUsa,
	                   fatca_sdt_usa FatcaSdtUsa,
	                   fatca_lenh_ck_usa FatcaLenhCkUsa,
	                   fatca_giay_uy_quyen_usa FatcaGiayUyQuyenUsa,
	                   fatca_dia_chi_nhan_thu_usa FatcaDiaChiNhanThuUsa,
                       tvsi_nguoi_dai_dien TvsiManName,
                       tvsi_chuc_vu TvsiManPosition,
                       tvsi_giay_uq TvsiManNo,
                       tvsi_ngay_uq TvsiManDateOn,
                       tvsi_so_cmnd TvsiManCardId,
                       tvsi_ngay_cap_cmnd TvsiManCardDateOn,
                       tvsi_noi_cap_cmnd TvsiManCardIssue
            from TVSI_DANG_KY_MO_TAI_KHOAN where sale_id = @UserName and dang_kyid = @Id";

        public string GetExtendCust => @" select CustCode from ExtendCategoryCust where CustCode = @CustCode";
    }
}