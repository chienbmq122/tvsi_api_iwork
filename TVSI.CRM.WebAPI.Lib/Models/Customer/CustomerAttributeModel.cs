﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class CustomerAttributeModel
    {
        public int AttributeID { get; set; }
        public string CustCode { get; set; }
        public string AttributeName { get; set; }
        public string AttributeLabel { get; set; }
        public string Value { get; set; }
        public int AttributeGroupID { get; set; }
        public int AttributeType { get; set; }
        public int Level { get; set; }
        public int? Priority { get; set; }
    }

    public class CustomerAttributeRequest : BaseRequest
    {
        [Description("CustCode")]
        [Required]
        public string CustCode { get; set; }
    }

    public class CustomerAttrinbuteCreate : BaseRequest
    {
        [Description("AttributeID")]
        [Required]
        public int AttributeID { get; set; }
        [Description("Customer code")]
        [Required]
        public string CustCode { get; set; }
        [Description("Giá tri của thuộc tính")]
        [Required]
        public string Value { get; set; }
        [Description("Độ ưu tiên")]
        [Required]
        public int Priority { get; set; }
    }

    public class CustomerAttrinbuteDelete : BaseRequest
    {
        [Description("AttributeID")]
        [Required]
        public int AttributeID { get; set; }
        [Description("Customer code")]
        [Required]
        public string CustCode { get; set; }
    }
}
