﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using TVSI.CRM.WebAPI.Lib.Infrastructure;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Account;
using TVSI.CRM.WebAPI.Lib.Models.System;
using TVSI.Utility;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class BaseService
    {
        private readonly string _emsConnectionString;
        private readonly string _systemNotificationConnectionString;
        private readonly string _crmdbConnectionString;
        private readonly string _commondbConnectionString;
        private readonly string _innodbConnectionString;
        private readonly string _timeworkConnectString;
        private readonly string _tbmConnectionString;
        private readonly string _bondConnectionString;
        private readonly string _assetConnectionString;
        protected BaseService()
        {
            _emsConnectionString = ConnectionFactory.EmsDbConnectionString;
            _crmdbConnectionString = ConnectionFactory.CrmDbConnectionString;
            _innodbConnectionString = ConnectionFactory.InnoConnectionString;
            _timeworkConnectString = ConnectionFactory.TimeWorkConnectionString;
            _commondbConnectionString = ConnectionFactory.CommonDbConnection;
            _tbmConnectionString = ConnectionFactory.TbmDbConnection;
            _bondConnectionString = ConnectionFactory.BondDbConnection;
            _assetConnectionString = ConnectionFactory.AssetDbConnection;
            _systemNotificationConnectionString = ConnectionFactory.SystemNotificationConnection;
        }

        #region EmsDB Connection
        /// <summary>
        /// Use for buffered queries that return a type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getData"></param>
        /// <returns></returns>
        protected async Task<T> WithEmsDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() SQL exception", GetType().FullName), ex);
            }
        }

        /// <summary>
        /// Use for buffered queries that do not return a type
        /// </summary>
        /// <param name="getData"></param>
        /// <returns></returns>
        protected async Task WithEmsDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_emsConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        /// <summary>
        /// Use for non-buffered queries that return a type
        /// </summary>
        /// <typeparam name="TRead"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="getData"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        protected async Task<TResult> WithEmsDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_emsConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDbConnection() SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithEmsDbConnection() SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
        
        #region "SystemNotificationDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithSystemNotificationConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithSystemNotificationConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithSystemNotificationConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_systemNotificationConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithSystemNotificationConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "CrmDb Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCrmDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCrmDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCrmDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_crmdbConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region "InnoDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithInnoDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innodbConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithInnoDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_innodbConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithInnoDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_innodbConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithInnoDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
        
        #region "TimeWorkDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithTimeWorkDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_timeworkConnectString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithTimeWorkDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_timeworkConnectString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithTimeWorkDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_timeworkConnectString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTimeWorkDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
        
        #region "CommonDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithCommonDBConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithCommonDBConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithCommonDBConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_commondbConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCommonDBConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region TBMDB Connection
        /// <summary>
        /// Use for buffered queries that return a type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getData"></param>
        /// <returns></returns>
        protected async Task<T> WithTbmDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tbmConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmDBConnection() SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() SQL exception", GetType().FullName), ex);
            }
        }

        /// <summary>
        /// Use for buffered queries that do not return a type
        /// </summary>
        /// <param name="getData"></param>
        /// <returns></returns>
        protected async Task WithTbmDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_tbmConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        /// <summary>
        /// Use for non-buffered queries that return a type
        /// </summary>
        /// <typeparam name="TRead"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="getData"></param>
        /// <param name="process"></param>
        /// <returns></returns>
        protected async Task<TResult> WithTbmDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_emsConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmDbConnection() SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithTbmDbConnection() SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region Common Function
        /// <summary>
        /// Load danh sách ID chi nhánh con của User trên TBM
        /// </summary>
        /// <param name="conn"></param>
        /// <param name="userName"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        protected List<int> LoadSubBranchIdByUserTbm(IDbConnection conn, string userName, int? branchId)
        {
            try
            {
                var subBranchIdList = new List<int>();

                if (branchId != null && branchId > 0)
                {
                    LoadSubBranchIdListTbm(conn, subBranchIdList, branchId.Value, 0);
                }

                return subBranchIdList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void LoadSubBranchIdListTbm(IDbConnection conn, List<int> branchIdList, int branchId, int countLoop)
        {
            if (countLoop == 7) return;
            countLoop++;

            var curBranch =
                conn.Query("Select * from SaleBranch where BranchID = @branchId and Status = 1",
                    new { branchId = branchId }).FirstOrDefault();

            if (curBranch != null)
            {
                if (!branchIdList.Contains(branchId))
                {
                    branchIdList.Add(branchId);
                }

                var subIds = conn.Query<int>("Select BranchID from SaleBranch where BranchParentID = @branchId and Status = 1",
                    new { branchId = branchId }).ToList();

                if (subIds.Any())
                {
                    foreach (var id in subIds)
                    {
                        LoadSubBranchIdListTbm(conn, branchIdList, id, countLoop);
                    }
                }
            }
        }
        #endregion
        
        
        
        #region "BondDB Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithBondDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_bondConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithBondDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_bondConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithBondDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_bondConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithBondDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion

        #region GetSaleList

        public async Task<IEnumerable<string>> GetSaleList(int systemID)
        {
            try
            {
                using (var conn = new SqlConnection(_emsConnectionString))
                {
                    var listChildIDs = new List<int>();
                    conn.Open();
                    var listLevelSystem = await conn.QueryAsync<ListChildLevelAndSelf>("SELECT * FROM dbo.TVSI_fDANH_SACH_CAP_DO_CON_VA_CHINH_NO(@cap_do_he_thong_muc_cha_id)",
                        new
                        {
                            cap_do_he_thong_muc_cha_id = systemID  
                        });
                    if (listLevelSystem.Count() > 0)
                    {
                        foreach (var data in listLevelSystem)
                        {
                            listChildIDs.Add(data.cap_do_he_thong_id.Value);
                        }
                    }

                    if (listChildIDs.Count() > 0)
                    {
                        var sql = @"SELECT A.ma_nhan_vien_quan_ly FROM TVSI_THONG_TIN_TRUY_CAP T 
                                INNER JOIN TVSI_NHAN_SU A on T.nhan_su_id = A.nhan_su_id
                                WHERE T.cap_do_he_thong_id IN @ListID and T.trang_thai=1";
                       return await conn.QueryAsync<string>(sql, new
                        {
                            ListID = listChildIDs,
                        });
                    }

                    return new List<string>();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(BaseService), ex.Message);
                throw;
            }
        }

        #endregion

        #region SystemID

        public async Task<UserLoginResult> GetSystemID(string userName)
        {
            try
            {
                using (var conn = new SqlConnection(_emsConnectionString))
                {
                    conn.Open();
                    var sql = @"SELECT A.ten_dang_nhap as UserName, B.ho_va_ten as FullName,
                                   B.ma_nhan_vien_quan_ly as SaleID, B.cap_quan_ly as Level,
                                   A.cap_do_he_thong_id as SystemID, C.ma_chi_nhanh as BranchID,
                                   A.nhan_su_id as UserID, C.ma_cap_do_he_thong as SystemCode
                                FROM TVSI_THONG_TIN_TRUY_CAP A 
                                INNER JOIN TVSI_NHAN_SU B on A.nhan_su_id = B.nhan_su_id 
                                LEFT JOIN TVSI_CAP_DO_HE_THONG C on A.cap_do_he_thong_id = C.cap_do_he_thong_id 
                                WHERE A.ten_dang_nhap = @ten_dang_nhap AND a.trang_thai = 1";
                    return await conn.QueryFirstOrDefaultAsync<UserLoginResult>(sql, new
                    {
                        @ten_dang_nhap = userName
                    });

                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(BaseService), ex.Message);
                throw;
            }
        }

        #endregion

        #region GetBranchList Giam doc chi nhanh

        public async Task<List<string>> GetBranchList(string ma_chi_nhanh)
        {
            try
            {
                using (var conn = new SqlConnection(_emsConnectionString))
                {
                    conn.Open();
                    var listChildBranch = new List<string>();
                    var sql = "SELECT * FROM dbo.fDANH_SACH_CHI_NHANH_CON_VA_CHINH_NO(@ma_chi_nhanh)";
                    var listSubBranch = await conn.QueryAsync<ListSubBranchAndSelf>(sql, new
                    {
                        ma_chi_nhanh = ma_chi_nhanh
                    });
                    if (listSubBranch.Count() > 0)
                    {
                        foreach (var data in listSubBranch)
                        {
                            listChildBranch.Add(data.ma_chi_nhanh);
                        }
                    }

                    return listChildBranch;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(BaseService), ex.Message);
                throw;
            }
        }

        #endregion
        
        #region GetSubList 

        public async Task<List<string>> GetSubSaleList(int systemID)
        {
            try
            {
                using (var conn = new SqlConnection(_emsConnectionString))
                {
                    conn.Open();
                    var listMasterLevel = conn.Query<TVSI_fDANH_SACH_CAP_DO_CON_VA_CHINH_NO_Result>("SELECT * FROM dbo.TVSI_fDANH_SACH_CAP_DO_CON_VA_CHINH_NO(@cap_do_he_thong_muc_cha_id)",
                        new { cap_do_he_thong_muc_cha_id = systemID }, commandType: CommandType.Text).ToList();
                    var listChildIDs = new List<int>();

                    if (listMasterLevel.Count > 0)
                        foreach (var item in listMasterLevel)
                        {
                            listChildIDs.Add(item.cap_do_he_thong_id.Value);
                        }

                    if (listChildIDs.Count > 0)
                    {
                        var sql = @"SELECT A.ma_nhan_vien_quan_ly FROM TVSI_THONG_TIN_TRUY_CAP T 
                                INNER JOIN TVSI_NHAN_SU A on T.nhan_su_id = A.nhan_su_id
                                WHERE T.cap_do_he_thong_id IN @ListID and T.trang_thai=1";
                        var listTenDangNhap = conn.Query<string>(sql, new
                        {
                            ListID = listChildIDs,
                        }).ToList();

                        return listTenDangNhap;
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(typeof(BaseService), string.Format("GetSubSaleList - {0}",ex.Message));
                throw;
            }
        }

        #endregion

        #region "CrmDb Connection"
        // use for buffered queries that return a type
        protected async Task<T> WithAssetDbConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_assetConnectionString))
                {
                    await connection.OpenAsync();
                    return await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for buffered queries that do not return a type
        protected async Task WithAssetDbConnection(Func<IDbConnection, Task> getData)
        {
            try
            {
                using (var connection = new SqlConnection(_assetConnectionString))
                {
                    await connection.OpenAsync();
                    await getData(connection);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }

        // use for non-buffered queries that return a type
        protected async Task<TResult> WithAssetDbConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            try
            {
                using (var connection = new SqlConnection(_assetConnectionString))
                {
                    await connection.OpenAsync();
                    var data = await getData(connection);
                    return await process(data);
                }
            }
            catch (TimeoutException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL timeout", GetType().FullName), ex);
            }
            catch (SqlException ex)
            {
                throw new Exception(string.Format("{0}.WithCrmDbConnection() experienced a SQL exception", GetType().FullName), ex);
            }
        }
        #endregion
    }
}
