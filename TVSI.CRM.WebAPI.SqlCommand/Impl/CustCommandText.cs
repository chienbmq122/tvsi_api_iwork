﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class CustCommandText : ICustCommandText
    {
        #region SQL Command List
        public string GetListCust => @"SELECT CustCode, CustName, Mobile, Email, ProfileType, Status, AssignUser
                        FROM CustInfo 
                        WHERE {cond1} {cond2} {cond3}
                        Order by CreatedDate DESC 
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetInfoCust => @"SELECT C.CUSTOMERID as CustomerID, C.CUSTOMERNO as CustomerNo, C.CUSTOMERNAME as CustomerName,
                      C.IDENTITYCARD as IdentityCard, C.CARDISSUE as CardIssue, C.PLACEISSUE as PlaceIssue, C.SEX as Sex, C.ADDRESS as Address, C.CELLPHONE as CellPhone,
                      C.PHONE as Phone,C.CONTACTPHONE as ContactPhone, C.EMAIL as Email, c.BIRTHDAY as Birthday, G.NAME as CustomerGroupName,
                      '#' as MarginGroup, A.DESCRIPTION as SaleName, B.BRANCHNAME as BranchName
                    FROM CUSTOMER C 
                    INNER JOIN CUSTOMERGROUP G ON C.CUSTOMERGROUPID = G.CUSTOMERGROUPID
                    LEFT JOIN AGENT A ON A.AGENTID = C.MKTID 
                    LEFT JOIN BRANCH B ON B.BRANCHID = C.BRANCHID 
                    WHERE C.CUSTOMERID = @custCode  ";
        public string GetPhoneCust => @"SELECT P.Phone, P.IsSMS, P.IsCC FROM CUSTOMERPHONE P WHERE P.CustomerID = @custCode AND P.IsActive = 1";

        public string GetBankTransfer => @"SELECT A.BANKACCOUNT as BankAccount, A.BENEFICIARY as BankAccountName,
                                           A.BANKNAME as BankName, B.BranchName as BranchName
                                           FROM BANKACCOUNT A 
                                           LEFT JOIN BRANCHNOLIST B ON A.BANKNO = b.BankNo AND A.BRANCHNO = b.BranchNo
                                           WHERE A.STATUS = 1 AND A.CUSTOMERID = @custCode";

        public  string GetAccPorfolio => @"SELECT LTRIM(RTRIM(A.ACCOUNTNO)) as AccountNo, A.PORTFOLIODATE as TradingDateOn,
                        LTRIM(RTRIM(A.SYMBOL)) as Symbol, A.STARTVOLUME as StartVolume, a.AVAIVOLUME as Volume,
                        A.AVGPRICE as AvgPrice, A.SECTYPE as SecType
                    FROM PORTFOLIO A 
                    WHERE A.ACCOUNTNO IN @accountList AND (A.AVAIVOLUME > 0 OR A.STARTVOLUME > 0)";

        /// <summary>
        /// Query lấy danh sách lệnh đặt
        /// </summary>
        public string GetOrderInfo => @"SELECT ACCOUNT as AccountNo, CANCELLEDVOLUME as CancelledVolume,
                            CANCELSTATUS as CancelStatus, CHANGESTATUS as ChangeStatus,
                            EXECUTEDPRICE as ExecutedPrice, EXECUTEDVOL as ExecutedVol,
                            ORDERSTATUS as OrderStatus, PRICE as Price, SIDE as Side,
                            SECSYMBOL as Symbol, TRANSTIME as TransTime, VOLUME as Volume
                    FROM ORDERINFO WHERE ACCOUNT IN @accountList";

        /// <summary>
        /// Query lấy thông tin danh sách lệnh khớp
        /// </summary>
        public string GetDealInfo => @"SELECT AccountNo, DealPrice, DealTime, DealVolume, Side, Symbol
                    FROM DEALINFO WHERE AccountNo IN @accountList";
        /// <summary>
        /// Query lấy danh sách list các thuộc tính mở rộng cho Sale & BB
        /// </summary>
        public string GetExCustInfo => @"SELECT Category as ExId, CategoryName as ExName from ExtendCust where status = 1 and CategoryType = @ExType   order by ExId";

        /// <summary>
        /// Query lấy danh sách hoạt động
        /// </summary>
        public string GetActivityInfo => @"SELECT ActivityID, ActivityName, Description, StartDate,EndDate , Status, AssignUser
                        FROM Activity
                        WHERE   AssignUser = @SaleId and CustCode = @custCode and Status <> 99
                        Order by CreatedDate DESC
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        /// <summary>
        /// Query lấy danh sách cơ hội
        /// </summary>
        public string GetOpportunityInfo => @"SELECT OpportunityID, OpportunityName, ExpectedRevenue, ExpectedCloseDate, Status, AssignUser, Description 
                                        FROM Opportunity
                                        WHERE   AssignUser = @SaleId and CustCode = @custCode
                                        Order by CreatedDate DESC";

        public string UpdateCustInfo => @"UPDATE CustInfo SET 
                                                        Description = @description,
                                                        UpdatedBy = @updatedBy,
                                                        UpdatedDate = @updatedDate
                                                        WHERE CustCode = @custCode AND Status <> 99";
        public string DeleteExCustInfo => @"DELETE ExtendCategoryCust WHERE CustCode = @custCode";




        /// <summary>
        /// Thêm mới thông tin mở rộng liên quan đến khách hàng
        /// </summary>
        public string PostCustExtInfo => @"INSERT INTO ExtendCategoryCust (Category,CustCode,CreatedBy,CreatedDate)
                                        VALUES (@category,@custCode,@createdBy,@createdDate)";


        public string GetListShareHolder => @"SELECT CustCode, CustName, ShareType_1, ShareType_2, ShareType_3, ShareType_4, StockCode, Status, CreatedBy, CreatedDate
                        FROM ShareHolder 
                        WHERE   CreatedBy = @SaleId  {cond1} {cond2} 
                        Order by CreatedDate DESC 
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";



        public string GetListCustSelect => @"SELECT CustCode, CustName FROM CustInfo WHERE CustCode LIKE @custCode AND Status <> 99 AND AssignUser = @assignUser 
                                                ORDER BY CustCode ASC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetListCustSelectCount => @"SELECT count(*) FROM CustInfo WHERE CustCode LIKE @custCode AND Status <> 99 AND AssignUser = @assignUser";

        public string PostShareHolderInfo => @"INSERT INTO ShareHolder (CustCode,CustName,Address, ShareType_1, ShareType_2, ShareType_3, ShareType_4, StockCode, Status, CreatedBy,CreatedDate)
                                        VALUES (@custCode,@custName, @address, @sharetype_1,@sharetype_2,@sharetype_3,@sharetype_4,@stockCode,@status,@createdBy,@createdDate)";

        public string ViewShareHolderInfo => @"SELECT CustCode,CustName,Address, ShareType_1, ShareType_2, ShareType_3, ShareType_4, StockCode, Status, CreatedBy,CreatedDate FROM ShareHolder where ShareHolderID = @shareHolderId";
        public string GetCustomerInfo => @"SELECT ma_khach_hang CustomerID, ten_khach_hang CustomerName , so_dien_thoai Phone ,dia_chi_email Email,dia_chi_lien_lac Address from TVSI_TAI_KHOAN_KHACH_HANG where ma_khach_hang = @CustCode and trang_thai = 1 ";
        public string GetInvestmentInfo => @"SELECT A.Category as ItemID,b.CategoryName as ItemName,b.CategoryType as ItemTypeID FROM ExtendCategoryCust A inner join ExtendCust b on a.Category = b.Category where A.CustCode = @CustCode";
        public string GetCustomerInfoSupport => @"SELECT AttributeID,CustCode,Value FROM CustAttribute where CustCode = @CustCode and Status = 1";

        public string GetListAttributeCus => @"SELECT * FROM CustAttribute JOIN Attribute ON Attribute.AttributeID = CustAttribute.AttributeID WHERE CustAttribute.CustCode = @custCode";

        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
