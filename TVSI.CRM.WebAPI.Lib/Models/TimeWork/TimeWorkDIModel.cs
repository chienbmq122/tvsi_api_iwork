﻿using System;
using Newtonsoft.Json;

namespace TVSI.CRM.WebAPI.Lib.Models.TimeWork
{
    public class GetEmployee
    {
        public string EmployeeNo { get; set; }
    }
    public class SaleWorkTimeModel
    {
        public string EmployeeNo { get; set; }
        public string HoTenUnicode { get; set; }
        public string CardNumber { get; set; }

        public string DateWork
        {
            get
            {
                return DateWorkOn.ToString("dd/MM/yyyy");
            }
        }


        public string TimeIn
        {
            get
            {
                return TimeInStart.ToString("HH:mm:ss");
            }
        }

        public string TimeOut
        {
            get
            {
                return TimeOutEnd.ToString("HH:mm:ss");
            }
        }
        public int TypeKeepingNum { get; set; }
        public string TypeKeeping { get; set; }
        
        
        public string BranchCodeStart { get; set; }
        public string BranchCodeEnd { get; set; }
        
        
        [JsonIgnore]
        public DateTime DateWorkOn { get; set; }
        

        [JsonIgnore]
        public DateTime TimeInStart { get; set; }
        
        [JsonIgnore]
        public DateTime TimeOutEnd { get; set; }
    }
    
}