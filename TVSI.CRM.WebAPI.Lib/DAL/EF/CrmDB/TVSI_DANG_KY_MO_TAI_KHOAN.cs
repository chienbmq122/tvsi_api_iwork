﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{

    [Table("TVSI_DANG_KY_MO_TAI_KHOAN")]
    public class TVSI_DANG_KY_MO_TAI_KHOAN
    {
        [Key]
        public int dang_kyid { get; set; }
        public int loai_hinh_tk { get; set; }
        [Required]
        [StringLength(250)]
        public string ho_ten { get; set; }
        [StringLength(50)]
        public string so_dien_thoai_01 { get; set; }
        [StringLength(50)]
        public string sale_id { get; set; }
        public string trang_thai_tk { get; set; }
        public string trang_thai_tt { get; set; }
        public DateTime ngay_tao { get; set; }
        [StringLength(50)]
        public string nguoi_cap_nhat { get; set; }
        public DateTime ngay_cap_nhat { get; set; }
        public string noi_dung_xu_ly { get; set; }
        public string sale_assign { get; set; }
        public int? ConfirmStatus { get; set; }
        [JsonIgnore]
        public string ConfirmCode { get; set; }
        public string ConfirmSms { get; set; }
        public string trang_thai_ho_so { get; set; }
        public string ma_khach_hang { get; set; }
        public string so_cmnd { get; set; }
        public int nguon_du_lieu { get; set; }
        public string email { get; set; }
        
    }
}
