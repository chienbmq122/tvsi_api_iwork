﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class ContactCommandText : IContactCommandText
    {
        #region SQL Command List
        public string GetListUserStaf => @"SELECT A.ten_dang_nhap AS UserName, B.ho_va_ten AS FullName,
                                                 B.ma_nhan_vien_quan_ly AS SaleID, B.cap_quan_ly AS UserLevel 
                                          FROM TVSI_THONG_TIN_TRUY_CAP A 
                                          INNER JOIN TVSI_NHAN_SU B ON A.nhan_su_id = B.nhan_su_id 
                                          LEFT JOIN TVSI_CAP_DO_HE_THONG C ON A.cap_do_he_thong_id = C.cap_do_he_thong_id 
                                          WHERE A.trang_thai = 1 AND B.trang_thai = 1 {CON1} ORDER BY thong_tin_truy_cap_id DESC 
                                          OFFSET @start ROWS FETCH NEXT @pageSize ROWS ONLY";
        public string GetCountContact => @"SELECT count(1) FROM TVSI_THONG_TIN_TRUY_CAP A 
                                          INNER JOIN TVSI_NHAN_SU B ON A.nhan_su_id = B.nhan_su_id 
                                          LEFT JOIN TVSI_CAP_DO_HE_THONG C ON A.cap_do_he_thong_id = C.cap_do_he_thong_id 
                                          WHERE a.trang_thai = 1 {CON1}";
        public string GetListRoleStaf => @"SELECT ten_dang_nhap, ma_chuc_nang, ten_chuc_nang, max(IsRead) IsRead,
                                                    max(IsEdit) IsEdit ,
                                                    max(IsCreate) IsCreate ,
                                                    max(IsDelete) IsDelete ,
                                                    max(IsPrint) IsPrint ,
                                                    max(IsFind) IsFind ,
                                                    max(IsAllBranch) IsAllBranch ,
                                                    max(IsApproval) IsApproval ,
                                                    max(IsRekey) IsRekey
                                            FROM (
	                                            SELECT ten_dang_nhap, ma_chuc_nang, ten_chuc_nang, 
		                                            CASE WHEN ma_quyen_xu_ly = 'R' THEN 1 ELSE 0 END IsRead,
		                                            CASE WHEN ma_quyen_xu_ly = 'E' THEN 1 ELSE 0 END IsEdit,
		                                            CASE WHEN ma_quyen_xu_ly = 'C' THEN 1 ELSE 0 END IsCreate,
		                                            CASE WHEN ma_quyen_xu_ly = 'D' THEN 1 ELSE 0 END IsDelete,
		                                            CASE WHEN ma_quyen_xu_ly = 'P' THEN 1 ELSE 0 END IsPrint,
		                                            CASE WHEN ma_quyen_xu_ly = 'S' THEN 1 ELSE 0 END IsFind,
		                                            CASE WHEN ma_quyen_xu_ly = 'L' THEN 1 ELSE 0 END IsAllBranch,
		                                            CASE WHEN ma_quyen_xu_ly = 'A' THEN 1 ELSE 0 END IsApproval,
		                                            CASE WHEN ma_quyen_xu_ly = 'K' THEN 1 ELSE 0 END IsRekey
	                                            FROM (
		                                            SELECT DISTINCT ma_chuc_nang, ten_chuc_nang, ma_quyen_xu_ly, ten_quyen_xu_ly, f.ten_dang_nhap 
		                                            FROM TVSI_PHAN_QUYEN_SU_DUNG A                                                                   
		                                            INNER JOIN  TVSI_QUYEN_XU_LY_CHUC_NANG B
		                                            ON A.danh_muc_quyen_su_dung_id = B.danh_muc_quyen_su_dung_id
		                                            LEFT JOIN TVSI_QUYEN_XU_LY_UNG_VOI_CHUC_NANG C
		                                            ON B.quyen_xu_ly_ung_voi_chuc_nang_id = C.quyen_xu_ly_ung_voi_chuc_nang_id
		                                            LEFT JOIN TVSI_DANH_MUC_QUYEN_XU_LY D
		                                            ON C.danh_muc_quyen_xu_ly_id =D.danh_muc_quyen_xu_ly_id
		                                            LEFT JOIN TVSI_DANH_MUC_CHUC_NANG E
		                                            ON C.danh_muc_chuc_nang_id = E.danh_muc_chuc_nang_id
		                                            LEFT JOIN TVSI_THONG_TIN_TRUY_CAP F
		                                            ON A.thong_tin_truy_cap_id = F.thong_tin_truy_cap_id
		                                            WHERE A.trang_thai = 1 AND B.trang_thai = 1
			                                            AND C.trang_thai = 1 AND D.trang_thai = 1
			                                            AND E.trang_thai = 1 AND F.trang_thai = 1
			                                            AND F.ten_dang_nhap IN @tenDangNhap
		                                            ) AS A 
	                                            ) AS A
                                            GROUP BY ma_chuc_nang, ten_chuc_nang, ten_dang_nhap";

        //public string GetListContacCustomer => @"SELECT CustName, CustCode, LeadID, BitIsCust = 1, Mobile FROM CustInfo WHERE (AssignUser = @UserName Or AssignUser = @SaleId) {CON1} union all 
        //                                          SELECT LeadName as CustName, CustCode = '', LeadID, BitIsCust = 0, Mobile FROM Lead WHERE (AssignUser = @UserName Or AssignUser = @SaleId) {CON2} 
        //                                          Order by CustName DESC OFFSET @start ROWS FETCH NEXT @PageSize ROWS ONLY";

        //public string GetCountContacCustomer => @"SELECT count(1) FROM CustInfo WHERE (AssignUser = @UserName Or AssignUser = @SaleId) {CON1} union all 
        //                                          SELECT count(1) FROM Lead WHERE (AssignUser = @UserName Or AssignUser = @SaleId) {CON2}";

        public string GetListContacCustomer => @"SELECT CUSTOMER.CUSTOMERID as CustCode, CUSTOMER.CUSTOMERNAME as CustName, BitIsCust = 1,
                                                  (CASE WHEN CUSTOMER.CONTACTPHONE is not null THEN CUSTOMER.CONTACTPHONE ELSE 
                                                  (CASE WHEN (CUSTOMER.PHONE is not null AND CUSTOMER.PHONE <> '') THEN CUSTOMER.PHONE ELSE CUSTOMERPHONE.Phone END) END) as Mobile 
                                                  FROM CUSTOMER LEFT JOIN CUSTOMERPHONE ON CUSTOMER.CUSTOMERID = CUSTOMERPHONE.CustomerID 
                                                  AND CUSTOMERPHONE.IsSMS = 1 AND CUSTOMERPHONE.IsActive = 1
                                                  WHERE CUSTOMER.MKTID = @UserName {CON1} ORDER BY CUSTOMER.CUSTOMERNAME DESC OFFSET @start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetCountContacCustomer => @"SELECT count(1) FROM CUSTOMER LEFT JOIN CUSTOMERPHONE ON CUSTOMER.CUSTOMERID = CUSTOMERPHONE.CustomerID AND CUSTOMERPHONE.IsSMS = 1 
                                                    AND CUSTOMERPHONE.IsActive = 1 WHERE CUSTOMER.MKTID = @UserName {CON1}";

        public string GetInfoStaff => @"SELECT ma_nhan_vien_quan_ly as SaleID, ho_va_ten as SaleName, cap_quan_ly as Level, ten_phong_ban as Department 
                                        FROM TVSI_NHAN_SU 
                                        LEFT JOIN TVSI_DANH_MUC_PHONG_BAN ON TVSI_NHAN_SU.danh_muc_phong_ban_id = TVSI_DANH_MUC_PHONG_BAN.danh_muc_phong_ban_id
                                        WHERE ma_nhan_vien_quan_ly = @SaleId";
        public string GetInfoLead => @"SELECT LeadID
                                          ,LeadName
                                          ,LeadSourceID
                                      FROM Lead WHERE LeadID = @LeadID";
        public string GetInfoCust => @"SELECT CustCode as CustomerID
                                          ,CustName as CustomerName
                                          ,ProfileType
                                      FROM CustInfo WHERE CustCode = @CustCode";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
