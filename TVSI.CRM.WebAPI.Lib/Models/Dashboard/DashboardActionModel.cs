﻿using System.ComponentModel;

namespace TVSI.CRM.WebAPI.Lib.Models.Dashboard
{
    public class DashboardActionRequest : BaseRequest {}

    public class DashboardActionResult
    {
        [Description("Phân loại")]
        public int EventType { get; set; }
        [Description("Nội dung sự kiện")]
        public string Title { get; set; }
    }
}
