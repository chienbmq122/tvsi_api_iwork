﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Lib.Models
{
    public class BaseRequest
    {
        
        [Description]
        public string SaleID { get; set; }
        
        [Required]
        [Description("Tên đăng nhập")]
        public string UserName { get; set; }

        [Required]
        [Description("Level của User: Giám đốc chi nhánh, Trưởng phòng hay Nhân viên")]
        public int UserLevel { get; set; }

        [Required]
        [Description("Role của User: BackOffice, Sale Trái phiếu, Sale Cổ phiếu")]
        public string UserRole { get; set; }
    }

    public class BaseIWRequest
    {
        
    }

    public class SuccessModel
    {
        public SuccessModel()
        {
            RetCode = CommonConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }
    }

    public class SuccessModel<T>
    {
        public SuccessModel()
        {
            RetCode = CommonConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public T RetData { get; set; }
    }

    public class SuccessListModel<T>
    {
        public SuccessListModel()
        {
            RetCode = CommonConst.SUCCESS_CODE;
        }

        [Description("Mã kết quả thực hiện")]
        public string RetCode { get; set; }

        [Description("Dữ liệu trả về")]
        public IEnumerable<T> RetData { get; set; }
    }

    public class ErrorModel
    {

        public ErrorModel()
        {
        }

        public ErrorModel(string retCode)
        {
            RetCode = retCode;
        }

        public ErrorModel(string retCode, string errorMsg)
        {
            RetCode = retCode;
            _retMsg = errorMsg;
        }

        [Description("Mã lỗi")]
        public string RetCode { get; set; }

        private string _retMsg;

        [Description("Nội dung message lỗi")]
        public string ErrorMsg
        {
            get
            {
                if (!string.IsNullOrEmpty(_retMsg))
                    return _retMsg + "(" + RetCode + ")";

                return GetDefaultMsg(RetCode);

            }
            set { _retMsg = value; }
        }


        private static string GetDefaultMsg(string retCode)
        {
            if (CommonConst.ERR999_SYSTEM_ERROR.Equals(retCode))
                return ResourceFile.CommonErrors.E999 + "(" + retCode + ")";

            return "";
        }

    }
    public class ListChildLevelAndSelf
    {
        public int? cap_do_he_thong_id { get; set; }
        public string ma_cap_do_he_thong { get; set; }
        public string ma_chi_nhanh { get; set; }
        public string ten_cap_do_he_thong { get; set; }
        public int? cap_do_he_thong_muc_cha_id { get; set; }
        public int? loai_cap_do_he_thong { get; set; }
        public double? he_so_phan_bo_chi_tieu { get; set; }
        public int? trang_thai { get; set; }
        public DateTime? ngay_cap_nhat_cuoi { get; set; }
        public string nguoi_cap_nhat_cuoi { get; set; }
    }

    public class ListSubBranchAndSelf
    {
        public string ma_chi_nhanh { get; set; }
    }
    
}
