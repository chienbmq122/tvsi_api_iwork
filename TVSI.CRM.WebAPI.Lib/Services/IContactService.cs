﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Contact;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IContactService
    {
        Task<ContactStaffResponse> GetListContactStaff(ContactStaffRequest model);

        Task<ContactCustomerResponse> GetListContactCust(ContactStaffRequest model);

        Task<ContactInfoStaff> GetInfoContactStaff(ContactinfoStaffRequest model);
        Task<ContactInfoLead> GetInfoContactLead(ContactinfoLeadRequest model);
        Task<ContactInfoCustomer> GetInfoContactCust(ContactinfoCustRequest model);
    }
}
