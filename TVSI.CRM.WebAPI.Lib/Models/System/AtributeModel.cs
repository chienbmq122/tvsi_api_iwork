﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.System
{
    public class AtributeModel
    {
      public int AttributeID { get; set; }
      public string AttributeName { get; set; }
      public string AttributeLabel { get; set; }
      public string Description { get; set; }
      public int AttributeType { get; set; }
      public string SelectList { get; set; }
      public int AttributeGroupID { get; set; }
      public int Status { get; set; }
      public int Level { get; set; }
      public int OrderBy { get; set; }
    }
}
