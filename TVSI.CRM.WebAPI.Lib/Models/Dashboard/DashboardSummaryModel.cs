﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace TVSI.CRM.WebAPI.Lib.Models.Dashboard
{
    public class DashboardSummaryRequest : BaseRequest {}

    public class DashboardSummaryResult
    {
        [Description("Tổng số cơ hội")]
        public int TotalOpportunity { get; set; }
        [Description("Tổng số KH tiềm năng")]
        public int TotalLead { get; set; }
        [Description("Tổng số Account")]
        public int TotalAccount { get; set; }
        [Description("Tổng số hoạt động")]
        public int TotalActivity { get; set; }
    }

    public class TradingInfoModel
    {
        [Description("Danh sách lệnh khớp")]
        public List<DealInfoModel> DealList { get; set; }
        [Description("Danh sách lệnh đặt")]
        public List<OrderInfoModel> OrderList { get; set; }
    }

    public class OrderInfoModel
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Khối lượng hủy")]
        public long CancelledVolume { get; set; }
        [Description("Trạng thái hủy")]
        public int CancelStatus { get; set; }
        [Description("Trạn thái thay đổi")]
        public int ChangeStatus { get; set; }
        [Description("Giá thực hiện")]
        public decimal ExecutedPrice { get; set; }
        [Description("Khối lượng thực hiện")]
        public long ExecutedVol { get; set; }
        [Description("Trạng thái order")]
        public int OrderStatus { get; set; }
        [Description("Giá")]
        public decimal Price { get; set; }

        public string Side { get; set; }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }

        public string TransTime { get; set; }
        [Description("Khối lượng đặt")]
        public long Volume { get; set; }
        [Description("Thời gian đặt")]
        public string TransTimeStr
        {
            get
            {
                DateTime today = DateTime.Now;
                return today.ToString("dd/MM/yyyy") + " " + TransTime.Substring(9, 2) + ":" + TransTime.Substring(11, 2) + ":" + TransTime.Substring(13, 2);
            }
        }
        [Description("Loại lệnh")]
        public string SideStr
        {
            get
            {
                return Side == "B" ? "Mua" : "Bán";
            }
        }
        [Description("Trạng thái")]
        public string OrderStatusStr
        {
            get
            {
                if (OrderStatus == 13)
                    return "Chờ kích hoạt";

                if (OrderStatus == 0 || OrderStatus == 1)
                {
                    if (ExecutedVol > 0)
                    {
                        if (Volume == ExecutedVol)
                            return "Khớp hết";

                        if (Volume > ExecutedVol)
                            return "Khớp 1 phần";
                    }
                    else
                    {
                        return "Lệnh đang chờ";
                    }
                }

                if (OrderStatus == 2)
                {
                    if (CancelledVolume == Volume)
                        return "Huỷ hết";

                    if (ExecutedVol == Volume)
                        return "Khớp hết";

                    if (ExecutedVol > 0 && CancelledVolume > 0)
                        return "Khớp/Huỷ 1 phần";

                    if (ExecutedVol > 0)
                        return "Khớp 1 phần";
                }

                if (OrderStatus == 3)
                    return "Lệnh bị từ chối";

                if (OrderStatus == 12)
                    return "Lệnh chờ duyệt";

                if (CancelStatus == 0 || CancelStatus == 1)
                    return "Lệnh đang chờ huỷ";

                if (CancelStatus == 3)
                    return "Huỷ bị từ chối";

                if (CancelStatus == 2)
                    return "Lệnh đã huỷ";

                if (ChangeStatus == 0 || ChangeStatus == 1)
                    return "Lệnh đang chờ sửa";

                if (ChangeStatus == 2)
                    return "Lệnh đã sửa";

                if (ChangeStatus == 3)
                    return "Lệnh sửa bị từ chối";

                return OrderStatus + "";
            }
        }
    }

    public class DealInfoModel
    {
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Giá")]
        public decimal DealPrice { get; set; }
        [Description("Thời gian")]
        public string DealTime { get; set; }
        [Description("Khối lượng")]
        public int DealVolume { get; set; }
        public string Side { get; set; }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        [Description("Thời gian")]
        public string DealTimeStr
        {
            get
            {
                DateTime today = DateTime.Now;
                return today.ToString("dd/MM/yyyy") + " " + DealTime.Substring(0, 2) + ":" + DealTime.Substring(2, 2) + ":" + DealTime.Substring(4, 2);
            }
        }
        [Description("Loại lệnh")]
        public string SideStr
        {
            get
            {
                return Side == "B" ? "Mua" : "Bán";
            }
        }
    }
}
