﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Utilities.ConstParams
{
    public class CommonConst
    {
        public const string SOURCE_TYPE_M = "M";  // Mobile App
        public const string SOURCE_TYPE_W = "W";  // Website

        public const string LANGUAGE_VI = "VI";   // Việt Nam
        public const string LANGUAGE_EN = "EN";   // English

        // Return Code
        public const string SUCCESS_CODE = "000";           // Thanh cong
        public const string ERR999_SYSTEM_ERROR = "E999";    // Loi he thong
    }

    public class CommonConstExtend : CommonConst
    {
        public const string FAIL_DATA_EMPTY = "E100";           // Không tìm thấy dữ liệu
        public const string CARDID_EXSTED = "E101";           // CMT/CCCD đã tồn tại trong hệ thống
        public const string FAIL_SAVE_FAILED = "E111";           // Lưu không thành công
        public const string FAIL_VALIDATE_FAILED = "E222";           // Thiếu dữ liệu đầu vào
        public const string FAIL_VALIDATE_ACCOUNT_EXISTED = "E333";           // Dữ liệu tồn tại
        public const string ERROR_OCCURRED_E888 = "E888"; // Cõ lỗi xảy ra
        
    }
    
}
