﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Customer API
    /// </summary>
    public class CustomerController : BaseController
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerController));

        /// <summary>
        /// 
        /// </summary>
        public ICustomerServices _customerService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="customerService"></param>
        public CustomerController(ICustomerServices customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Lấy danh sách khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_LI_CustomerList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<CustomerListResult>>))]
        [Note("Quản lý khách hàng > Danh sách khách hàng", true)]
        public async Task<IHttpActionResult> CT_LI_CustomerList(CustomerListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_LI_CustomerList");
                SetLanguage(lang);

                var retData = await _customerService.GetListCust(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<CustomerListResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_LI_CustomerList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_LI_CustomerList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy thông tin cơ bản khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_GC_GetDetailCust")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustInfoResult>))]
        [Note("Lấy chi tiết khách hàng theo mã khách hàng - CustCode")]
        public async Task<IHttpActionResult> CT_GC_GetInfoCust(CustInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_GC_GetInfoCust");
                SetLanguage(lang);

                var retData = await _customerService.GetInfoCust(model);

                if (retData != null)
                {
                    var dataRet = new SuccessModel<CustInfoResult> { RetData = retData };
                    LogResult(log, dataRet, "CT_GC_GetInfoCust");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel
                {
                        
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_GC_GetInfoCust");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// API trả về danh sách tài khoản ngân hàng của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_BA_BankTranferList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<BankAccTransferResult>))]
        [Note("Thông tin chi tiết > Thông tin tài khoản đăng ký chuyển tiền", true)]
        public async Task<IHttpActionResult> CT_BA_BankTranferList(BankAccTransferRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_BA_BankTranferList");
                SetLanguage(lang);

                var retData = await _customerService.GetBankTranfer(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<BankAccTransferResult>> 
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_BA_BankTranferList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_BA_BankTranferList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// Lấy số dư có thể rút đuôi 1 và 6
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CB_CashBalanceInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CashBalanceInfoResult>))]
        [Note("Thông tin chi tiết > Số dư hiện tại(đuôi 1 và 6)", true)]
        public async Task<IHttpActionResult> CT_CB_CashBalanceInfo(CashBalanceInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CB_CashBalanceInfo");
                SetLanguage(lang);

                var retData = await _customerService.GetCashBalanceInfo(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<CashBalanceInfoResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_CB_CashBalanceInfo");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_BA_BankTranferList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
        /// <summary>
        /// Lấy danh mục đầu tư
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>

        [ActionName("CT_AP_AccPorfolio")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AccPorfolioResult>))]
        [Note("Thông tin chi tiết > Lấy danh mục đầu tư", true)]
        public async Task<IHttpActionResult> CT_AP_AccPorfolio(AccPorfolioRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_AP_AccPorfolio");
                SetLanguage(lang);
                
                
                var retData = await _customerService.GetAccPorfolio(model);
                if (retData.Any())
                {
                    var retModel = new SuccessModel<IEnumerable<AccPorfolioResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CT_AP_AccPorfolio");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                    
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_AP_AccPorfolio");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// Lấy thông tin giao dịch trong ngày
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>

        [ActionName("CT_TN_TradingInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<TradingInfoResult>))]
        [Note("Thông tin chi tiết > Giao dịch trong ngày", true)]
        public async Task<IHttpActionResult> CT_TN_TradingInfoList(TradingInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_TN_TradingInfoList");
                SetLanguage(lang);

                var retData = await _customerService.GetTradingInfoList(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<TradingInfoResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_TN_TradingInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_TN_TradingInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Danh sách thuộc tính mở rộng của khách hàng
        /// </summary>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_EC_ExCustInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ExCustInfoResult>))]
        [Note("Danh sách thuộc tính mở rộng của khách hàng", true)]
        public async Task<IHttpActionResult> CT_EC_ExCustInfoList(string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequestAnonymous(log, null, "CT_EC_ExCustInfoList");
                SetLanguage(lang);

                var retData = await _customerService.GetExCustInfoList();

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<ExCustInfoResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_EC_ExCustInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogRequestAnonymous(log, null, "CT_EC_ExCustInfoList" + ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách hoạt động theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_CA_CustActivityInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustActivityResult>))]
        [Note("Thông tin chi tiết > Lịch sử hoạt động", true)]
        public async Task<IHttpActionResult> CT_CA_CustActivityInfoList(CustActivityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CA_CustActivityInfoList");
                SetLanguage(lang);

                var retData = await _customerService.GetActivityCustList(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<CustActivityResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_CA_CustActivityInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                
                LogException(log, model, ex, "CT_CA_CustActivityInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }



        /// <summary>
        /// Lấy danh sách cơ hội theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_CO_CustOpportunityInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustOpportunityResult>))]
        [Note("Thông tin chi tiết > Lịch sử hoạt động", true)]
        public async Task<IHttpActionResult> CT_CO_CustOpportunityInfoList(CustOpportunityRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CO_CustOpportunityInfoList");
                SetLanguage(lang);
                var retData = await _customerService.GetOpportunityCustList(model);
                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<CustOpportunityResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_CO_CustOpportunityInfoList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_CO_CustOpportunityInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }



        /// <summary>
        /// Update thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_CE_CustEdit")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("KH tiềm năng > Update thông tin khách hàng", true)]
        public async Task<IHttpActionResult> CT_CE_CustEdit(CustEditRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {

             
                LogRequest(log, model, "CT_CE_CustEdit");
                SetLanguage(lang);
                var retData = await _customerService.UpdateCustInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "LD_LE_LeadEdit");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                
                LogException(log, model, ex, "CT_CE_CustEdit");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách cổ đông
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_SH_ShareHolderList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<ShareHolderListResult>>))]
        [Note("Quản lý khách hàng > Danh sách cổ đông", true)]
        public async Task<IHttpActionResult> CT_SH_ShareHolderList(ShareHolderListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_SH_ShareHolderList");
                SetLanguage(lang);

                var retData = await _customerService.GetListShareHolder(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<IEnumerable<ShareHolderListResult>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_SH_ShareHolderList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_SH_ShareHolderList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// Lấy danh sách khách hàng select
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        [ActionName("CT_CS_CustListSelect")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustomerListSelectResponse>))]
        [Note("Lấy danh sách khách hàng select")]
        public async Task<IHttpActionResult> CT_CS_CustListSelect(CustomerListSelectRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_CS_CustListSelect");
                SetLanguage(lang);
                var retData = await _customerService.GetListCustSelect(model);
                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<CustomerListSelectResponse>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "CT_CS_CustListSelect");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogRequest(log, model, "CT_CS_CustListSelect" + ex);
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Update thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_SH_ShareHolderAdd")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Danh sách đổ đông > Đăng ký thông tin cổ đông", true)]
        public async Task<IHttpActionResult> CT_SH_ShareHolderAdd(ShareHolderAddRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {


                LogRequest(log, model, "CT_SH_ShareHolderAdd");
                SetLanguage(lang);
                var retData = await _customerService.AddNewShareHolderInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "CT_SH_ShareHolderAdd");
                return Ok(retModel);
            }
            catch (Exception ex)
            {

                LogException(log, model, ex, "CT_SH_ShareHolderAdd");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// View thông tin cổ đông lớn
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_SH_ShareHolderInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<ShareHolderInfoResult>))]
        [Note("Danh sách đổ đông > View thông tin chi tiết cổ đông lớn", true)]
        public async Task<IHttpActionResult> CT_SH_ShareHolderInfo(ShareHolderInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_SH_ShareHolderInfo");
                SetLanguage(lang);
                var retData = await _customerService.GetShareHolderInfo(model);
                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var dataRet = new SuccessModel<ShareHolderInfoResult> { RetData = retData };
                LogResult(log, dataRet, "CT_SH_ShareHolderInfo");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_SH_ShareHolderInfo");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
        
        /// <summary>
        /// lấy thông tin chi tiết của KH
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_GE_GetCustomerInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<CustomerInfoResult>))]
        [Note("Danh sách đổ đông > View thông tin chi tiết cổ đông lớn", true)]
        public async Task<IHttpActionResult> CT_GE_GetCustomerInfo(CustomerDetailInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_GE_GetCustomerInfo");
                SetLanguage(lang);
                var retData = await _customerService.GetCustomerInfo(model);
                if (retData != null)
                {
                    var dataRet = new SuccessModel<CustomerInfoResult> { RetData = retData };
                    LogResult(log, dataRet, "CT_GE_GetCustomerInfo");
                    return Ok(dataRet);

                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_GE_GetCustomerInfo");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        /// <summary>
        /// Kiểm tra số điện thoại khách hàng tiềm năng tồn tại 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CT_EP_ExistsPhonePotential")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Danh sách đổ đông > View thông tin chi tiết cổ đông lớn", true)]
        public async Task<IHttpActionResult> CT_EP_ExistsPhonePotential(PhonePotentialInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CT_EP_ExistsPhonePotential");
                SetLanguage(lang);
                var retData = await _customerService.ExistsPhonePotential(model);
                if (retData)
                {
                    var dataRet = new SuccessModel<bool> { RetData = true };
                    LogResult(log, dataRet, "CT_EP_ExistsPhonePotential");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_VALIDATE_ACCOUNT_EXISTED,
                    ErrorMsg = ResourceFile.CommonErrors.E333
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CT_EP_ExistsPhonePotential");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }  
        
        
        /// <summary>
        /// Kiểm tra số điện thoại theo danh sách
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_CA_GetPhoneInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<PhoneInfoListResult>))]
        [Note("Danh sách đổ đông > View thông tin chi tiết cổ đông lớn", true)]
        public async Task<IHttpActionResult> CC_CA_GetPhoneInfoList(PhoneInfoListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_CA_GetPhoneInfoList");
                SetLanguage(lang);
                var retData = await _customerService.GetPhoneInfoList(model);
                if (retData.Any())
                {
                    var dataRet = new SuccessListModel<PhoneInfoListResult>(){ RetData = retData };
                    LogResult(log, dataRet, "CC_CA_GetPhoneInfoList");
                    return Ok(dataRet);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_CA_GetPhoneInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
        
        
    }
}