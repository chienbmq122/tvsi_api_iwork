﻿namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class TimeWorkCommandText : ITimeWorkCommandText
    {
        public string GetCardNumberEmsDb =>
            @"SELECT EmployeeNo FROM TVSI_THONG_TIN_TRUY_CAP_CHI_TIET WHERE id_he_thong = @username AND trang_thai=1";

        public string GetTimeWorkByDate => @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd ,N'Vân tay' TypeKeeping, 1 TypekeepingNum
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd,N'Vân tay' TypeKeeping, 1 TypekeepingNum
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd ,N'Vân tay' TypeKeeping, 1 TypekeepingNum
                                            from DataReader 
                                            where CardNumber = @CardNumber and CAST(TimeReader as date) = CAST( @Date AS DATE)  
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetTimeWorkByDateGPSCRM => @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd ,N'GPS' TypeKeeping, 2 TypekeepingNum
                                from [TVSI_TIMEWORKS].[dbo].Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd,N'GPS' TypeKeeping, 2 TypekeepingNum
                                        from (
                                             select CardNumber, CONVERT(varchar(10), TimeCheckin, 103) DateWorkOn, min(TimeCheckin) TimeInStart, max(TimeCheckin) TimeOutEnd, N'GPS' TypeKeeping, 2 TypekeepingNum
                                            from [TimeworkGPS] 
                                            where CardNumber = @CardNumber and CAST(TimeCheckin as date) = CAST( @Date AS DATE)     
                                            group by CardNumber, CONVERT(varchar(10), TimeCheckin, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";


        /*public string GetTimeWorkByDate => @"
  select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd,TypeKeepingStartNum,TypeKeepingEndNum,
         case when TypeKeepingStartNum = 1 then N'Vân tay' 
             when TypeKeepingStartNum = 2 then N'GPS' end as TypeKeepingStart,case when TypeKeepingEndNum = 1 then N'Chấm công' when TypeKeepingEndNum = 2 then N'GPS' end as TypeKeepingEnd,BranchCodeStart,BranchCodeEnd      
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd,TypeKeepingStartNum,TypeKeepingEndNum,BranchCodeStart,BranchCodeEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), d.TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd, 
                                  case															  
								   when cast(MIN(TimeReader) as time) IN (SELECT CAST(TimeReader AS time) FROM DataReader
								   where CardNumber = @CardNumber and cast(TimeReader as date) = CAST( @Date AS DATE)) 
								   then 1 
								   when
								    cast(MIN(TimeReader) as time) IN (SELECT CAST(TimeCheckin AS time) FROM [TVSI_CRM].[dbo].[TimeworkGPS]
								   where CardNumber = @CardNumber and cast(TimeCheckin as date) =  CAST( @Date AS DATE) )
								   Then 2 end as TypeKeepingStartNum
									,
									case 
								   when 
								   cast(Min(TimeReader) as time) IN (SELECT CAST(TimeCheckin AS time) FROM [TVSI_CRM].[dbo].[TimeworkGPS]
								   where CardNumber = @CardNumber  and cast(TimeCheckin as date) = CAST( @Date AS DATE) )
								    then (select BranchCode FROM [TVSI_CRM].[dbo].[TimeworkGPS] where CAST(Min(TimeReader) as time) = CAST(TimeCheckin AS time)) 
									end BranchCodeStart,
									 case 
								   		when cast(max(TimeReader) as time) IN (SELECT CAST(TimeReader AS time) FROM DataReader
								   where CardNumber = @CardNumber and cast(TimeReader as date) =  CAST( @Date AS DATE)  )
								   then  1
								   when 
								   cast(MAX(TimeReader) as time) IN (SELECT CAST(TimeCheckin AS time) FROM [TVSI_CRM].[dbo].[TimeworkGPS]
								   where CardNumber = @CardNumber and cast(TimeCheckin as date) =  CAST( @Date AS DATE)  )
								    then 2		end TypeKeepingEndNum,
                                    case 
								   when 
								   cast(MAX(TimeReader) as time) IN (SELECT CAST(TimeCheckin AS time) FROM [TVSI_CRM].[dbo].[TimeworkGPS]
								   where CardNumber = @CardNumber and cast(TimeCheckin as date) =  CAST( @Date AS DATE))
								    then(select BranchCode FROM [TVSI_CRM].[dbo].[TimeworkGPS] where CAST(MAX(TimeReader) as time) = CAST(TimeCheckin AS time))
									 end BranchCodeEnd
                                            from  (																														
											select CardNumber CardNumber, TimeReader TimeReader , N'Chấm công'  TypeKeeping
                                            from DataReader where CardNumber = @CardNumber and cast(TimeReader as date) =  CAST( @Date AS DATE)  
											UNION ALL
											select CardNumber CardNumber, TimeCheckin as TimeReader, N'GPS' TypeKeeping
                                            from  [TVSI_CRM].[dbo].[TimeworkGPS]  where CardNumber = @CardNumber and cast(TimeCheckin as date) =  CAST( @Date AS DATE)   																																																										
											) as d group by CardNumber, CONVERT(varchar(10),d.TimeReader, 103) 
											) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc"; */                          

        public string GetTimeWorkFromDateAndToDate =>
            @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and  CAST(TimeReader as DATE ) >= CAST(@FromDate as DATE ) and CAST(TimeReader as DATE ) <= CAST(@ToDate as DATE ) 
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetTimeWorkByMonth => @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@Month as date)) and YEAR(TimeReader) = year(Cast(@Month as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetLeaveApplications =>
            @" SELECT TitleName as Title,
              AssignUser as UserName, 
              Description as NoteReason,
              StartDate as TimeLeaveStartIn, 
              EndDate as TimeLeaveEndOut, 
              CreatedDate as DateOfApplicants, 
              ConfirmBy as Approved, 
			  case 
			  when TimeWorkType = 1 then N'Chấm công' 
			  when TimeWorkType = 2 then N'Làm thêm giờ' 
			  when TimeWorkType = 3 then N'Nghỉ phép'
			  ELSE N'Không có dữ liệu' END 
			   as TypeLeave,       
        TimeWorkType as TypeLeaveNum,       	
        	VacationType as PolicyLeave,
        	case 
        	    when VacationType = 1 then N'Nghỉ theo chế độ phép năm'
        	    when VacationType = 2 then N'Nghỉ phép đặc biệt'
        	    when VacationType = 3 then N'Nghỉ bù'
        	    when VacationType = 4 then N'Nghỉ ốm'
        	    when VacationType = 5 then N'Nghỉ con ốm'
        	    when VacationType = 6 then N'Nghỉ không lương'
        	    when VacationType = 7 then N'Khác'
        	    else N'Không có dữ liệu' END as PolicyLeaveName,
        	VacationNumber as NumLeave,
			   case 
			   when ConfirmStatus = 1 then N'Đã duyệt'
			   when ConfirmStatus = 0 then N'Đang chờ duyệt'
			   when ConfirmStatus = 99 then N'Đã xóa'
			   when ConfirmStatus = -1 then N'Từ chối'
			   when ConfirmStatus = 2 then N'DB đã duyệt'
			   ELSE N'Không có dữ liệu' END as StatusLeave,
			   ConfirmStatus as StatusLeaveNumber
			    FROM [dbo].[TimeWorks] WHERE AssignUser = @username and TimeWorkID = @leaveid";

        public string GetNameStaffEmsDb =>
            @"SELECT N.ho_va_ten  as FullName FROM [dbo].[TVSI_NHAN_SU] N WHERE N.id_he_thong = @saleid";

        public string GetNameStaffEmsDbtttcct =>  @"SELECT N.ho_ten  as FullName FROM [dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] N WHERE N.id_he_thong = @UserName and N.user_domain = @UserDomain and trang_thai = 1";


        public string GetDepartmentStaffEmsDb =>
            @"select c.ten_phong_ban as Department from [dbo].[TVSI_THONG_TIN_TRUY_CAP]
	                                               a left join [dbo].[TVSI_NHAN_SU] b 
                                                   ON a.nhan_su_id = b.nhan_su_id left join [dbo].[TVSI_DANH_MUC_PHONG_BAN] c
                                                   ON b.danh_muc_phong_ban_id = c.danh_muc_phong_ban_id
                                                   where a.ten_dang_nhap = @username";

        public string GetLeaveApplicationsApproved =>
            @"select COUNT(TimeWorkID) as TimeWorkID from [TimeWorks] where AssignUser = @username and TimeWorkType =  3 and ConfirmStatus = 1";

        public string GetWithoutLeave =>
            @" select COUNT(TimeWorkID) from [TimeWorks] where AssignUser = @username and TimeWorkType =  3 and ConfirmStatus = 0 and CreatedDate < GETDATE()";

        public string GetTimeWorkLate => @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and  CAST(TimeReader as DATE ) >= CAST(@FromDate as DATE ) and CAST(TimeReader as DATE ) <= CAST(@ToDate as DATE ) 
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
                                            having CAST(Min(TimeReader) as time) > '08:00:00'
                                            ) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetTimeWorkLateByMonth =>
            @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@Month as date)) and YEAR(TimeReader) = year(Cast(@Month as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
                                            having CAST(Min(TimeReader) as time) > '08:00:00'
                                            ) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";


        public string GetTimeWorkBackEarly =>
            @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and  CAST(TimeReader as DATE ) >= CAST(@FromDate as DATE ) and CAST(TimeReader as DATE ) <= CAST(@ToDate as DATE ) 
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
                                            having CAST(max(TimeReader) as time) < '17:00:00'
                                            ) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetTimeWorkBackEarlyByMonth =>
            @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd 
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@Month as date)) and YEAR(TimeReader) = year(Cast(@Month as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
                                            having CAST(max(TimeReader) as time) < '17:00:00'
                                            ) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetMinuteLate => @"select SUM(tw.TimeInStart)
											 as TimeInStart from
											(select   DATEDIFF(MINUTE,'08:00:00', cast(min(TimeReader) as time)) as TimeInStart
                                            from DataReader D
                                            where CardNumber = @CardNumber and  CAST(TimeReader as DATE ) >= CAST(@FromDate as DATE ) and CAST(TimeReader as DATE ) <= CAST(@ToDate as DATE ) 
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
											 having CAST(min(TimeReader) as time) > '08:00:00') as tw";

        public string GetMinuteLateByMonth => @"select SUM(tw.TimeInStart)
											 as TimeInStart from
											(select   DATEDIFF(MINUTE,'08:00:00', cast(min(TimeReader) as time)) as TimeInStart
                                            from DataReader D
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@Month as date)) and YEAR(TimeReader) = year(Cast(@Month as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
											 having CAST(min(TimeReader) as time) > '08:00:00') as tw";

        public string GetMinuteEarlyWork => @"select SUM(tw.TimeOutEnd)
											 as TimeOutEnd from
											(select   DATEDIFF(MINUTE,'17:00:00', cast(max(TimeReader) as time)) as TimeOutEnd
                                            from DataReader D
                                            where CardNumber = @CardNumber and  CAST(TimeReader as DATE ) >= CAST(@FromDate as DATE ) and CAST(TimeReader as DATE ) <= CAST(@ToDate as DATE ) 
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
											 having CAST(max(TimeReader) as time) < '17:00:00') as tw";

        public string GetMinuteEarlyWorkByMonth => @"select SUM(tw.TimeOutEnd)
											 as TimeOutEnd from
											(select   DATEDIFF(MINUTE,'17:00:00', cast(max(TimeReader) as time)) as TimeOutEnd
                                            from DataReader D
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@Month as date)) and YEAR(TimeReader) = year(Cast(@Month as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)
											 having CAST(max(TimeReader) as time) < '17:00:00') as tw";

        /*public string GetLeaveApplicationsStaff => @"SELECT TitleName as Title, case 
											  when TimeWorkType = 1 then N'Chấm công' 
											  when TimeWorkType = 2 then N'Làm thêm giờ' 
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as TypeLeave,
											   CreatedDate as DateApplication,
											   Description as NoteReason,
											   StartDate as TimeStart,
											   EndDate as TimeEnd,
											   ConfirmBy as Approved,
											   ConfirmStatus as StatusNumber,
											   case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusName
											      from [dbo].[TimeWorks] where AssignUser = @UserName and CreatedDate >= cast(@FromDate as date) 
											      
												and TimeWorkType = @TypeLeave  and ConfirmStatus = @ApprovedStatus
 												ORDER BY TimeWorkID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY
 												";  */
        public string GetLeaveApplicationsStaff => @"
                                              SELECT TimeWorkID as LeaveId,
                                              TitleName as Title, case 
											  when TimeWorkType = 1 then N'Chấm công' 
											  when TimeWorkType = 2 then N'Làm thêm giờ' 
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as TypeLeave,
                                                   TimeWorkType as  TypeLeaveNum ,
                                                     VacationType as PolicyLeave,
        	case 
        	    when VacationType = 1 then N'Nghỉ theo chế độ phép năm'
        	    when VacationType = 2 then N'Nghỉ phép đặc biệt'
        	    when VacationType = 3 then N'Nghỉ bù'
        	    when VacationType = 4 then N'Nghỉ ốm'
        	    when VacationType = 5 then N'Nghỉ con ốm'
        	    when VacationType = 6 then N'Nghỉ không lương'
        	    when VacationType = 7 then N'Khác'
        	    else N'Không có dữ liệu' END as PolicyLeaveName,
        	VacationNumber as NumLeave,
											   CreatedDate as DateApplication,
											   Description as NoteReason,
											   StartDate as TimeStart,
											   EndDate as TimeEnd,
											   ConfirmBy as Approved,
											   ConfirmStatus as StatusNumber,
											   case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusName
											      from [dbo].[TimeWorks] where AssignUser = @UserName {cond1} {cond2} {cond3} {cond4}
 										        ORDER BY TimeWorkID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY
 												";  
        public string GetLeaveApplicationsStaffByLeader => @"SELECT
        										   TimeWorkID as leaveId,
                                                   TitleName as Title, case 
											  when TimeWorkType = 1 then N'Chấm công' 
											  when TimeWorkType = 2 then N'Làm thêm giờ' 
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as TypeLeave,
       											AssignUser as UserName,
											   CreatedDate as DateApplication,
											   ConfirmStatus as StatusNumber,
											   case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusName
											      from [dbo].[TimeWorks] where AssignUser = @UserName and CreatedDate >= cast(@FromDate as date) and CreatedDate <= cast(@ToDate as date) 
												and TimeWorkType = @TypeLeave  and ConfirmStatus = @ApprovedStatus
 												ORDER BY TimeWorkID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY
 												";

        public string GetLeaveCrmByDay => @"SELECT
        										   TimeWorkID as leaveId,
                                                   TitleName as Title, case 
											  when TimeWorkType = 1 then N'Chấm công' 
											  when TimeWorkType = 2 then N'Làm thêm giờ' 
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as TypeLeave,
       											AssignUser as UserName,
											   CreatedDate as DateApplication,
											   ConfirmStatus as StatusNumber,
											   case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusName
											      from [dbo].[TimeWorks] where AssignUser = @UserName and cast(CreatedDate as date) >= cast(@FromDate as date) and cast(CreatedDate as date) <= cast(@ToDate as date)					
 												ORDER BY TimeWorkID DESC";

        public string GetNameLeaderApprovedByUserName =>
					        @" select id_he_thong as UserName from 
				  [dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] where  
				  user_domain in ( select REPLACE(SUBSTRING([email_man],1,CHARINDEX('@',[email_man])),'@','') as 
				  email_man from [dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] where  user_domain = @UserDomain and id_he_thong = @UserName) GROUP BY id_he_thong
				";

        public string GetNameMemberApprovedByUserName => @"select id_he_thong as UserName from [dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] where email_man = @EmailMan and trang_thai = 1 
  GROUP BY id_he_thong";

        public string GetUserDomainByUserName => @"SELECT user_domain FROM TVSI_THONG_TIN_TRUY_CAP_CHI_TIET WHERE id_he_thong = @username AND trang_thai = 1";
        public string GetUserDomain => @"SELECT user_domain FROM TVSI_THONG_TIN_TRUY_CAP_CHI_TIET WHERE id_he_thong = @UserName and user_domain = @UserDomain AND trang_thai=1";

        public string GetLeaveApplicationsMemberByLeader => @"
             SELECT 
                 TimeWorkID as LeaveId,
                TitleName as Title,
              AssignUser as UserName, 
              CreatedDate as DateApplication,
				ConfirmStatus as StatusNumber,
                TimeWorkType as TimeWorkType,
				case 
				when ConfirmStatus = 1 then N'Đã duyệt'
				when ConfirmStatus = 0 then N'Đang chờ duyệt'
				when ConfirmStatus = 99 then N'Đã xóa'
				when ConfirmStatus = -1 then N'Từ chối'
				when ConfirmStatus = 2 then N'DB đã duyệt'
				ELSE N'Không có dữ liệu' END as StatusName
			    FROM [dbo].[TimeWorks]
  				where ConfirmStatus <> 2 and 
  				ConfirmStatus = 0 and  AssignUser in( select id_he_thong from [TVSI_EMSDB].[dbo].[TVSI_THONG_TIN_TRUY_CAP_CHI_TIET] where email_man = @EmailMan ) 
 				ORDER BY TimeWorkID DESC
                 OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY ";   
        public string GetLeaveApplicationsMemberByLeaderBD => @"
             SELECT 
                 TimeWorkID as LeaveId,
                TitleName as Title,
              AssignUser as UserName, 
              CreatedDate as DateApplication,
				ConfirmStatus as StatusNumber,
				case 
				when ConfirmStatus = 1 then N'Đã duyệt'
				when ConfirmStatus = 0 then N'Đang chờ duyệt'
				when ConfirmStatus = 99 then N'Đã xóa'
				when ConfirmStatus = -1 then N'Từ chối'
				when ConfirmStatus = 2 then N'DB đã duyệt'
				ELSE N'Không có dữ liệu' END as StatusName
			    FROM [dbo].[TimeWorks]
  				where ConfirmStatus = 0 and TimeWorkType <> 2 and BranchID in @listID
 				ORDER BY TimeWorkID DESC
                 OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY ";
        
        public string GetLeaveApplicationsMemberByLeaderBDNotCtype2 => @"
             SELECT 
                 TimeWorkID as LeaveId,
                TitleName as Title,
              AssignUser as UserName, 
              CreatedDate as DateApplication,
				ConfirmStatus as StatusNumber,
				case 
				when ConfirmStatus = 1 then N'Đã duyệt'
				when ConfirmStatus = 0 then N'Đang chờ duyệt'
				when ConfirmStatus = 99 then N'Đã xóa'
				when ConfirmStatus = -1 then N'Từ chối'
				when ConfirmStatus = 2 then N'DB đã duyệt'
				ELSE N'Không có dữ liệu' END as StatusName
			    FROM [dbo].[TimeWorks]
  				where TimeWorkType = 2 and ConfirmStatus = 1 and BranchID in @listID
 				ORDER BY TimeWorkID DESC
                 OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetHisTWAndLAStaffByDay => @"select TimeWorkID as LeaveId,TitleName as Title,TimeWorkType AS TypeLeave, RejectReason as NoteReason,CreatedDate as TimeApp, ConfirmBy as Approved, ConfirmStatus as StatusNumber, case 
				when ConfirmStatus = 1 then N'Đã duyệt'
				when ConfirmStatus = 0 then N'Đang chờ duyệt'
				when ConfirmStatus = 99 then N'Đã xóa'
				when ConfirmStatus = -1 then N'Từ chối'
				when ConfirmStatus = 2 then N'DB đã duyệt'
				ELSE N'Không có dữ liệu' END as StatusName from  [dbo].[TimeWorks] where AssignUser = @UserName and CAST(CreatedDate as Date) = CAST(@DateWork as Date) and TimeWorkType = @TypeLeave ";

        public string GetTimeKeepingByMonth => @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd, case when DATEDIFF(day, TimeInStart, TimeOutEnd) = 0 then  DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0
  else             DATEDIFF(minute, TimeInStart, DATEADD(day, DATEDIFF(day, 0, TimeInStart), 1)) / 60.0
   end as TotalHours, 
   case
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 < 4 then 0
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 > 4 and  DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 < 8 then 0.5
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 >= 8 then 1
   end as TimeKeeping
                                from Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd
                                        from (
                                            select CardNumber, CONVERT(varchar(10), TimeReader, 103) DateWorkOn, min(TimeReader) TimeInStart, max(TimeReader) TimeOutEnd 
                                            from DataReader 
                                            where CardNumber = @CardNumber and month(TimeReader) = month(Cast(@MonthWork as date)) and YEAR(TimeReader) = year(Cast(@MonthWork as date))
                                            group by CardNumber, CONVERT(varchar(10), TimeReader, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetTimeKeepingByMonthGPS =>
	        @"select EmployeeNo, HoTenUnicode, E.CardNumber, convert(datetime, DateWorkOn, 103) as DateWorkOn, TimeInStart, TimeOutEnd ,N'GPS' TypeKeeping, 2 TypekeepingNum,case when DATEDIFF(day, TimeInStart, TimeOutEnd) = 0 then  DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0
  else             DATEDIFF(minute, TimeInStart, DATEADD(day, DATEDIFF(day, 0, TimeInStart), 1)) / 60.0
   end as TotalHours, 
   case
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 < 4 then 0
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 > 4 and  DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 < 8 then 0.5
   when DATEDIFF(minute, TimeInStart, TimeOutEnd) / 60.0 >= 8 then 1
   end as TimeKeeping
                                from [TVSI_TIMEWORKS].[dbo].Employees E
                                inner join 
                                    (Select CardNumber, DateWorkOn, CONVERT(varchar(8), TimeInStart, 114) TimeInStart, CONVERT(varchar(8), TimeOutEnd, 114) TimeOutEnd,N'GPS' TypeKeeping, 2 TypekeepingNum
                                        from (
                                             select CardNumber, CONVERT(varchar(10), TimeCheckin, 103) DateWorkOn, min(TimeCheckin) TimeInStart, max(TimeCheckin) TimeOutEnd, N'GPS' TypeKeeping, 2 TypekeepingNum
                                            from [TimeworkGPS] 
                                            where CardNumber = @CardNumber and month(TimeCheckin) = month(Cast(@MonthWork as date)) and YEAR(TimeCheckin) = year(Cast(@MonthWork as date))    
                                            group by CardNumber, CONVERT(varchar(10), TimeCheckin, 103)) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, DateWorkOn, 103) desc";

        public string GetIdTimeWorkLeaveApp =>
	        @"  select  TimeWorkID as LeaveId , TitleName as Title, AssignUser as UserName,Description as NoteReason, StartDate as TimeLeaveStartIn,
  EndDate as TimeLeaveEndOut,CreatedDate as TimeApp,ConfirmBy as Approved, case 
											  when TimeWorkType = 1 then N'Chấm công' 
											  when TimeWorkType = 2 then N'Làm thêm giờ' 
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as NameTypeLeave, case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusLeave
    from [dbo].[TimeWorks] where AssignUser = @UserName and CAST(CreatedDate as Date) = CAST(@DateWork as Date) and TimeWorkID = @Id";
        

        public string GetTimeWorkTypeByDay => @"select case 
			  when TimeWorkType = 1 then N'Đơn chấm công' 
			  when TimeWorkType = 2 then N'Đơn làm thêm giờ' 
			  when TimeWorkType = 3 then N'Đơn nghỉ phép'
			  ELSE N'Không có dữ liệu' END 
			   as NameTypeGroup,
       			TimeWorkType AS TypeLeave,
       			COUNT(TimeWorkType) AS CountTypeLeave from [dbo].[TimeWorks] where AssignUser = @UserName  and CAST(CreatedDate as Date) = CAST(@DateWork as Date) group by TimeWorkType
			   ";

        public string GetLeaveApplicationsByMonth => @"SELECT
              TimeWorkID as LeaveId,
              CASE 
			  when TimeWorkType = 1 then N'Đơn chấm công' 
			  when TimeWorkType = 2 then N'Đơn làm thêm giờ' 
			  when TimeWorkType = 3 then N'Đơn nghỉ phép'
			  ELSE N'Không có dữ liệu' END 
			   as TypeLeaveName,
       		  TimeWorkType as TypeLeaveNumber,
       		  case 
				when ConfirmStatus = 1 then N'Đã duyệt'
				when ConfirmStatus = 0 then N'Đang chờ duyệt'
				when ConfirmStatus = 99 then N'Đã xóa'
				when ConfirmStatus = -1 then N'Từ chối'
				when ConfirmStatus = 2 then N'DB đã duyệt'
				ELSE N'Không có dữ liệu' END as StatusName,
       		  ConfirmStatus as StatusNumber,
       CreatedDate  as DateLeave
          FROM [dbo].[TimeWorks] where AssignUser = @UserName  and MONTH(CAST(CreatedDate as Date)) = MONTH(CAST(@MonthWork as Date))";

        
        /// <summary>
        /// Get thông tin chấm công của tháng
        /// </summary>
        public string GetTimeKeepingFromMonthToMonthTW => @"select convert(datetime, MonthWork, 103) MonthWork
                                    from Employees E
                                    inner join 
                                    (Select CardNumber, convert(datetime, MonthWork, 103) as MonthWork
                                        from (
                                            select CardNumber, FORMAT(TimeReader, '01-MM-yyyy')  as MonthWork 
                                            from DataReader 
                                            where CardNumber = @CardNumber and month(TimeReader) >= month(Cast(@FromMonth as date)) and YEAR(TimeReader) >= year(Cast(@FromMonth as date))
											 and month(TimeReader) <= month(Cast(@ToMonth as date))  and YEAR(TimeReader) <= year(Cast(@ToMonth as date))
                                            group by CardNumber, FORMAT(TimeReader, '01-MM-yyyy')) t) as DR
                                    on E.CardNumber = DR.CardNumber order by convert(datetime, MonthWork, 103) desc";

        public string GetTimeWorkLeaveByMonthCrm => @"select FORMAT(CAST(CreatedDate AS datetime), '01-MM-yyyy') as MonthWork 
  from dbo.TimeWorks where  AssignUser = @UserName and month(CAST(CreatedDate AS datetime)) >= month(Cast(@FromMonth as datetime))
   and YEAR(CAST(CreatedDate AS datetime)) >= year(Cast(@FromMonth as datetime))
											 and CAST(CreatedDate AS datetime) <= Cast(@ToMonth as datetime) and YEAR(CAST(CreatedDate AS datetime)) <= year(Cast(@ToMonth as datetime))
											 group by FORMAT(CAST(CreatedDate AS datetime), '01-MM-yyyy') ";

        public string FillterLAMemberReportByLeader => @"SELECT
        										   TimeWorkID as leaveId,
                                                   TitleName as Title, case 
											  when TimeWorkType = 1 then N'Chấm công'
											  when TimeWorkType = 2 then N'Làm thêm giờ'
											  when TimeWorkType = 3 then N'Nghỉ phép'
											  ELSE N'Không có dữ liệu' END 
											   as TypeLeave,
       											AssignUser as UserName,
											   CreatedDate as DateApplication,
											   ConfirmStatus as StatusNumber,
											   case 
											   when ConfirmStatus = 1 then N'Đã duyệt'
											   when ConfirmStatus = 0 then N'Đang chờ duyệt'
											   when ConfirmStatus = 99 then N'Đã xóa'
											   when ConfirmStatus = -1 then N'Từ chối'
											   when ConfirmStatus = 2 then N'DB đã duyệt'
											   ELSE N'Không có dữ liệu' END as StatusName
											      from TimeWorks where AssignUser IS NOT NULL {condition1} {condition2} {condition3} {condition4} {condition5} ORDER BY TimeWorkID DESC
                                                OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        //Stored sql
        public string StoredRegisLeaveApplications => @"[dbo].[TVSI_sDANG_KY_DON_TU_IWORK]";
        public string StoredApprovedLeaveApplicationsByLeader => @"[dbo].[TVSI_sDUYET_DON_TU_LEADER_IWORK]";
        public string StoredFillterLaMemberReportByLeader => @"TVSI_sGET_DON_TU_CUA_THANH_VIEN_THEO_LEADER";
        public string StoredInsertCheckinTimeKeepingGPS => @"TVSI_sINSERT_CHECKIN_TIMEKEEPING_GPS_IWORK";
        public string StoredUpdateLAMember => @"[dbo].[TVSI_sEDIT_DON_TU_IWORK]";
        public string StoredActionsLeaveApplications => @"[dbo].[TVSI_sACTIONS_DON_TU_MEMBER_IWORK]";
    }
}