﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Account
{
    public class BankInfo
    {
        public string BankNo { get; set; }
        public string ShortName { get; set; }
    }

    public class SubBranchInfo
    {
        public string BranchNo { get; set; }
        public string ShortBranchName { get; set; }
    }
    public class ProvinceInfo
    {
        public string ProvinceID { get; set; }
        public string ProvinceName { get; set; }
    }
    public class BranchRequest : BaseRequest
    {
        [Description("Bank no của ngân hàng cần lấy chi nhánh")]
        public string BankNo { get; set; }
    }
}
