﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.ENum;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Account;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Services;

using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;
using TVSI.CRM.WebAPI.ResourceFile;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// OpenAccount API
    /// </summary>
    public class AccountController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(AccountController));
        private IAccountService _accountService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="accountService"></param>
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Đăng ký tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_CA_CreateAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Màn hình đăng ký mở tài khoản")]
        public async Task<IHttpActionResult> OP_CA_CreateAccount(CreateAccountRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_CA_CreateAccount");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retCardId = await _accountService.CheckCardIdExsted(model.CardId);
                if (!retCardId)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.CARDID_EXSTED,
                        ErrorMsg = CommonErrors.E101
                    });
                }

                
                var custCodeExsited = await _accountService.OP_CA_CheckAccountNumExisted(model.CustCode);
                if (!custCodeExsited)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_ACCOUNT_EXISTED,
                        ErrorMsg = CommonErrors.E333
                    });
                }
                var retData = await _accountService.CreateAccount(model);
                if (retData)
                {
                    var retModel = new SuccessModel<bool>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                        
                    };
                    LogResult(log, retModel, "OP_CA_CreateAccount");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_CA_CreateAccount");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }   
        
        
        
             
        /// <summary>
        /// lấy tài khoản đăng ký theo sale id
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GE_GetListAccountBySale")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetAccountBySaleResult>))]
        [Note("Màn hình đăng ký mở tài khoản")]
        public async Task<IHttpActionResult> AC_GE_GetListAccountBySale(GetAccountBySaleRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GE_GetListAccountBySale");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _accountService.AC_GE_GetListAccountBySale(model);
                
                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetAccountBySaleResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GE_GetListAccountBySale");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GE_GetListAccountBySale");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }      
        
        /// <summary>
        /// search tài khoản đăng ký theo saleid
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GE_GetListAccountFillBySale")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetAccountBySaleResult>))]
        [Note("Màn hình đăng ký mở tài khoản")]
        public async Task<IHttpActionResult> AC_GE_GetListAccountFillBySale(GetAccountFillBySaleRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AC_GE_GetListAccountFillBySale");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _accountService.AC_GE_GetListAccountFillBySale(model);
                
                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetAccountBySaleResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GE_GetListAccountFillBySale");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AC_GE_GetListAccountFillBySale");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }  
        
        /// <summary>
        /// Mở tài khoản trực tuyến -> khởi tạo tk mới
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_CA_CheckAccountNumExisted")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Màn hình đăng ký mở tài khoản")]
        public async Task<IHttpActionResult> OP_CA_CheckAccountNumExisted(AccountNoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_CA_CheckAccountNumExisted");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                

                var retData = await _accountService.OP_CA_CheckAccountNumExisted(model.AccountNo);
                
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "OP_CA_CheckAccountNumExisted");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_VALIDATE_ACCOUNT_EXISTED,
                    ErrorMsg = ResourceFile.CommonErrors.E333
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_CA_CheckAccountNumExisted");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        /// <summary>
        /// Mở tài khoản trực tuyến -> khởi tạo tk mới
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_CA_GetAccountDetailBySale")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetAccountDetailBySaleResult>))]
        [Note("Màn hình đăng ký mở tài khoản")]
        public async Task<IHttpActionResult> OP_CA_GetAccountDetailBySale(GetAccountDetailBySaleRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_CA_GetAccountDetailBySale");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }

                var retData = await _accountService.OP_CA_GetAccountDetailBySale(model);
                
                if (retData != null)
                {
                    var retModel = new SuccessModel<GetAccountDetailBySaleResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "OP_CA_GetAccountDetailBySale");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_CA_GetAccountDetailBySale");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }    
        
        
        /// <summary>
        /// Mở tài khoản trực tuyến -> khởi tạo tk mới
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_DE_DeleteOpenAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Màn hình đăng ký mở tài khoản -> xóa tài khoản")]
        public async Task<IHttpActionResult> OP_DE_DeleteOpenAccount(GetAccountDetailBySaleRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_DE_DeleteOpenAccount");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                var retData = await _accountService.OP_DE_DeleteOpenAccount(model);
                
                if (retData)
                {
                    var retModel = new SuccessModel<bool>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "OP_DE_DeleteOpenAccount");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_DE_DeleteOpenAccount");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }     
        
        /// <summary>
        /// Mở tài khoản trực tuyến -> khởi tạo tk mới -> noi cap cmnd
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_GE_GetListCardIssue")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetCardIssue>))]
        [Note("Màn hình đăng ký mở tài khoản -> xóa tài khoản")]
        public async Task<IHttpActionResult> OP_GE_GetListCardIssue(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_GE_GetListCardIssue");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                var retData = await _accountService.OP_GE_GetListCardIssue(model);
                
                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetCardIssue>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "OP_GE_GetListCardIssue");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_GE_GetListCardIssue");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }  
        
        /// <summary>
        /// Get thông tin cá nhân sale
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_FO_GetProfileUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SaleProfileModel>))]
        [Note("Màn hình đăng ký mở tài khoản -> xóa tài khoản")]
        public async Task<IHttpActionResult> GE_FO_GetProfileUser(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_FO_GetProfileUser");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors.Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                var retData = await _accountService.GE_FO_GetProfileUser(model);
                
                if (retData != null)
                {
                    var retModel = new SuccessModel<SaleProfileModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "GE_FO_GetProfileUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_FO_GetProfileUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        /// <summary>
        /// Get thông tin cá nhân sale
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("UD_AV_ChangeImageUser")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<SaleProfileModel>))]
        [Note("Màn hình đăng ký mở tài khoản -> xóa tài khoản")]
        public async Task<IHttpActionResult> UD_AV_ChangeImageUser(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "UD_AV_ChangeImageUser");
                SetLanguage(lang);
                
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                
                var retData = await _accountService.GE_FO_GetProfileUser(model);
                
                if (retData != null)
                {
                    var retModel = new SuccessModel<SaleProfileModel>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "UD_AV_ChangeImageUser");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "UD_AV_ChangeImageUser");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Get thông tin tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("GE_GA_GetInfoAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AccountInfoReponse>))]
        [Note("")]
        public async Task<IHttpActionResult> GE_GA_GetInfoAccount(DetailAccountRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "GE_GA_GetInfoAccount");
                SetLanguage(lang);

                var retData = await _accountService.GetAccountInfo(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<AccountInfoReponse>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "GE_GA_GetInfoAccount");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "GE_GA_GetInfoAccount");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Update thông tin tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("UD_IA_UpdateAccount")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("")]
        public async Task<IHttpActionResult> UD_IA_UpdateAccount(AccountInfoReponse model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "UD_IA_UpdateAccount");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retData = await _accountService.UpdateAccountInfo(model);
                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "UD_IA_UpdateAccount");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = ResourceFile.CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "UD_IA_UpdateAccount");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }




        /// <summary>
        /// Lấy danh sách đăng ký mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_AC_AccWebList")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<AccWebListResult>>))]
        [Note("Mở tài khoản trực tuyến > Danh sách mở tài khoản từ website", true)]
        public async Task<IHttpActionResult> OP_AC_AccWebList(AccWebListRequest model,
            string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_AC_AccWebList");
                SetLanguage(lang);

                var retData = await _accountService.GetListAccWeb(model);

                if (retData.Any())
                {
                    var retModel = new SuccessModel<IEnumerable<AccWebListResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "OP_AC_AccWebList");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_AC_AccWebList");
                return Ok(new ErrorModel {RetCode = CommonConst.ERR999_SYSTEM_ERROR});
            }
        }
        
        
        /// <summary>
        /// Yêu cầu mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RQ_CE_RequestAccountWebsite")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Mở tài khoản trực tuyến > Danh sách mở tài khoản từ website", true)]
        public async Task<IHttpActionResult> RQ_CE_RequestAccountWebsite(AccWebListRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_AC_AccWebList");
                SetLanguage(lang);
                
                var retData = await _accountService.GetListAccWeb(model);

                if (retData == null)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                        ErrorMsg = ResourceFile.CommonErrors.E100
                    });
                var retModel = new SuccessModel<bool>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = true
                };
                LogResult(log, retModel, "OP_AC_AccWebList");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "OP_AC_AccWebList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }     
        
        /// <summary>
        /// Yêu cầu mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RQ_PA_PushAccountWebsite")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Mở tài khoản trực tuyến > Danh sách mở tài khoản từ website", true)]
        public async Task<IHttpActionResult> RQ_PA_PushAccountWebsite(AccountDetailRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RQ_PA_PushAccountWebsite");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retCardId = await _accountService.CheckCardIdExsted(model.CardId);
                if (!retCardId)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.CARDID_EXSTED,
                        ErrorMsg = CommonErrors.E101
                    });
                }

                var validAccNo = await _accountService.OP_CA_CheckAccountNumExisted(model.CustCode);
                if (!validAccNo)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_ACCOUNT_EXISTED,
                        ErrorMsg = ResourceFile.CommonErrors.E333
                    });
                }
                var retData = await _accountService.PushAccountWebsite(model);

                if (retData)
                {
                    var retModel = new SuccessModel<bool>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "RQ_PA_PushAccountWebsite");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
                
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RQ_PA_PushAccountWebsite");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }  
        
        
        
        /// <summary>
        /// Get Detail Account From Website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("RQ_GE_DetailAccountWebsite")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<AccountDetailResult>))]
        [Note("Mở tài khoản trực tuyến > Danh sách mở tài khoản từ website", true)]
        public async Task<IHttpActionResult> RQ_GE_DetailAccountWebsite(AccountDetailWebsiteRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "RQ_GE_DetailAccountWebsite");
                SetLanguage(lang);

                var retData = 
                    await _accountService.RQ_GE_DetailAccountWebsite(model);
                if (retData != null)
                {
                    var retModel = new SuccessModel<AccountDetailResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "RQ_GE_DetailAccountWebsite");
                    return Ok(retModel);  
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "RQ_GE_DetailAccountWebsite");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }
        
        /// <summary>
        /// Update trạng thái hồ sơ mở tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_AC_UpdateStatusAccOpen")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Mở tài khoản trực tuyến > Update trạng thái hồ sơ mở tài khoản", true)]
        public async Task<IHttpActionResult> OP_AC_UpdateStatusAccOpen(AccWebEditRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "OP_AC_UpdateStatusAccOpen");
                SetLanguage(lang);
                var retData = await _accountService.UpdateAccWebInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = ResourceFile.CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "OP_AC_UpdateStatusAccOpen");
                return Ok(retModel);
            }
            catch (Exception ex)
            {

                LogException(log, model, ex, "OP_AC_UpdateStatusAccOpen");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }



        /// <summary>
        /// Xóa hồ sơ mở tài khoản từ website
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("OP_AC_DeleteAccOpen")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Mở tài khoản trực tuyến > Xóa hồ sơ mở tài khoản", true)]
        public async Task<IHttpActionResult> OP_AC_DeleteAccOpen(AccWebDeleteRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {


                LogRequest(log, model, "OP_AC_DeleteAccOpen");
                SetLanguage(lang);
                var retData = await _accountService.DeleteAccWebInfo(model);
                if (!retData)
                    return Ok(new ErrorModel
                    {
                        RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                        ErrorMsg = CommonErrors.E111
                    });
                var retModel = new SuccessModel
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                };
                LogAnonymousResult(log, retModel, "OP_AC_DeleteAccOpen");
                return Ok(retModel);
            }
            catch (Exception ex)
            {

                LogException(log, model, ex, "OP_AC_DeleteAccOpen");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }   
        
        /// <summary>
        /// Lấy chi tiết thông tin người đại diện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GE_GetDetailManInfo")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<GetManInfoResult>))]
        [Note("Mở tài khoản CRM > Lấy thông tin người đại diện", true)]
        public async Task<IHttpActionResult> AC_GE_GetDetailManInfo(GetManInfoRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                
                LogRequest(log, model, "AC_GE_GetDetailManInfo");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retData = await _accountService.GetManInfo(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<GetManInfoResult>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GE_GetDetailManInfo");
                    return Ok(retModel); 
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {

                LogException(log, model, ex, "AC_GE_GetDetailManInfo");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }  
        
        
        /// <summary>
        /// Lấy danh sách thông tin người đại diện
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AC_GE_GetManInfoList")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<GetManInfoListResult>))]
        [Note("Mở tài khoản CRM > Lấy thông tin người đại diện", true)]
        public async Task<IHttpActionResult> AC_GE_GetManInfoList(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {


                LogRequest(log, model, "AC_GE_GetManInfoList");
                SetLanguage(lang);
                if (!ModelState.IsValid)
                {
                    return Ok(new ErrorModel()
                    {
                        RetCode = CommonConstExtend.FAIL_VALIDATE_FAILED,
                        ErrorMsg = ModelState.Values.Select(m => m.Errors
                            .Select(e => e.ErrorMessage)).FirstOrDefault().First()
                    });
                }
                var retData = await _accountService.GetManInfoList(model);

                if (retData.Count() > 0)
                {
                    var retModel = new SuccessListModel<GetManInfoListResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AC_GE_GetManInfoList");
                    return Ok(retModel); 
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {

                LogException(log, model, ex, "AC_GE_GetManInfoList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BL_AC_GetListBank")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<BankInfo>>))]
        [Note("Lấy danh sách ngân hàng", true)]
        public async Task<IHttpActionResult> BL_AC_GetListBank(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BL_AC_GetListBank");
                SetLanguage(lang);

                var retData = await _accountService.GetListBank();

                var retModel = new SuccessModel<List<BankInfo>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "BL_AC_GetListBank");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BL_AC_GetListBank");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }

        /// <summary>
        /// Lấy danh sách chi nhánh ngân hàng
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("BL_AC_GetListBranchOfbank")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<SubBranchInfo>>))]
        [Note("Lấy danh sách ngân hàng", true)]
        public async Task<IHttpActionResult> BL_AC_GetListBranchOfbank(BranchRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "BL_AC_GetListBranchOfbank");
                SetLanguage(lang);

                var retData = await _accountService.GetListBranch(model);

                var retModel = new SuccessModel<List<SubBranchInfo>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "BL_AC_GetListBranchOfbank");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "BL_AC_GetListBranchOfbank");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }

        }


        /// <summary>
        /// Lấy danh sách thành phố
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PL_AC_GetListProvince")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<ProvinceInfo>>))]
        [Note("Lấy danh sách ngân hàng", true)]
        public async Task<IHttpActionResult> PL_AC_GetListProvince(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PL_AC_GetListProvince");
                SetLanguage(lang);

                var retData = await _accountService.GetListProvince();

                var retModel = new SuccessModel<List<ProvinceInfo>>
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "PL_AC_GetListProvince");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PL_AC_GetListProvince");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        /// <summary>
        /// filter trạng thái tài khoản 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PL_AC_GetAccountStatus")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<GetAccountStatusResult>>))]
        [Note("Lấy danh trạng thái tài khoản", true)]
        public async Task<IHttpActionResult> PL_AC_GetAccountStatus(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PL_AC_GetAccountStatus");
                SetLanguage(lang);

                var retData = await _accountService.GetAccountStatus();

                var retModel = new SuccessListModel<GetAccountStatusResult>()
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "PL_AC_GetAccountStatus");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PL_AC_GetAccountStatus");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
        
         
        /// <summary>
        /// filter trạng thái hồ sơ tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("PL_AC_ProfileStatus")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<GetAccountStatusResult>>))]
        [Note("Lấy danh trạng thái hồ sơ tài khoản", true)]
        public async Task<IHttpActionResult> PL_AC_ProfileStatus(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "PL_AC_ProfileStatus");
                SetLanguage(lang);

                var retData = await _accountService.ProfileStatus();

                var retModel = new SuccessListModel<GetAccountStatusResult>()
                {
                    RetCode = CommonConst.SUCCESS_CODE,
                    RetData = retData
                };
                LogResult(log, retModel, "PL_AC_ProfileStatus");
                return Ok(retModel);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "PL_AC_ProfileStatus");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }   
        /// <summary>
        /// filter trạng thái hồ sơ tài khoản
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_CL_InsertCalllog")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<bool>))]
        [Note("Lấy danh trạng thái hồ sơ tài khoản", true)]
        public async Task<IHttpActionResult> CC_CL_InsertCalllog(CalllogRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_CL_InsertCalllog");
                SetLanguage(lang);

                var retData = await _accountService.InsertCalllog(model);
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "CC_CL_InsertCalllog");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_SAVE_FAILED,
                    ErrorMsg = CommonErrors.E111
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_CL_InsertCalllog");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
        /// <summary>
        /// Phương thức dùng để lấy tỉnh thành (cho mở tài khoản)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetProvinces")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<ProvinceResult>))]
        [Note("Lấy danh trạng thái hồ sơ tài khoản", true)]
        public async Task<IHttpActionResult> CC_GC_GetProvinces(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetProvinces");
                SetLanguage(lang);

                var retData = await _accountService.GetProvinces();
                if (retData.Any())
                {
                    var retModel = new SuccessListModel<ProvinceResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetProvinces");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetProvinces");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }  
        
        
        /// <summary>
        /// Phương thức dùng để lấy quận huyện  (cho mở tài khoản)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetDistricts")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<DistrictResult>))]
        [Note("Lấy danh trạng thái hồ sơ tài khoản", true)]
        public async Task<IHttpActionResult> CC_GC_GetDistricts(DistrictRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetDistricts");
                SetLanguage(lang);

                var retData = await _accountService.GetDistricts(model.ProvinceCode);
                if (retData.Any())
                {
                    var retModel = new SuccessListModel<DistrictResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetDistricts");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetDistricts");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
        
              
        /// <summary>
        /// Phương thức dùng để lấy xã phường
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_GetWards")]
        [HttpPost]
        [ResponseType(typeof(SuccessListModel<WardResult>))]
        [Note("Lấy danh trạng thái hồ sơ tài khoản", true)]
        public async Task<IHttpActionResult> CC_GC_GetWards(WardRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_GetWards");
                SetLanguage(lang);

                var retData = await _accountService.GetWards(model.DistrictCode);
                if (retData.Any())
                {
                    var retModel = new SuccessListModel<WardResult>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CC_GC_GetWards");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CC_GC_GetWards");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }  
        /// <summary>
        /// Phương thức dùng để check cardID tồn tại
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CC_GC_CheckCardIDExsted")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Kiểm tra số CMT/CCCD tồn tại", true)]
        public async Task<IHttpActionResult> CC_GC_CheckCardIDExsted(CardIDRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CC_GC_CheckCardIDExsted");
                SetLanguage(lang);

                var retData = await _accountService.CheckCardIdExsted(model.CardId);
                if (retData)
                {
                    var retModel = new SuccessModel<bool>()
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = true
                    };
                    LogResult(log, retModel, "CC_GC_CheckCardIDExsted");
                    return Ok(retModel);   
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.CARDID_EXSTED,
                    ErrorMsg = CommonErrors.E101
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CheckCardIdExsted");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        } 
        
        
    }
}