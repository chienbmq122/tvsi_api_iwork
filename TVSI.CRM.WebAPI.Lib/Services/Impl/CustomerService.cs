﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class CustomerService: BaseService, ICustomerServices
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerService));
        private readonly ICustCommandText _commandText;
        public CustomerService(ICustCommandText commandText)
        {
            _commandText = commandText;
        }
        /// <summary>
        /// Lấy danh sách khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CustomerListResult>> GetListCust(CustomerListRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            /*var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
            var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");*/
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<CustomerListResult>();
                    var sql = _commandText.GetListCust;
                    sql = sql.Replace("{cond2}", model.Status > 0 ? " and Status = @Status" : "");
                    sql = sql.Replace("{cond3}", !string.IsNullOrEmpty(model.keySearch) ? " And ( CustName like @keySearch or CustCode like @keySearch or Mobile like @keySearch ) " : "");
                    log.Error(sql);
                    var subSaleList = new List<string>();
                    var branchList = new List<string>();
                    var systemID = await GetSystemID(model.UserName);
                    if (systemID != null)
                    {
                        // Role Trưởng phòng thì đc get ra subSale
                        if (systemID.Level == 2)
                        {
                            subSaleList.Add(model.UserName);
                            var saleListRes = await GetSaleList(systemID.SystemID);
                            foreach (var item in saleListRes)
                            {
                                if (!subSaleList.Contains(item))
                                    subSaleList.Add(item);
                            }
                        }
                        // Role giám đốc thì đc get ra branchID
                        if (systemID.Level == 1)
                        {
                            var branchListRes = await GetBranchList(systemID.BranchID);
                            if (branchListRes != null)
                            {
                                foreach (var data in branchListRes)
                                    branchList.Add(data);
                                
                            }
                        }
                    }

                    switch (systemID.Level)
                    {
                        case 1:
                            sql = sql.Replace("{cond1}", "BranchID in @BranchID");
                            resultData = (List<CustomerListResult>) await conn.QueryAsync<CustomerListResult>(sql, new
                            {
                                Start = start,
                                PageSize = model.PageSize,
                                Status = model.Status, 
                                keySearch = "%" +model.keySearch+ "%",
                                @BranchID = branchList,
                            });
                            break;
                        case 2:
                           sql = sql.Replace("{cond1}", "AssignUser in @AssignUser");
                           resultData = (List<CustomerListResult>) await conn.QueryAsync<CustomerListResult>(sql, new
                           {
                               Start = start,
                               PageSize = model.PageSize,
                               Status = model.Status, 
                               keySearch = "%" +model.keySearch+ "%",
                               @AssignUser = subSaleList,
                           });
                           break;
                       case 0:
                           sql = sql.Replace("{cond1}", "AssignUser = @AssignUser");
                           resultData = (List<CustomerListResult>) await conn.QueryAsync<CustomerListResult>(sql, new
                           {
                               Start = start,
                               PageSize = model.PageSize,
                               Status = model.Status, 
                               keySearch = "%"+model.keySearch+"%",
                               @AssignUser = model.UserName,
                           });
                           break;
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// kiểm tra số điện thoại KH tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        public async Task<bool> ExistsPhonePotential(PhonePotentialInfoRequest model)
        {
            try
            {
                var sql = "SELECT Mobile FROM Lead WHERE Mobile = @Phone";
                var result = await WithCrmDbConnection(async conn => await conn.QueryFirstOrDefaultAsync<string>(sql,
                    new
                    {
                        @Phone = model.Phone
                    }));
                return string.IsNullOrEmpty(result);

            }
            catch (Exception ex)
            {
                log.Error("ExistsPhonePotential:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<PhoneInfoListResult>> GetPhoneInfoList(PhoneInfoListRequest model)
        {
            try
            {
                var sql = "select a.CUSTOMERNAME FullName, (case when A.CONTACTPHONE != '' or A.CONTACTPHONE != null then A.CONTACTPHONE when A.PHONE != '' or A.PHONE != null then A.PHONE when B.PHONE != '' or B.PHONE != null then B.PHONE  end) as Phone from CUSTOMER A inner join CUSTOMERPHONE B on a.CUSTOMERID = b.CustomerID where  A.CONTACTPHONE in @PhoneList OR a.PHONE in @PhoneList or (b.Phone in @PhoneList and b.IsActive =1)" ;
                var customerPhone = await WithInnoDBConnection(async conn => await conn.QueryAsync<PhoneInfoListResult>(
                    sql, new
                    {
                        PhoneList = model.PhoneList
                    }));
                return customerPhone;
            }
            catch (Exception ex)
            {
                log.Error("GetPhoneInfoList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Thông tin chi tiết khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<CustInfoResult> GetInfoCust(CustInfoRequest model)
        {
            try
            {
                return await WithInnoDBConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<CustInfoResult>(_commandText.GetInfoCust,new { custCode = model.CustCode });
                    if (data != null)
                    {
                        var dataphone = await conn.QueryAsync(_commandText.GetPhoneCust,
                        new { custCode = model.CustCode });
                        var phonelist = dataphone.ToList();
                        data.PhoneSMS = string.Join(", ", phonelist.Where(x => x.IsSMS).Select(x => x.Phone).ToArray());
                        data.PhoneCC = string.Join(", ", phonelist.Where(x => x.IsCC).Select(x => x.Phone).ToArray());
                    }

                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetInfoCust:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Lấy thông tin đăng ký ngân hàng chuyển tiền
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<BankAccTransferResult>> GetBankTranfer(BankAccTransferRequest model)
        {
            try
            {

                var sql = _commandText.GetBankTransfer;
                return await WithInnoDBConnection(async conn =>
                {

                        var retdata = await conn.QueryAsync<string>(sql, new { custCode = model.CustCode });
                        var bankList = retdata.ToList();
                        return await conn.QueryAsync<BankAccTransferResult>(sql,
                                new
                                {
                                    custCode = model.CustCode
                                });
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBankTranfer:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Lấy số dư có thể rút tài khoản đuôi 1 & 6
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CashBalanceInfoResult>> GetCashBalanceInfo(CashBalanceInfoRequest model)
        {
            try
            {
                var resultData = new List<CashBalanceInfoResult>();
                var cashBalance_01 = "#";
                var cashBalance_06 = "#";

                var ipgws = new TVSIWS_IPG.IPGWS();
                var dataInfo_01 = ipgws.IPGWS530(model.CustCode + "1", "CRM_" + model.UserName);
                var ws530 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS530>(dataInfo_01);
               
                if (ws530?.so_tien > 0)
                {
                    cashBalance_01 = ws530.so_tien.ToString("N0");
                }
                var dataInfo_06 = ipgws.IPGWS540(model.CustCode + "6");
                var ws540 = TVSI.Utility.WebServiceEntitySerializer.Deserialize<TVSI.WebServices.Entity.PaymentGateway.IPGWS540>(dataInfo_06);
                if (ws540?.so_du_tien > 0)
                {
                    cashBalance_06 = ws540.so_du_tien.ToString("N0");
                }
                var _custResult = new CashBalanceInfoResult
                {

                    CashBalanceInfo_1 = cashBalance_01,
                    CashBalanceInfo_6 = cashBalance_06
                };
                resultData.Add(_custResult);
                return resultData;

            }
            catch (Exception ex)
            {
                log.Error("GetCashBalanceInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Lấy danh mục đầu tư
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<AccPorfolioResult>> GetAccPorfolio(AccPorfolioRequest model)
        {
            try
            {
                var accountList = new List<string> { model.CustCode + "1", model.CustCode + "6" };

                return await WithInnoDBConnection(async conn => await conn.QueryAsync<AccPorfolioResult>(_commandText.GetAccPorfolio,
                    new
                    {
                        accountList = accountList
                    }));

            }
            catch (Exception ex)
            {
                log.Error("AccPorfolioResult:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        public async Task<IEnumerable<TradingInfoResult>> GetTradingInfoList(TradingInfoRequest model)
        {
            try
            {
                var accountList = new List<string> { model.CustCode + "1", model.CustCode + "6" };

                return await WithInnoDBConnection(async conn =>
                {
                    var orderInfo = await conn.QueryAsync<OrderInfoResult>(_commandText.GetOrderInfo,
                        new { accountList = accountList });
                    var dealInfo = await conn.QueryAsync<DealInfoResult>(_commandText.GetDealInfo,
                        new { accountList = accountList });
                    var lstReturn = new List<TradingInfoResult>
                    {
                        new TradingInfoResult
                        {
                            OrderList = orderInfo.ToList(),
                            DealList = dealInfo.ToList()
                        }
                    };
                    return lstReturn;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetOrderInfoList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Lấy danh sách thuộc tính mở rộng khách hàng
        /// </summary>
        /// <returns></returns>

        public async Task<IEnumerable<ExCustInfoResult>> GetExCustInfoList()
        {
            try
            {
               
                return await WithCrmDbConnection(async conn =>
                {
                    var jobInfo = await conn.QueryAsync<JobInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_1 });
                    var finAbilityInfo = await conn.QueryAsync<FinAbilityInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_2 });
                    var knowledExpInfo = await conn.QueryAsync<KnowledExpInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_3 });
                    var opInvestInfo = await conn.QueryAsync<OpInvestInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_4 });
                    var sourceInfo = await conn.QueryAsync<SourceInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_5 });
                    var jobCityInfo = await conn.QueryAsync<JobCityInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_6 });
                    var finlOrganizeInfo = await conn.QueryAsync<FinlOrganizeInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_7 });
                    var investExperInfo = await conn.QueryAsync<InvestExperInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_8 });
                    var riskInfo = await conn.QueryAsync<RiskInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_9 });
                    var investPreInfo = await conn.QueryAsync<InvestPreInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_10 });
                    var noteCommInfo = await conn.QueryAsync<NoteCommInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_11 });
                    var saleRelInfo = await conn.QueryAsync<SaleRelInfo>(_commandText.GetExCustInfo,
                        new { ExType = CustConst.EXCUST_TYPE_12 });
                    var lstReturn = new List<ExCustInfoResult>
                    {
                        new ExCustInfoResult
                        {
                            JobList = jobInfo.ToList(),
                            FinAbilityList = finAbilityInfo.ToList(),
                            KnowledExpList = knowledExpInfo.ToList(),
                            OpInvestList = opInvestInfo.ToList(),
                            SourceList = sourceInfo.ToList(),
                            JobCityList = jobCityInfo.ToList(),
                            FinlOrganizeList = finlOrganizeInfo.ToList(),
                            InvestExperList = investExperInfo.ToList(),
                            RiskList = riskInfo.ToList(),
                            InvestPreList = investPreInfo.ToList(),
                            NoteCommList = noteCommInfo.ToList(),
                            SaleRelList = saleRelInfo.ToList()
                        }
                    };
                    return lstReturn;
                });
            }
            catch (Exception ex)
            {
                log.Error("ExCustInfoResult:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Lấy danh sách hoạt động của khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CustActivityResult>> GetActivityCustList(CustActivityRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<CustActivityResult>();
                    var result = await conn.QueryAsync<Activity>(_commandText.GetActivityInfo,
                        new { Start = start, PageSize = model.PageSize, SaleId = model.UserName, custCode = model.CustCode });
                    foreach (var data in result)
                    {
                        var _custResult = new CustActivityResult
                        {
                            ActivityId = int.Parse(data.ActivityID.ToString()),
                            ActivityName = data.ActivityName,
                            StartDate = data.StartDate.ToString("dd/MM/yyyy"),
                            EndDate = data.EndDate.ToString("dd/MM/yyyy"),
                            Status = data.Status,
                            Description = data.Description,
                            AssignUser = data.AssignUser
                        };
                        resultData.Add(_custResult);
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityCustList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Lấy danh sách cơ hội theo khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CustOpportunityResult>> GetOpportunityCustList(CustOpportunityRequest model)
        {
            //var start = (model.PageIndex - 1) * model.PageSize;

            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    //var resultData = new List<CustOpportunityResult>();
                    var result = await conn.QueryAsync<CustOpportunityResult>(_commandText.GetOpportunityInfo,
                        new { SaleId = model.UserName, custCode = model.CustCode });
                    //foreach (var data in result)
                    //{
                    //    var _custResult = new CustOpportunityResult
                    //    {
                    //        OpportunityId = int.Parse(data.OpportunityID.ToString()),
                    //        OpportunityName = data.OpportunityName,
                    //        ExpectedCloseDate = data.ExpectedCloseDate.ToString("dd/MM/yyyy"),
                    //        ExpectedRevenue = data.ExpectedRevenue,
                    //        Status = data.Status,
                    //        AssignUser = data.AssignUser
                    //    };
                    //    resultData.Add(_custResult);
                    //}
                    return result;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetOpportunityCustList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Update thông tin khách hàng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public async Task<bool> UpdateCustInfo(CustEditRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var retData = await conn.ExecuteAsync(_commandText.UpdateCustInfo, new
                    {
                        
                        description = model.Description,
                        updatedBy = model.AssignUser,
                        updatedDate = DateTime.Now,
                        custCode = model.CustCode
                    });

                    await conn.ExecuteAsync(_commandText.DeleteExCustInfo, new
                    {
                        custCode = model.CustCode
                    });

                    var arrListExtend = model.CustInfoExtend.Split('|');
                    foreach (var exten_item in arrListExtend)
                    {
                        if (!string.IsNullOrEmpty(exten_item))
                        {
                            await conn.ExecuteAsync(_commandText.PostCustExtInfo, new
                            {
                                category = exten_item,
                                custCode = model.CustCode,
                                createdBy = model.AssignUser,
                                createdDate = DateTime.Now
                            });
                        }
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateCustInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Lấy danh sách cổ đông lớn, cổ đông nội bộ
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ShareHolderListResult>> GetListShareHolder(ShareHolderListRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            //var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
            //var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<ShareHolderListResult>();
                    var sql = _commandText.GetListShareHolder;
                    //sql = sql.Replace("{cond1}", !string.IsNullOrEmpty(model.FromDate) ? " and CreatedDate >= @fromDate AND CreatedDate <= @toDate" : "");
                    sql = sql.Replace("{cond1}", model.Status > 0 ? " and Status = @Status" : "");
                    sql = sql.Replace("{cond2}", !string.IsNullOrEmpty(model.keySearch) ? " and (CustName like  @keySearch  or CustCode like @keySearch)" : "");
                    var result = await conn.QueryAsync<ShareHolder>(sql,
                        new { Start = start, PageSize = model.PageSize, Status = model.Status, keySearch = "%" + model.keySearch + "%", SaleId = model.UserName });
                    foreach (var data in result)
                    {
                        var _custResult = new ShareHolderListResult
                        {
                            CreatedDate = data.CreatedDate.ToString("dd/MM/yyyy"),
                            CustCode = data.CustCode,
                            CustName = data.CustName,
                            ShareType_1 = data.ShareType_1,
                            ShareType_2 = data.ShareType_2,
                            ShareType_3 = data.ShareType_3,
                            ShareType_4 = data.ShareType_4,
                            StockCode = data.StockCode,
                            Status = data.Status,
                            AssignUser = data.CreatedBy
                        };
                        resultData.Add(_custResult);
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetListShareHolder:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Lấy danh sách khách hàng selected
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<CustomerListSelectResponse> GetListCustSelect(CustomerListSelectRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                return await WithCrmDbConnection(async conn =>
                {
                    CustomerListSelectResponse dataResponse = new CustomerListSelectResponse();
                    dataResponse.totalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.GetListCustSelectCount, new { custCode = "%" + model.CustCode + "%", assignUser = model.UserName });
                    dataResponse.Items = await conn.QueryAsync<CustomerListSelectRessult>(_commandText.GetListCustSelect, new { custCode = "%" + model.CustCode + "%", assignUser = model.UserName, Start = start, PageSize = model.PageSize });
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Thêm mới thông tin cổ đông lớn
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> AddNewShareHolderInfo(ShareHolderAddRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteScalarAsync(_commandText.PostShareHolderInfo, new
                    {
                        custCode = model.CustCode,
                        custName = model.CustName,
                        address = model.Address,
                        sharetype_1 = model.ShareType_1,
                        sharetype_2 = model.ShareType_2,
                        sharetype_3 = model.ShareType_3,
                        sharetype_4 = model.ShareType_4,
                        stockCode = model.StockCode,
                        status = CustConst.S_SHARE_TYPE,
                        assignUser = model.AssignUser,
                        createdBy = model.AssignUser,
                        createdDate = DateTime.Now
                    });
                   
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("AddNewShareHolderInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Thông tin chi tiết khách hàng tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<ShareHolderInfoResult> GetShareHolderInfo(ShareHolderInfoRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var data = await conn.QueryFirstOrDefaultAsync<ShareHolderInfoResult>(_commandText.ViewShareHolderInfo,
                        new { shareHolderId = model.ShareHolderId });
                   
                    return data;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetShareHolderInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<CustomerInfoResult> GetCustomerInfo(CustomerDetailInfoRequest model)
        {
            try
            {
                return await WithCommonDBConnection(async conn =>
                {
                    var result = new CustomerInfoResult();
                    var custInfo = await conn.QueryFirstOrDefaultAsync<CustomerInfoContact>(_commandText.GetCustomerInfo, new 
                    {
                        @CustCode = model.CustCode
                    });

                    var sqlCrm =
                        "SELECT A.Category as ItemID,b.CategoryName as ItemName,b.CategoryType as ItemTypeID FROM ExtendCategoryCust A inner join ExtendCust b on a.Category = b.Category where A.CustCode = @CustCode";
                    
                    log.Info(string.Format("SQL CRM {0}",sqlCrm));
                    
                    var investment = await WithCrmDbConnection(async crm => 
                        await crm.QueryAsync<CustomerInvestmentInfo>(sqlCrm, new
                    {
                        @CustCode = model.CustCode
                    }));
                    
                    result.CustomerInfoContact = new CustomerInfoContact();
                    result.CustomerInfoContact = custInfo;
                    result.CustInfoContactCategory = new List<GroupCategoryDetail>();

                    var contactInfo = new GroupCategoryDetail();
                    contactInfo.ItemGroupType = "ContactInfo";
                    contactInfo.ItemGroupTypeName = "Thông tin liên hệ";
                    contactInfo.DataItem = new List<CustomerInvestmentInfo>();

                    var investmentInfo = new GroupCategoryDetail();
                    investmentInfo.ItemGroupType = "InvestmentInfo";
                    investmentInfo.ItemGroupTypeName = "Thông tin đầu tư";
                    investmentInfo.DataItem = new List<CustomerInvestmentInfo>();

                    var custCareInfo = new GroupCategoryDetail();
                    custCareInfo.ItemGroupType = "CustCareInfo";
                    custCareInfo.ItemGroupTypeName = "Thông tin CSKH";
                    custCareInfo.DataItem = new List<CustomerInvestmentInfo>();

                    var extendInfo = new GroupCategoryDetail();
                    extendInfo.ItemGroupType = "ExtendInfo";
                    extendInfo.ItemGroupTypeName = "Thông tin mở rộng";
                    extendInfo.DataItem = new List<CustomerInvestmentInfo>();

                    foreach (var invest in investment)
                    {

                        //if (invest.ItemTypeID == 1 || invest.ItemTypeID == 6)
                        //{
                        //    contactInfo.DataItem.Add(invest);
                        //}
                        //else if (invest.ItemTypeID == 2 || invest.ItemTypeID == 7 || invest.ItemTypeID == 8 || invest.ItemTypeID == 4 || invest.ItemTypeID == 3 || invest.ItemTypeID == 5)
                        //{
                        //    InvestmentInfo.DataItem.Add(invest);
                        //}
                        //else if (invest.ItemTypeID == 9 || invest.ItemTypeID == 11 || invest.ItemTypeID == 10 || invest.ItemTypeID == 12)
                        //{
                        //    CustCareInfo.DataItem.Add(invest);
                        //}
                        //else
                        //{
                        //    ExtendInfo.DataItem.Add(invest);
                        //}
                        if (model.UserRole.Equals("CRM_BB"))
                        {
                            if (invest.ItemTypeID == 1 || invest.ItemTypeID == 6)
                            {
                                contactInfo.DataItem.Add(invest);
                            }
                            else if (invest.ItemTypeID == 2 || invest.ItemTypeID == 7 || invest.ItemTypeID == 8)
                            {
                                investmentInfo.DataItem.Add(invest);
                            }
                            else if (invest.ItemTypeID == 9 || invest.ItemTypeID == 11 || invest.ItemTypeID == 10 || invest.ItemTypeID == 12)
                            {
                                custCareInfo.DataItem.Add(invest);
                            }
                        }
                        else if (model.UserRole.Equals("CRM_SALE"))
                        {
                            if (invest.ItemTypeID == 1)
                            {
                                contactInfo.DataItem.Add(invest);
                            }
                            else if (invest.ItemTypeID == 2 || invest.ItemTypeID == 3 || invest.ItemTypeID == 4 || invest.ItemTypeID == 5)
                            {
                                investmentInfo.DataItem.Add(invest);
                            }
                        }
                    }

                    result.CustInfoContactCategory.Add(contactInfo);
                    result.CustInfoContactCategory.Add(investmentInfo);
                    result.CustInfoContactCategory.Add(custCareInfo);
                    result.CustInfoContactCategory.Add(extendInfo);

                    return result;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetCustomerInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
