﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{
    [Table("Lead")]
    public partial class Lead
    {
        public long LeadID { get; set; }

        [Required]
        [StringLength(250)]
        public string LeadName { get; set; }

        [Required]
        [StringLength(15)]
        public string Mobile { get; set; }

        [StringLength(150)]
        public string Email { get; set; }

        [StringLength(300)]
        public string Address { get; set; }

        public int? ProfileType { get; set; }

        public int Status { get; set; }

        [StringLength(250)]
        public string Reason { get; set; }

        [StringLength(5)]
        public string BranchID { get; set; }

        [StringLength(50)]
        public string AssignUser { get; set; }

        [StringLength(50)]
        public string Picture { get; set; }

        [StringLength(800)]
        public string Description { get; set; }

        public int LeadSourceID { get; set; }
        public string SourceName { get; set; }

        [Required]
        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
