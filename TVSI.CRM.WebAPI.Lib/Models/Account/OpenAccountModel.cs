﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.ENum;
using NotImplementedException = System.NotImplementedException;

namespace TVSI.CRM.WebAPI.Lib.Models.Account
{
    public class OpenAccountModel : BaseIWRequest
    {
        
    }

    public class CreateAccountRequest : BaseRequest,IValidatableObject
    {
        [Description("So tai khoan")]
        [Required]
        public string CustCode { get; set; }
        // iw co
        [Description("Họ và Tên")]
        [Required] 
        public string FullName { get; set; }
        
        // iw co
        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        [Required]
        public string CardId { get; set; }
        
        // iw co
        [Description("Ngày cấp")] 
        [Required] 
        public string IssueDate { get; set; }
        
        // iw co
        [Description("Nơi cấp CMND/CCCD của KH ")]
        [Required]
        public string CardIssuer { get; set; }

        // iw co
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        [Required]
        public int Sex { get; set; }
        
        // iw co
        [Description("Ngày sinh dd/MM/yyyy")]
        [Required]
        public string Birthday { get; set; }
        // iw co
        [Description("Địa chỉ liên hệ")]
        [Required]
        public string Address_01 { get; set; }
        
        // iw co
        [Description("Ma Quốc tịch")]
        [Required]
        public string NationalityCode { get; set; }
        
        [Description("Ma Quốc tịch")]
        [Required]
        public string NationalityName { get; set; }
        
        [Description("Hạn mức dư nợ")]
        public string CreditLimit { get; set; } // Hạn mức dư nợ
        
        [Description("Thời hạn nợ hợp đồng")]
        public string DebtTerm { get; set; } // Thời hạn nợ hợp đồng
        
        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; } // Tỷ lệ ký quỹ
        
        // iw co
        [Description("Số điện thoại của KH 01")]
        public string Phone_01 { get; set; } 
        
        // iw co
        [Description("Số điện thoại của KH 02")]
        public string Phone_02 { get; set; }
        
        // iw co
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        
        /*// iw co
        [Description("Mã nhân viên tư vấn. Nếu nhập thì bắt buộc là 4 ký tự = số")]
        public string SaleID { get; set; } */
        
        // iw co
        [Description("Hồ sơ mở hợp đồng 0:HĐ thường, 1:HĐ Margin , 2: Phái sinh ")]
        public int? OpenContact { get; set; }
        
        // iw co
        [Description("Trạng thái hợp đồng 1: Hoàn thiên, 2: nợ hồ sơ ")]
        public int StatusContact { get; set; }
        
        // iw co
        [Description("Nơi mở tài khoản")]
        public string AddressOpenAccount { get; set; }    
        
        // iw co
        [Description("Đại diện TVSI")]
        public string RepresentTvsi { get; set; }
        
        // iw co
        [Description("Chức vụ")]
        public string RollTvsi { get; set; }   
        
        // iw co
        [Description("Giấy ủy quyền số")]
        public string PowerofAttorneyNum { get; set; }
        
        // iw co
        [Description("Ngày cấp giấu UQ dd/MM/yyyy")]
        
        public string PowerofAttorneyDate { get; set; }

        // iw co
        [Description("Số CMND/CCCD của đại diện tvsi")]
        public string TVSICardId { get; set; }

        // iw co
        [Description("Ngày cấp CMND/CCCD của đại diện tvsi")]
        public string TVSIIssueDate { get; set; }

        // iw co
        [Description("Nơi cấp CMND/CCCD của đại diện tvsi")]
        public string TVSICardIssuer { get; set; }

        // iw co
        [JsonIgnore]
        [Description("Kênh thông tin đặt lệnh")]
        public bool RegistChannelInfo { get; set; }

        // iw co
        [Description("Phương thức giao dịch 1:Qua điện thoại,2:Qua Email,12:Qua điện thoại + Email")]
        public string TradingMethod { get; set; }
        
        // iw co
        [Description("Phương thức nhận KQGD 1: sms, 2: email")]
        public string TradingResultMethod { get; set; }
        
        // iw co
        [Description("Phương thức nhận sao kê cuối ngày 1:thư đảm bảo, 2:Qua Email")]
        public string TradingReportMethod { get; set; }
        
        // iw co
        [Description("Có Đăng ký dịch vụ chuyển tiền 0:Không đăng ký, 1: là đăng ký")]
        public int BankTransferMethod { get; set; }
        
        // iw co
        [Description("Danh sách tài khoản ngân hàng List<BankAccountData>")]
        public List<BankAccountData> BankAccountList { get; set; }

        // iw co
        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        public FolderPath FolderPath { get; set; } = new FolderPath();
        
        // iw co
        [Description("Thông tin mở rộng")]
        [Required]
        public InfoExtend InfoExtend { get; set; }

        // iw co
        [Description("Thông tin Fatca")]
        [Required]
        public FatcaInfo FatcaInfo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var result = new List<ValidationResult>();
            if (StatusContact == 2)
                if (FolderPath.ImageBack.Length == 0 || FolderPath.ImageFront.Length == 0)
                    result.Add(new ValidationResult("Field FolderPath is Required."));

            if (TradingResultMethod.Contains("2"))
                if (string.IsNullOrEmpty(Email))
                    result.Add(new ValidationResult("Field Email is Required."));

            if (TradingResultMethod.Contains("1"))
                if (string.IsNullOrEmpty(Phone_01))
                    result.Add(new ValidationResult("Field Phone_01 is Required."));
                

            return result;
        }
    }
    
    public class InfoExtend
    {
           
        // iw co
        [Description("Nghề nghiệp")]
        public int Occupation  { get; set; }
        
        // iw co
        [Description("Nơi làm việc")]
        public int Workplace  { get; set; }
        
        // iw co
        [Description("Khả năng tài chính")]
        public int FinancialResources  { get; set; } 
        
        // iw co
        [Description("Kinh nghiệm đầu tư")]
        public int InvestmentExperience  { get; set; } 
        
        // iw co
        [Description("Đã tham trái phiếu từ")]
        public int JoinedBondSince  { get; set; } 
        
        // iw co
        [Description("Khả năng chấp nhận rủi ro")]
        public int RiskAcceptance  { get; set; }    
        
        // iw co
        [Description("Sở thích đầu tư")]
        public int HobbyInvestment  { get; set; } 
        
        // iw co
        [Description("Có quan hệ với sale")]
        public int RelationshipSale  { get; set; }
        
        // iw co
        [Description("Lưu ý khi nói chuyện")]
        public int NoteTalking  { get; set; }
        
        [Description("Kiến thức, Kinh nghiệm")]
        public int KnowledExpID { get; set; }
        
        [Description("Quan điểm đầu tư")]
        public int OpinionInvestID { get; set; }
        
        [Description("Nguồn khai thác")]
        public int SourceID { get; set; }
        
    }
    public class InfoExtendCreateAccountResult
    {
           
        // iw co
        [Description("Nghề nghiệp")]
        public int Occupation  { get; set; }
        
        
        // iw co
        [Description("Khả năng tài chính")]
        public int FinancialResources  { get; set; }

        [Description("Kiến thức, Kinh nghiệm")]
        public int KnowledExpID { get; set; }
        
        [Description("Quan điểm đầu tư")]
        public int OpinionInvestID { get; set; }
        
        [Description("Nguồn khai thác")]
        public int SourceID { get; set; }
        
    }
    public class BankAccountData
    {
        
        [Description("Tên chủ tài khoản")]
        public string BankAccName { get; set; } 
        
        [Description("Số tài khoản")]
        public string BankAccNo { get; set; }
        
        [Description("Chọn ngân hàng...")]
        public string BankNo { get; set; }
        
        [Description("Ten ngan hang")]
        public string BankName { get; set; }
        
        [Description("Chon chi nhanh")]
        public string SubBranchNo { get; set; }
        
        public string SubBranchName { get; set; } 
        


    }
    
    public class ReceiveContractData
    {
        [Description("Tên người nhận")]
        public string RecieverName { get; set; }  
        
        [Description("Số điện thoại người nhận")]
        public string ReceiverMobile { get; set; }  
        
        [Description("Địa chỉ người nhận")]
        public string ReceiverAddress { get; set; }
        
    }
    public class FolderPath
    {
        [Description("Đường dẫn chứa ảnh mặt trước CCCD/CMND")]
        public string ImageFront { get; set; }
        
        [Description("Đường dẫn chứa ảnh sau trước CCCD/CMND")]
        public string ImageBack { get; set; }
    }

    public class AccountNoRequest : BaseRequest
    {
        [Description("Số tài khoản KH cần tạo")]
        [MaxLength(6,ErrorMessageResourceName = nameof(ResourceFile.AccountErrors.MaxLength_AccountNum),ErrorMessageResourceType = typeof(ResourceFile.AccountErrors))]
        [Required(ErrorMessageResourceName = nameof(ResourceFile.AccountErrors.Validate_AccountNo),ErrorMessageResourceType = typeof(ResourceFile.AccountErrors))]
        public string AccountNo { get; set; }

        [Description("(Kiểu) Khách hàng")] public int CustType { get; set; } = 1;

    }

    public class FatcaInfo
    {
        [Description("KH là công dân Hoa Kỳ 0: false, 1: true")]
        public int FatcaKhUsa { get; set; }

        [Description("KH có nơi sinh tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiSinhUsa { get; set; }

        [Description("KH có địa chỉ lưu ký tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaNoiCuTruUsa { get; set; }

        [Description("KH có sdt liên lạc tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaSdtUsa { get; set; }

        [Description("KH có lệnh CK Hoa Kỳ 0: false, 1: true")]
        public int FatcaLenhCkUsa { get; set; }

        [Description("KH có giấy ủy quyền tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaGiayUyQuyenUsa { get; set; }

        [Description("KH có đại chỉ nhận thư hộ tại Hoa Kỳ 0: false, 1: true")]
        public int FatcaDiaChiNhanThuUsa { get; set; }
        
    }
}