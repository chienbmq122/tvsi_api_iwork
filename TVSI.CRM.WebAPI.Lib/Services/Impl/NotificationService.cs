﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Notification;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class NotificationService : BaseService, INotificationService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(NotificationService));
        private INotificationCommandText _commandText;
        public NotificationService(INotificationCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<NotificationResponseModel> GetNotification(NotificationRequestModel model)
        {
            try
            {
                return await WithSystemNotificationConnection(async conn =>
                {
                    var dataResponse = new NotificationResponseModel();
                    var start = (model.PageIndex - 1) * model.PageSize;
                    var data = await conn.QueryAsync<NotificationModel>(
                        _commandText.GetNotification, new { staffCode = model.UserName, Start = start, PageSize = model.PageSize });

                    dataResponse.PageSize = model.PageSize;
                    dataResponse.Items = data.ToList();
                    dataResponse.TotalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CountNotification, new { staffCode = model.UserName });
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetNotification - " + ex.Message);
                log.Error("GetNotification - " + ex.InnerException);
                throw;
            }
        }
    }
}
