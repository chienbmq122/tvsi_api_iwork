﻿using System.Web.Http;
using System.Web.Mvc;

namespace TVSI.CRM.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configuration.Formatters.XmlFormatter.SupportedMediaTypes.Clear();

            log4net.LogicalThreadContext.Properties["UserId"] = "Other";
            log4net.LogicalThreadContext.Properties["ApiName"] = "Other";
            log4net.Config.XmlConfigurator.Configure();
        }
    }
}
