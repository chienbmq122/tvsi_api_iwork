﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Activity;
using TVSI.CRM.WebAPI.Lib.Models.Dashboard;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IDashboardService
    {
        Task<IEnumerable<ActivityResult>> Get3TaskNextActivity(DashboardActionRequest model);
        Task<DashboardChartResult> TradeInfo(DashboardChartRequest model);
        Task<DashboardTradeResult> TradeTotalInfoHistory(DashboardTradeRequest model);
        Task<DashboardSummaryResult> TradeTotalHistory(DashboardSummaryRequest model);
        Task<TradingInfoModel> TradingInfoDaily(DashboardSummaryRequest model);
    }
}
