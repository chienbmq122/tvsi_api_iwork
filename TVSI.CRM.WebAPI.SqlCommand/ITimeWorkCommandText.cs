﻿namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface ITimeWorkCommandText
    {
        // Sql Get 
        string GetCardNumberEmsDb { get; }
        string GetTimeWorkByDate { get; }
        string GetTimeWorkByDateGPSCRM { get; }
        string GetTimeWorkFromDateAndToDate { get; }
        string GetTimeWorkByMonth { get; }
        string GetLeaveApplications { get; }
        string GetNameStaffEmsDb { get; }
        string GetNameStaffEmsDbtttcct { get; }
        string GetDepartmentStaffEmsDb { get; }
        string GetLeaveApplicationsApproved { get; }
        string GetWithoutLeave { get; }
        string GetTimeWorkLate { get; }
        string GetTimeWorkLateByMonth { get; }
        string GetTimeWorkBackEarly { get; }
        string GetTimeWorkBackEarlyByMonth { get; }
        string GetMinuteLate { get; }
        string GetMinuteLateByMonth { get; }
        string GetMinuteEarlyWork { get; }
        string GetMinuteEarlyWorkByMonth { get; }
        string GetLeaveApplicationsStaff { get; }
        string GetLeaveApplicationsStaffByLeader { get; }
        string GetLeaveCrmByDay { get; }

        string GetNameLeaderApprovedByUserName { get; }
        string GetNameMemberApprovedByUserName { get; }
        string GetUserDomainByUserName { get; }
        string GetUserDomain { get; }
        string GetLeaveApplicationsMemberByLeader { get; }
        string GetLeaveApplicationsMemberByLeaderBD { get; }
        string GetLeaveApplicationsMemberByLeaderBDNotCtype2 { get; }
        
        string GetHisTWAndLAStaffByDay { get; }
        string GetTimeKeepingByMonth { get; }
        string GetTimeKeepingByMonthGPS { get; }
        string GetIdTimeWorkLeaveApp { get; }
        string GetTimeWorkTypeByDay { get; }
        string GetLeaveApplicationsByMonth { get; }
        
        string GetTimeKeepingFromMonthToMonthTW { get; }
        
        string GetTimeWorkLeaveByMonthCrm { get; }
        
        string FillterLAMemberReportByLeader { get; }
        
        
        //Sql Insert
        
        
        // stored sql Insert
        string StoredRegisLeaveApplications { get; }
        string StoredApprovedLeaveApplicationsByLeader { get; }
        
        string StoredFillterLaMemberReportByLeader { get; }
        string StoredInsertCheckinTimeKeepingGPS { get; }
        string StoredUpdateLAMember { get; }
        string StoredActionsLeaveApplications { get; }
        
        // Stored sql Get
        
    }
}