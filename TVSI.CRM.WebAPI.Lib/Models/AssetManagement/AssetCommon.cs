﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.AssetManagement
{
    public class AssetType
    {
        [Description("Id asset type")]
        public int TypeAssetId { get; set; }
        [Description("Tên loại thiết bị")]
        public string NameType { get; set; }
        [Description("Loại của loại thiết bị 0: quản lý theo qrcode 1:quản lý số lượng")]
        public int Type { get; set; }
        [Description("Tổng số thiết bị trong kho")]
        public int TotalAssetValid { get; set; }
        [Description("Tổng số thiết bị đang dùng")]
        public int TotalAssetUsed { get; set; }
        [Description("Tổng số thiết bị")]
        public int TotalAsset {
            get {
                return TotalAssetValid - TotalAssetUsed;
            }
        }
    }

    public class AssetState
    {
        [Description("Id asset state")]
        public int StateAssetId { get; set; }
        [Description("Tên trạng thái")]
        public string NameState { get; set; }
        [Description("Loại trang thái 1: trạng thái thiết bị, 2: tình trạng thiết bị")]
        public int TypeState { get; set; }
        [Description("Màu sắc của trạng thái")]
        public string Color { get; set; }
    }

    public class RoleUserAsset
    {
        [Description("Username")]
        public string UserName { get; set; }
        [Description("Role accepct")]
        public string RoleUser { get; set; }
    }
}
