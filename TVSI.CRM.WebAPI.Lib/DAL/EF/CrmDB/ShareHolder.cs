﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{

    [Table("ShareHolder")]
    public partial class ShareHolder
    {
        [Key]
        public  int ShareHolderID { get; set; }
        [Required]
        [StringLength(25)]
        public string CustCode { get; set; }
        [Required]
        [StringLength(250)]
        public string CustName { get; set; }
        [StringLength(300)]
        public string Address { get; set; }
        public int ShareType_1 { get; set; }
        public int ShareType_2 { get; set; }
        public int ShareType_3 { get; set; }
        public int ShareType_4 { get; set; }
        [StringLength(255)]
        public string StockCode { get; set; }
        public int Status { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [StringLength(50)]
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
