﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Notification
{
    public class NotificationModel
    {
        [Description("Thông báo id")]
        public int notification_id { get; set; }
        [Description("loại thông báo id")]
        public int type_notification_id { get; set; }
        [Description("mẫu thông báo id")]
        public int template_notification_id { get; set; }
        [Description("Tiêu đề")]
        public string title_notification { get; set; }
        [Description("nội dung")]
        public string content_notification { get; set; }
        [Description("nội dung tiếng anh")]
        public string content_en_notification { get; set; }
        [Description("staff code")]
        public string staff_code { get; set; }
        [Description("độ ưu tiên")]
        public int priority { get; set; }
        [Description("trạng thái")]
        public int status { get; set; }
        [Description("nguồn thông báo")]
        public string src_notification { get; set; }

        public DateTime dateSend { get; set; }

        [Description("thời gian gửi")]
        public string time_send
        {
            get
            {
                return dateSend.ToString("dd/MM/yyyy HH:mm:ss");
            }
        }
        [Description("ảnh")]
        public string img { get; set; }
        [Description("is active")]
        public bool is_active { get; set; }
        [Description("người tạo")]
        public string create_by { get; set; }
        [Description("ngày tạo")]
        public DateTime create_at { get; set; }
        [Description("người cập nhật")]
        public string update_by { get; set; }
        [Description("ngày cập nhật")]
        public DateTime update_at { get; set; }
    }
    public class NotificationRequestModel : BaseRequest
    {
        [Description("Trang")]
        public int PageIndex { get; set; }
        [Description("Page size")]
        public int PageSize { get; set; }
    }

    public class NotificationResponseModel
    {
        [Description("Trang")]
        public int PageSize { get; set; }
        [Description("Tong so luong")]
        public int TotalItem { get; set; }
        [Description("data")]
        public List<NotificationModel> Items { get; set; }
    }
}
