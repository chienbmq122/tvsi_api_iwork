﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Activity;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IActivityService
    {
        Task<List<ActivityResult>> GetActivity(ActivityRequest model);
        Task<List<ActivityResult>> GetActivityByEventType(ActivityRequestByEventType model);
        Task<ActivityResultData> GetActivityByFiltter(FilterActivity model);
        Task<ActivityResult> GetDetailActivity(DeleteActivityRequest model);
        Task<IEnumerable<ActivityTypeResult>> GetActivityType();
        Task<bool> CreateActivity(CreateActivityRequest model);
        Task<bool> UpdateActivity(UpdateActivityRequest model);
        Task<bool> DeleteActivity(DeleteActivityRequest model);
        Task<bool> UpdateOrDeleteListActivity(UpdateListActivity model);
        Task<bool> ConfirmOrRejectActivity(ConfirmOrRejectRequest model);
        Task<List<ActivityDataMonthCheckResponse>> CheckDataByMonth(ActivityDataMonthRequest model);
        Task<List<ActivityDataMonthResponse>> GetHisTimeWorkStaffByMonth(ActivityDataMonthRequest model);
        Task<bool> CheckCustCode(string custCode, string assignUser);
    }
}
