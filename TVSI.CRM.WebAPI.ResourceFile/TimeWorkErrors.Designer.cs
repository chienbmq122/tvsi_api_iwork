﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TVSI.CRM.WebAPI.ResourceFile {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AccountErrors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AccountErrors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TVSI.CRM.WebAPI.ResourceFile.AccountErrors", typeof(AccountErrors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Khách hàng là tổ chức.
        /// </summary>
        public static string E800 {
            get {
                return ResourceManager.GetString("E800", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to giới hạn 6 số .
        /// </summary>
        public static string MaxLength_AccountNum {
            get {
                return ResourceManager.GetString("MaxLength_AccountNum", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu số tài khoản .
        /// </summary>
        public static string Validate_AccountNo {
            get {
                return ResourceManager.GetString("Validate_AccountNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu loại khách hàng.
        /// </summary>
        public static string Validate_CustType {
            get {
                return ResourceManager.GetString("Validate_CustType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Số ngày nghỉ.
        /// </summary>
        public static string Validate_NumLeave {
            get {
                return ResourceManager.GetString("Validate_NumLeave", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dữ liệu vượt quá kích thước cho phép.
        /// </summary>
        public static string Validate_PageSize {
            get {
                return ResourceManager.GetString("Validate_PageSize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Chế độ nghỉ phép.
        /// </summary>
        public static string Validate_PolicyLeave {
            get {
                return ResourceManager.GetString("Validate_PolicyLeave", resourceCulture);
            }
        }
    }
}
