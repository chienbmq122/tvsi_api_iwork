﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;


namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{

    #region "View chi tiết khách hàng"
    /// <summary>
    /// CustInfoRequest
    /// </summary>
    public class CustInfoRequest : BaseRequest
    {
        [Description("Mã khách hàng : 6 số")]
        [Required]
        public string CustCode { get; set; }
    }

    /// <summary>
    /// CustInfoResult
    /// </summary>
    public class CustInfoResult
    {
        [Description("Mã khách hàng : 6 số")]
        public string CustomerID { get; set; }
        [Description("Mã khách hàng : 044C")]
        public string CustomerNo { get; set; }
        [Description("Họ tên")]
        public string CustomerName { get; set; }
        [Description("CMND")]
        public string IdentityCard { get; set; }
        [Description("Ngày cấp cmnd")]
        public DateTime? CardIssue { get; set; }

        [Description("Nơi cấp cmnd")]
        public string PlaceIssue { get; set; }

        [Description("Giới tính : M, F")]
        public string Sex { get; set; }
        [Description("Địa chỉ liên hệ")]
        public string Address { get; set; }
        [Description("Điện thoại đi động")]
        public string CellPhone { get; set; }
        [Description("điện thoại di động")]
        public string ContactPhone { get; set; }
        [Description("Điện thoại cố định")]
        public string Phone { get; set; }
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        [Description("Ngày sinh")]
        public DateTime? Birthday { get; set; }
        [Description("Điện thoại nhận SMS")]
        public string PhoneSMS { get; set; }
        [Description("Điện thoại Contact Center")]
        public string PhoneCC { get; set; }
        [Description("Loại khách hàng")]
        public string CustomerGroupName { get; set; }
        [Description("Nhóm ký quỹ")]
        public string MarginGroup { get; set; }
        [Description("Nhân viên môi giới")]
        public string SaleName { get; set; }
        [Description("Chi nhánh")]
        public string BranchName { get; set; }
        
        [Description("Ngày mở tài khoản")]
        public string CreatedAccount {
            get
            {
                return CreatedAccountOn?.ToString("dd/MM/yyyy HH:mm:ss");
            } }
        
        [JsonIgnore]
        public DateTime? CreatedAccountOn { get; set; }

    }
    #endregion


    #region "Lấy danh sách tài khoản đăng ký chuyển tiền"
    /// <summary>
    /// BankAccTransferRequest
    /// </summary>
    public class BankAccTransferRequest : BaseRequest
    {
        [Description("Mã khách hàng : 6 số")]
        [Required]
        public int CustCode { get; set; }
    }
    /// <summary>
    /// BankAccTransferResult
    /// </summary>
    public class BankAccTransferResult
    {
        [Description("Số tài khoản ngân hàng")]
        public string BankAccount { get; set; }
        [Description("Chủ tài khoản")]
        public string BankAccountName { get; set; }
        [Description("Tên ngân hàng")]
        public string BankName { get; set; }
        [Description("Chi nhánh ngân hàng")]
        public string BranchName { get; set; }
    }
    #endregion



    #region "Lấy số dư tiền đuôi 1 và 6"
    /// <summary>
    /// CashBalanceInfoRequest
    /// </summary>
    public class CashBalanceInfoRequest : BaseRequest
    {
        [Description("Mã khách hàng : 6 số")]
        [Required]
        public int CustCode { get; set; }
    }
    /// <summary>
    /// CashBalanceInfoResult
    /// </summary>
    public class CashBalanceInfoResult
    {
        [Description("Số dư tiền đuôi 1")]
        public string CashBalanceInfo_1 { get; set; }
        [Description("Số dư tiền đuôi 6")]
        public string CashBalanceInfo_6 { get; set; }

    }
    #endregion


    #region "Danh mục đầu tư"
    /// <summary>
    /// AccPorfolioRequest
    /// </summary>
    public class AccPorfolioRequest : BaseRequest
    {
        [Description("Mã khách hàng : 6 số")]
        [Required]
        public string CustCode { get; set; }
    }
    /// <summary>
    /// AccPorfolioResult
    /// </summary>
    public class AccPorfolioResult
    {
        [Description("Số tài khoản : 7 số")]
        public string AccountNo { get; set; }
        [JsonIgnore]
        public DateTime TradingDateOn { get; set; }
        
        [Description("Ngày giao dịch")]
        public string TradingDate
        {
            get
            {
                return TradingDateOn.ToString("dd/MM/yyyy");
            }
        }

        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        [Description("Khối lượng")]
        public decimal Volume { get; set; }
        [Description("Giá mua trung bình")]
        public decimal AvgPrice { get; set; }
        [Description("Loại chứng khoán")]
        public int SecType { get; set; }

        [Description("Loại chứng khoán")]
        public string SecTypeName
        {
            get
            {
                switch (SecType)
                {
                    case 2:
                        return "CK tự do chuyển nhượng";
                    case 3:
                        return "CK lưu ký";
                    case 9:
                        return "CK hưởng quyền";
                    case 12:
                        return "CK hưởng quyền chờ giao dịch";
                    case 42:
                        return "CK Chứng khoán mua chờ nhận về";
                }

                switch (SecType)
                {
                    case 42:
                        return "CK bán chờ chuyển đi";
                    case 43:
                        return "CK chờ chuyển đi";
                    case 46:
                        return "CK cầm cố";
                    case 49:
                        return "CK hạn chế chuyển nhượng";
                }

                return SecType + "";
            }
        }
    }
    #endregion
    #region "Thông tin đặt lệnh"
    public class TradingInfoRequest : BaseRequest
    {
        [Description("Mã khách hàng : 6 số")]
        [Required]
        public int CustCode { get; set; }
    }

    public class TradingInfoResult 
    {
        [Description("Danh sách lệnh đặt")]
        public List<OrderInfoResult> OrderList { get; set; }
        [Description("Danh sách lệnh khớp")]
        public List<DealInfoResult> DealList { get; set; }
    }
    #endregion

    #region "Danh sách lệnh đặt"

    /// <summary>
    /// OrderInfoResult
    /// </summary>
    public class OrderInfoResult
    {
        [Description("Thời gian giao dịch")]
        public string TransTime { get; set; }
        [Description("Tài khoản đặt lệnh")]
        public string AccountNo { get; set; }
        [Description("Mua/bán")]
        public string Side { get; set; }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }          
        [Description("Khối lượng đặt")]
        public long Volume { get; set; }
        [Description("Giá đặt")]
        public decimal Price { get; set; }
        [Description("Trạng thái lệnh")]
        public int OrderStatus { get; set; }
        [Description("Trạng thái hủy")]
        public int CancelStatus { get; set; }
        [Description("Trạng thái sửa")]
        public int ChangeStatus { get; set; }
        [Description("Khối lượng đặt")]
        public long ExecutedVol { get; set; }
        [Description("Khối lượng khớp")]
        public decimal ExecutedPrice { get; set; }
        [Description("Khối lượng hủy")]
        public long CancelledVolume { get; set; }

        public string TransTimeStr => TransTime.Substring(9, 2) + ":" + TransTime.Substring(11, 2) + ":" + TransTime.Substring(13, 2);

        public string SideStr => Side == "B" ? "Mua" : "Bán";

        public string OrderStatusStr
        {
            get
            {
                switch (OrderStatus)
                {
                    case 13:
                        return "Chờ kích hoạt";
                    case 0:
                    case 1:
                        if (ExecutedVol > 0)
                        {
                            if (Volume == ExecutedVol)
                                return "Khớp hết";

                            if (Volume > ExecutedVol)
                                return "Khớp 1 phần";
                        }
                        else
                        {
                            return "Lệnh đang chờ";
                        }
                        break;
                }

                if (OrderStatus == 2)
                {
                    if (CancelledVolume == Volume)
                        return "Huỷ hết";

                    if (ExecutedVol == Volume)
                        return "Khớp hết";

                    if (ExecutedVol > 0 && CancelledVolume > 0)
                        return "Khớp/Huỷ 1 phần";

                    if (ExecutedVol > 0)
                        return "Khớp 1 phần";
                }

                switch (OrderStatus)
                {
                    case 3:
                        return "Lệnh bị từ chối";
                    case 12:
                        return "Lệnh chờ duyệt";
                }

                switch (CancelStatus)
                {
                    case 0:
                    case 1:
                        return "Lệnh đang chờ huỷ";
                    case 3:
                        return "Huỷ bị từ chối";
                    case 2:
                        return "Lệnh đã huỷ";
                }

                switch (ChangeStatus)
                {
                    case 0:
                    case 1:
                        return "Lệnh đang chờ sửa";
                    case 2:
                        return "Lệnh đã sửa";
                    case 3:
                        return "Lệnh sửa bị từ chối";
                }

                return OrderStatus + "";
            }
        }

    }
    #endregion


    #region "Danh sách lệnh khớp"
    /// <summary>
    /// DealInfoResult
    /// </summary>
    public class DealInfoResult
    {
        [Description("Thời gian khớp lệnh")]
        public string DealTime { get; set; }
        [Description("Số tài khoản")]
        public string AccountNo { get; set; }
        [Description("Mã chứng khoán")]
        public string Symbol { get; set; }
        [Description("Mua/Bán")]
        public string Side { get; set; }
        [Description("Khối lượng khớp")]
        public int DealVolume { get; set; } 
        [Description("Giá khớp")]
        public decimal DealPrice { get; set; } 

        public string DealTimeStr => DealTime.Substring(0, 2) + ":" + DealTime.Substring(2, 2) + ":" + DealTime.Substring(4, 2);

        public string SideStr => Side == "B" ? "Mua" : "Bán";
    }
    #endregion



    public class ExCustInfoRequest : BaseRequest
    {
       
    }

    public class ExCustInfoResult
    {
        [Description("Ngành nghề, công việc")]
        public List<JobInfo> JobList { get; set; } // Occupation
        public int JobListId { get; set; } // Occupation
        
        [Description("Nơi làm việc")]
        public List<JobCityInfo> JobCityList { get; set; } // Workplace
        [Description("Khả năng tài chính")]
        public List<FinAbilityInfo> FinAbilityList { get; set; } // FinancialResources
        [Description("Thời gian tham gia đầu tư")]
        public List<KnowledExpInfo> KnowledExpList { get; set; } // KnowledExpID
        [Description("Quan điểm đầu tư")]
        public List<OpInvestInfo> OpInvestList { get; set; } // OpinionInvestID
        [Description("Nguồn giới thiệu")]
        public List<SourceInfo> SourceList { get; set; } // SourceID
        [Description("Tổ chức tài chính /Sale BB dùng")]
        public List<FinlOrganizeInfo> FinlOrganizeList { get; set; } // JoinedBondSince
        
        [Description("Kinh nghiệm đầu tư /Sale BB dùng")]
        public List<InvestExperInfo> InvestExperList { get; set; } // InvestmentExperience
        [Description("Khả năng chấp nhận rủi ro /Sale BB dùng")]
        public List<RiskInfo> RiskList { get; set; } // RiskAcceptance
        [Description("Sở thích đầu tư /Sale BB dùng")]
        public List<InvestPreInfo> InvestPreList { get; set; }  // HobbyInvestment
        [Description("Lưu ý khi giao tiếp /Sale BB dùng")]
        public List<NoteCommInfo> NoteCommList { get; set; } // NoteTalking
        [Description("Mối quan hệ với Sale /Sale BB dùng")]
        public List<SaleRelInfo> SaleRelList { get; set; } // RelationshipSale
    }

    public class JobInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class JobCityInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class FinAbilityInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class KnowledExpInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class OpInvestInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class SourceInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class FinlOrganizeInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class InvestExperInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class RiskInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class InvestPreInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class NoteCommInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class SaleRelInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }



    public class CustActivityRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }
    public class CustActivityResult
    {
        [Description("ID activity")]
        public int ActivityId { get; set; }
        [Description("Tiêu đề hoạt động")]
        public string ActivityName { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Ngày bắt đầu")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc")]
        public string EndDate { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.ActivityStatus.ResourceManager.GetString("Status_" + Status);

        [Description("AssignUser Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string AssignUser { get; set; }
       
    }

    public class CustOpportunityRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
    }
    public class CustOpportunityResult
    {
        [Description("ID Opportunity")]
        public int OpportunityId { get; set; }
        [Description("Tiêu đề cơ hội")]
        public string OpportunityName { get; set; }
        [Description("Lợi nhuận dự kiến")]
        public double ExpectedRevenue { get; set; }
        [Description("Ngày dự kiến đóng")]
        public string ExpectedCloseDate { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.ActivityStatus.ResourceManager.GetString("Status_" + Status);

        [Description("AssignUser Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string AssignUser { get; set; }

    }




    public class CustViewInfoRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public int CustCode { get; set; }
    }

    /// <summary>
    /// LeadInfoResult
    /// </summary>
    public class CustViewInfoResult
    {
        [Description("Mã khách hàng tiềm năng")]
        public string LeadId { get; set; }
        [Description("Họ và tên")]
        [Required]
        public string LeadName { get; set; }

        [Description("Số điện thoại khách hàng")]
        [Required]
        public string Mobile { get; set; }
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        [Description("Địa chỉ liên hệ")]
        public string Address { get; set; }
        [Description("Loại hình - Cá nhân: 1, Tổ chức: 2")]
        public int ProfileType { get; set; }

        [Description("Mô tả về lead")]
        public string Description { get; set; }

        [Description("Nguồn thông tin qua - Hội thảo: 1, Quảng cáo: 2, Người giới thiệu: 3, Sự kiện công ty tổ chức: 4, Qua website: 5, Khác: 6")]
        public int LeadSourceID { get; set; }

        [Description("Nguồn thông tin qua - Hội thảo: 1, Quảng cáo: 2, Người giới thiệu: 3, Sự kiện công ty tổ chức: 4, Qua website: 5, Khác: 6")]
        public string SourceName { get; set; }
        public List<ExLeadInfo> ExLeadList { get; set; }
    }

    public class ExLeadInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }

    public class CustomerInfoResult
    {

        [Description("Thông tin liên hệ")]
        public CustomerInfoContact CustomerInfoContact { get; set; }

        [Description("thông tin khách hàng")]
        public List<GroupCategoryDetail> CustInfoContactCategory { get; set; }

    }

    public class CustomerInfoContact
    {
        [Description("Mã khách hàng : 6 số")]
        public string CustomerID { get; set; }
        
        [Description("Họ tên")]
        public string CustomerName { get; set; }
        
        [Description("Dien thoai")]
        public string Phone { get; set; }

        [Description("Địa chỉ email")]
        public string Email { get; set; }
        
        [Description("Địa chỉ liên hệ")]
        public string Address { get; set; }   
    }

    public class GroupCategoryDetail
    {
        [Description("Item group type")]
        public string ItemGroupType { get; set; }
        [Description("Item group type name")]
        public string ItemGroupTypeName { get; set; }
        [Description("Data item")]
        public List<CustomerInvestmentInfo> DataItem { get; set; }

    }

    public class CustomerInvestmentInfo
    {
        [Description("Tên category")]
        public string ItemName { get; set; }
        [Description("Id category")]
        public int ItemID { get; set; }
        [Description("Category type")]
        public int ItemTypeID { get; set; }
        [Description("Category type text")]
        public string ItemTypeLabel
        {
            get
            {
                switch (ItemTypeID)
                {
                    case 1:
                        return "Nghề nghiệp"; // sale // bb
                    case 2:
                        return "Khả năng tài chính"; // sale // bb
                    case 3:
                        return "Thời gian đầu tư"; // sale
                    case 4:
                        return "Quan điểm đầu tư"; // sale
                    case 5:
                        return "Nguồn giới thiệu"; // sale
                    case 6:
                        return "Nơi làm việc"; // bb
                    case 7:
                        return "Dã tham gia trái phiếu tại"; //bb
                    case 8:
                        return "Kinh nghiêm đầu tư";  //bb
                    case 9:
                        return "Khả năng chấp nhận rủi ro"; //bb
                    case 10:
                        return "Sở thích đầu tư"; //bb
                    case 11:
                        return "Lưu ý khi giao tiếp"; //bb
                    case 12:
                        return "Mối quan hệ với Sale"; //bb
                    default:
                        return "Sắp ra mắt";
                };
            }
        }
        //[Description("Category type text")]
        //public string ItemGroupType
        //{
        //    get
        //    {
        //        if (ItemTypeID == 1 || ItemTypeID == 6)
        //        {
        //            return "ContactInfo";
        //        }
        //        else if (ItemTypeID == 2 || ItemTypeID == 7 || ItemTypeID == 8 || ItemTypeID == 4 || ItemTypeID == 3 || ItemTypeID == 5)
        //        {
        //            return "InvestmentInfo";
        //        }
        //        else if (ItemTypeID == 9 || ItemTypeID == 11 || ItemTypeID == 10 || ItemTypeID == 12)
        //        {
        //            return "CustCareInfo";
        //        }
        //        else
        //        {
        //            return "ExtendInfo";
        //        }
        //    }
        //}
    }

    public class ExtendCustJoin
    {
        public int CategoryType { get; set; }
        public string Category { get; set; }
        public string CategoryName { get; set; }
    }

    public class CustomerInfoSupport
    {
        [Description("Lưu ý khi giao tiếp")]
        public string NoteTalk { get; set; }
        
        [Description("Nhu cầu/ mong muốn")]
        public string Desire { get; set; }
    }

    public class CustomerInfoExtend
    {
        [Description("Nghề nghiệp")]
        public string Work { get; set; }
        
        [Description("Nơi làm việc")]
        public string Workplace { get; set; }
    }

    public class CustomerSupport
    {
        public int AttributeID { get; set; }
        public string Value { get; set; }

    }

    public class CustomerDetailInfoRequest : BaseRequest
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
    }

    public class PhonePotentialInfoRequest : BaseRequest
    {
        [Description("Số điện thoại kiểm tra")]
        public string Phone { get; set; }
    }

    public class PhoneInfoListRequest : BaseRequest
    {
        public List<string> PhoneList { get; set; }
    }

    public class PhoneInfoListResult
    {
        public string Phone { get; set; }
        public string FullName { get; set; }
    }

    public class PhoneInfoInno
    {
        public string FullName { get; set; }
        public string ContactPhone { get; set; }
        public string Phone { get; set; }

    }

    public class PhoneInfo
    {
        public string Phone { get; set; }
    }
    
    
}
