﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface ICommentText
    {
        /// <summary>
        /// get comment cust
        /// </summary>
        string GetListCommentCust { get; }

        /// <summary>
        /// get comment lead
        /// </summary>
        string GetListCommentLead { get; }

        /// <summary>
        /// get reply
        /// </summary>
        string GetListReply { get; }

        /// <summary>
        /// Create comment
        /// </summary>
        string CreateComment { get; }

        /// <summary>
        /// Create reply
        /// </summary>
        string CreateReply { get; }
    }
}
