﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IOpportunityCommandText
    {
        /// <summary>
        /// get Opportunity
        /// </summary>
        string GetListOpportunity { get; }

        /// <summary>
        /// get Opportunity
        /// </summary>
        string CreateOpportunity { get; }

        /// <summary>
        /// get Opportunity
        /// </summary>
        string UpdateOpportunity { get; }

        /// <summary>
        /// get Opportunity
        /// </summary>
        string DeleteOpportunity { get; }

        /// <summary>
        /// get detail opportunity
        /// </summary>
        string GetDetailOpportunity { get; }
    }
}
