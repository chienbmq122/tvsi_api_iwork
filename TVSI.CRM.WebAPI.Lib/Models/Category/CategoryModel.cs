﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Category
{
    public class CategoryRequset : BaseRequest
    {}

    public class CategoryResult
    {
        [Description("Id category")]
        public int ItemID { get; set; }
        [Description("tên hiển thị category")]
        public string ItemName { get; set; }
        [Description("Id type category")]
        public int ItemTypeID { get; set; }
    }
}