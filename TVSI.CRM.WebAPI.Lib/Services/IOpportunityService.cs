﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Opportunity;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IOpportunityService
    {
        Task<IEnumerable<OpportunityResult>> GetListOpportunity(OpportunityRequest model);
        Task<OpportunityResult> GetDetailOpportunity(OpportunityDalete model);
        Task<bool> CreateOpportunity(OpportunityCreate model);

        Task<bool> UpdateOpportunity(OpportunityUpdate model);

        Task<bool> DeleteOpportunity(OpportunityDalete model);

        Task<bool> CheckCustCode(string custCode, string assignUser);
    }
}
