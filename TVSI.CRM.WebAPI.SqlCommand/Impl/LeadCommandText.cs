﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class LeadCommandText : ILeadCommandText
    {
        #region SQL Command List
        /// <summary>
        /// Danh sách KH tiềm năng
        /// </summary>
        public string GetLeadList => @"SELECT L.CreatedDate, L.LeadId, L.LeadName, L.Mobile, L.Email, L.ProfileType, S.SourceName, L.Status, L.AssignUser
                        FROM Lead as L
						inner join LeadSource as S on L.LeadSourceID = S.LeadSourceID
                        WHERE (L.AssignUser = @SaleId or L.AssignUser = @UserName) {cond2} {cond3} AND L.Status = 1
                        Order by L.CreatedDate DESC 
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";


        /// <summary>
        /// Query lấy danh sách hoạt động
        /// </summary>
        public string GetActivityLeadInfo => @"SELECT ActivityID, ActivityName, Description, StartDate,EndDate , Status, AssignUser
                        FROM Activity 
                        WHERE   AssignUser = @SaleId and LeadID = @leadId and Status = 1
                        Order by CreatedDate DESC 
                        OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        /// <summary>
        /// Query lấy danh sách cơ hội
        /// </summary>
        public string GetOpportunityLeadInfo => @"select  OpportunityID, OpportunityName, ExpectedRevenue, ExpectedCloseDate, Status, AssignUser, Description 
                                        from Opportunity
                                        WHERE   AssignUser = @SaleId and LeadId = @leadId
                                        Order by CreatedDate DESC";

        /// <summary>
        /// Thêm mới thông tin KH tiềm năng
        /// </summary>
        public string PostLeadAddInfo => @"INSERT INTO Lead (LeadName,Mobile,Email,Address,ProfileType,Status,AssignUser,
                                                            Description,LeadSourceID,CreatedBy,CreatedDate)
                                        OUTPUT Inserted.LeadId
                                        VALUES (@leadName,@mobile,@email,@address,@profileType,@status,@assignUser,@description,
                                                @leadSourceID,@createdBy,@createdDate)";

        /// <summary>
        /// Thêm mới thông tin mở rộng liên quan đến khách hàng tiềm năng
        /// </summary>
        public string PostLeadExtInfo => @"INSERT INTO ExtendCategoryLead (Category,LeadID,CreatedBy,CreatedDate)
                                        VALUES (@category,@leadId,@createdBy,@createdDate)";


        /// <summary>
        /// Query lấy danh sách list các thuộc tính mở rộng cho khách hàng tiềm năng
        /// </summary>
        public string GetExLeadInfo => @"SELECT Category as ExId, CategoryName as ExName from ExtendCategory where status = 1 and CategoryType = @ExType   order by ExId";


        /// <summary>
        /// Thông tin chi tiết khách hàng tiềm năng
        /// </summary>
        public string GetLeadInfo => @"SELECT L.LeadId, L.LeadName, L.Mobile, L.Email, L.Address, L.ProfileType, L.LeadSourceID, S.SourceName, L.Description
                                        FROM Lead as L
						                inner join LeadSource as S on L.LeadSourceID = S.LeadSourceID
                                        WHERE   L.LeadID = @leadId";

        public string GetExLeadDetails => @"select A.Category as ItemID, A.CategoryName as ItemName, A.CategoryType as ItemTypeID from ExtendCategory as A
                                            inner join ExtendCategoryLead as B on A.Category = B.Category where B.LeadID = @leadId";

        public string UpdateLeadInfo => @"UPDATE Lead SET LeadName = @leadName,
					                                    Mobile = @mobile,
					                                    Email = @email,
					                                    Address = @address,
					                                    ProfileType = @profileType,
					                                    AssignUser = @assignUser,
                                                        Description = @description,
                                                        LeadSourceId = @leadSourceID,
                                                        UpdatedBy = @updatedBy,
                                                        UpdatedDate = @updatedDate
                                                        WHERE LeadId = @leadId AND Status <> 99";
        public string DeleteExLeadInfo => @"DELETE ExtendCategoryLead WHERE LeadId = @leadId";

        public string DeleteLeadInfo => @"UPDATE Lead SET 
                                                        Status = @status,
                                                        UpdatedBy = @updatedBy,
                                                        UpdatedDate = @updatedDate
                                                        WHERE LeadId = @leadId";

        public string GetLeadsDropbox => @"SELECT LeadID, LeadName FROM Lead WHERE LeadName LIKE @leadName AND AssignUser = @assignUser AND Status <> 99 
                                            Order by LeadName ASC OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

        public string GetLeadsDropboxCount => @"SELECT count(*) FROM Lead WHERE LeadName LIKE @leadName AND AssignUser = @assignUser AND Status <> 99";

        public string GetListAttributeLead => @"SELECT *  FROM LeadAttribute JOIN Attribute ON Attribute.AttributeID = LeadAttribute.AttributeID WHERE LeadAttribute.LeadID = @leadId";

        public string CreateAttributeLeadToCustomer => @"INSERT INTO CustAttribute(AttributeID, CustCode, Value, Status, Priority, CreatedBy, CreatedDate) VALUES (@AttributeID, @CustCode, @Value, 1, @Priority, @CreatedBy, @CreatedDate)";

        public string UpdateCommentLeadToCustomer => @"UPDATE Comment SET CustCode = @CustCode, LeadID = -1 WHERE LeadID = @leadID";

        public string UpdateOpportunityToCustomer => @"UPDATE Opportunity SET CustCode = @CustCode WHERE LeadID = @leadID";

        public string UpdateActivityToCustomer => @"UPDATE Activity SET CustCode = @CustCode WHERE LeadID = @leadID";

        public string CheckCustInfo => @"SELECT COUNT(1) FROM CustInfo WHERE CustCode = @CustCode AND LeadID is not null";

        public string UpdateCustInfo => @"UPDATE CustInfo SET LeadID = @LeadID WHERE CustCode = @CustCode";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion

    }
}
