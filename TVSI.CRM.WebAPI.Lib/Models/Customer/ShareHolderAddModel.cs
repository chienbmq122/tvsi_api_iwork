﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class ShareHolderAddRequest: BaseRequest
    {
        [Description("Mã khách hàng")]
        [Required]
        public string CustCode { get; set; }

        [Description("Họ và tên")]
        [Required]
        public string CustName { get; set; }
        [Description("Địa chỉ liên hệ")]
        public string Address { get; set; }
        [Description("Cổ đông lớn")]
        public int ShareType_1 { get; set; }
        [Description("Cổ đông nội bộ")]
        public int ShareType_2 { get; set; }
        [Description("Người liên quan cổ đông lớn")]
        public int ShareType_3 { get; set; }
        [Description("Sắp thành cổ đông lớn")]
        public int ShareType_4 { get; set; }

        [Description("Danh sách mã chứng khoán theo dõi")]
        public string StockCode { get; set; }

        [Description("User được chỉ định : Thường SaleId nhập vào 4 ký tự số")]
        public string AssignUser { get; set; }


    }
}
