﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Comment;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface ICommentService
    {
        Task<List<CommentDBModel>> GetListComment(CommentModelRequest model);
        Task<bool> CreateComent(CreateCommentRequest model);
        Task<bool> CreateReply(CreateReplyRequest model);
    }
}
