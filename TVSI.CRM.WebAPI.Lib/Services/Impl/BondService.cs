﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.CRM.WebAPI.Lib.Models.Bond;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class BondService : BaseService, IBondService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CustomerService));
        
        public async Task<IEnumerable<BondRequestOrderListResult>> GetBondRequestOrderList(BondRequestOrderListRequest request)
        {
            try
            {
                var start = (request.PageIndex - 1) * request.PageSize;
                return await WithTbmDbConnection(async conn =>
                {
                    var sql = @"select A.RequestOrderID, A.CreatedDate, A.AccountNo, A.Side, A.ContractNo, A.AccountName, A.BondCode, 
                            A.Volume, A.Status, A.ReferID, B.BranchID, A.SaleID,
                            (case when A.TradeDate = C.MaturityDate and A.Side = 'S' then 1
                            else 0 end) IsSellMaturity
                            from BondRequestOrder A 
                            left join [User] B on A.CreatedBy = B.UserName 
                            left join ContractHist C on A.ContractNo = C.Contractno
                            where A.Status != 99";

                    var condition = "";

                    if (request.UserLevel == 0)
                    {
                        condition += " and A.CreatedBy=@UserName";
                    }
                    else
                    {
                    // Trường hợp là trưởng nhóm hoặc GĐCN
                    var branchId = await conn.QueryFirstOrDefaultAsync<int?>("select BranchId from [User] where UserName = @userName",
                                    new { userName = request.UserName });

                        var subBranchList = LoadSubBranchIdByUserTbm(conn, request.UserName, branchId);
                        var strBranchIds = string.Join("', '", subBranchList.ToArray());
                        condition += " and B.BranchID in ('" + strBranchIds + "')";
                    }

                    DateTime fromDate = new DateTime(1753, 1, 1);
                    DateTime toDate = DateTime.MaxValue;

                    if (!string.IsNullOrEmpty(request.FromDate))
                    {
                        condition += " and A.CreatedDate >= @FromDate";
                        fromDate = DateTime.ParseExact(request.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (!string.IsNullOrEmpty(request.ToDate))
                    {
                        condition += " and A.CreatedDate <= @ToDate";
                        toDate = DateTime.ParseExact(request.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }

                    if (!string.IsNullOrEmpty(request.SearchKey))
                    {
                        condition += " and (A.AccountNo like '%" + request.SearchKey + "%' or  A.AccountName like '%" + request.SearchKey + "%' or A.ContractNo like '%" + request.SearchKey + "%' or A.ReferID like '%" + request.SearchKey + "%')";
                    }

                    if (request.Status >= 0)
                    {
                        condition += " and A.Status=@Status";
                    }

                    condition += " order by A.CreatedDate desc";
                    sql += condition;
                    sql += " OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";

                    var data = await conn.QueryAsync<BondRequestOrderListResult>(sql, new
                    {
                        UserName = request.UserName,
                        FromDate = fromDate,
                        ToDate = toDate,
                        Status = request.Status,
                        Start = start,
                        PageSize = request.PageSize
                    });

                    return data;

                });
            }
            catch (Exception ex)
            {
                log.Error("GetBondRequestOrderList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BondRequestOrderDetailResult> GetBondRequestOrderDetail(long id)
        {
            try
            {
                return await WithTbmDbConnection(async conn =>
                    {
                        var sql = "select * from BondRequestOrder where RequestOrderID = @ID";

                        var data = await conn.QueryFirstAsync<BondRequestOrderDetailResult>(sql,
                                                        new { ID = id });

                        if (data != null)
                        {
                            var custCode = data.AccountNo.Substring(0, 6);
                            var custData = await conn.QueryFirstOrDefaultAsync<dynamic>("select Address, CustType from CustInfo where CustCode = @CustCode", new { CustCode = custCode });

                            if (custData != null)
                            {
                                data.Address = !string.IsNullOrEmpty(custData.Address) ? custData.Address : "";
                                data.CustType = custData.CustType;
                            }
                            else
                            {
                                data.CustType = 1;
                                data.Address = "";
                            }

                            sql = @"select BondID, BondCode, BondName, Term, CouponRate, Issuer, 
                                    MaturityDate, ParValue, IssueDate, IssuerGroup from BondInfo where BondCode=@bondCode";

                            var bondInfo = await conn.QueryFirstOrDefaultAsync<BondInfo>(sql, new { bondCode = data.BondCode });
                            data.BondInfo = bondInfo;

                            if (data.NavConfigID > 0)
                            {
                                var navName = await conn.QueryFirstOrDefaultAsync<string>("Select NavName from NavConfig where NavConfigId = @navConfigId",
                                    new { navConfigId = data.NavConfigID });
                                data.NavName = navName;
                            }

                            if (data.Side == "S" && data.PaymentMethod == 1)
                            {
                                var sql2 = @"select BankName, SubBranchName, BankAccountName, 
                                            BankAccount from CustBank c
                                            left join BankInfo b on c.BankID = b.BankID where CustBankID = @ID and c.Status = 1";
                                var bankData = await conn.QueryFirstOrDefaultAsync<BankInfo>(sql2, new { ID = @data.CustBankID.Value });
                                if (bankData != null)
                                {
                                    data.BankAccountName = bankData.BankName + " - "
                                                           + bankData.SubBranchName + " | " + bankData.BankAccountName + " - " +
                                                           bankData.BankAccount;
                                }
                            }
                        }

                        return data;
                    });
            }
            catch (Exception ex)
            {
                log.Error("GetBondRequestOrderDetail:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        #region Mua Trái phiếu

        public async Task<IEnumerable<BondGroupResult>> GetBondGroupList()
        {
            try
            {
                return await WithTbmDbConnection(async conn =>
                        {
                            var sql = "select distinct IssuerGroup GroupCode from BondInfo where CanBuy = 1 order by IssuerGroup";

                            return await conn.QueryAsync<BondGroupResult>(sql);
                        });
            }
            catch (Exception ex)
            {
                log.Error("GetBondGroupList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<BondCodeResult>> GetBondCodeListByGroupCode(string groupCode)
        {
            try
            {
                return await WithTbmDbConnection(async conn =>
                    {
                        var sql = "select BondID, BondCode from BondInfo where IssuerGroup=@groupCode and Status=1 and CanBuy=1";

                        return await conn.QueryAsync<BondCodeResult>(sql, new { groupCode = groupCode });
                    });
            }
            catch (Exception ex)
            {
                log.Error("GetBondCodeListByGroupCode:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<IEnumerable<BondNavConfigResult>> GetBondNavConfigList()
        {
            try
            {
                return await WithTbmDbConnection(async conn =>
                    {
                        var sql = "select NavConfigID, NavName from NavConfig where Status = 1 order by NavCode";

                        return await conn.QueryAsync<BondNavConfigResult>(sql);
                    });
            }
            catch (Exception ex)
            {
                log.Error("GetBondNavConfigList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<List<BondPaymentMethodResult>> GetBondPaymentMethodList()
        {
            try
            {
                var list = new List<BondPaymentMethodResult>
                           {
                               new BondPaymentMethodResult
                               {
                                   PaymentMethod = 0,
                                   PaymentMethodName = ResourceFile.BondModule.PaymentMethod_0
                               },
                               new BondPaymentMethodResult
                               {
                                   PaymentMethod = 1,
                                   PaymentMethodName = ResourceFile.BondModule.PaymentMethod_1
                               }
                           };


                return await Task.FromResult(list);
            }
            catch (Exception ex)
            {
                log.Error("GetBondNavConfigList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        

        public async Task<BondCustInfoResult> GetCustInfo(string userName)
        {
            try
            {
                return await WithTbmDbConnection(async conn =>
                    {
                        var saleId = userName.Length > 4 ? userName.Substring(0, 4) : "";
                        var sql = @"select CustID, CustCode, CustName, SaleID from CustInfo 
                        where SaleID = @saleId and Status in (1,2,3)";

                        var custInfo = await conn.QueryAsync<CustData>(sql, new { saleId = saleId });

                        sql = @"select A.ReferID, U.DisplayName as ReferName, A.SaleID from AssignRefer A 
                            join[User] U on A.ReferID = U.SaleID
                            where A.SaleId = @saleId and A.Status = 1";
                        var referInfo = await conn.QueryAsync<ReferData>(sql, new { saleId = saleId });

                        return new BondCustInfoResult
                        {
                            CustInfoList = custInfo.ToList(),
                            ReferInfoList = referInfo.ToList()
                        };
                    });
            }
            catch (Exception ex)
            {
                log.Error("GetCustInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BondCustInfoDetailResult> GetCustInfoDetail(string custCode, string saleID)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var sql = @"select CustID, CustCode, CustName, Address, ProfesType, ProfesEffectiveDate 
                        from CustInfo where CustCode = @custCode and Status in (1,2,3) and SaleID=@saleID";

                            var data = await conn.QueryFirstOrDefaultAsync<BondCustInfoDetailResult>(sql, new
                            {
                                custCode = custCode,
                                saleID = saleID
                            });

                            if (data != null)
                            {
                                data.BankList = new List<BondCustBankData>();
                                sql = @"select CustBankID, CustCode, BankName, BankAccount, BankAccountName, SubBranchName 
                                from CustBank c left join BankInfo b on c.BankID = b.BankID
                                where CustID = @custID and c.Status = 1";

                                var bankList = await conn.QueryAsync<BondCustBankData>(sql, new { custID = data.CustID });
                                data.BankList = bankList.ToList();

                        // Get thông tin Nav hiện tại
                        var navInfo = await conn.QueryFirstOrDefaultAsync<BondCustNavData>("TVSI_sGET_CURRENT_NAV_ACCOUNT_INFO",
                                    new { accountNo = custCode + "1" }, commandType: CommandType.StoredProcedure);

                                if (navInfo != null)
                                {
                                    if (navInfo.FirstBuyDate.HasValue)
                                        data.FirstTradeDate = navInfo.FirstBuyDate.Value.ToString("dd/MM/yyyy");

                                    data.NavContractActive = navInfo.NavContractActive;
                                    data.NavContractProcess = navInfo.NavContractProcess;
                                    data.NavContractQueue = navInfo.NavContractQueue;
                                    data.CurNav = navInfo.TotalNav;
                                }

                            }

                            return data;
                        });
            }
            catch (Exception ex)
            {
                log.Error("GetCustInfoDetail:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BondInfoResult> GetBondInfo(string bondCode, string custCode)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var sql = @"select BondID, BondCode, BondName, Term, CouponRate, Issuer, 
                                    MaturityDate, ParValue, IssueDate, IssuerGroup from BondInfo where BondCode=@bondCode";

                            var data = await conn.QueryFirstAsync<BondInfoResult>(sql, new { bondCode = bondCode });
                            data.ProfesMsg = "";
                    // Check NĐT chuyên nghiệp
                    if (!string.IsNullOrEmpty(custCode))
                            {
                                sql = @"select CustID, CustCode, CustName, Address, ProfesType, ProfesEffectiveDate " +
                                      "from CustInfo where CustCode = @custCode and Status in (1,2,3)";
                                var custData = await conn.QueryFirstOrDefaultAsync<BondCustInfoDetailResult>(sql, new { custCode = custCode });
                                if (custData != null)
                                {
                            // Check NDT ko chuyên
                            if (custData.ProfesType == 0)
                                    {
                                        sql = "select ConfigValue from Config where ConfigCode = 'ProfesDateApprove'";
                                        var profesDateApprove = conn.Query<string>(sql).FirstOrDefault();
                                        if (!string.IsNullOrEmpty(profesDateApprove))
                                        {
                                            var dtxProfesDateApprove = DateTime.ParseExact(profesDateApprove, "yyyyMMdd", CultureInfo.InvariantCulture);
                                            if (data.IssueDate >= dtxProfesDateApprove.Date)
                                            {
                                                data.ProfesMsg = "KH phải là NĐT chuyên nghiệp mới có thể mua mã này!";
                                            }
                                        }
                                    }
                                }
                            }

                            return data;
                        });
            }
            catch (Exception ex)
            {
                log.Error("GetBondInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BondRateResult> GetBondRate(BondRateRequest model)
        {
            try { 
            return await WithTbmDbConnection(
                async conn =>
                {
                    var custCode = model.CustCode;
                    var custType = await conn.QueryFirstOrDefaultAsync<int?>("select CustType from CustInfo where CustCode = @CustCode",
                            new { CustCode = model.CustCode });

                    if (custType == null)
                    {
                        custType = 1;
                        custCode = "071086";
                    }

                    var bondInfo = await conn.QueryFirstOrDefaultAsync<BondInfo>("select BondCode, IssueDate, BondType, MaturityDate, IssuerGroup, Term from BondInfo where BondCode = @BondCode and Status = 1",
                                    new { BondCode = model.BondCode });

                    var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var maturityDate = tradeDate;

                    // 20210406 CuongLM Sửa theo chinh sách sp mới
                    // OUTRIGHT/OUTCUT có kỳ hakn từ 12 tháng trở lên => Lãi suất biên của Sale có biên độ tối đa 0.4%
                    // FIX/FLEX/HDMG tất cả kỳ hạn và OUTRIGHT/OUTCUT Kỳ hạn < 12 tháng => lãi suất biên của Sale tối đa 0.6%
                    var bondRate = 0m;
                    if (!string.IsNullOrEmpty(model.BondRate))
                        bondRate = decimal.Parse(model.BondRate);
                    

                    var difRateMax = 0.006m;
                    var bondRateMax = -1m;
                    var bondRateMin = -1m;
                    var firstBondRate = bondRate;
                    var basicRate = 0m;

                    if (bondInfo != null)
                    {
                        if (bondInfo.BondType == 7 || bondInfo.BondType == 8 || bondInfo.BondType == 11)
                            maturityDate = bondInfo.MaturityDate;


                        if ((new List<int> { 7, 8, 9, 10, 11 }).Contains(bondInfo.BondType) && bondInfo.Term >= 12)
                        {
                            if ((new List<int> { 7, 11 }).Contains(bondInfo.BondType))
                            {
                                if ((bondInfo.MaturityDate.Date - tradeDate.Date).TotalDays >= 365)
                                {
                                    difRateMax = 0.004m;
                                }
                            }
                            if ((new List<int> { 8, 9, 10 }).Contains(bondInfo.BondType))
                            {
                                difRateMax = 0.004m;
                            }
                        }
                        else if (bondInfo.BondType == 6 || bondInfo.BondType == 81) // FIX.00
                        {
                            difRateMax = -1m;
                        }
                    }

                    bondRate = bondRate / 100;


                    // Tìm BondRate
                    var bondRateTemp = await conn.QueryFirstOrDefaultAsync<decimal?>("TVSI_sCALC_BOND_GET_RATE_V1", new
                    {
                        BondCode = model.BondCode,
                        TradeDate = tradeDate,
                        CustType = custType,
                        IsMaturity = true,
                        SellDate = maturityDate
                    }, commandType: CommandType.StoredProcedure);

                    if (bondRateTemp.HasValue)
                    {
                        if (bondRate <= 0)
                        {
                            bondRate = bondRateTemp.Value;

                        }
                        basicRate = bondRateTemp.Value;

                        if (difRateMax > 0)
                        {
                            bondRateMax = bondRateTemp.Value + difRateMax;
                            bondRateMin = bondRateTemp.Value - difRateMax;
                        }
                    }

                    // Tìm giá mua
                    var buyPrice = 0m;

                    var data = new List<BondPriceCalc>();

                    if (bondInfo != null && (bondInfo.BondType == 8 || bondInfo.BondType == 11))
                    {
                        data = conn.Query<BondPriceCalc>("TVSI_sCALC_BOND_PRICE_BUY_BY_ACCOUNT",
                            new
                            {
                                AccountNo = custCode + "1",
                                BondCode = model.BondCode,
                                TradeDate = tradeDate,
                                RealRate = bondRate
                            }, commandType: CommandType.StoredProcedure).ToList();
                    }
                    else
                    {
                        data = conn.Query<BondPriceCalc>("TVSI_sCALC_BOND_PRICE_BY_ACCOUNT", new
                        {
                            AccountNo = custCode + "1",
                            BondCode = model.BondCode,
                            TradeDate = tradeDate
                        }, commandType: CommandType.StoredProcedure).ToList();
                    }

                    if (data.Count > 0)
                    {
                        var rowData = data.FirstOrDefault(x => x.RowCode == "Cost");
                        if (rowData != null)
                            buyPrice = rowData.Income;
                    }

                    // Tìm số dư khả dụng của kho
                    var availableVolume = 0L;

                    if (bondInfo != null)
                    {
                        var availableData =
                            conn.Query<BuyAvaiableViewModel>("TVSI_sGetBuyAvaiableAmount", new { IssuerGroup = bondInfo.IssuerGroup },
                                commandType: CommandType.StoredProcedure).FirstOrDefault();

                        if (availableData != null)
                            availableVolume = availableData.Volume;

                        var pendingVolume =
                            conn.Query<int?>(
                                "select sum(Volume) from BondRequestOrder where Side='B' and Status=0 and BondCode=@BondCode",
                                new { BondCode = model.BondCode }).FirstOrDefault();
                        if (pendingVolume.HasValue)
                        {
                            availableVolume = availableVolume - pendingVolume.Value;
                        }

                    }

                    // 2021612 CuongLM Sửa theo chinh sách sp mới
                    // TP phát hành trước ngày 01/01/2021
                    // NDT chuyen nghiep: -0.2%/năm
                    // NDT thông thường_KH cũ: Nav >= 3 tỷ: -0.2/năm
                    // NDT thông thường_KH mới: Nav >= 10 tỷ: -0.2%/năm
                    var companyRate = 0m;
                    if (bondInfo != null)
                    {

                        companyRate = conn.QueryFirstOrDefault<decimal>("TVSI_sBondProduct_GetIRRCompany",
                           new
                           {
                               TradeDate = tradeDate,
                               AccountNo = custCode + "1",
                               BondCode = model.BondCode,
                               Amount = buyPrice * model.Volume,
                               IsRoll = 0,
                               ServiceType = 0
                           }, commandType: CommandType.StoredProcedure);
                        
                    }

                    return new BondRateResult
                    {
                        BondCode = model.BondCode,
                        BondRate = firstBondRate <= 0 ? bondRate + companyRate : bondRate,
                        BuyPrice = buyPrice,
                        BuyAvailable = availableVolume,
                        BondRateMax = bondRateMax + companyRate,
                        BondRateMin = bondRateMin + companyRate,
                        CompanyRate = companyRate,
                        BasicRate = basicRate
                    };
                });
            }
            catch (Exception ex)
            {
                log.Error("GetBondRate:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> InsertBuyOrderRequest(BondBuyOrderRequest model)
        {
            try
            {


                return await WithTbmDbConnection(
                    async conn =>
                          {
                              var accountName = model.CustName;
                              var bankAccountName = "";

                              if (model.PaymentMethod == 1)
                              {
                                  var sqlSelect =
                                      @"select CustBankID, CustCode, BankName, BankAccount, BankAccountName, SubBranchName 
                    from CustBank c left join BankInfo b on c.BankID = b.BankID
                    where CustBankID = @CustBankID and c.Status = 1";

                                  var bankInfo =
                                      await
                                          conn.QueryFirstOrDefaultAsync<BondCustBankData>(sqlSelect,
                                              new {CustBankID = model.CustBankID});
                                  if (bankInfo != null)
                                  {
                                      bankAccountName = bankInfo.BankName + " - " + bankInfo.SubBranchName + " | "
                                                        + bankInfo.BankAccountName + " - " + bankInfo.BankAccount;
                                  }
                              }

                              var navContractActive = 0m;
                              var navContractProcess = 0m;
                              var navContractQueue = 0m;
                              var totalNav = 0m;
                              var rateBasic = 0m;
                              var irrCompany = 0m;
                              var irrSale = 0m;
                              var amount = model.Volume*model.BuyPrice;

                              // Get thông tin Nav hiện tại
                              var navInfo =
                                  conn.QueryFirstOrDefault<BondCustNavData>("TVSI_sGET_CURRENT_NAV_ACCOUNT_INFO",
                                      new {accountNo = model.CustCode + "1"}, commandType: CommandType.StoredProcedure);

                              if (navInfo != null)
                              {
                                  navContractActive = navInfo.NavContractActive;
                                  navContractProcess = navInfo.NavContractProcess;
                                  navContractQueue = navInfo.NavContractQueue + amount;
                                  totalNav = navInfo.TotalNav + amount;
                              }


                              var rateInfo = await GetBondRate(new BondRateRequest
                                                               {
                                                                   BondCode = model.BondCode,
                                                                   TradeDate = model.TradeDate,
                                                                   CustCode = model.CustCode,
                                                                   BondRate = "0",
                                                                   Volume = model.Volume
                                                               });

                              if (rateInfo != null)
                              {
                                  rateBasic = rateInfo.BasicRate;
                                  irrCompany = rateInfo.CompanyRate;
                                  irrSale = model.Rate - rateBasic - irrCompany;
                              }


                              var sqlInsert = @"insert into BondRequestOrder 
		                (Side, AccountNo, AccountName, ContractNo, TradeDate, 
                        CouponDate, BondCode, Volume, Rate, 
                        PaymentMethod, BankAccountName, CustBankID,
                        NavConfigID, SaleID, ReferID,
                        Description, Status, Note,
                        CreatedBy, CreatedDate, NavContractActive,
                        NavContractProcess, NavContractQueue, TotalNav,
                        Amount, RateBasic, IRRCompany, IRRSale)
                    values
                        (@Side, @AccountNo, @AccountName, @ContractNo, @TradeDate,
                            @CouponDate, @BondCode, @Volume, @Rate,
                            @PaymentMethod, @BankAccountName, @CustBankID, 
                            @NavConfigID, @SaleID, @ReferID, 
                            @Description, @Status, @Note, 
                            @CreatedBy, @CreatedDate, @NavContractActive,
                            @NavContractProcess, @NavContractQueue, @TotalNav,
                            @Amount, @RateBasic, @IRRCompany, @IRRSale)";

                              var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy",
                                  CultureInfo.InvariantCulture);
                              var saleId = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";

                              var affectedRow = conn.Execute(sqlInsert, new
                                                                        {
                                                                            Side = "B",
                                                                            AccountNo = model.CustCode + "1",
                                                                            AccountName = accountName,
                                                                            ContractNo = "",
                                                                            TradeDate = tradeDate,
                                                                            CouponDate = DateTime.ParseExact(model.CouponDate, "dd/MM/yyyy",
                                                                            CultureInfo.InvariantCulture),
                                                                            BondCode = model.BondCode,
                                                                            Volume = (long) model.Volume,
                                                                            Rate = model.Rate,
                                                                            PaymentMethod = model.PaymentMethod,
                                                                            BankAccountName = bankAccountName,
                                                                            CustBankID = model.CustBankID,
                                                                            NavConfigID = model.NavConfigID,
                                                                            SaleID = saleId,
                                                                            ReferID = model.ReferID,
                                                                            Description = model.Description,
                                                                            Status = 0,
                                                                            Note = string.Empty,
                                                                            CreatedBy = model.UserName,
                                                                            CreatedDate = DateTime.Now,
                                                                            NavContractActive = navContractActive,
                                                                            NavContractProcess = navContractProcess,
                                                                            NavContractQueue = navContractQueue,
                                                                            TotalNav = totalNav,
                                                                            Amount = amount,
                                                                            RateBasic = rateBasic,
                                                                            IRRCompany = irrCompany,
                                                                            IRRSale = irrSale

                                                                        });
                              return affectedRow;
                          });
            }
            catch (Exception ex)
            {
                log.Error("InsertBuyOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> UpdateBuyOrderRequest(BondBuyOrderUpdateRequest model)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var curStatus = await conn.QueryFirstOrDefaultAsync<int>("select Status from BondRequestOrder where RequestOrderID = @ID",
                                                        new { ID = model.RequestOrderID });

                            if (curStatus == 0 || curStatus == -1)
                            {
                                var accountName = "";
                                var bankAccountName = "";

                                var sqlSelect = "select AccountName from BondRequestOrder where RequestOrderID = @ID";
                                var custName = conn.Query<string>(sqlSelect, new { ID = model.RequestOrderID }).FirstOrDefault();
                                if (!string.IsNullOrEmpty(custName))
                                    accountName = custName;

                                sqlSelect = @"select CustBankID, CustCode, BankName, BankAccount, BankAccountName, SubBranchName 
                                from CustBank c left join BankInfo b on c.BankID = b.BankID
                                where CustBankID = @CustBankID and c.Status = 1";

                                var bankInfo = await conn.QueryFirstOrDefaultAsync<BondCustBankData>(sqlSelect,
                                    new { CustBankID = model.CustBankID });
                                if (bankInfo != null)
                                {
                                    bankAccountName = bankInfo.BankName + " - " + bankInfo.SubBranchName + " | "
                                        + bankInfo.BankAccountName + " - " + bankInfo.BankAccount;
                                }

                        // Hủy lệnh cũ và sinh 1 lệnh mới
                        var sqlUpdate = @"update BondRequestOrder set Status = 99 where RequestOrderID = @ID";
                                var affectedRow = conn.Execute(sqlUpdate, new { ID = model.RequestOrderID });


                                var navContractActive = 0m;
                                var navContractProcess = 0m;
                                var navContractQueue = 0m;
                                var totalNav = 0m;
                                var rateBasic = 0m;
                                var irrCompany = 0m;
                                var irrSale = 0m;
                                var amount = model.Volume * model.BuyPrice;

                        // Get thông tin Nav hiện tại
                        var navInfo = conn.QueryFirstOrDefault<BondCustNavData>("TVSI_sGET_CURRENT_NAV_ACCOUNT_INFO",
                                    new { accountNo = model.CustCode + "1" }, commandType: CommandType.StoredProcedure);

                                if (navInfo != null)
                                {
                                    navContractActive = navInfo.NavContractActive;
                                    navContractProcess = navInfo.NavContractProcess;
                                    navContractQueue = navInfo.NavContractQueue + (amount - navInfo.NavContractQueue);
                                    totalNav = navInfo.TotalNav + (amount - navInfo.NavContractQueue);
                                }

                                var rateInfo = await GetBondRate(new BondRateRequest
                                {
                                    BondCode = model.BondCode,
                                    TradeDate = model.TradeDate,
                                    CustCode = model.CustCode,
                                    BondRate = "0",
                                    Volume = model.Volume
                                });

                                if (rateInfo != null)
                                {
                                    rateBasic = rateInfo.BasicRate;
                                    irrCompany = rateInfo.CompanyRate;
                                    irrSale = model.Rate - rateBasic - irrCompany;
                                }

                                var tradeDate = DateTime.ParseExact(model.TradeDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                var saleId = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";

                                var sqlInsert = @"insert into BondRequestOrder 
		                        (Side, AccountNo, AccountName, ContractNo, TradeDate, 
                                CouponDate, BondCode, Volume, Rate, 
                                PaymentMethod, BankAccountName, CustBankID,
                                NavConfigID, SaleID, ReferID,
                                Description, Status, Note,
                                CreatedBy, CreatedDate, NavContractActive,
                                NavContractProcess, NavContractQueue, TotalNav,
                                Amount, RateBasic, IRRCompany, IRRSale)
                            values
                                (@Side, @AccountNo, @AccountName, @ContractNo, @TradeDate,
                                    @CouponDate, @BondCode, @Volume, @Rate,
                                    @PaymentMethod, @BankAccountName, @CustBankID, 
                                    @NavConfigID, @SaleID, @ReferID, 
                                    @Description, @Status, @Note, 
                                    @CreatedBy, @CreatedDate, @NavContractActive,
                                    @NavContractProcess, @NavContractQueue, @TotalNav,
                                    @Amount, @RateBasic, @IRRCompany, @IRRSale)";

                                var affectedRow2 = conn.Execute(sqlInsert, new
                                {
                                    Side = "B",
                                    AccountNo = model.CustCode + "1",
                                    AccountName = accountName,
                                    ContractNo = "",
                                    TradeDate = tradeDate,
                                    CouponDate = model.CouponDate,
                                    BondCode = model.BondCode,
                                    Volume = (long)model.Volume,
                                    Rate = model.Rate,
                                    PaymentMethod = model.PaymentMethod,
                                    BankAccountName = bankAccountName,
                                    CustBankID = model.CustBankID,
                                    NavConfigID = model.NavConfigID,
                                    SaleID = saleId,
                                    ReferID = model.ReferID,
                                    Description = model.Description,
                                    Status = 0,
                                    Note = string.Empty,
                                    CreatedBy = model.UserName,
                                    CreatedDate = DateTime.Now,
                                    NavContractActive = navContractActive,
                                    NavContractProcess = navContractProcess,
                                    NavContractQueue = navContractQueue,
                                    TotalNav = totalNav,
                                    Amount = amount,
                                    RateBasic = rateBasic,
                                    IRRCompany = irrCompany,
                                    IRRSale = irrSale

                                });

                                return 1;

                            }

                            return 0;
                        });
            }
            catch (Exception ex)
            {
                log.Error("UpdateBuyOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        #endregion

        #region Bán Trái phiếu

        public async Task<IEnumerable<BondSellContractTempListResult>> GetSellContractTempList(
            BondSellContractTempListRequest model)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var dtxFromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            var dtxToDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                            var saleID = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";

                            var sql = @"select ContractTempID, c.TradeDate, c.ContractNo, c.AccountNo, cu.CustName, c.BondCode, c.Volume, c.Amount, c.SaleID 
	                        from ContractTemp c
                            join AccountPosition ap on ap.ContractNo = c.ContractNo
		                    left join BondRequestOrder b on c.ContractNo = b.ContractNo and b.status not in (-1, 2, 99) and (b.side = 'S' or b.side = 'R')
	                        inner join AccountInfo a on c.AccountNo = a.AccountNo
	                        inner join CustInfo cu on a.CustID = cu.CustID
	                    where c.Status in (0,1) and b.RequestOrderID is null and ap.Volume > 0
		                    and c.TradeDate >= @fromDate and c.TradeDate <= @toDate and c.SaleID = @saleID ";

                            var condition = "";

                            var searchKey = model.SearchKey;
                            if (!string.IsNullOrEmpty(searchKey))
                            {
                                condition += " and (c.ContractNo like '%" + searchKey + "%' or c.AccountNo like '%" + searchKey + "%')";
                            }

                            condition += " order by C.TradeDate asc";
                            sql += condition;

                            var start = (model.PageIndex - 1) * model.PageSize;
                            sql += " OFFSET " + start + " ROWS FETCH NEXT " + model.PageSize + " ROWS ONLY";

                            var data = await conn.QueryAsync<BondSellContractTempListResult>(sql,
                                new { fromDate = dtxFromDate, toDate = dtxToDate, saleID = saleID });

                            return data;
                        });
            }
            catch (Exception ex)
            {
                log.Error("GetSellContractTempList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<BondSellContractTempDetailResult> GetSellContractTempDetail(long contractTempID, string saleID)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var sql = @"select ContractTempID, TradeDate, ContractNo, c.AccountNo, cu.CustName, BondCode, Volume, Amount, 
                                c.SaleID, c.ReferID, c.PaymentMethod, c.Tax, c.CustBankID, cu.CustType 
	                        from ContractTemp c
	                            inner join AccountInfo a on c.AccountNo = a.AccountNo
	                            inner join CustInfo cu on a.CustID = cu.CustID
	                            where c.Status in (0,1) and c.SaleID = @SaleID and ContractTempID=@ID";

                            var data = await conn.QueryFirstOrDefaultAsync<BondSellContractTempDetailResult>(sql, new { SaleID = saleID, ID = contractTempID });

                            if (data != null && data.PaymentMethod == 1 && data.CustBankID.HasValue)
                            {
                                var sql2 = @"select BankName, SubBranchName, BankAccountName, 
                                            BankAccount from CustBank c
                                            left join BankInfo b on c.BankID = b.BankID where CustBankID = @ID and c.Status = 1";
                                var bankData = await conn.QueryFirstOrDefaultAsync<BankInfo>(sql2, new { ID = @data.CustBankID.Value });
                                if (bankData != null)
                                {
                                    data.BankAccountInfo = bankData.BankName + " - "
                                                           + bankData.SubBranchName + " | " + bankData.BankAccountName + " - " +
                                                           bankData.BankAccount;
                                }

                            }

                            if (data != null)
                            {
                        // Get thông tin Nav hiện tại
                        var navInfo = await conn.QueryFirstOrDefaultAsync<BondCustNavData>("TVSI_sGET_CURRENT_NAV_ACCOUNT_INFO",
                                    new { accountNo = data.AccountNo }, commandType: CommandType.StoredProcedure);

                                if (navInfo != null)
                                {
                                    data.CurNav = navInfo.TotalNav;
                                }
                            }

                            return data;
                        });
            }
            catch (Exception ex)
            {
                log.Error("GetSellContractTempDetail:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> InsertSellOrderRequest(BondSellOrderRequest model)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var saleID = model.UserName.Length > 4 ? model.UserName.Substring(0, 4) : "";
                            var sql = @"select c.AccountNo, cu.CustName, c.ContractNo, c.TradeDate, c.DealDate,
	                                c.BondCode, c.Volume, c.PaymentMethod, c.CustBankID, c.SaleID, c.ReferID
	                                from ContractTemp c
	                                inner join AccountInfo a on c.AccountNo = a.AccountNo
	                                inner join CustInfo cu on a.CustID = cu.CustID
                                where ContractTempID = @ID and Side = 'S' and c.SaleID = @SaleID and c.Status in (0,1)";

                            var contractInfo = await conn.QueryFirstOrDefaultAsync<BondContractTempInfo>(sql,
                                new { ID = model.ContractTempID, SaleID = saleID });

                            if (contractInfo != null)
                            {
                                var sqlInsert = @"insert into BondRequestOrder 
		                            (Side, AccountNo, AccountName, ContractNo, TradeDate, 
                                    CouponDate, BondCode, Volume, Rate, 
                                    PaymentMethod, BankAccountName, CustBankID,
                                    NavConfigID, SaleID, ReferID,
                                    Description, Status, Note,
                                    CreatedBy, CreatedDate)
                                values
                                    (@Side, @AccountNo, @AccountName, @ContractNo, @TradeDate,
                                     @CouponDate, @BondCode, @Volume, @Rate,
                                     @PaymentMethod, @BankAccountName, @CustBankID, 
                                     @NavConfigID, @SaleID, @ReferID, 
                                     @Description, @Status, @Note, 
                                     @CreatedBy, @CreatedDate)";

                                var tradeDate = contractInfo.TradeDate;
                                var volume = contractInfo.Volume;

                                if (!string.IsNullOrEmpty(model.SellDate))
                                {
                                    tradeDate = DateTime.ParseExact(model.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                    volume = model.SellVolume;
                                }

                                var affectedRow = conn.Execute(sqlInsert, new
                                {
                                    Side = model.Side,
                                    AccountNo = contractInfo.AccountNo,
                                    AccountName = contractInfo.CustName,
                                    ContractNo = contractInfo.ContractNo,
                                    TradeDate = tradeDate,
                                    CouponDate = contractInfo.DealDate,
                                    BondCode = contractInfo.BondCode,
                                    Volume = (long)volume,
                                    Rate = 0,
                                    PaymentMethod = contractInfo.PaymentMethod,
                                    BankAccountName = "",
                                    CustBankID = contractInfo.CustBankID,
                                    NavConfigID = 0,
                                    SaleID = contractInfo.SaleID,
                                    ReferID = contractInfo.ReferID,
                                    Description = model.Note,
                                    Status = 0,
                                    Note = "",
                                    CreatedBy = model.UserName,
                                    CreatedDate = DateTime.Now
                                });
                                return affectedRow;
                            }

                            return 0;
                        });
            }
            catch (Exception ex)
            {
                log.Error("InsertSellOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<int> UpdateSellOrderRequest(BondSellOrderUpdateRequest model)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var curStatus = await conn.QueryFirstOrDefaultAsync<int>("select Status from BondRequestOrder where RequestOrderID = @ID",
                                                        new { ID = model.RequestOrderID });

                    // Trạng thái = chờ xử lý mới được update
                    if (curStatus == 0 || curStatus == -1)
                            {
                                using (var transaction = conn.BeginTransaction())
                                {
                            // Hủy lệnh cũ và sinh 1 lệnh mới
                            var sqlUpdate = @"update BondRequestOrder set Status = 99 where RequestOrderID = @ID";

                                    var sqlInsert = @"insert into BondRequestOrder 
	                                    (Side, AccountNo, AccountName, ContractNo, TradeDate, 
	                                    CouponDate, BondCode, Volume, Rate, PaymentMethod, BankAccountName, 
                                        CustBankID, NavConfigID, SaleID, ReferID, Description, Status, Note,
	                                    CreatedBy, CreatedDate)
	                                        select @Side, AccountNo, AccountName, ContractNo, @TradeDate, 
	                                            CouponDate, BondCode, @Volume, Rate, PaymentMethod, BankAccountName, 
                                                CustBankID, NavConfigID, SaleID, ReferID, @Description, @Status, Note,
	                                            @CreatedBy, @CreatedDate from BondRequestOrder where RequestOrderID = @RequestOrderID";

                                    var tradeDate = DateTime.ParseExact(model.SellDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                                    var affectedRow = conn.Execute(sqlUpdate, new { ID = model.RequestOrderID }, transaction);
                                    var affectedRow2 = conn.Execute(sqlInsert, new
                                    {
                                        Side = model.Side,
                                        TradeDate = tradeDate,
                                        Volume = (long)model.SellVolume,
                                        Description = model.Note,
                                        Status = 0,
                                        CreatedBy = model.UserName,
                                        CreatedDate = DateTime.Now,
                                        RequestOrderID = model.RequestOrderID
                                    }, transaction);

                                    transaction.Commit();
                                    return 1;
                                }
                            }

                            return 0;
                        });
            }
            catch (Exception ex)
            {
                log.Error("UpdateSellOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        public async Task<SellPriceResult> GetSellPrice(string contractNo, DateTime sellDate, int volume)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var sql = @"select AccountNo, ContractNo, BondCode, TradeDate, MaturityDate, CouponDate, Rate, [RollBack], DebitType 
                                    from ContractHist where ContractNo = @contractNo";
                            var contractItem = await conn.QueryFirstOrDefaultAsync<BondSellContractInfo>(sql, new { contractNo = contractNo });

                            if (contractItem != null)
                            {
                                // Truong hop ban dung han
                                if (contractItem.MaturityDate.Date == sellDate.Date.Date)
                                {
                                    var data = await conn.QueryAsync<BondPriceCalc>("TVSI_sCALC_BOND_BUY_BY_DATE",
                                        new
                                        {
                                            ContractNo = contractNo,
                                            SellDate = sellDate,
                                            IsHistory = true
                                        }, commandType: CommandType.StoredProcedure);

                                    var sellPriceItem = data.FirstOrDefault(x => x.RowCode == "SellPrice");
                                    if (sellPriceItem != null)
                                        return new SellPriceResult
                                        {
                                            SellPrice = sellPriceItem.Income,
                                            SellAmount = 0.99m * sellPriceItem.Income
                                        };
                                }
                                // Truong hop ban truoc han
                                else
                                {
                                    // Tim SellRate
                                    var custCode = contractItem.AccountNo.Substring(0, 6);
                                    var custType = await conn.QueryFirstOrDefaultAsync<int?>("select CustType from CustInfo where CustCode = @CustCode",
                                        new { CustCode = custCode });
                                    custType = custType ?? 1;

                                    var bondRate = await conn.QueryFirstOrDefaultAsync<decimal>("TVSI_sCALC_BOND_GET_RATE_V1",
                                        new
                                        {
                                            BondCode = contractItem.BondCode,
                                            TradeDate = contractItem.RollBack > 0 ? contractItem.CouponDate : contractItem.TradeDate,
                                            CustType = custType,
                                            IsMaturity = false,
                                            SellDate = sellDate
                                        }, commandType: CommandType.StoredProcedure);

                                    // Tim SellPrice
                                    var buyDate = contractItem.TradeDate;

                                    if (contractItem.RollBack > 0)
                                        buyDate = contractItem.CouponDate;

                                    var data = await conn.QueryAsync<BondPriceCalc>("TVSI_sCALC_BOND_SELL_BY_DATE_V1",
                                        new
                                        {
                                            BondCode = contractItem.BondCode,
                                            CustType = custType,
                                            DebitType = contractItem.DebitType,
                                            RateReal = bondRate,
                                            BuyCouponDate = contractItem.CouponDate,
                                            BuyDate = buyDate,
                                            SellDate = sellDate,
                                            BuyRate = contractItem.Rate,
                                            MaturityDateReal = sellDate
                                        }, commandType: CommandType.StoredProcedure);

                                    var sellPriceItem = data.FirstOrDefault(x => x.RowCode == "SellPrice");
                                    if (sellPriceItem != null)
                                        return new SellPriceResult
                                               {
                                                   SellPrice = sellPriceItem.Income,
                                                   SellAmount = 0.99m * sellPriceItem.Income * volume
                                               };
                                }
                            }

                            return new SellPriceResult
                            {
                                SellPrice = 0,
                                SellAmount = 0
                            };
                        });
        }
            catch (Exception ex)
            {
                log.Error("UpdateSellOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
}
        #endregion

        #region Xóa hợp đồng

        public async Task<int> DeleteOrderRequest(long requestOrderID, string userName)
        {
            try
            {
                return await WithTbmDbConnection(
                        async conn =>
                        {
                            var count = await conn.QueryFirstOrDefaultAsync<int>(@"select count(RequestOrderID) from BondRequestOrder where RequestOrderID = @ID 
                        and CreatedBy = @CreatedBy and Status = 0", new { ID = requestOrderID, CreatedBy = userName });

                            if (count > 0)
                            {
                                var sqlUpdate =
                                    @"update BondRequestOrder set Status = 99 where RequestOrderID = @ID and CreatedBy = @CreatedBy";
                                var affectedRow = conn.Execute(sqlUpdate, new { ID = requestOrderID, CreatedBy = userName });
                                return affectedRow;
                            }
                            return 0;
                        });
            }
            catch (Exception ex)
            {
                log.Error("DeleteOrderRequest:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        #endregion

        #region "Xử lý chia kho nguồn"
        /*private int UpdateBondSourceShareCreateNew(BondBuyOrderRequest model, SqlConnection conn, SqlTransaction transaction)
        {
            // 20211129 Xử lý kho nguồn
            // Lấy mã chi nhánh của Sale
            var sqlBranch = @"SELECT branchCode FROM [User] A 
                                INNER JOIN SaleBranch B ON A.BranchID = B.BranchID
                                WHERE saleID = @SaleID";

            var branchCode = conn.QueryFirstOrDefault<string>(sqlBranch, new { SaleID = model.SaleID }, transaction);
            if (!string.IsNullOrEmpty(branchCode))
            {
                // Lấy Team ID
                sqlBranch = @"select TeamID from BondSourceTeam where BranchIds like @BranchCode";
                var teamId = conn.QueryFirstOrDefault<int?>(sqlBranch, new { BranchCode = "%" + branchCode + "%" }, transaction);

                if (teamId != null)
                {
                    var sqlBondInfo = "SELECT IssuerCode FROM BondInfo where BondCode = @BondCode";
                    var issuerCode = conn.QueryFirstOrDefault<string>(sqlBondInfo, new { BondCode = model.BondCode }, transaction);

                    var sqlBondSourceShare = @"SELECT * FROM BondSourceShare 
                                        WHERE TeamID = @TeamID AND IssuerCode=@IssuerCode AND TradeDate=@TradeDate AND Status = 1";

                    var bondSourceShare = conn.QueryFirstOrDefault<BondSourceShareModel>(sqlBondSourceShare,
                        new { TeamID = teamId, IssuerCode = issuerCode, TradeDate = DateTime.Now.Date }, transaction);

                    if (bondSourceShare != null)
                    {
                        var newVolume = bondSourceShare.Volume - model.Volume;

                        var sqlUpdate = @"UPDATE BondSourceShare SET Volume = @NewVolume, 
                            UpdatedBy = @UpdatedBy, @UpdatedDate=@UpdatedDate 
                            OUTPUT inserted.RowVersion
                            WHERE SharedID = @ID AND RowVersion = @RowVer";

                        var rowVersion = conn.ExecuteScalar(sqlUpdate, new
                        {
                            NewVolume = newVolume,
                            UpdatedBy = model.SaleID,
                            UpdatedDate = DateTime.Now,
                            ID = bondSourceShare.SharedID,
                            RowVer = bondSourceShare.RowVersion
                        }, transaction);

                        if (rowVersion != null)
                        {
                            // Insert Log
                            var sqlInsert = @"INSERT INTO dbo.ChangeBondSourceLog 
                            (
                                SharedId, TradeDate, IssuerCode, FromTeamId, FromContractNo, 
                                TeamId, StartVolume, ChangeVolume, Description, Status, CreatedDate, CreatedBy
                            ) 
                            VALUES (
                                @SharedId, @TradeDate, @IssuerCode, @FromTeamId, @FromContractNo, 
                                @TeamId, @StartVolume, @ChangeVolume, @Description, @Status, @CreatedDate, @CreatedBy
                            )";

                            conn.Execute(sqlInsert, new
                            {
                                SharedId = bondSourceShare.SharedID,
                                TradeDate = DateTime.Now.Date,
                                IssuerCode = issuerCode,
                                FromTeamId = teamId,
                                FromContractNo = "",
                                TeamId = teamId,
                                StartVolume = bondSourceShare.Volume,
                                ChangeVolume = model.Volume,
                                Description = "CRM: TK " + model.CustCode + " mua " + model.Volume + " TP " + model.BondCode,
                                Status = 1,
                                CreatedBy = model.SaleID,
                                CreatedDate = DateTime.Now,
                            }, transaction);
                        }

                        if (rowVersion != null)
                            return 1;
                    }
                }
            }

            return 0;
        }

        private int UpdateBondSourceShareUpdate(BondBuyOrderUpdateRequest model, SqlConnection conn, SqlTransaction transaction, int difVolume)
        {
            // 20211129 Xử lý kho nguồn
            // Lấy mã chi nhánh của Sale
            var sqlBranch = @"SELECT branchCode FROM [User] A 
                                INNER JOIN SaleBranch B ON A.BranchID = B.BranchID
                                WHERE saleID = @SaleID";

            var branchCode = conn.QueryFirstOrDefault<string>(sqlBranch, new { SaleID = model.SaleID }, transaction);
            if (!string.IsNullOrEmpty(branchCode))
            {
                // Lấy Team ID
                sqlBranch = @"select TeamID from BondSourceTeam where BranchIds like @BranchCode";
                var teamId = conn.QueryFirstOrDefault<int?>(sqlBranch, new { BranchCode = "%" + branchCode + "%" }, transaction);

                if (teamId != null)
                {
                    var sqlBondInfo = "SELECT IssuerCode FROM BondInfo where BondCode = @BondCode";
                    var issuerCode = conn.QueryFirstOrDefault<string>(sqlBondInfo, new { BondCode = model.BondCode }, transaction);

                    var sqlBondSourceShare = @"SELECT * FROM BondSourceShare 
                                        WHERE TeamID = @TeamID AND IssuerCode=@IssuerCode AND TradeDate=@TradeDate AND Status = 1";

                    var bondSourceShare = conn.QueryFirstOrDefault<BondSourceShareModel>(sqlBondSourceShare,
                        new { TeamID = teamId, IssuerCode = issuerCode, TradeDate = DateTime.Now.Date }, transaction);

                    if (bondSourceShare != null)
                    {
                        var newVolume = bondSourceShare.Volume - difVolume;

                        var sqlUpdate = @"UPDATE BondSourceShare SET Volume = @NewVolume, 
                            UpdatedBy = @UpdatedBy, @UpdatedDate=@UpdatedDate 
                            OUTPUT inserted.RowVersion
                            WHERE SharedID = @ID AND RowVersion = @RowVer";

                        var rowVersion = conn.ExecuteScalar(sqlUpdate, new
                        {
                            NewVolume = newVolume,
                            UpdatedBy = model.SaleID,
                            UpdatedDate = DateTime.Now,
                            ID = bondSourceShare.SharedID,
                            RowVer = bondSourceShare.RowVersion
                        }, transaction);

                        if (rowVersion != null)
                        {
                            // Insert Log
                            var sqlInsert = @"INSERT INTO dbo.ChangeBondSourceLog 
                            (
                                SharedId, TradeDate, IssuerCode, FromTeamId, FromContractNo, 
                                TeamId, StartVolume, ChangeVolume, Description, Status, CreatedDate, CreatedBy
                            ) 
                            VALUES (
                                @SharedId, @TradeDate, @IssuerCode, @FromTeamId, @FromContractNo, 
                                @TeamId, @StartVolume, @ChangeVolume, @Description, @Status, @CreatedDate, @CreatedBy
                            )";

                            conn.Execute(sqlInsert, new
                            {
                                SharedId = bondSourceShare.SharedID,
                                TradeDate = DateTime.Now.Date,
                                IssuerCode = issuerCode,
                                FromTeamId = teamId,
                                FromContractNo = "",
                                TeamId = teamId,
                                StartVolume = bondSourceShare.Volume,
                                ChangeVolume = model.Volume,
                                Description = "CRM: Cập nhật TK " + model.CustCode + " mua "
                                    + model.Volume + " TP " + model.BondCode + "(Thay đổi " + difVolume + ")",
                                Status = 1,
                                CreatedBy = model.SaleID,
                                CreatedDate = DateTime.Now,
                            }, transaction);
                        }

                        if (rowVersion != null)
                            return 1;
                    }
                }
            }

            return 0;
        }

        private int UpdateBondSourceShareDelete(SqlConnection conn, SqlTransaction transaction, string userName, long id)
        {
            // 20211129 Xử lý kho nguồn

            // Cap nhat kho nguồn
            var sql = "SELECT * FROM BondRequestOrder where RequestOrderID = @ID";
            var data = conn.QueryFirstOrDefault<BondRequestOrderDetail>(sql, new { ID = id }, transaction);
            if (data != null && data.Side != "B")
                return 0;

            // Lấy mã chi nhánh của Sale
            var sqlBranch = @"SELECT branchCode FROM [User] A 
                                INNER JOIN SaleBranch B ON A.BranchID = B.BranchID
                                WHERE saleID = @SaleID";

            var branchCode = conn.QueryFirstOrDefault<string>(sqlBranch, new { SaleID = data.SaleID }, transaction);
            if (!string.IsNullOrEmpty(branchCode))
            {
                // Lấy Team ID
                sqlBranch = @"select TeamID from BondSourceTeam where BranchIds like @BranchCode";
                var teamId = conn.QueryFirstOrDefault<int?>(sqlBranch, new { BranchCode = "%" + branchCode + "%" }, transaction);

                if (teamId != null)
                {
                    var sqlBondInfo = "SELECT IssuerCode FROM BondInfo where BondCode = @BondCode";
                    var issuerCode = conn.QueryFirstOrDefault<string>(sqlBondInfo, new { BondCode = data.BondCode }, transaction);

                    var sqlBondSourceShare = @"SELECT * FROM BondSourceShare 
                                        WHERE TeamID = @TeamID AND IssuerCode=@IssuerCode AND TradeDate=@TradeDate AND Status = 1";

                    var bondSourceShare = conn.QueryFirstOrDefault<BondSourceShareModel>(sqlBondSourceShare,
                        new { TeamID = teamId, IssuerCode = issuerCode, TradeDate = DateTime.Now.Date }, transaction);

                    if (bondSourceShare != null)
                    {
                        var newVolume = bondSourceShare.Volume + data.Volume;

                        var sqlUpdate = @"UPDATE BondSourceShare SET Volume = @NewVolume, 
                            UpdatedBy = @UpdatedBy, @UpdatedDate=@UpdatedDate 
                            OUTPUT inserted.RowVersion
                            WHERE SharedID = @ID AND RowVersion = @RowVer";

                        var rowVersion = conn.ExecuteScalar(sqlUpdate, new
                        {
                            NewVolume = newVolume,
                            UpdatedBy = userName,
                            UpdatedDate = DateTime.Now,
                            ID = bondSourceShare.SharedID,
                            RowVer = bondSourceShare.RowVersion
                        }, transaction);

                        if (rowVersion != null)
                        {
                            // Insert Log
                            var sqlInsert = @"INSERT INTO dbo.ChangeBondSourceLog 
                            (
                                SharedId, TradeDate, IssuerCode, FromTeamId, FromContractNo, 
                                TeamId, StartVolume, ChangeVolume, Description, Status, CreatedDate, CreatedBy
                            ) 
                            VALUES (
                                @SharedId, @TradeDate, @IssuerCode, @FromTeamId, @FromContractNo, 
                                @TeamId, @StartVolume, @ChangeVolume, @Description, @Status, @CreatedDate, @CreatedBy
                            )";

                            conn.Execute(sqlInsert, new
                            {
                                SharedId = bondSourceShare.SharedID,
                                TradeDate = DateTime.Now.Date,
                                IssuerCode = issuerCode,
                                FromTeamId = teamId,
                                FromContractNo = "",
                                TeamId = teamId,
                                StartVolume = bondSourceShare.Volume,
                                ChangeVolume = data.Volume,
                                Description = "CRM: DELETE lệnh đặt ID=" + data.RequestOrderID + ", TK " + data.AccountNo
                                    + " mua " + data.Volume + " TP " + data.BondCode,
                                Status = 1,
                                CreatedBy = userName,
                                CreatedDate = DateTime.Now,
                            }, transaction);
                        }

                        if (rowVersion != null)
                            return 1;
                    }
                }
            }

            return 0;
        }*/
        #endregion
    }
    
}
