﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using log4net;
using TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Utilities;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class LeadService : BaseService, ILeadService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(LeadService));
        private readonly ILeadCommandText _commandText;
        public LeadService(ILeadCommandText commandText)
        {
            _commandText = commandText;
        }

        /// <summary>
        /// Danh sách khách hàng tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LeadListResult>> GetLeadList(LeadListRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;
            /*var fromDate = DateTime.ParseExact(model.FromDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");
            var toDate = DateTime.ParseExact(model.ToDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd HH:mm:ss");*/
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<LeadListResult>();
                    var sql = _commandText.GetLeadList;
                    /*
                    sql = sql.Replace("{cond1}", !string.IsNullOrEmpty(model.FromDate) ? " and L.CreatedDate >= @fromDate AND L.CreatedDate <= @toDate" : "");
                    */
                    sql = sql.Replace("{cond2}", model.Status > 0 ? " and L.Status = @Status" : "");
                    sql = sql.Replace("{cond3}", !string.IsNullOrEmpty(model.keySearch) ? " and (L.LeadName like @keySearch or L.Mobile like @keySearch )" : "");
                    
                    var result = await conn.QueryAsync<Lead>(sql,
                        new { 
                        Start = start, 
                        PageSize = model.PageSize, 
                        /*fromDate = fromDate, 
                        toDate = toDate, */
                        Status = model.Status, 
                        keySearch = "%"  + model.keySearch + "%",
                        SaleId = model.SaleID,
                        UserName = model.UserName,
                    });
                    foreach (var data in result)
                    {
                        var _leadResult = new LeadListResult
                        {
                            LeadID = data.LeadID,
                            CreatedDate =data.CreatedDate.ToString("dd/MM/yyyy"),
                            LeadName = data.LeadName,
                            Mobile = data.Mobile,
                            Email = data.Email,
                            ProfileType = data.ProfileType,
                            Status = data.Status,
                            SourceName = data.SourceName,
                            AssignUser = data.AssignUser
                        };
                        resultData.Add(_leadResult);
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetLeadList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Lấy danh sách hoạt động của lead
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LeadActivityResult>> GetActivityLeadList(LeadActivityRequest model)
        {
            var start = (model.PageIndex - 1) * model.PageSize;

            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var resultData = new List<LeadActivityResult>();
                    var result = await conn.QueryAsync<Activity>(_commandText.GetActivityLeadInfo,
                        new { Start = start, PageSize = model.PageSize, SaleId = model.UserName, leadId = model.LeadId });
                    
                    foreach (var data in result)
                    {
                        var _custResult = new LeadActivityResult
                        {
                            ActivityId = int.Parse(data.ActivityID.ToString()),
                            ActivityName = data.ActivityName,
                            StartDate = data.StartDate.ToString("dd/MM/yyyy"),
                            EndDate = data.EndDate.ToString("dd/MM/yyyy"),
                            Status = data.Status,
                            Description = data.Description,
                            AssignUser = data.AssignUser
                        };
                        resultData.Add(_custResult);
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityLeadList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Lấy danh sách cơ hội của lead
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<IEnumerable<LeadOpportunityResult>> GetOpportunityLeadList(LeadOpportunityRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    //var resultData = new List<LeadOpportunityResult>();
                    var result = await conn.QueryAsync<LeadOpportunityResult>(_commandText.GetOpportunityLeadInfo,
                        new {SaleId = model.UserName, leadId = model.LeadId});
                    //foreach (var data in result)
                    //{
                    //    var _custResult = new LeadOpportunityResult
                    //    {
                    //        OpportunityId = int.Parse(data.OpportunityID.ToString()),
                    //        OpportunityName = data.OpportunityName,
                    //        ExpectedCloseDate = data.ExpectedCloseDate.ToString("dd/MM/yyyy"),
                    //        ExpectedRevenue = data.ExpectedRevenue,
                    //        Status = data.Status,
                    //        AssignUser = data.AssignUser
                    //    };
                    //    resultData.Add(_custResult);
                    //}
                    return result;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetOpportunityLeadList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        /// <summary>
        /// Thêm mới thông tin khách hàng tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> AddNewLeadInfo(LeadAddRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                        var retData = await conn.ExecuteScalarAsync(_commandText.PostLeadAddInfo, new
                        {
                            leadName = model.LeadName,
                            mobile = model.LeadMobile,
                            email = model.LeadEmail,
                            address = model.LeadAddress,
                            profileType = model.LeadProfileType,
                            status = 1,
                            assignUser = model.AssignUser,
                            description = model.LeadDescription,
                            leadSourceID = model.LeadSourceID,
                            createdBy = model.AssignUser,
                            createdDate = DateTime.Now
                        });
                    if (retData != null)
                    {
                        var leadID = Convert.ToInt32(retData);
                        var arrListExtend = model.LeadInfoExtend.Split('|');
                        foreach (var extend_item in arrListExtend)
                        {
                            if (!string.IsNullOrEmpty(extend_item))
                            {
                                await conn.ExecuteAsync(_commandText.PostLeadExtInfo, new
                                {
                                    category = extend_item,
                                    leadId = leadID,
                                    createdBy = model.AssignUser,
                                    createdDate = DateTime.Now
                                });
                            }
                        }
                    }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("AddNewLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Lấy danh sách thuộc tính mở rộng khách hàng
        /// </summary>
        /// <returns></returns>

        public async Task<IEnumerable<ExLeadInfoResult>> GetExLeadInfoList()
        {
            try
            {

                return await WithCrmDbConnection(async conn =>
                {
                    var jobInfo = await conn.QueryAsync<JobInfo>(_commandText.GetExLeadInfo,
                        new { ExType = LeadConst.EXLEAD_TYPE_1 });
                    var finAbilityInfo = await conn.QueryAsync<FinAbilityInfo>(_commandText.GetExLeadInfo,
                        new { ExType = LeadConst.EXLEAD_TYPE_2 });
                    var riskInfo = await conn.QueryAsync<RiskInfo>(_commandText.GetExLeadInfo,
                        new { ExType = LeadConst.EXLEAD_TYPE_3 });
                    var knowledExpInfo = await conn.QueryAsync<KnowledExpInfo>(_commandText.GetExLeadInfo,
                        new { ExType = LeadConst.EXLEAD_TYPE_4 });
                    var opInvestInfo = await conn.QueryAsync<OpInvestInfo>(_commandText.GetExLeadInfo,
                        new { ExType = LeadConst.EXLEAD_TYPE_5 });
                    
                    var lstReturn = new List<ExLeadInfoResult>
                    {
                        new ExLeadInfoResult
                        {
                            JobList = jobInfo.ToList(),
                            FinAbilityList = finAbilityInfo.ToList(),
                            RiskList = riskInfo.ToList(),
                            KnowledExpList = knowledExpInfo.ToList(),
                            OpInvestList = opInvestInfo.ToList()
                        }
                    };
                    return lstReturn;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetExLeadInfoList:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }


        /// <summary>
        /// Thông tin chi tiết khách hàng tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<LeadInfoResult> GetLeadInfo(LeadInfoRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var result = new LeadInfoResult();
                    result.LeadInfoContact = new LeadInfo();
                    result.LeadInfoContactCategory = new List<GroupCategoryDetail>();
                    var data = await conn.QueryFirstOrDefaultAsync<LeadInfo> (_commandText.GetLeadInfo,
                        new { leadId = model.LeadId });
                    var investment = await WithCrmDbConnection(async crm => await crm.QueryAsync<LeadInvestmentInfo>
                    (_commandText.GetExLeadDetails, new
                    {
                        @leadId = model.LeadId
                    }));
                    result.LeadInfoContact = data;
                    var contactInfo = new GroupCategoryDetail();
                    contactInfo.ItemGroupType = "ContactInfo";
                    contactInfo.ItemGroupTypeName = "Thông tin liên hệ";
                    contactInfo.DataItem = new List<LeadInvestmentInfo>();

                    var InvestmentInfo = new GroupCategoryDetail();
                    InvestmentInfo.ItemGroupType = "InvestmentInfo";
                    InvestmentInfo.ItemGroupTypeName = "Thông tin đầu tư";
                    InvestmentInfo.DataItem = new List<LeadInvestmentInfo>();

                    var CustCareInfo = new GroupCategoryDetail();
                    CustCareInfo.ItemGroupType = "CustCareInfo";
                    CustCareInfo.ItemGroupTypeName = "Thông tin CSKH";
                    CustCareInfo.DataItem = new List<LeadInvestmentInfo>();

                    var ExtendInfo = new GroupCategoryDetail();
                    ExtendInfo.ItemGroupType = "ExtendInfo";
                    ExtendInfo.ItemGroupTypeName = "Thông tin mở rộng";
                    ExtendInfo.DataItem = new List<LeadInvestmentInfo>();

                    foreach (var invest in investment)
                    {
                        if (invest.ItemTypeID == 1 || invest.ItemTypeID == 6)
                        {
                            contactInfo.DataItem.Add(invest);
                        }
                        else if (invest.ItemTypeID == 2 || invest.ItemTypeID == 7 || invest.ItemTypeID == 8 || invest.ItemTypeID == 4 || invest.ItemTypeID == 3 || invest.ItemTypeID == 5)
                        {
                            InvestmentInfo.DataItem.Add(invest);
                        }
                        else if (invest.ItemTypeID == 9 || invest.ItemTypeID == 11 || invest.ItemTypeID == 10 || invest.ItemTypeID == 12)
                        {
                            CustCareInfo.DataItem.Add(invest);
                        }
                        else
                        {
                            ExtendInfo.DataItem.Add(invest);
                        }
                    }

                    result.LeadInfoContactCategory.Add(contactInfo);
                    result.LeadInfoContactCategory.Add(InvestmentInfo);
                    result.LeadInfoContactCategory.Add(CustCareInfo);
                    result.LeadInfoContactCategory.Add(ExtendInfo);
                    return result;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Danh sách khách hàng select
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<LeadListUserResponse> GetLeadDropBox(LeadListUserRequest model)
        {
            try
            {
                var start = (model.PageIndex - 1) * model.PageSize;
                return await WithCrmDbConnection(async conn =>
                {
                    LeadListUserResponse dataResponse = new LeadListUserResponse();
                    dataResponse.totalItem = await conn.QueryFirstOrDefaultAsync<int>(_commandText.GetLeadsDropboxCount, new { leadName = "%" + model.LeadName + "%", assignUser = model.UserName });
                    dataResponse.Items = await conn.QueryAsync<LeadListUserRessult>(_commandText.GetLeadsDropbox, new { leadName = "%" + model.LeadName + "%", assignUser = model.UserName, Start = start, PageSize = model.PageSize });
                    return dataResponse;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// Update thông tin KH tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public async Task<bool> UpdateLeadInfo(LeadEditRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    var retData = await conn.ExecuteAsync(_commandText.UpdateLeadInfo, new
                    {
                        leadName = model.LeadName,
                        mobile = model.LeadMobile,
                        email = model.LeadEmail,
                        address = model.LeadAddress,
                        profileType = model.LeadProfileType,
                        assignUser = model.AssignUser,
                        description = model.LeadDescription,
                        leadSourceID = model.LeadSourceID,
                        updatedBy = model.AssignUser,
                        updatedDate = DateTime.Now,
                        leadId = model.LeadId
                    });

                    await conn.ExecuteAsync(_commandText.DeleteExLeadInfo, new
                    {
                        leadId = model.LeadId
                    });

                    var arrListExtend = model.LeadInfoExtend.Split('|');
                        foreach (var exten_item in arrListExtend)
                        {
                            if (!string.IsNullOrEmpty(exten_item))
                            {
                                await conn.ExecuteAsync(_commandText.PostLeadExtInfo, new
                                {
                                    category = exten_item,
                                    leadId = model.LeadId,
                                    createdBy = model.AssignUser,
                                    createdDate = DateTime.Now
                                });
                            }
                        }
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("UpdateLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        
        /// <summary>
        /// Xóa thông tin khách hàng tiềm năng
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        public async Task<bool> DeleteLeadInfo(LeadDeleteRequest model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                     await conn.ExecuteAsync(_commandText.DeleteLeadInfo, new
                    {
                        status = LeadConst.LEAD_STATUS_99,
                        updatedBy = model.AssignUser,
                        updatedDate = DateTime.Now,
                        leadId = model.LeadId
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("DeleteLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }

        /// <summary>
        /// đổi từ lead sang customer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<bool> SwitchLeadToCustomer(LeadUpdateAccount model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    int countId = await conn.QueryFirstOrDefaultAsync<int>(_commandText.CheckCustInfo, new { CustCode = model.CustCode });
                    if (countId > 0)
                    {
                        return false;
                    }
                    var attributeLead = await conn.QueryAsync<LeadAttrinbuteViewModel>(_commandText.GetListAttributeLead, new { leadId = model.LeadId });
                    foreach(var atribute in attributeLead)
                    {
                        await conn.ExecuteAsync(_commandText.CreateAttributeLeadToCustomer, new
                        {
                            AttributeID = atribute.AttributeID,
                            CustCode = model.CustCode,
                            Value = atribute.Value,
                            Priority = atribute.Priority,
                            createdBy = model.UserName,
                            createdDate = DateTime.Now
                        });
                    }
                    await conn.ExecuteAsync(_commandText.UpdateCommentLeadToCustomer, new
                    {
                        CustCode = model.CustCode,
                        leadID = model.LeadId
                    });
                    await conn.ExecuteAsync(_commandText.UpdateOpportunityToCustomer, new
                    {
                        CustCode = model.CustCode,
                        leadID = model.LeadId
                    });
                    await conn.ExecuteAsync(_commandText.UpdateActivityToCustomer, new
                    {
                        CustCode = model.CustCode,
                        leadID = model.LeadId
                    });
                    await conn.ExecuteAsync(_commandText.UpdateCustInfo, new
                    {
                        CustCode = model.CustCode,
                        leadID = model.LeadId
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("DeleteLeadInfo:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
