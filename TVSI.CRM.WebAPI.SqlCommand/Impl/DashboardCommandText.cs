﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class DashboardCommandText : IDashboardCommandText
    {
        public string SumTradeInfo => @"SELECT COALESCE(SUM(gia_tri_tien),0) as Transaction_Value, COALESCE(SUM(phi_giao_dich),0) as Transaction_Fee, ngay_giao_dich as Transaction_Date 
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich < @end AND ngay_giao_dich > @start 
                            AND trang_thai = 1 AND ma_nhan_vien_quan_ly = @userCode
                            GROUP BY ngay_giao_dich";

        public string SumFeeDaily => @"SELECT COALESCE(SUM(phi_giao_dich),0) 
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich = @DateIndex 
                            AND ma_nhan_vien_quan_ly = @userCode AND trang_thai = 1";

        public string SumFeeLastWeek => @"SELECT COALESCE(SUM(phi_giao_dich),0) as Transaction_Fee
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich < @end AND ngay_giao_dich > @start 
                            AND trang_thai = 1 AND ma_nhan_vien_quan_ly = @userCode";

        public string HistoryTrandeDaily => @"SELECT COALESCE(SUM(A.DealVolume),0) TodayVolume, COALESCE(SUM(DealVolume * DealPrice),0) TodayAmount 
                                                FROM DealInfo A
                                                INNER JOIN Customer B ON LEFT(A.AccountNo, 6) = B.CustomerID
                                                WHERE B.MKTID = @saleId AND A.insertTime < @end AND A.insertTime > @start";

        public string TotalAmountLastDay => @"SELECT COALESCE(SUM(gia_tri_tien),0) as DailyAmount 
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich = DATEADD(Day, -1, @dateIndex) 
                            AND trang_thai = 1 AND ma_nhan_vien_quan_ly = @userCode";

        public string TotalLastMonth => @"SELECT COALESCE(SUM(khoi_luong),0) as MonthVolume, COALESCE(SUM(gia_tri_tien),0) as MonthAmount 
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich < DATEADD(Day, 1, @dateIndex) AND ngay_giao_dich > DATEADD(Day, -30, @dateIndex) 
                            AND trang_thai = 1 AND ma_nhan_vien_quan_ly = @userCode";

        public string TotalAmountMonthBefore => @"SELECT COALESCE(SUM(gia_tri_tien),0) as MonthAmount 
                            FROM TVSI_DU_LIEU_PHI_VA_THUE 
                            WHERE ngay_giao_dich < DATEADD(Day, -29, @dateIndex) AND ngay_giao_dich > DATEADD(Day, -60, @dateIndex) 
                            AND trang_thai = 1 AND ma_nhan_vien_quan_ly = @userCode";

        public string CountAccount => @"SELECT count(*) FROM TVSI_THONG_TIN_TAI_KHOAN A 
                            JOIN TVSI_THONG_TIN_KHACH_HANG B ON A.ma_khach_hang = B.ma_khach_hang 
                            WHERE a.ngay_mo_tai_khoan = @dateIndex AND a.loai_tai_khoan = 1 AND b.ma_quan_ly = @saleId";

        public string OrderInfo => @"SELECT LTRIM(RTRIM(ACCOUNT)) as AccountNo, CANCELLEDVOLUME as CancelledVolume,
                            CANCELSTATUS as CancelStatus, CHANGESTATUS as ChangeStatus,
                            EXECUTEDPRICE as ExecutedPrice, EXECUTEDVOL as ExecutedVol,
                            ORDERSTATUS as OrderStatus, PRICE as Price, SIDE as Side,
                            LTRIM(RTRIM(SECSYMBOL)) as Symbol, TRANSTIME as TransTime, VOLUME as Volume
                    FROM ORDERINFO JOIN CUSTOMER ON CUSTOMER.CUSTOMERID = LEFT(ORDERINFO.ACCOUNT, 6) WHERE CUSTOMER.MKTID = @mktid";

        public string DealInfo => @"SELECT LTRIM(RTRIM(AccountNo)) as AccountNo, DealPrice, DealTime, DealVolume, Side, LTRIM(RTRIM(Symbol)) as Symbol
                    FROM DEALINFO JOIN CUSTOMER ON CUSTOMER.CUSTOMERID = LEFT(DEALINFO.AccountNo, 6) WHERE CUSTOMER.MKTID = @mktid";
    }
}
