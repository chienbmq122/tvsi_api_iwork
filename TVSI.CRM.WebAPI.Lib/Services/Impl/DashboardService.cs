﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Activity;
using TVSI.CRM.WebAPI.Lib.Models.Dashboard;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class DashboardService : BaseService, IDashboardService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(SystemService));
        private IActivityCommandText _commandText;
        private IDashboardCommandText _commandTextDashboard;
        public DashboardService(IActivityCommandText commandText, IDashboardCommandText commandTextDashboard)
        {
            _commandText = commandText;
            _commandTextDashboard = commandTextDashboard;
        }
        public async Task<IEnumerable<ActivityResult>> Get3TaskNextActivity(DashboardActionRequest model)
        {
            var sql = _commandText.GetNext3Task;
            var dateTimeRequest = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    List<ActivityResult> resultData = new List<ActivityResult>();
                    if (model.UserRole.Contains("BO"))
                    {
                        sql = sql.Replace("Activity", "BOActivity");
                        IEnumerable<BOActivityDatabase> result = await conn.QueryAsync<BOActivityDatabase>(sql, new { date = dateTimeRequest, userAssign = model.UserName });
                        foreach (BOActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.BOActivityID;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else if (model.UserRole.Contains("CF"))
                    {
                        sql = sql.Replace("Activity", "CFActivity");
                        sql = sql.Replace("WHERE", "LEFT JOIN Lead ON Lead.LeadID = CFActivity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = CFActivity.CustCode WHERE");
                        IEnumerable<CFActivityDatabase> result = await conn.QueryAsync<CFActivityDatabase>(sql, new { date = dateTimeRequest, userAssign = model.UserName });
                        foreach (CFActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            resultData.Add(_activityResult);
                        }
                    }
                    else
                    {
                        sql = sql.Replace("WHERE", "LEFT JOIN Lead ON Lead.LeadID = Activity.LeadID LEFT JOIN CustInfo ON CustInfo.CustCode = Activity.CustCode WHERE");
                        IEnumerable<ActivityDatabase> result = await conn.QueryAsync<ActivityDatabase>(sql, new { date = dateTimeRequest, userAssign = model.UserName });
                        foreach (ActivityDatabase data in result)
                        {
                            ActivityResult _activityResult = new ActivityResult();
                            _activityResult.ActivityId = data.ActivityID;
                            _activityResult.LeadId = data.LeadID;
                            _activityResult.LeadName = data.LeadName;
                            _activityResult.CustCode = data.CustCode;
                            _activityResult.CustName = data.CustName;
                            _activityResult.Title = data.ActivityName;
                            _activityResult.EventType = data.EventType;
                            _activityResult.Type = data.ActivityType;
                            _activityResult.StartDate = data.StartDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.EndDate = data.EndDate.ToString("dd/MM/yyyy HH:mm:ss");
                            _activityResult.Location = data.Location;
                            _activityResult.Priority = data.Priority;
                            _activityResult.Status = data.Status;
                            _activityResult.Assigned = data.AssignUser;
                            _activityResult.Detail = data.Description;
                            _activityResult.ConfirmBy = data.ConfirmBy;
                            _activityResult.ConfirmCode = data.ConfirmCode;
                            _activityResult.ConfirmDate = data.ConfirmDate;
                            _activityResult.ConfirmStatus = data.ConfirmStatus;
                            _activityResult.RejectCode = data.RejectCode;
                            _activityResult.RejectReason = data.RejectReason;
                            _activityResult.ReasonType = data.ReasonType;
                            resultData.Add(_activityResult);
                        }
                    }
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<DashboardChartResult> TradeInfo(DashboardChartRequest model)
        {
            try
            {
                DateTime today = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                DayOfWeek currentDay = today.DayOfWeek;
                int daysTillCurrentDay = currentDay - DayOfWeek.Monday;
                DateTime currentWeekStartDate = today.AddDays(-daysTillCurrentDay-1);
                DateTime currentWeekEndDate = today.AddDays(1);
                DateTime lastWeekStartDate = currentWeekStartDate.AddDays(-7);
                DateTime lastWeekEndDate = currentWeekStartDate.AddDays(1);
                DateTime StartDateLastMonth = new DateTime(today.Month == 1 ? today.Year - 1 : today.Year, today.Month == 1 ? 12 : today.Month - 1, 1);
                DateTime StartDateMonth = new DateTime(today.Year, today.Month, 1);
                DateTime EndDateMonth = new DateTime(today.Month == 12 ? today.Year +1: today.Year, today.Month == 12 ? 1 : today.Month + 1, 1);
                string date = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                DashboardChartResult data = new DashboardChartResult();
                string sql = _commandTextDashboard.SumTradeInfo;
                IEnumerable<DashboardChartDetail> chartDetail = await WithEmsDbConnection(async conn =>
                {
                    
                    double sumLastWeek = await conn.QueryFirstOrDefaultAsync<double>(_commandTextDashboard.SumFeeLastWeek, new
                    {
                        dateIndex = date,
                        userCode = model.UserName,
                        start = model.Status == 1 ? lastWeekStartDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateLastMonth.AddDays(-1).ToString("yyyy-MM-dd") + " 00:00:00.000",
                        end = model.Status == 1 ? lastWeekEndDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000"
                    });
                    double sumThisWeek = 0.0;
                    data.TodayFee = await conn.QueryFirstOrDefaultAsync<double>(_commandTextDashboard.SumFeeDaily, new
                    {
                        dateIndex = date + " 00:00:00.000",
                        userCode = model.UserName
                    });
                    IEnumerable<DashboardChartDetail> resultData = await conn.QueryAsync<DashboardChartDetail>(sql, new
                    {
                        dateIndex = date,
                        userCode = model.UserName,
                        start = model.Status == 1 ? currentWeekStartDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : StartDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000",
                        end = model.Status == 1 ? currentWeekEndDate.ToString("yyyy-MM-dd") + " 00:00:00.000" : EndDateMonth.ToString("yyyy-MM-dd") + " 00:00:00.000"
                    });
                    foreach (DashboardChartDetail dataSum in resultData)
                    {
                        sumThisWeek += dataSum.Transaction_Fee;
                    }
                    data.PercentOfWeek = sumLastWeek == 0.0 ? 100 : Math.Round(((sumThisWeek / sumLastWeek) * 100), 2);
                    return resultData;
                });
                data.Transaction_Data = chartDetail;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("TradeInfo:" + ex.Message);
                log.Error(ex.InnerException);
                DashboardChartResult data = new DashboardChartResult();
                return data;
                throw;
            }
        }
        public async Task<DashboardTradeResult> TradeTotalInfoHistory(DashboardTradeRequest model)
        {
            try
            {
                string date = DateTime.ParseExact(model.DateIndex, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                DashboardTradeResult data = new DashboardTradeResult();
                DashboardMonthlyTradeInfoData dataMonthly = new DashboardMonthlyTradeInfoData();
                double TotalAmountMonthBefore = 0.0;
                double TotalAmountLastDay = 0.0;
                await WithEmsDbConnection(async conn =>
                {
                    TotalAmountLastDay = await conn.QueryFirstOrDefaultAsync<double>(_commandTextDashboard.TotalAmountLastDay, new { dateIndex = date, userCode = model.UserName });
                    TotalAmountMonthBefore = await conn.QueryFirstOrDefaultAsync<double>(_commandTextDashboard.TotalAmountMonthBefore, new { dateIndex = date, userCode = model.UserName });
                    dataMonthly = await conn.QueryFirstOrDefaultAsync<DashboardMonthlyTradeInfoData>(_commandTextDashboard.TotalLastMonth, new { dateIndex = date, userCode = model.UserName });
                });
                DashboardDailyTradeInfoData dataDaily = await WithInnoDBConnection(async conn => {
                    return await conn.QueryFirstOrDefaultAsync<DashboardDailyTradeInfoData>(_commandTextDashboard.HistoryTrandeDaily, new { saleId = model.UserName.Substring(0, 4), start = date + " 00:00:00.000", end = date + " 23:59:59.000" });
                });
                int coutAccount = await WithEmsDbConnection(async conn =>
                {
                    return await conn.QueryFirstOrDefaultAsync<int>(_commandTextDashboard.CountAccount, new { dateIndex = date + " 00:00:00.000", saleId = model.UserName.Substring(0, 4) });
                });
                data.TodayAmount = dataDaily.TodayAmount;
                data.TodayVolume = dataDaily.TodayVolume;
                data.TodayFee = dataDaily.TodayAmount * 0.003;
                data.TodayPercent = TotalAmountLastDay == 0.0 ? 100 : Math.Round(((dataDaily.TodayAmount / TotalAmountLastDay) * 100), 2);
                data.MonthVolume = dataMonthly.MonthVolume;
                data.MonthAmount = dataMonthly.MonthAmount;
                data.MonthFee = data.MonthAmount * 0.003;
                data.MonthPercent = TotalAmountMonthBefore == 0.0 ? 100 : Math.Round(((dataMonthly.MonthAmount / TotalAmountMonthBefore) * 100), 2);
                data.OpenAccount = coutAccount;
                return data;
            }
            catch (Exception ex)
            {
                log.Error("TradeInfo:" + ex.Message);
                log.Error(ex.InnerException);
                DashboardTradeResult data = new DashboardTradeResult();
                return data;
                throw;
            }
        }
        public async Task<DashboardSummaryResult> TradeTotalHistory(DashboardSummaryRequest model)
        {
            DashboardSummaryResult data = await WithCrmDbConnection(async conn => {
                return await conn.QueryFirstOrDefaultAsync<DashboardSummaryResult>("EXEC TVSI_sDashboard_TotalInfo @userName = @user,@systemLevel = 0,@branchID = '', @saleList = '';", new { user = model.UserName });
            });
            return data;
        }
        public async Task<TradingInfoModel> TradingInfoDaily(DashboardSummaryRequest model)
        {
            TradingInfoModel data = new TradingInfoModel();
            IEnumerable<OrderInfoModel> orderInfoData = await WithInnoDBConnection(async conn => {
                return await conn.QueryAsync<OrderInfoModel>(_commandTextDashboard.OrderInfo, new { mktid = model.UserName.Substring(0, 4) });
            });
            IEnumerable<DealInfoModel> dealInfoData = await WithInnoDBConnection(async conn => {
                return await conn.QueryAsync<DealInfoModel>(_commandTextDashboard.DealInfo, new { mktid = model.UserName.Substring(0, 4) });
            });
            data.DealList = dealInfoData.ToList();
            data.OrderList = orderInfoData.ToList();
            return data;
        }
    }
}
