﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.TimeWork;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface ITimeWorkService
    {
        Task<GetTimeWorkByStaffResult> GetHisTimeWorkStaffByDay(GetTimeWorkByStaffRequest model);
        Task<IEnumerable<GetTimeWorkByStaffByMonthResult>> GetHisTimeWorkStaffByMonth(GetTimeWorkByStaffByMonthRequest model);
        Task<GetTimeWorkStaffReportResult> GetTimeWorkStaffReport(GetTimeWorkStaffReportRequest model);
        
        Task<IEnumerable<GetLeaveApplicationStaffReportResult>> GetLeaveApplicationStaffReport(GetLeaveApplicationStaffReportRequest model);
        Task<bool> CreateApplicationFormStaff(CreateApplicationFormStaffRequest model);
        Task<bool> CheckinTimeKeepingGPSStaff(CheckinTimeKeepingGPSStaffRequest model);
        Task<bool> CheckoutTimeKeepingGPSStaff(CheckoutTimeKeepingGPSStaffRequest model);
        Task<GetCheckinTimeKeepingGPSStaffResult> GetCheckinTimeKeepingGPSStaff(GetCheckinTimeKeepingGPSStaffRequest model);
        Task<GetCheckoutTimeKeepingGPSStaffResult> GetCheckoutTimeKeepingGPSStaff(GetCheckoutTimeKeepingGPSStaffRequest model);
        Task<IEnumerable<GetFingerprintingStaffResult>> GetFingerprintingStaff(GetFingerprintingStaffRequest model);
        Task<GetTWAndLaByStaffResult> GetHisTWAndLAStaffByDay(GetHisTWAndLAStaffByDayRequest model);
        Task<IEnumerable<CheckDataByDateResult>> CheckDataByDay(CheckDataByDateRequest model);
        Task<GetDetailLeaveApplicationsResult> GetDetailLeaveApplications(GetDetailLeaveApplicationsRequest model);
        Task<bool> TW_ED_EditLAMember(TW_ED_EditLAMemberReportRequest model);
        Task<bool> TW_MB_ActionLeaveApplications(ActionLeaveApplicationsRequest model);
        Task<IEnumerable<checkDataByMonthResult>> CheckDataByMonth(CheckDataByMonthRequest model);

        Task<IEnumerable<GetNameLeaderApprovedByUserNameResult>> GetNameLeaderApprovedByUserName(BaseRequest model);
        Task<IEnumerable<GetNameLeaderApprovedByUserNameResult>> GetNameMemberApprovedByUserName(BaseRequest model);
        Task<IEnumerable<GetGetLeaveApplicationMemberReportByLeaderResult>> GetLeaveApplicationsMemberByLeader(GetLeaveApplicationsMemberByLeaderRequest model);
        Task<IEnumerable<GetGetLeaveApplicationMemberReportByLeaderResult>> GetLAMemberReportByLeader(GetLAMemberReportByLeaderRequest model);

        Task<bool> LeaveApplicationApprovedByLeaderAll(LeaveApplicationApprovedByLeaderAllRequest model);
        // DI su dung rieng
        Task<string> GetNumBackEarly(string timeIn, string timeOut);
        Task<string> GetNameStaff(string userName);
        
        // Stored
    }
}