﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.AssetManagement
{
    public class AssetManagement
    {
        [Description("Id mượn trả")]
        public int ManagementAssetId { get; set; }
        [Description("Id thiết bị")]
        public int AssetId { get; set; }
        [Description("Ngày mượn")]
        public string DateReceiveAsset { get; set; }
        [Description("Ngày trả")]
        public string DateResponseAsset { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Người mượn")]
        public string UserId { get; set; }
        [Description("Thuộc phòng")]
        public int DepartmentId { get; set; }
    }

    public class RequestCreateManagement : BaseRequest
    {
        [Description("Id thiết bị")]
        public string AssetId { get; set; }
        [Description("Receive user")]
        public string ReceiveUserid { get; set; }
        [Description("Ngày mượn  format YYYY-MM-DD")]
        public string DateReceiveAsset { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Thuộc phòng")]
        public int DepartmentId { get; set; }
        [Description("Số lượng")]
        public int Quantity { get; set; }
    }

    public class RequestUpdateManagement : BaseRequest
    {
        [Description("ID mượn trả")]
        public string ManagementId { get; set; }
        [Description("Trạng thái PrePass: Bàn giao, Pass: Xác nhận bàn giao, Response: Hoàn trả, ConfirmRespon: xác nhận hoàn trả")]
        public string Status { get; set; }
        [Description("Số lượng")]
        public int Quantity { get; set; }
        [Description("Ngày mượn format YYYY-MM-DD")]
        public string DateReceiveAsset { get; set; }
        [Description("Ngày trả format YYYY-MM-DD")]
        public string DateResponseAsset { get; set; }
    }
    
    public class FliterManagementAsset : BaseRequest
    {
        [Description("Người mượn")]
        public string UserId { get; set; }
        [Description("Thuộc phòng")]
        public int DepartmentId { get; set; }
        [Description("Ngày mượn")]
        public string DateReceiveAsset { get; set; }
        [Description("Ngày trả")]
        public string DateResponseAsset { get; set; }
        [Description("Số trang")]
        public int PageIndex { get; set; }
        [Description("Số item 1 trang")]
        public int PageSize { get; set; }
    }

    public class DepartmentManagementAsset
    {
        [Description("Id phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Mã phòng ban")]
        public string DepartmentCode { get; set; }
        [Description("Tên phòng ban")]
        public string DepartmentName { get; set; }
    }

    public class UserManagementAssetRequest : BaseRequest
    {
        [Description("Id phòng ban")]
        public int DepartmentId { get; set; }
    }

    public class UserManagementAsset
    {
        [Description("User name")]
        public string UserName { get; set; }
        [Description("Họ tên user")]
        public string FullName { get; set; }
        [Description("Id phòng ban")]
        public int DepartmentId { get; set; }
    }
}
