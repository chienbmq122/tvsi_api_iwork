﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Category;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Models.System;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IAttributeService
    {
        Task<IEnumerable<AtributeModel>> GetListAttribute();

        Task<IEnumerable<LeadAttrinbuteViewModel>> GetLeadAttribute(LeadAttributeRequest model);

        Task<IEnumerable<CustomerAttributeModel>> GetCustAttribute(CustomerAttributeRequest model);

        Task<bool> CreateLeadAttribute(LeadAttrinbuteCreate model);

        Task<bool> CreateCustAttribute(CustomerAttrinbuteCreate model);

        Task<bool> DeleteLeadAttribute(LeadAttrinbuteDelete model);

        Task<bool> DeleteCustAttribute(CustomerAttrinbuteCreate model);

        Task<bool> CheckLeadAttributeExit(LeadAttrinbuteCreate model);

        Task<bool> CheckCustAttributeExit(CustomerAttrinbuteCreate model);

        Task<IEnumerable<CategoryResult>> GetListCategory(CategoryRequset model);
    }
}
