﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface INotificationCommandText
    {
        /// <summary>
        /// get notification
        /// </summary>
        string GetNotification { get; }

        /// <summary>
        /// get count notification
        /// </summary>
        string CountNotification { get; }
    }
}
