﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class ShareHolderListRequest : BaseRequest
    {

        [Description("Từ khóa tìm kiếm : Họ tên, mã khách hàng")]
        public string keySearch { get; set; }

        [Description("Trạng thái : Null lấy tất cả, 1 hoạt động, 99 không hoạt động")]
        [Required]
        public int Status { get; set; }

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class ShareHolderListResult
    {
        [Description("Ngày tạo")]
        public string CreatedDate { get; set; }

        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        [Description("Họ và tên")]
        public string CustName { get; set; }

        [Description("Cổ đông lớn")]
        public int ShareType_1 { get; set; }
        [Description("Cổ đông nội bộ")]
        public int ShareType_2 { get; set; }
        [Description("Người liên quan cổ đông lớn")]
        public int ShareType_3 { get; set; }
        [Description("Sắp thành cổ đông lớn")]
        public int ShareType_4 { get; set; }

        [Description("Loại cổ đông")]
        public string ShareType => (ResourceFile.SystemModule.ResourceManager.GetString("ShareType_1_" + ShareType_1) + "," + ResourceFile.SystemModule.ResourceManager.GetString("ShareType_2_" + ShareType_2) + "," + ResourceFile.SystemModule.ResourceManager.GetString("ShareType_3_" + ShareType_3) + "," + ResourceFile.SystemModule.ResourceManager.GetString("ShareType_4_" + ShareType_4)).Replace(",,",",");

        [Description("Mã chứng khoán")]
        public string StockCode { get; set; }

        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.SystemModule.ResourceManager.GetString("CustStatus_" + Status);
        [Description("Mã nhân viên : SaleId")]
        public string AssignUser { get; set; }
    }
}
