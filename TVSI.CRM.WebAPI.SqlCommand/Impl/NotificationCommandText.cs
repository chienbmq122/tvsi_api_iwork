﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace TVSI.CRM.WebAPI.SqlCommand.Impl
{
    public class NotificationCommandText : INotificationCommandText
    {
        #region SQL Command List
        public string GetNotification => @"SELECT notification_id, type_notification_id, template_notification_id, title_notification,
                                                       content_notification, content_en_notification, priority , status, src_notification,
                                                       time_send as dateSend,staff_code, img, method_create, channel_send, is_active, create_by, create_at,
                                                       update_by,update_at 
                                                  FROM TVSI_HANG_DOI_THONG_BAO_NHAN_VIEN 
                                                  WHERE staff_code = @staffCode AND channel_send = 3 
                                                  ORDER BY priority,time_send  DESC 
                                                  OFFSET @Start ROWS FETCH NEXT @PageSize ROWS ONLY";
        public string CountNotification => @"SELECT count(1) FROM TVSI_HANG_DOI_THONG_BAO_NHAN_VIEN 
                                                  WHERE staff_code = @staffCode AND channel_send = 3";
        #endregion

        #region Private function
        /// <summary>
        /// Biến đánh dấu file xml thay đổi thì load lại xml
        /// </summary>
        private static DateTime xmlFile_LastModifiedDate = new DateTime(1970, 1, 1);

        /// <summary>
        /// Biến lưu danh sach câu query trong bộ nhớ
        /// </summary>
        private static Dictionary<string, string> _sqlQueriesList;

        private static string LoadSqlQuery(string sqlName)
        {
            var xmlFile = HttpContext.Current.Server.MapPath("~/bin/SqlQueries/SystemSqlQuery.xml");

            var fileInfo = new FileInfo(xmlFile);

            if (xmlFile_LastModifiedDate != fileInfo.LastWriteTime)
            {
                LoadSystemSqlXMLFile(xmlFile);
                xmlFile_LastModifiedDate = fileInfo.LastWriteTime;
            }
            return _sqlQueriesList.ContainsKey(sqlName) ? _sqlQueriesList[sqlName] : string.Empty;
        }

        private static void LoadSystemSqlXMLFile(string filePath)
        {
            _sqlQueriesList = new Dictionary<string, string>();

            var xdoc = XDocument.Load(filePath);
            var sqlQueryList = xdoc.Descendants("SqlQuery").ToList();
            foreach (var sqlItem in sqlQueryList)
            {
                _sqlQueriesList.Add(sqlItem.Attribute("Name").Value,
                   sqlItem.Descendants("Sql").Single().Value);
            }
        }
        #endregion
    }
}
