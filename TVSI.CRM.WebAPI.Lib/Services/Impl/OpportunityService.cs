﻿using Dapper;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Opportunity;
using TVSI.CRM.WebAPI.SqlCommand;

namespace TVSI.CRM.WebAPI.Lib.Services.Impl
{
    public class OpportunityService : BaseService, IOpportunityService
    {
        private readonly ILog log = LogManager.GetLogger(typeof(ActivityService));
        private IOpportunityCommandText _commandText;
        public OpportunityService(IOpportunityCommandText commandText)
        {
            _commandText = commandText;
        }

        public async Task<IEnumerable<OpportunityResult>> GetListOpportunity(OpportunityRequest model)
        {
            var sql = _commandText.GetListOpportunity;
            sql += "AND Opportunity.AssignUser = @assignUser ";
            try
            {
                if (!string.IsNullOrEmpty(model.KeyWord))
                {
                    sql += "AND Opportunity.OpportunityName LIKE '%" + model.KeyWord.Trim() + "%'";
                }

                if (model.Status != 0)
                {
                    sql += "AND Opportunity.Status = " + model.Status;
                }
                sql += " ORDER BY Opportunity.CreatedDate DESC";
                return await WithCrmDbConnection(async conn =>
                {
                    IEnumerable<OpportunityResult> resultData = await conn.QueryAsync<OpportunityResult>(sql, new { assignUser = model.UserName });
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<OpportunityResult> GetDetailOpportunity(OpportunityDalete model)
        {
            //await conn.QueryFirstOrDefaultAsync<BOActivityDatabase>
            var sql = _commandText.GetListOpportunity;
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    OpportunityResult resultData = await conn.QueryFirstOrDefaultAsync<OpportunityResult>(_commandText.GetDetailOpportunity, new { opportunityID = model.OpportunityID, assignUser = model.UserName });
                    return resultData;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> CreateOpportunity(OpportunityCreate model)
        {
            try
            {
                string timeClose = DateTime.ParseExact(model.ExpectedCloseDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd");
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.CreateOpportunity, new
                    {
                        OpportunityName = model.OpportunityName,
                        BranchID = model.BranchID,
                        AssignUser = model.AssignUser,
                        LeadID = model.LeadID,
                        CustCode = model.CustCode,
                        ExpectedRevenue = model.ExpectedRevenue,
                        ExpectedCloseDate = timeClose + " 00:00:00.000",
                        Description = model.Description,
                        Status = 1,
                        CreatedBy = model.UserName,
                        CreatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex);
                throw;
            }
        }
        public async Task<bool> UpdateOpportunity(OpportunityUpdate model)
        {
            try
            {
                string timeClose = !String.IsNullOrEmpty(model.ExpectedCloseDate) ? DateTime.ParseExact(model.ExpectedCloseDate, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd") : "";
                return await WithCrmDbConnection(async conn =>
                {
                    OpportunityResult resultData = await conn.QueryFirstOrDefaultAsync<OpportunityResult>(_commandText.GetDetailOpportunity, new { opportunityID = model.OpportunityID, assignUser = model.UserName });
                    await conn.ExecuteAsync(_commandText.UpdateOpportunity, new
                    {
                        OpportunityName = !String.IsNullOrEmpty(model.OpportunityName) ? model.OpportunityName : resultData.OpportunityName,
                        BranchID = !String.IsNullOrEmpty(model.BranchID) ? model.BranchID : resultData.BranchID,
                        AssignUser = !String.IsNullOrEmpty(model.AssignUser) ? model.AssignUser : resultData.AssignUser,
                        LeadID = !String.IsNullOrEmpty(model.LeadID.ToString()) ? model.LeadID : resultData.LeadID,
                        CustCode = !String.IsNullOrEmpty(model.CustCode) ? model.CustCode : resultData.CustCode,
                        ExpectedRevenue = !String.IsNullOrEmpty(model.ExpectedRevenue.ToString()) ? model.ExpectedRevenue : resultData.ExpectedRevenue,
                        ExpectedCloseDate = !String.IsNullOrEmpty(timeClose) ? timeClose + " 00:00:00.000" : resultData.ExpectedCloseDate,
                        Description = !String.IsNullOrEmpty(model.Description) ? model.Description : resultData.Description,
                        Status = !String.IsNullOrEmpty(model.Status.ToString()) ? model.Status : resultData.Status,
                        opportunityID = model.OpportunityID,
                        UpdatedBy = model.UserName,
                        UpdatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> DeleteOpportunity(OpportunityDalete model)
        {
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    await conn.ExecuteAsync(_commandText.DeleteOpportunity, new
                    {
                        opportunityID = model.OpportunityID,
                        UpdatedBy = model.UserName,
                        UpdatedDate = DateTime.Now
                    });
                    return true;
                });
            }
            catch (Exception ex)
            {
                log.Error("GetActivityType:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
        public async Task<bool> CheckCustCode(string custCode, string assignUser)
        {
            var sql = @"SELECT CustCode FROM CustInfo WHERE CustCode = @custCode AND Status <> 99 AND AssignUser = @assignUser";
            try
            {
                return await WithCrmDbConnection(async conn =>
                {
                    string result = await conn.QueryFirstOrDefaultAsync<string>(sql,
                           new { custCode = custCode, assignUser = assignUser });
                    if (string.IsNullOrEmpty(result))
                    {
                        return true;
                    }
                    return false;
                });
            }
            catch (Exception ex)
            {
                log.Error("CheckCustCode:" + ex.Message);
                log.Error(ex.InnerException);
                throw;
            }
        }
    }
}
