﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Contact
{
    public class ContactStaff
    {
        public string SaleID { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public int UserLevel { get; set; }
        public string UserRole { get; set; }
    }

    public class ContactStaffResponse
    {
        public List<ContactStaff> ListContactStaffs { get; set; }
        public int TotalContact { get; set; }
    }

    public class ContactStaffRequest : BaseRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string KeySearch { get; set; }
    }

    public class ContactCustomer
    {
        public string CustName { get; set; }
        public string CustCode { get; set; }
        public int LeadID { get; set; }
        public int BitIsCust { get; set; }

        public string Mobile { get; set; }
    }

    public class ContactRole
    {
        public string ten_dang_nhap { get; set; }
        public string ma_chuc_nang { get; set; }
        public string ten_chuc_nang { get; set; }
        public int IsRead { get; set; }
        public int IsEdit { get; set; }
        public int IsCreate { get; set; }
        public int IsDelete { get; set; }
        public int IsPrint { get; set; }
        public int IsFind { get; set; }
        public int IsAllBranch { get; set; }
        public int IsApproval { get; set; }
        public int IsRekey { get; set; }

    }

    public class ContactCustomerResponse
    {
        public List<ContactCustomer> ListContactCustomers { get; set; }
        public int TotalContact { get; set; }
    }
}
