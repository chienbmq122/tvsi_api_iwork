﻿using System;
using System.Configuration;
using log4net.Repository.Hierarchy;
using Logger = TVSI.Utility.Logger;

namespace TVSI.CRM.WebAPI.Lib.Utilities.ConfigUtils
{
    public class ConfigUtilities
    {
        public static string GetAppsettingValue(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch (Exception e)
            {
               Logger.Error(typeof(ConfigUtilities),string.Format("Loi Method GetAppsettingValue -  {0}",e.Message));
               return "";
            }
        }
    }
}