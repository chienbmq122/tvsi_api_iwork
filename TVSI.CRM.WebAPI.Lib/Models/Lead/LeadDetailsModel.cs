﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Lead
{

    #region "View chi tiết khách hàng"
    /// <summary>
    /// LeadInfoRequest
    /// </summary>
    public class LeadInfoRequest : BaseRequest
    {
        [Description("ID Lead")]
        [Required]
        public int LeadId { get; set; }
    }

    /// <summary>
    /// LeadInfoResult
    /// </summary>
    public class LeadInfoResult
    {
        [Description("thông tin liên hệ")]
        public LeadInfo LeadInfoContact { get; set; }

        [Description("thông tin khách hàng")]
        public List<GroupCategoryDetail> LeadInfoContactCategory { get; set; }
    }

    public class LeadInfo
    {
        [Description("Mã khách hàng tiềm năng")]
        public string LeadId { get; set; }
        [Description("Họ và tên")]
        [Required]
        public string LeadName { get; set; }
        [Description("Số điện thoại khách hàng")]
        [Required]
        public string Mobile { get; set; }
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        [Description("Địa chỉ liên hệ")]
        public string Address { get; set; }
        [Description("Loại hình - Cá nhân: 1, Tổ chức: 2")]
        public int ProfileType { get; set; }

        [Description("Mô tả về lead")]
        public string Description { get; set; }

        [Description("Nguồn thông tin qua - Hội thảo: 1, Quảng cáo: 2, Người giới thiệu: 3, Sự kiện công ty tổ chức: 4, Qua website: 5, Khác: 6")]
        public int LeadSourceID { get; set; }

        [Description("Nguồn thông tin qua - Hội thảo: 1, Quảng cáo: 2, Người giới thiệu: 3, Sự kiện công ty tổ chức: 4, Qua website: 5, Khác: 6")]
        public string SourceName { get; set; }
    }

    public class GroupCategoryDetail
    {
        [Description("Item group type")]
        public string ItemGroupType { get; set; }
        [Description("Item group type name")]
        public string ItemGroupTypeName { get; set; }
        [Description("Data item")]
        public List<LeadInvestmentInfo> DataItem { get; set; }
    }

    public class LeadInvestmentInfo
    {
        [Description("Tên category")]
        public string ItemName { get; set; }
        [Description("Id category")]
        public int ItemID { get; set; }
        [Description("Category type")]
        public int ItemTypeID { get; set; }
        [Description("Category type text")]
        public string ItemTypeLabel
        {
            get
            {
                switch (ItemTypeID)
                {
                    case 1:
                        return "Nghề nghiệp";
                    case 2:
                        return "Khả năng tài chính";
                    case 3:
                        return "Thời gian đầu tư";
                    case 4:
                        return "Quan điểm đầu tư";
                    case 5:
                        return "Nguồn giới thiệu";
                    case 6:
                        return "Nơi làm việc";
                    case 7:
                        return "Dã tham gia trái phiếu tại";
                    case 8:
                        return "Kinh nghiêm đầu tư";
                    case 9:
                        return "Khả năng chấp nhận rủi ro";
                    case 10:
                        return "Sở thích đầu tư";
                    case 11:
                        return "Lưu ý khi giao tiếp";
                    case 12:
                        return "Mối quan hệ với Sale";
                    default:
                        return "Sắp ra mắt";
                };
            }
        }
       // [Description("Category type text")]
       // public string ItemGroupType {
       //     get
       //     {
       //         if (ItemTypeID == 1 || ItemTypeID == 6)
       //         {
       //             return "ContactInfo";
       //         }
       //         else if (ItemTypeID == 2 || ItemTypeID == 7 || ItemTypeID == 8 || ItemTypeID == 4 || ItemTypeID == 3 || ItemTypeID == 5)
       //         {
       //             return "InvestmentInfo";
       //         }
       //         else if (ItemTypeID == 9 || ItemTypeID == 11 || ItemTypeID == 10 || ItemTypeID == 12)
       //         {
       //             return "CustCareInfo";
       //         }
       //         else
       //         {
       //             return "ExtendInfo";
       //         }
       //     }
       //}
    }

    public class ExLeadInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    #endregion

    public class LeadActivityRequest : BaseRequest
    {
        [Description("Mã khách hàng tiềm năng")]
        public string LeadId { get; set; }
        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }
    public class LeadActivityResult
    {
        [Description("ID activity")]
        public int ActivityId { get; set; }
        [Description("Tiêu đề hoạt động")]
        public string ActivityName { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Ngày bắt đầu")]
        public string StartDate { get; set; }
        [Description("Ngày kết thúc")]
        public string EndDate { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.ActivityStatus.ResourceManager.GetString("Status_" + Status);

        [Description("AssignUser Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string AssignUser { get; set; }

    }


    public class LeadOpportunityRequest : BaseRequest
    {
        [Description("Mã khách hàng tiềm năng")]
        public int LeadId { get; set; }
    }
    public class LeadOpportunityResult
    {
        [Description("ID Opportunity")]
        public int OpportunityId { get; set; }
        [Description("Tiêu đề cơ hội")]
        public string OpportunityName { get; set; }
        [Description("Lợi nhuận dự kiến")]
        public double ExpectedRevenue { get; set; }
        [Description("Mô tả")]
        public string Description { get; set; }
        [Description("Ngày dự kiến đóng")]
        public string ExpectedCloseDate { get; set; }
        [Description("Trạng thái")]
        public int Status { get; set; }
        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.ActivityStatus.ResourceManager.GetString("Status_" + Status);

        [Description("AssignUser Nếu là nhân viên thì mặc định = UserID của nhân viên và ko cho sửa / Trưởng phòng và GĐCN mặc định = UserID đăng nhập và cho phép sửa")]
        public string AssignUser { get; set; }

    }


    public class ExLeadInfoRequest : BaseRequest
    {

    }

    public class ExLeadInfoResult
    {
        [Description("Nghề nghiệp")]
        public List<JobInfo> JobList { get; set; }
        [Description("Khả năng tài chính")]
        public List<FinAbilityInfo> FinAbilityList { get; set; }
        [Description("Kiến thức, kinh nghiệm")]
        public List<KnowledExpInfo> KnowledExpList { get; set; }
        [Description("Quan điểm đầu tư")]
        public List<OpInvestInfo> OpInvestList { get; set; }
       
        [Description("Mức độ rủi ro")]
        public List<RiskInfo> RiskList { get; set; }
 
    }

    public class JobInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class FinAbilityInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class KnowledExpInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class OpInvestInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
    public class RiskInfo
    {
        public int ExId { get; set; }
        public string ExName { get; set; }
    }
}
