﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.AssetManagement;

namespace TVSI.CRM.WebAPI.Lib.Services
{
    public interface IAssetService
    {
        Task<RoleUserAsset> GetRoleUser(BaseRequest model);
        Task<List<AssetState>> GetListStateAsset();
        Task<List<AssetType>> GetListTypeAsset();
        Task<List<DepartmentManagementAsset>> GetListDepartment();
        Task<List<UserManagementAsset>> GetListUser(UserManagementAssetRequest model);
        Task<AssetListCommonResponse> GetListAsset(AssetListRequest model);
        Task<AssetListResponse> GetListConfirmAsset(AssetListConfirmRequest model);
        Task<bool> CreateAsset(CreateAsset model);
        Task<bool> UpdateAsset(UpdateAsset model);
        Task<bool> DeleteAsset(DeleteAsset model);
        Task<AssetDetailModel> GetDetailAsset(DeleteAsset model);
        Task<AssetListCommonResponse> FilterAsset(FilteAsset model);
        Task<List<AssetChildModelSelect>> GetListAssetChildSeclect(AssetChildModelSelectReq model);
        Task<AssetListCommonResponse> GetListAssetUser(AssetListConfirmRequest model);
        Task<AssetListResponse> GetHistoryAssetUser(AssetListConfirmRequest model);
        Task<List<AssetChildModel>> GetListAssetChild(RequestAssetChild model);
        Task<bool> AsignAssetChild(AginAssetChild model);
        Task<bool> RemoveAssetChild(RemoveAssetChild model);
        Task<List<AssetManagement>> GetHistoryReceiveAsset(DeleteAsset model);
        Task<bool> CreateManagement(RequestCreateManagement model);
        Task<bool> UpdateManagement(RequestUpdateManagement model);
        Task<AssetListResponse> FilterManagementAsset(FliterManagementAsset model);
        Task<AssetInspectionDetailModel> GetInspectionDetail(InspectionDeleteRequest model);
        Task<List<AssetInspectionModel>> GetHistoryInspection(RequestAssetInspectionModel model);
        Task<List<AssetInspectionModel>> GetListInspection();
        Task<bool> CreateInspectionAsset(InspectionCreateRequest model);
        Task<bool> UpdateInspectionAsset(InspectionUpdateRequest model);
        Task<bool> DeleteInspectionAsset(InspectionDeleteRequest model);
    }
}
