﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Bond
{
    public class BondGroupRequest : BaseRequest { }

    public class BondGroupResult
    {
        public string GroupCode { get; set; }
    }

    public class BondCodeRequest : BaseRequest
    {
        public string GroupCode { get; set; }
    }

    public class BondCodeResult
    {
        public int BondID { get; set; }
        public string BondCode { get; set; }
    }

    public class BondNavConfigRequest: BaseRequest { }

    public class BondNavConfigResult
    {
        public int NavConfigID { get; set; }
        public string NavName { get; set; }
    }

    public class BondCustInfoRequest : BaseRequest { }

    public class BondCustInfoResult
    {
        [Description("Danh sách Khách hàng")]
        public List<CustData> CustInfoList { get; set; }
        [Description("Danh sách CTV")]
        public List<ReferData> ReferInfoList { get; set; }
    }

    public class CustData
    {
        public int CustID { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string SaleID { get; set; }
    }

    public class ReferData
    {
        public string ReferID { get; set; }
        public string ReferName { get; set; }
        public string SaleID { get; set; }
    }

    public class BondCustInfoDetailRequest : BaseRequest
    {
        public string CustCode { get; set; }
    }

    public class BondCustInfoDetailResult
    {
        public int CustID { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string Address { get; set; }
        public int ProfesType { get; set; }
        public DateTime ProfesEffectiveDate { get; set; }

        public List<BondCustBankData> BankList { get; set; }
        public string FirstTradeDate { get; set; }

        public decimal NavContractActive { get; set; }
        public decimal NavContractProcess { get; set; }
        public decimal NavContractQueue { get; set; }
        public decimal CurNav { get; set; }
    }

    public class BondCustBankData
    {
        public int CustBankID { get; set; }
        public string CustCode { get; set; }
        public string BankName { get; set; }
        public string BankAccount { get; set; }
        public string BankAccountName { get; set; }
        public string SubBranchName { get; set; }

    }
    public class BondCustNavData
    {
        public string AccountNo { get; set; }
        public decimal NavContractActive { get; set; }
        public decimal NavContractProcess { get; set; }
        public decimal NavContractQueue { get; set; }
        public decimal TotalNav { get; set; }
        public DateTime? FirstBuyDate { get; set; }
    }
    #region "BM_BI_BondInfoDetail"
    public class BondInfoRequest : BaseRequest
    {
        public string BondCode { get; set; }
        public string CustCode { get; set; }
    }

    public class BondInfoResult
    {
        public int BondID { get; set; }
        public string BondCode { get; set; }
        public string BondName { get; set; }
        public int Term { get; set; }
        public decimal CouponRate { get; set; }
        public string Issuer { get; set; }
        public DateTime MaturityDate { get; set; }
        public decimal ParValue { get; set; }

        public DateTime IssueDate { get; set; }
        public string IssuerGroup { get; set; }
        public int BondType { get; set; }

        public string ProfesMsg { get; set; }
    }
    #endregion

    public class BondRateRequest : BaseRequest
    {
        public string BondCode { get; set; }
        public string TradeDate { get; set; }
        public string CustCode { get; set; }
        public string BondRate { get; set; }
        public int Volume { get; set; }
    }

    public class BondRateResult
    {
        public string BondCode { get; set; }
        public decimal BondRate { get; set; }
        public decimal BuyPrice { get; set; }
        public long BuyAvailable { get; set; }
        public decimal BondRateMax { get; set; }
        public decimal BondRateMin { get; set; }
        public decimal CompanyRate { get; set; }
        public decimal BasicRate { get; set; }
    }

    public class BondPriceCalc
    {
        public string Description { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }

        public decimal Income { get; set; }

        public decimal? Interest { get; set; }

        public decimal? TotalIncome { get; set; }

        public decimal? ReinvestRate { get; set; }

        public int? UnitType { get; set; }

        public string Unit { get; set; }

        public string RowCode { get; set; }
    }

    public class BuyAvaiableViewModel
    {
        public long Volume { get; set; }
        public string AccountNo { get; set; }
    }

    public class BondBuyOrderRequest : BaseRequest
    {
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string TradeDate { get; set; }
        public string CouponDate { get; set; }
        public string BondCode { get; set; }
        public int Volume { get; set; }
        public decimal Rate { get; set; }
        public int PaymentMethod { get; set; }
        public int CustBankID { get; set; }
        public int NavConfigID { get; set; }
        public string ReferID { get; set; }
        public string Description { get; set; }
        public decimal BuyPrice { get; set; }
    }

    public class BondBuyOrderUpdateRequest : BondBuyOrderRequest
    {
        public long RequestOrderID { get; set; }
    }

    public class BondPaymentMethodRequest : BaseRequest { }

    public class BondPaymentMethodResult
    {
        public int PaymentMethod { get; set; }
        public string PaymentMethodName { get; set; }
    }
}
