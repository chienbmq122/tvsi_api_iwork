﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Category;
using TVSI.CRM.WebAPI.Lib.Models.Customer;
using TVSI.CRM.WebAPI.Lib.Models.Lead;
using TVSI.CRM.WebAPI.Lib.Models.System;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.AttributeUtils;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Attribute API
    /// </summary>
    public class AttributeController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(AttributeController));
        private IAttributeService _attributeService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="attributeService"></param>
        public AttributeController(IAttributeService attributeService)
        {
            _attributeService = attributeService;
        }

        /// <summary>
        /// Danh sách attribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GA_GetListAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<AtributeModel>>))]
        [Note("Danh sách attribute")]
        public async Task<IHttpActionResult> AT_GA_GetListAttribute(BaseRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GA_GetListAttribute");
                SetLanguage(lang);

                var retData = await _attributeService.GetListAttribute();

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<AtributeModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GA_GetListAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GA_GetListAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách lead attribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GL_GetLeadAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<LeadAttrinbuteViewModel>>))]
        [Note("Danh sách attribute của lead")]
        public async Task<IHttpActionResult> AT_GL_GetLeadAttribute(LeadAttributeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GL_GetLeadAttribute");
                SetLanguage(lang);

                var retData = await _attributeService.GetLeadAttribute(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<LeadAttrinbuteViewModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GL_GetLeadAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GL_GetLeadAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách cust attribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GL_GetCustAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<CustomerAttributeModel>>))]
        [Note("Danh sách attribute của cust")]
        public async Task<IHttpActionResult> AT_GL_GetCustAttribute(CustomerAttributeRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GL_GetCustAttribute");
                SetLanguage(lang);

                var retData = await _attributeService.GetCustAttribute(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<CustomerAttributeModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GL_GetCustAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GL_GetCustAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới lead atribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_CA_CreateLeadAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới lead atribute")]
        public async Task<IHttpActionResult> AT_CA_CreateLeadAttribute(LeadAttrinbuteCreate model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_CA_CreateLeadAttribute");
                SetLanguage(lang);

                var retDataExit = await _attributeService.CheckLeadAttributeExit(model);

                if (retDataExit)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = AtributeConst.ATTRIBUTE_AREADY_EXITS,
                        ErrorMsg = ResourceFile.AttributeErrors.E999
                    });
                }

                var retData = await _attributeService.CreateLeadAttribute(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AT_CA_CreateLeadAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_CA_CreateLeadAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới customer atribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_CA_CreateCustAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới customer atribute")]
        public async Task<IHttpActionResult> AT_CA_CreateCustAttribute(CustomerAttrinbuteCreate model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_CA_CreateCustAttribute");
                SetLanguage(lang);

                var retDataExit = await _attributeService.CheckCustAttributeExit(model);

                if (retDataExit)
                {
                    return Ok(new ErrorModel
                    {
                        RetCode = AtributeConst.ATTRIBUTE_AREADY_EXITS,
                        ErrorMsg = ResourceFile.AttributeErrors.E999
                    });
                }

                var retData = await _attributeService.CreateCustAttribute(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AT_CA_CreateCustAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_CA_CreateCustAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// xóa lead atribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_DA_DeleteLeadAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("xóa lead atribute")]
        public async Task<IHttpActionResult> AT_DA_DeleteLeadAttribute(LeadAttrinbuteDelete model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_DA_DeleteLeadAttribute");
                SetLanguage(lang);

                var retData = await _attributeService.DeleteLeadAttribute(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AT_DA_DeleteLeadAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_DA_DeleteLeadAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// xóa customer atribute
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_DA_DeleteCustAttribute")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("xóa customer atribute")]
        public async Task<IHttpActionResult> AT_DA_DeleteCustAttribute(CustomerAttrinbuteCreate model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_DA_DeleteCustAttribute");
                SetLanguage(lang);

                var retData = await _attributeService.DeleteCustAttribute(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "AT_DA_DeleteCustAttribute");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_UA_UpdateLeadAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Danh sách option category
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("AT_GL_GetCategoryBytype")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<IEnumerable<CategoryResult>>))]
        [Note("Danh sách option category")]
        public async Task<IHttpActionResult> AT_GL_GetCategoryBytype(CategoryRequset model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "AT_GL_GetCategoryBytype");
                SetLanguage(lang);

                var retData = await _attributeService.GetListCategory(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<CategoryResult>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "AT_GL_GetCategoryBytype");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "AT_GL_GetCustAttribute");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
