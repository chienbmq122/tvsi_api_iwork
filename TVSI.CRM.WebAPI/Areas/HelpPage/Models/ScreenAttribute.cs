﻿using System;

namespace TVSI.Crm.WebAPI.Areas.HelpPage.Models
{
    public class NoteAttribute : Attribute
    {
        public NoteAttribute(string note)
        {
            Note = note;
            IsImplement = true;
        }

        public NoteAttribute(string note, bool isImplement)
        {
            Note = note;
            IsImplement = isImplement;
        }

        public string Note { get; set; }
        public bool IsImplement { get; set; }
    }
}