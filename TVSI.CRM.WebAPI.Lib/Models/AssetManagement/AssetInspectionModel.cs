﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.AssetManagement
{
    public class RequestAssetInspectionModel : BaseRequest
    {
        [Description("Phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Ngày kiểm kê")]
        public string DateInspection { get; set; }
    }

    public class AssetInspectionModel
    {
        [Description("Kiểm kê Id")]
        public int InspectionId { get; set; }
        [Description("Ngày kiểm kê")]
        public string DateInspection { get; set; }
        [Description("Phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Danh sách tài sản đã kiểm kê")]
        public string AssetCheckedInspection { get; set; }
        [Description("Danh sách tài sản chưa kiểm kê")]
        public string AssetNotCheckedInspection { get; set; }
        [Description("Ngày kiểm kê")]
        public string CreatedDate { get; set; }
        [Description("Người kiểm kê")]
        public string CreatedBy { get; set; }
        [Description("Ngày cập nhật")]
        public string UpdatedDate { get; set; }
        [Description("Người cập nhật")]
        public string UpdatedBy { get; set; }
    }

    public class AssetInspectionDetailModel
    {
        [Description("Kiểm kê Id")]
        public int InspectionId { get; set; }
        [Description("Ngày kiểm kê")]
        public string DateInspection { get; set; }
        [Description("Phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Danh sách tài sản đã kiểm kê")]
        public List<AssetDetailModel> AssetCheckedInspection { get; set; }
        [Description("Danh sách tài sản chưa kiểm kê")]
        public List<AssetDetailModel> AssetNotCheckedInspection { get; set; }
        [Description("Ngày kiểm kê")]
        public string CreatedDate { get; set; }
        [Description("Người kiểm kê")]
        public string CreatedBy { get; set; }
        [Description("Ngày cập nhật")]
        public string UpdatedDate { get; set; }
        [Description("Người cập nhật")]
        public string UpdatedBy { get; set; }
    }

    public class InspectionCreateRequest : BaseRequest
    {
        [Description("Ngày kiểm kê")]
        public string DateInspection { get; set; }
        [Description("Phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Danh sách tài sản đã kiểm kê")]
        public string AssetCheckedInspection { get; set; }
        [Description("Danh sách tài sản chưa kiểm kê")]
        public string AssetNotCheckedInspection { get; set; }
    }

    public class InspectionUpdateRequest : BaseRequest
    {
        [Description("Kiểm kê Id")]
        public int InspectionId { get; set; }
        [Description("Ngày kiểm kê")]
        public string DateInspection { get; set; }
        [Description("Phòng ban")]
        public int DepartmentId { get; set; }
        [Description("Danh sách tài sản đã kiểm kê")]
        public string AssetCheckedInspection { get; set; }
        [Description("Danh sách tài sản chưa kiểm kê")]
        public string AssetNotCheckedInspection { get; set; }
    }

    public class InspectionDeleteRequest : BaseRequest
    {
        [Description("Kiểm kê Id")]
        public int InspectionId { get; set; }
    }
}
