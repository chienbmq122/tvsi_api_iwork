﻿namespace TVSI.CRM.WebAPI.Lib.ENum
{
    public class ENumCommon
    {
        public enum TypeLeave
        {
            TimeKeeping = 1, // Chấm công
            OverTime = 2, // Làm thêm giờ
            LeaveApplications = 3 // Nghỉ phép
        }

        public enum ApprovedStatus
        {
            PendingApproved = 0, // Đang chờ duyệt
            Approved = 1, // Đã duyệt
            Deleted = 99, // Đã xóa
            Reject = -1, // Từ chối
            ExecutiveBoardapproved = 2 //DB đã duyệt
        }

        public enum ProfileStatus
        {
            Pending = 000, // chờ xử lý
            PendingApproved = 100 ,// chờ duyệt
            DebtProfile = 101, // Nợ hồ sơ
            CompleteProfile = 200, // Hoàn thành
            Reject = 199 // hoặc 299 là trạng thái từ chối
        }

        public enum Activated
        {
            Pending = 000, // chờ xử lý
            PendingBUMANApproved = 001, // chờ BU_MAN duyệt
            PengdingFSApproved = 002, // Chờ FS duyệt
            BUMANReject  = 099, // BU_MAN từ chối
            Processing  = 100, // Đang xử lý
            ActivatedKSV = 102, // Kích hoạt(KSV SS)
            CompleteAccount = 200 , // Hoàn thành
            FSReject = 399, // FS từ chối
            CancelAccount = 999 // Đã hủy
        }

        public enum TypeContact
        {
            OpenAccountCRM = 2, // mở tài khoản trên CRM
            OpeneKYCMobile = 6, // mở tài khoản eKYC trên Mobile
            OpeneKYCWeb = 4, // mở tài khoản eKYC trên Website
            OpenAccountEMS = 1, // mở tài khoản trên EMS
            OpenWebsite = 3 // mở tài khoản thường trên website
        }
        public enum CustTypeConst
        {
            INDIVIDUAL_DOMESTIC_INVEST = 1,     // CN trong nuoc
            ORGANIZATION_DOMESTIC_INVEST = 2,   // TC trong nuoc
            INDIVIDUAL_FOREIGN_INVEST = 3,       // CN nuoc ngoai
            ORGANIZATION_FOREIGN_INVEST = 4     // TC nuoc ngoai
        }
        public enum ValidateAccountConst
        {
            ACCOUNT_NOT_EXIST = 0,
            ACCOUNT_EXIST_NOT_MARGIN = 1,
            ACCOUNT_EXIST_MARGIN = 2,
            ACCOUNT_EXIST_DIF_CUSTYPE = 3,
            OPEN_CONTRACT_EXSIT = 4,
            ACCOUNT_EXIST_DERIVATIVE = 5,
            ACCOUNT_EXIST_MARGIN_AND_DERIVATIVE = 6,
            CUSTOMER_EXIST_BUT_ACCOUNT_NOT_EXIST = 7,       // Trường hợp mới mở Customer, chưa mở Account (Đuôi 1)
            SYSTEM_ERROR = 9,
            SYSTEM_PERMISSION = 99 // Trường hợp Sales không có quyền truy khuất tài khoản này
        }
        
        public class OpenAccountStatus
        {
            public const string CHO_XU_LY_000 = "000";
            public const string DANG_XU_LY_100 = "100";
            public const string BU_MAN_XU_LY_001 = "001";
            public const string FS_XU_LY_002 = "002";
            public const string BU_MAN_TU_CHOI_099 = "099";
            public const string DA_KICK_HOAT_200 = "200";
            public const string HOAN_THANH_300 = "300";
            public const string NVSS_TU_CHOI_199 = "199";
            public const string KSVSS_TU_CHOI_299 = "299";
            public const string FS_TU_CHOI_399 = "399";
            public const string HUY_999 = "999";
        }
    }
}