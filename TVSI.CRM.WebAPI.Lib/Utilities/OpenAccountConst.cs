﻿namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public class OpenAccountConst
    {
        public const string OA1000_DATA_SAVEFAIL = "OA_1000";            // Lưu không thành công
        public const string TW901_DATA_EMPTY = "TW_901";            // ko tìm thấy dữ liệu
    }
    
}