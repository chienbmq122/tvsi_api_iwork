﻿using System;

namespace TVSI.CRM.WebAPI.Lib.Utilities.TimeWorkUtils
{
    public class TimeWorkUtils
    {
        public static string RandomNumber(int length = 6)
        {
            //Initiate objects & vars  
            byte[] seed = Guid.NewGuid().ToByteArray();
            Random random = new Random(BitConverter.ToInt32(seed, 0));
            int randNumber = 0;
            //Loop ‘length’ times to generate a random number or character
            String randomNumber = "";
            for (int i = 0; i < length; i++)
            {
                randNumber = random.Next(48, 58);
                randomNumber = randomNumber + (char)randNumber;
                //append random char or digit to randomNumber string

            }
            return randomNumber;
        }
        public static string EncriptMd5(string content)
        {
            byte[] textBytes = System.Text.Encoding.Default.GetBytes(content);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
        
        public enum FrequencyType
        {
            None = 0,
            Daily = 1,
            Weekly = 2,
            Monthly = 3,
            Quarterly = 4,
            Annually = 5,
        }

        public static string[] GetRange(FrequencyType frequency, DateTime dateToCheck)
        {
            string[] result = new string [2];
            DateTime dateRangeBegin = dateToCheck;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0); //One day 
            DateTime dateRangeEnd = DateTime.Today.Add(duration);

            switch (frequency)
            {
                case FrequencyType.Daily:
                    dateRangeBegin = dateToCheck;
                    dateRangeEnd = dateRangeBegin;
                    break;

                case FrequencyType.Weekly:
                    dateRangeBegin = dateToCheck.AddDays(-(int)dateToCheck.DayOfWeek);
                    dateRangeEnd = dateToCheck.AddDays(6 - (int)dateToCheck.DayOfWeek);
                    break;

                case FrequencyType.Monthly:
                    duration = new TimeSpan(DateTime.DaysInMonth ( dateToCheck.Year, dateToCheck.Month) - 1 , 0, 0, 0);
                    dateRangeBegin = dateToCheck.AddDays((-1) * dateToCheck.Day + 1);
                    dateRangeEnd = dateRangeBegin.Add(duration); 
                    break;

                case FrequencyType.Quarterly:
                    int currentQuater = (dateToCheck.Date.Month - 1) / 3 + 1;
                    int daysInLastMonthOfQuarter = DateTime.DaysInMonth(dateToCheck.Year, 3 * currentQuater );
                    dateRangeBegin = new DateTime ( dateToCheck.Year, 3 * currentQuater - 2, 1);
                    dateRangeEnd = new DateTime(dateToCheck.Year, 3 * currentQuater ,  daysInLastMonthOfQuarter);
                    break;

                case FrequencyType.Annually:
                    dateRangeBegin = new DateTime(dateToCheck.Year, 1, 1);
                    dateRangeEnd = new DateTime(dateToCheck.Year, 12, 31); 
                    break;
            }
            result[0] = dateRangeBegin.Date.ToString();
            result[1] = dateRangeEnd.Date.ToString();
            return result; 
        }
        
        public static string CompareDayOfWeekVn(DateTime dateTime)
        {
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Sunday)
            {
                return "CN";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Monday)
            {
                return "T2";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Tuesday)
            {
                return "T3";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Wednesday)
            {
                return "T4";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Thursday)
            {
                return "T5";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Friday)
            {
                return "T6";
            }
            if (dateTime.Date.DayOfWeek == System.DayOfWeek.Saturday)
            {
                return "T7";
            }
            return null;
        }
        public enum DayOfWeek
        {
            Sunday = 0,
            Monday = 1,
            Tuesday = 2,
            Wednesday = 3,
            Thursday = 4,
            Friday = 5,
            Saturday = 6
        }
    }
}