﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Dashboard
{
    public class DashboardChartRequest : BaseRequest
    {
        [Description("Loại thống kê: 1 tuần này, 2 tháng này")]
        public int Status { get; set; }
        [Description("Ngày gửi format dd/mm/yyyy")]
        public string DateIndex { get; set; }
    }

    public class DashboardChartResult
    {
        [Description("Phí trong ngày")]
        public double TodayFee { get; set; }

        [Description("Thay đổi trong tuần")]
        public double PercentOfWeek { get; set; }

        [Description("Giá trị ngày T")]
        public IEnumerable<DashboardChartDetail> Transaction_Data { get; set; }

    }

    public class DashboardChartDetail
    {
        [Description("Ngày")]
        public DateTime Transaction_Date { get; set; }
        [Description("Ngày format")]
        public string Transaction_Date_Str
        {
            get
            {
                return Transaction_Date.ToString("dd/MM/yyyy");
            }
        }

        [Description("Giá trị giao dịch")]
        public double Transaction_Value { get; set; }

        [Description("Phí giao dịch")]
        public double Transaction_Fee { get; set; }
    }
}
