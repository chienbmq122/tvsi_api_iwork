﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TVSI.CRM.WebAPI.ResourceFile {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class TimeWorkErrors {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal TimeWorkErrors() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("TVSI.CRM.WebAPI.ResourceFile.TimeWorkErrors", typeof(TimeWorkErrors).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu địa chỉ.
        /// </summary>
        public static string Validate_Address {
            get {
                return ResourceManager.GetString("Validate_Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu người viết đơn.
        /// </summary>
        public static string Validate_Applicants {
            get {
                return ResourceManager.GetString("Validate_Applicants", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu người duyệt.
        /// </summary>
        public static string Validate_Approved {
            get {
                return ResourceManager.GetString("Validate_Approved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu trạng thái duyệt.
        /// </summary>
        public static string Validate_ApprovedStatus {
            get {
                return ResourceManager.GetString("Validate_ApprovedStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu ngày làm việc.
        /// </summary>
        public static string Validate_DateCheckinandout {
            get {
                return ResourceManager.GetString("Validate_DateCheckinandout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu phòng ban làm việc.
        /// </summary>
        public static string Validate_Department {
            get {
                return ResourceManager.GetString("Validate_Department", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu  đến ngày làm việc .
        /// </summary>
        public static string Validate_FromDate {
            get {
                return ResourceManager.GetString("Validate_FromDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dữ liệu lựa chọn không đúng.
        /// </summary>
        public static string Validate_GetTimeWorkOption {
            get {
                return ResourceManager.GetString("Validate_GetTimeWorkOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Vĩ độ.
        /// </summary>
        public static string Validate_Latitude {
            get {
                return ResourceManager.GetString("Validate_Latitude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu ID.
        /// </summary>
        public static string Validate_LeaveId {
            get {
                return ResourceManager.GetString("Validate_LeaveId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Kinh độ.
        /// </summary>
        public static string Validate_Longitude {
            get {
                return ResourceManager.GetString("Validate_Longitude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Tháng làm việc.
        /// </summary>
        public static string Validate_MonthWork {
            get {
                return ResourceManager.GetString("Validate_MonthWork", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Lý do.
        /// </summary>
        public static string Validate_Reason {
            get {
                return ResourceManager.GetString("Validate_Reason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu trạng thái duyệt.
        /// </summary>
        public static string Validate_StatusLeaveNumber {
            get {
                return ResourceManager.GetString("Validate_StatusLeaveNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu trạng thái đơn từ.
        /// </summary>
        public static string Validate_StatusNumber {
            get {
                return ResourceManager.GetString("Validate_StatusNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu giờ vào làm việc.
        /// </summary>
        public static string Validate_TimeCheckin {
            get {
                return ResourceManager.GetString("Validate_TimeCheckin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu hết giờ làm việc.
        /// </summary>
        public static string Validate_TimeCheckout {
            get {
                return ResourceManager.GetString("Validate_TimeCheckout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu tiêu đề.
        /// </summary>
        public static string Validate_Title {
            get {
                return ResourceManager.GetString("Validate_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Từ ngày làm việc .
        /// </summary>
        public static string Validate_ToDate {
            get {
                return ResourceManager.GetString("Validate_ToDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Thiếu dữ liệu Loại đơn từ.
        /// </summary>
        public static string Validate_TypeLeave {
            get {
                return ResourceManager.GetString("Validate_TypeLeave", resourceCulture);
            }
        }
    }
}
