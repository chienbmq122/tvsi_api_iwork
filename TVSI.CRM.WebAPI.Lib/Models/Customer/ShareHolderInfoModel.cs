﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TVSI.CRM.WebAPI.Lib.Models.Customer
{
    public class ShareHolderInfoRequest : BaseRequest
    {
        [Description("Id cổ đông")]
        [Required]
        public int ShareHolderId { get; set; }
    }

    public class ShareHolderInfoResult
    {
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }

        [Description("Họ và tên")]
        public string CustName { get; set; }

        [Description("Cổ đông lớn")]
        public int ShareType_1 { get; set; }

        [Description("Tên cổ đông lớn")]
        public string ShareTypeName_1
            => ResourceFile.SystemModule.ResourceManager.GetString("ShareType_1_" + ShareType_1);

        [Description("Cổ đông nội bộ")]
        public int ShareType_2 { get; set; }

        [Description("Tên đông nội bộ")]
        public string ShareTypeName_2
            => ResourceFile.SystemModule.ResourceManager.GetString("ShareType_2_" + ShareType_2);

        [Description("Người liên quan cổ đông lớn")]
        public int ShareType_3 { get; set; }

        [Description("Tên người liên quan cổ đông lớn")]
        public string ShareTypeName_3
           => ResourceFile.SystemModule.ResourceManager.GetString("ShareType_3_" + ShareType_3);

        [Description("Sắp thành cổ đông lớn")]
        public int ShareType_4 { get; set; }
        [Description("Tên sắp thành cổ đông lớn")]
        public string ShareTypeName_4
           => ResourceFile.SystemModule.ResourceManager.GetString("ShareType_4_" + ShareType_4);


        [Description("Mã chứng khoán")]
        public string StockCode { get; set; }

        [Description("Trạng thái")]
        public int Status { get; set; }

        [Description("Tên trạng thái")]
        public string StatusName => ResourceFile.SystemModule.ResourceManager.GetString("CustStatus_" + Status);

        [Description("Mã nhân viên : SaleId")]
        public string CreatedBy { get; set; }

        [Description("Ngày tạo")]
        public string CreatedDate { get; set; }


    }

}
