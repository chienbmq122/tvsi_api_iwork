﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IActivityCommandText
    {
        /// <summary>
        /// next 3 task 
        /// </summary>
        string GetNext3Task  { get; }

        /// <summary>
        /// Get list activity 
        /// </summary>
        string GetListActivity { get; }

        /// <summary>
        /// Get list activity CF
        /// </summary>
        string GetListActivityByEvenType { get; }

        /// <summary>
        /// Get list type activity
        /// </summary>
        string GetListActivityType { get; }

        /// <summary>
        /// Get count activity
        /// </summary>
        string GetCountActivity { get; }

        /// <summary>
        /// Get count activity ByEvenType
        /// </summary>
        string GetCountActivityByEvenType { get; }

        /// <summary>
        /// Get detail activity
        /// </summary>
        string GetDetailActivity { get; }

        /// <summary>
        /// Post activity
        /// </summary>
        string PostActivity { get;  }

        /// <summary>
        /// Post activity BO
        /// </summary>
        string PostBOActivity { get; }

        /// <summary>
        /// Post activity CF
        /// </summary>
        string PostCFActivity { get;  }

        /// <summary>
        /// Put activity
        /// </summary>
        string PutActivity { get; }

        /// <summary>
        /// Put activity BO
        /// </summary>
        string PutBOActivity { get; }

        /// <summary>
        /// Put activity CF
        /// </summary>
        string PutCFActivity { get; }

        /// <summary>
        /// DeLete activity
        /// </summary>
        string DeleteActivity { get; }

        /// <summary>
        /// update list activity
        /// </summary>
        string UpdateListActivity { get; }

        /// <summary>
        /// confirm list activity
        /// </summary>
        // string ConfirmListActivity { get; }

        /// <summary>
        /// Reject list activity
        /// </summary>
       //  string RejectListActivity { get; }

        /// <summary>
        /// check Data by month
        /// </summary>
        string CheckDataByMonth { get; }

        /// <summary>
        /// Get His TimeWork Staff ByMonth
        /// </summary>
        string GetHisTimeWorkStaffByMonth { get; }
    }
}
