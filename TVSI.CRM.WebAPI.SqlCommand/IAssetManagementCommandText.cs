﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface IAssetManagementCommandText
    {
        /// <summary>
        /// common commandline
        /// </summary>
        string GetListTypeAsset { get; }

        string GetListStateAsset { get; }

        string GetListDepartment { get; }

        string GetListUserFromDepartment { get; }

        /// <summary>
        /// asset commandline
        /// </summary>
        string GetListAsset { get; }

        string CountListAsset { get; }

        string GetListAssetConfirm { get; }

        string CountListAssetConfirm { get; }

        string GetListAssetSelect { get; }

        string GetListAssetInIds { get; }

        string CreateAsset { get; }

        string UpdateAsset { get; }

        string DeleteAsset { get; }

        string GetDetailAsset { get; }

        string FilterAsset { get; }

        string FilterCount { get; }

        string GetListAssetUser { get; }

        string CountListAssetUser { get; }

        string GetHistoryAssetUser { get; }

        string CountHistoryAssetUser { get; }

        /// <summary>
        /// asset child commandline
        /// </summary>
        string GetListAssetChild { get; }

        string AsignAssetChild { get; }

        string RemoveAssetChild { get; }

        string GetHistoryAsignAsset { get; }

        /// <summary>
        /// management commandline
        /// </summary>
        string CreateManagementAsset { get; }

        string UpdateManagementAsset { get; }

        string GetHistoryManagementAsset { get; }

        string FilterManagementAsset { get; }

        string FilterCountManagement { get; }

        /// <summary>
        /// invertory commandline
        /// </summary>
        string GetListInspection { get; }

        string GetInspectionDetail { get; }

        string GetHistoryInspection { get; }

        string CreateInspectionAsset { get; }

        string UpdateInspectionAsset { get; }

        string DeleteInspectionAsset { get; }
    }
}
