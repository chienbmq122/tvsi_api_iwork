﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.Activity
{
    public class ActivityDataMonthRequest : BaseRequest
    {
        [Description("Ngày cần lấy")]
        [Required]
        public string DateIndex { get; set; }
    }
    public class ActivityDataMonthResponse
    {
        [Description("Ngày làm việc")]
        public string DateWork { get; set; }
        [Description("Số lượng sự kiến")]
        public int EventCount { get; set; }
        [Description("Số lượng công việc")]
        public int WorkCount { get; set; }
    }
    public class ActivityDataMonthCheckResponse
    {
        [Description("Ngày làm việc")]
        public string DateWork { get; set; }
        [Description("Check Data")]
        public bool HasData { get; set; }
    }
    public class ActivityDataFromQuery
    {
        [Description("Ngày làm việc")]
        public DateTime StartDate { get; set; }
        [Description("Ngày format")]
        public string DateWork
        {
            get
            {
                return StartDate.ToString("dd/MM/yyyy");
            }
        }
        [Description("Count Activity")]
        public int CountActivity { get; set; }
    }
}
