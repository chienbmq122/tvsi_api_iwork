﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using log4net;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.System;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// System API
    /// </summary>
    public class SystemController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(SystemController));
        private ISystemService _systemService;

        /// <summary>
        /// Controller Constructor
        /// </summary>
        /// <param name="systemService"></param>
        public SystemController(ISystemService systemService)
        {
            _systemService = systemService;
        }

        /// <summary>
        /// Lây thông tin và quyền của user đăng nhập
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("SM_LO_SystemLogin")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<UserLoginResult>))]
        [Note("Màn hình đăng nhập")]
        public async Task<IHttpActionResult> SM_LO_SystemLogin(UserLoginRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_LO_SystemLogin");
                SetLanguage(lang);

                var retData = await _systemService.Login(model.UserName, model.Password);

                if (retData != null)
                {
                    var retModel = new SuccessModel<UserLoginResult> {RetData = retData};
                    LogResult(log, retModel, "SM_LO_SystemLogin");
                    return Ok(retModel);
                }

                return Ok(new ErrorModel
                {
                    RetCode = "MSYLO_E001_NOT_FOUND",
                    ErrorMsg = ResourceFile.SystemModule.MSYLO_E001_NOT_FOUND
                });

                
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_LO_SystemLogin");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }




        /// <summary>
        /// Lấy danh sách thông báo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_NO_NotificationList")]
        [ResponseType(typeof(SuccessModel<NotificationResult>))]
        [Note("All Màn hình > Icon thông báo > Màn hình thông báo  ")]
        public async Task<IHttpActionResult> SM_NO_NotificationList(NotificationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_NO_NotificationList");
                SetLanguage(lang);

                //// Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                var notificationList = await _systemService.GetNotificationList(model.PageIndex, model.PageSize, model.UserName, lang);
                var dataRet = new SuccessModel<NotificationResult> { RetData = notificationList };

                LogResult(log, dataRet, "SM_NO_NotificationList");
                return Ok(dataRet);
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_NO_NotificationList");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Đồng bộ những thông báo đã đọc về TVSI
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [HttpPost]
        [ActionName("SM_NO_SyncReadNotification")]
        [ResponseType(typeof(SuccessModel))]
        [Note("All Màn hình > Icon thông báo > Màn hình thông báo  ")]
        public async Task<IHttpActionResult> SM_NO_SyncReadNotification(SyncNotificationRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "SM_NO_SyncReadNotification");
                SetLanguage(lang);
                // Validate empty data of request model
                //var errModel = ValidateRequestModel(model, lang);
                //if (errModel != null)
                //    return Ok(errModel);

                await _systemService.SyncNotificationRead(model.MessageIdList, model.UserName);
                LogResult(log, null, "SM_NO_NotificationList");
                return Ok(new SuccessModel());
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "SM_NO_SyncReadNotification");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}