﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TVSI.CRM.WebAPI.Lib.Models.System
{
    public class NotificationRequest : BaseRequest
    {

        [Description("Thứ tự trang (tính từ 1)")]
        [Required]
        public int PageIndex { get; set; }
        [Description("Số bản ghi cần lấy trên một trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class SyncNotificationRequest : BaseRequest
    {
        [Required]
        [Description("Danh sách MessageID đã đọc")]
        public List<long> MessageIdList { get; set; }
    }

    /// <summary>
    /// Thông báo trả về
    /// </summary>
    public class NotificationResult
    {
        [Description("Số thông báo chưa đọc")]
        public int NotificationCount { get; set; }

        public IEnumerable<NotificationData> NotificationDatas { get; set; }
    }

    public class NotificationData
    {
        [Description("ID của thông báo")]
        public long MessageId { get; set; }
        [Description("Loại thông báo")]
        public int NotificationType { get; set; }

        [Description("Ngày thông báo")]
        public string TradeDate => TradeDateDT.ToString("dd/MM/yyyy HH:mm:ss");

        [Description("Title của thông báo")]
        public string Title { get; set; }
        [Description("Nội dung thông báo")]
        public string Message { get; set; }

        [Description("Cờ đánh dấu đã đọc hay chưa")]
        public bool IsRead { get; set; }

        [JsonIgnore]
        public DateTime TradeDateDT { get; set; }
    }
}
