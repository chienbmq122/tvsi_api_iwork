﻿
using System.Drawing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TVSI.CRM.WebAPI.Lib.Models.Account;
using TVSI.Utility;

namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public static class DefaultStatus
    {
        public static readonly int New = 0;
        public static readonly int Active = 1;
        public static readonly int Deleted = 99;
        public static readonly int InActive = 2;
        public static readonly int Rejected = 1;
    }

    public static class SaleLevel
    {
        public static readonly int Sale = 0;
        public static readonly int Leader = 2;
        public static readonly int Director = 1;
        
    }

    public static class Folder
    {
        public static readonly string FolderAvatar = @"AvatarUsers\";
    }

    public static class AccountCommonDb
    {
        public static string Account044C = "044C";
    }
    

    public class AccountUtis
    {
        public static string GetImageAvatarUser(string UserName)
        {

            var folderCAvatar =   ConfigurationManager.AppSettings["FOLDER_IMAGE_AVATAR"];
            var folderCAvatarCustCode =   ConfigurationManager.AppSettings["FOLDER_IMAGE_AVATAR_CUSTCODE"];

            var folder = folderCAvatar.Replace("{UserName}", UserName);
            var fileImage = folderCAvatarCustCode.Replace("{UserName}", UserName);
            if (Directory.Exists(folder))
            {
                if (File.Exists(fileImage))
                {
                    return Path.GetFileName(fileImage);
                }
            }

            return "";
        }

        /*
        public static void SaveImageCardBase64(int id, string nameFront,string nameBack, string custcode, byte[] base64Fe,byte[] base64Be)
        {
            var folder = AccountConst.FolderCImageCustCode.Replace("{custcode}", custcode);
            if (!Directory.Exists(folder))
            {
                if (!Directory.Exists(AccountConst.FolderC))
                {
                    Directory.CreateDirectory(AccountConst.FolderC);
                }
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                }
                var urlFront = AccountConst.FolderCImageUrl.Replace("{custcode}", custcode);
                urlFront = urlFront.Replace("{name}", nameFront);
            
                var urlBack  = AccountConst.FolderCImageUrl.Replace("{custcode}", custcode);
                urlBack = urlBack.Replace("{name}", nameBack);
                File.WriteAllBytes(urlFront, base64Fe);
                File.WriteAllBytes(urlBack, base64Be);   
            }
            
        }
        */
        public static List<string> GetAllPhotosExtensions()
        {
            var list = new List<string>();
            list.Add(".jpg");
            list.Add(".png");
            list.Add(".bmp");
            list.Add(".gif");
            list.Add(".jpeg");
            list.Add(".tiff");
            return list;
        }
        public static bool IsPhoto(string fileName)
        {
            var list = GetAllPhotosExtensions();
            var filename= fileName.ToLower();
            bool isThere = false;
            foreach(var item in list)
            {
                if (filename.EndsWith(item))
                {
                    isThere = true;
                    break;
                }
            }
            return isThere;     
        }
        public static string EncriptMd5(string content)
        {
            byte[] textBytes = Encoding.Default.GetBytes(content);
            try
            {
                System.Security.Cryptography.MD5CryptoServiceProvider cryptHandler;
                cryptHandler = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] hash = cryptHandler.ComputeHash(textBytes);
                string ret = "";
                foreach (byte a in hash)
                {
                    ret += a.ToString("x2");
                }
                return ret;
            }
            catch (Exception ex)
            {
                ex.ToString();
                throw;
            }
        }
        
        public static bool IsRegistBankTransferService(AccountDetailRequest model)
        {
            return !string.IsNullOrEmpty(model.BankAccountList[0].BankAccName) || !string.IsNullOrEmpty(model.BankAccountList[1].BankAccName) || !string.IsNullOrEmpty(model.BankAccountList[2].BankAccName) || !string.IsNullOrEmpty(model.BankAccountList[3].BankAccName);
        } 
        
        
        public  static int BankServiceStatus(AccountDetailRequest model, int orderNumber)
        {
            // Kiem tra co dang ky TK Ngan hang 01 hay khong
            if (orderNumber == 1)
            {
                if (!string.IsNullOrEmpty(model.BankAccountList[0].BankAccNo))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            // Kiem tra co dang ky TK Ngan hang 02 hay khong
            if (orderNumber == 2)
            {
                if (!string.IsNullOrEmpty(model.BankAccountList[1].BankAccNo))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            if (orderNumber == 3)
            {
                if (!string.IsNullOrEmpty(model.BankAccountList[2].BankAccNo))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            if (orderNumber == 4)
            {
                if (!string.IsNullOrEmpty(model.BankAccountList[3].BankAccNo))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            return ServiceStatusConst.KO_DANG_KY.GetHashCode();
        }

        
        public static int AccountTransferService(AccountDetailRequest model, int orderNumber)
        {
            // Kiem tra co dang ky TK Ngan hang 01 hay khong
            if (orderNumber == 1)
            {
                if (!string.IsNullOrEmpty(model.AccountContraName_01))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            // Kiem tra co dang ky TK Ngan hang 02 hay khong
            if (orderNumber == 2)
            {
                if (!string.IsNullOrEmpty(model.AccountContraName_02))
                {
                    return ServiceStatusConst.CHO_XU_LY.GetHashCode();
                }
            }

            return ServiceStatusConst.KO_DANG_KY.GetHashCode();
        }
        

        public static int IntValue(Enum argEnum)
        {
            return Convert.ToInt32(argEnum);
        }

        public static string Value(Enum argEnum)
        {
            return Convert.ToInt32(argEnum) + "";
        }
        
        public static bool IsRegistAccountTransferService(AccountDetailRequest model)
        {
//            return View.TransferType.Contains(TransferMoneyConst.Account.Value());
            return !string.IsNullOrEmpty(model.AccountContraName_01) || !string.IsNullOrEmpty(model.AccountContraName_02);
        }
        
        /// <summary>
        /// lấy ảnh theo url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static byte[] GetImage(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
        
        /// <summary>
        /// Convert từ url sang base 64
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string ConvertImageURLToBase64(string url)
        {
            StringBuilder _sb = new StringBuilder();

            Byte[] _byte = GetImage(url);

            _sb.Append(Convert.ToBase64String(_byte, 0, _byte.Length));

            return _sb.ToString();
        }
        
        
        /// <summary>
        /// Convert string to byte[]
        /// </summary>
        /// <param name="str"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static byte[] ConvertToByteArray(string str, Encoding encoding)
        {
            return encoding.GetBytes(str);
        }
        
        
       public static byte[] GetImageUrl(string url)
        {
            Stream stream = null;
            byte[] buf;

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (MemoryStream ms = new MemoryStream())
                {
                    stream.CopyTo(ms);
                    buf = ms.ToArray();
                }

                stream.Close();
                response.Close();
            }
            catch (Exception exp)
            {
                buf = null;
            }

            return (buf);
        }
       
        
        
        
    }

    
}