﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.SqlCommand
{
    public interface ILeadCommandText
    {
        /// <summary>
        /// Get Lead list 
        /// </summary>
        string GetLeadList { get; }
        /// <summary>
        /// Query lấy danh sách hoạt động
        /// </summary>
        string GetActivityLeadInfo { get; }
        /// <summary>
        /// Query lấy danh sách cơ hội
        /// </summary>
        string GetOpportunityLeadInfo { get; }
        /// <summary>
        /// Thêm mới thông tin KH tiềm năng
        /// </summary>
        string PostLeadAddInfo { get; }
        /// <summary>
        /// Thêm mới thông tin mở rộng liên quan đến KH tiềm năng
        /// </summary>
        string PostLeadExtInfo { get; }
        /// <summary>
        /// Lấy danh sách thuộc tính mở rộng cho KH tiềm năng
        /// </summary>
        string  GetExLeadInfo { get; }
        /// <summary>
        /// Chi tiết KH tiềm năng
        /// </summary>
        string GetLeadInfo { get; }

        /// <summary>
        /// Lấy thuộc tính mở rộng theo KH tiềm năng
        /// </summary>
        string GetExLeadDetails { get; }
        /// <summary>
        /// Update KH tiềm năng
        /// </summary>
        string UpdateLeadInfo { get; }
        /// <summary>
        /// Xóa thông tin mở rộng khách hàng tiềm năng
        /// </summary>
        string DeleteExLeadInfo { get; }
        /// <summary>
        /// Xóa thông tin KH tiềm năng
        /// </summary>
        string DeleteLeadInfo { get; }

        /// <summary>
        /// Get danh sách lead dropbox
        /// </summary>
        string GetLeadsDropbox { get; }

        /// <summary>
        /// đếm lead dropbox
        /// </summary>
        string GetLeadsDropboxCount { get; }

        /// <summary>
        /// attribute lead
        /// </summary>
        string GetListAttributeLead { get; }

        /// <summary>
        /// create attribute to customer
        /// </summary>
        string CreateAttributeLeadToCustomer { get; }

        /// <summary>
        /// update Comment
        /// </summary>
        string UpdateCommentLeadToCustomer { get; }

        /// <summary>
        /// update Opportunity
        /// </summary>
        string UpdateOpportunityToCustomer { get; }

        /// <summary>
        /// update Activity
        /// </summary>
        string UpdateActivityToCustomer { get; }

        /// <summary>
        /// Check custinfo
        /// </summary>
        string CheckCustInfo { get; }

        /// <summary>
        /// update custinfo
        /// </summary>
        string UpdateCustInfo { get; }
    }
}
