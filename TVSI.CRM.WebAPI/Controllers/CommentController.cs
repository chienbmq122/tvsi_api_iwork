﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TVSI.Crm.WebAPI.Areas.HelpPage.Models;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Models.Comment;
using TVSI.CRM.WebAPI.Lib.Services;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Comment API
    /// </summary>
    public class CommentController : BaseController
    {
        private ILog log = LogManager.GetLogger(typeof(OpportunityController));
        private ICommentService _commentServicer;

        /// <summary>
        /// Controller Comment
        /// </summary>
        /// <param name="commentServicer"></param>
        public CommentController(ICommentService commentServicer)
        {
            _commentServicer = commentServicer;
        }

        /// <summary>
        /// Danh sách comment
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_GC_GetListComment")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel<List<CommentDBModel>>))]
        [Note("Danh sách comment")]

        public async Task<IHttpActionResult> CM_GC_GetListComment(CommentModelRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_GC_GetListComment");
                SetLanguage(lang);

                var retData = await _commentServicer.GetListComment(model);

                if (retData != null)
                {
                    var retModel = new SuccessModel<IEnumerable<CommentDBModel>>
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                        RetData = retData
                    };
                    LogResult(log, retModel, "CM_GC_GetListComment");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_GC_GetListComment");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới comment
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_GC_CreateComment")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới comment")]
        public async Task<IHttpActionResult> CM_GC_CreateComment(CreateCommentRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_GC_CreateComment");
                SetLanguage(lang);

                var retData = await _commentServicer.CreateComent(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "CM_GC_CreateComment");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_GC_CreateComment");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }

        /// <summary>
        /// Tạo mới reply
        /// </summary>
        /// <param name="model"></param>
        /// <param name="src">Nguồn yêu cầu</param>
        /// <param name="lang">Ngôn ngữ</param>
        /// <returns></returns>
        [ActionName("CM_GC_CreateReply")]
        [HttpPost]
        [ResponseType(typeof(SuccessModel))]
        [Note("Tạo mới reply")]
        public async Task<IHttpActionResult> CM_GC_CreateReply(CreateReplyRequest model, string src = CommonConst.SOURCE_TYPE_M, string lang = CommonConst.LANGUAGE_VI)
        {
            try
            {
                LogRequest(log, model, "CM_GC_CreateReply");
                SetLanguage(lang);

                var retData = await _commentServicer.CreateReply(model);

                if (retData)
                {
                    var retModel = new SuccessModel
                    {
                        RetCode = CommonConst.SUCCESS_CODE,
                    };
                    LogAnonymousResult(log, retModel, "CM_GC_CreateReply");
                    return Ok(retModel);
                }
                return Ok(new ErrorModel
                {
                    RetCode = CommonConstExtend.FAIL_DATA_EMPTY,
                    ErrorMsg = ResourceFile.CommonErrors.E100
                });
            }
            catch (Exception ex)
            {
                LogException(log, model, ex, "CM_GC_CreateReply");
                return Ok(new ErrorModel { RetCode = CommonConst.ERR999_SYSTEM_ERROR });
            }
        }
    }
}
