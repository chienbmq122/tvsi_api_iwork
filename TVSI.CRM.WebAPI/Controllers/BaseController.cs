﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Http;
using log4net;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.Models;
using TVSI.CRM.WebAPI.Lib.Utilities.ConstParams;

namespace TVSI.CRM.WebAPI.Controllers
{
    /// <summary>
    /// Abstract Base Controller
    /// </summary>
    public abstract class BaseController : ApiController
    {
        /// <summary>
        /// Set ngôn ngữ trả về của API
        /// </summary>
        /// <param name="lang"></param>
        protected void SetLanguage(string lang)
        {
            if (CommonConst.LANGUAGE_EN.Equals(lang))
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-GB");
            }
        }

        #region Log Request and Result
        /// <summary>
        /// Log Request
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="apiName"></param>
        protected void LogRequest(ILog log, BaseRequest requestModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            log4net.LogicalThreadContext.Properties["UserId"] = requestModel.UserName;
            log4net.LogicalThreadContext.Properties["ApiName"] = apiName;
            log4net.Config.XmlConfigurator.Configure();
            var requestJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                
            });
            log.Info("Api=" + apiName + "\t Request=" + requestJson);
        }
        protected void LogRequestAnonymous(ILog log, BaseIWRequest requestModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            log4net.LogicalThreadContext.Properties["UserId"] = null;
            log4net.LogicalThreadContext.Properties["ApiName"] = apiName;
            log4net.Config.XmlConfigurator.Configure();
            var requestJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                
            });
            log.Info("Api=" + apiName + "\t Request=" + requestJson);
        }

        
        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogAnonymousResult(ILog log, SuccessModel resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            log.Debug("Api=" + apiName + "\t Response=" + resultJson);
        }
        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult<T>(ILog log, SuccessModel<T> resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                
            });
            log.Debug("Api=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log"></param>
        /// <param name="resultModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult<T>(ILog log, SuccessListModel<T> resultModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(resultModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            log.Debug("Api=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log kết quả trả về
        /// </summary>
        /// <param name="log"></param>
        /// <param name="errorModel"></param>
        /// <param name="apiName"></param>
        protected void LogResult(ILog log, ErrorModel errorModel, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var resultJson = JsonConvert.SerializeObject(errorModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            log.Debug("Api=" + apiName + "\t Response=" + resultJson);
        }

        /// <summary>
        /// Log lỗi exception
        /// </summary>
        /// <param name="log"></param>
        /// <param name="requestModel"></param>
        /// <param name="ex"></param>
        /// <param name="apiName"></param>
        protected void LogException(ILog log, BaseRequest requestModel, Exception ex, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var errorJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            log.Error(apiName + ":" + ex.Message + "\t ErrorData=" + errorJson);
        } 
        protected void LogExceptionAnonymous(ILog log, BaseIWRequest requestModel, Exception ex, string apiName = "")
        {
            if (string.IsNullOrEmpty(apiName))
                apiName = this.ControllerContext.RouteData.Values["action"].ToString();

            var errorJson = JsonConvert.SerializeObject(requestModel, Formatting.None, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
            log.Error(apiName + ":" + ex.Message + "\t ErrorData=" + errorJson);
        }
        #endregion
    }
}