﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TVSI.CRM.WebAPI.Lib.Utilities;
using System.Collections.Generic;

namespace TVSI.CRM.WebAPI.Lib.Models.Account
{

    public class GetAccountBySaleResult
    {
        [Description("Id khách hàng")]
        public int CustId { get; set; }
        
        [Description("Thông tin khách hàng")]
        public string FullName { get; set; }
        
        [Description("Mã khách hàng")]
        public string CustCode { get; set; }
        
        [Description("Trạng thái hồ sơ")]
        public string ProfileStatusNum { get; set; }
        
        [Description("Tên Trạng thái hồ sơ")]
        public string ProfileStatusName {
            get
            {
                return ProfileStatusNum.StatusProfile();
            } }
        
        [Description("Loại hồ sơ")]
        public string TypeProfile { get; set; }
        
        [Description("Loại hợp đồng")]
        public string TypeContract { get; set; }
        
        [Description("Ngày tạo")]
        public string CreateDate {
            get
            {
                return CreateDateOn.ToString("dd/MM/yyyy");
            }
            
        }
        [Description("Trạng thái kích hoạt kiểu số")]
        public string ActivatedNum { get; set; }
        
        [Description("Tên Trạng thái kích hoạt")]
        
        public string ActivatedName { get; set; }
        [Description("Nguồn dữ liệu Number")]
        public int SourceAccountNum { get; set; }
        
        [Description("Tên Nguồn dữ liệu")]
        public string SourceAccountName { get; set; }
        
        [JsonIgnore]
        public DateTime CreateDateOn { get; set; }
        
    }

    public class GetAccountBySaleRequest : BaseRequest
    {
        [Description("Thứ tự trang")]
        [Required]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        [Required]
        public int PageSize { get; set; }
    }

    public class GetAccountFillBySaleRequest : BaseRequest
    {
        [Description("Thứ tự trang")]
        [Required]
        public int PageIndex { get; set; }

        [Description("Số Item trên 1 trang")]
        
        [Required]
        [Range(1,100,ErrorMessageResourceName = nameof(ResourceFile.AccountErrors.Validate_PageSize),ErrorMessageResourceType = typeof(ResourceFile.AccountErrors))]
        public int PageSize { get; set; }
        
        [Description("Từ ngày dd/MM/yyyy")]
        public string FromDate { get; set; }
        
        [Description("Đến ngày dd/MM/yyyy")]
        public string ToDate { get; set; }
        
        
        [Description("Trạng thái hồ sơ")]
        public string ProfileStatus { get; set; }
        
        [Description("Trạng thái tài khoản")]
        public string Activated { get; set; }
        
        [Description("Nguồn đăng ký")]
        public int? SourceAccount { get; set; }
        
        [Description("Tên khách hàng")]
        public string KeySearch { get; set; }

    }
    

    public class GetAccountDetailBySaleRequest : BaseRequest
    {
        public int CustId { get; set; }
    }
    

    public class GetAccountDetailBySaleResult
    {
        public GetAccountDetailBasicBySaleResult AccountDetailBasic { get; set; } =
            new GetAccountDetailBasicBySaleResult();

        public GetAccountDetailRegisterServiceResult AccountDetailRegisterService { get; set; } =
            new GetAccountDetailRegisterServiceResult();

        public InfoExtendDetailResult AccountDetailExtend { get; set; } = new InfoExtendDetailResult();
    }
    public class GetAccountDetailBasicBySaleResult
    {
        [Description("mã khách hàng")]
        public string CustCode { get; set; }
        
        [Description("Thông tin khách hàng")]
        public string FullName { get; set; }
        
        [Description("Ngày sinh dd/MM/yyyy")]
        public string Birthday {
            get
            {
                return BirthdayOn?.ToString("dd/MM/yyyy");
            }
            
        }
        
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        public string Gender { get; set; }
        
        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        public string CardId { get; set; }
        
        [Description("Ngày cấp")] 
        public string IssueDate {
            get
            {
                return IssueDateOn?.ToString("dd/MM/yyyy");
            }
            
        }
        
        [Description("Nơi cấp CMND/CCCD của KH")]
        public string CardIssuer { get; set; }
        
        [Description("Quốc tịch")]
        public string Nationality { get; set; }
        
        [Description("Địa chỉ ")]
        public string Address01 { get; set; }
        
        [Description("Mã nhân viên tư vấn. Nếu nhập thì bắt buộc là 4 ký tự = số")]
        public string SaleId { get; set; } 
        
        [Description("Profile id")]
        public int ProfileId { get; set; }
        
        [Description("Đường dẫn chứa ảnh mặt trước CCCD/CMND")]
        public string ImageFront { get; set; }
        
        [Description("Đường dẫn chứa ảnh sau trước CCCD/CMND")]
        public string ImageBack { get; set; }

        [Description("Hồ sơ mở hợp đồng ")]
        public string OpenContactName { get; set; }

        [Description("Tên trạng thái hợp đồng")]
        public string StatusContactName { get; set; }
        
        [Description("Nơi mở tài khoản")]
        public string AddressOpenAccount { get; set; }
        
      
        // iw co
        [Description("Đại diện TVSI")]
        public string RepresentTvsi { get; set; }

        // iw co
        [Description("Chức vụ")]
        public string RollTvsi { get; set; }

        // iw co
        [Description("Giấy ủy quyền số")]
        public string PowerofAttorneyNum { get; set; }

        // iw co
        [Description("Ngày cấp giấu UQ dd/MM/yyyy")]
        public string PowerofAttorneyDate {
            get
            {
                return AuthorizationDateOn?.ToString("dd/MM/yyyy");
            }
            
        }

        // iw co
        [Description("Số CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSICardId { get; set; }

        // iw co
        [Description("Ngày cấp CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSIIssueDate { 
            get
            {
                return TVSIIssueDateOn?.ToString("dd/MM/yyyy");
            }
            
        }
        [JsonIgnore]
        public DateTime? TVSIIssueDateOn { get; set; }

        // iw co
        [Description("Nơi cấp CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSICardIssuer { get; set; }
        
        [JsonIgnore]
        public DateTime? AuthorizationDateOn { get; set; }
        
        [JsonIgnore]
        public DateTime? BirthdayOn { get; set; }
        
        [JsonIgnore]
        public DateTime? IssueDateOn { get; set; }
        
        [Description("Fatca khách hàng usa")] public int? CustomerUSA { get; set; } = null;
        
        [Description("Fatca nơi sinh usa")]
        public int? PlaceOfBirthUSA { get; set; } = null;
        
        [Description("Fatca nơi cư trú usa")]
        public int? DepositoryUSA { get; set; } = null;
        
        [Description("Fatca số điện usa")]
        public int? CustomerPhoneNumberUSA { get; set; } = null;
        
        [Description("Fatca có lệnh định kỳ chuyển khoản")]
        public int? CustomerTransferBankUSA { get; set; } = null;
        
        [Description("Fatca giấy ủy quyền")]
        public int? CustomerAuthorizationUSA { get; set; } = null;
        
        [Description("Fatca nhận thư hộ")]
        public int? CustomerReceiveMailUSA { get; set; } = null;
        
        [JsonIgnore]
        public int? SourceData { get; set; }
        
        [JsonIgnore]
        public int? CrmId { get; set; }

        [Description("Hồ sơ mở hợp đồng 0:HĐ thường, 1:HĐ Margin , 2: Phái sinh ")]
        public int? OpenContact { get; set; } = null;
        
		
        [Description("Hạn mức dư nợ")]
        public string CreditLimit { get; set; } // Hạn mức dư nợ
        
        [Description("Thời hạn nợ hợp đồng")]
        public string DebtTerm { get; set; } // Thời hạn nợ hợp đồng
        
        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; } // Tỷ lệ ký quỹ
   
    }

    public class GetAccountDetailRegisterServiceResult
    {
        [Description("Phương thức giao dịch 1:Qua điện thoại,2:Qua Email,12:Qua điện thoại + Email")]
        public string TradingMethod { get; set; }
        
        [Description("Phương thức nhận KQGD")]
        public string TradingResultMethod { get; set; }
        
        [Description("Phương thức nhận sao kê cuối ngày 1, thu dam bao 2:Qua Email")]
        public string TradingReportMethod { get; set; }
        
        [Description("Số điện thoại 01")]
        public string Phone01 { get; }    
        
        [Description("Số điện thoại 02")]
        public string Phone02 { get; }
        
        [Description("Địa chỉ email")]
        public string Email { get; set; }
        
        [Description("Dịch vụ chuyển tiền")]
        public string TransferCashService { get; set; }
        
        
        

        [Description("Chủ tài khoản ngân hàng 01")]
        public string BankAccName_01 { get; set; }
        
        [Description("Số tài khoản ngân hàng 01")]
        public string BankAccNo_01 { get; set; }
        
        [Description("Tên ngân hàng 01")]
        public string BankName_01 { get; set; }

        [Description("Chi nhánh ngân hàng 01")]
        public string SubBranchName_01 { get; set; }
        

        [Description("Chủ tài khoản ngân hàng 02")]
        public string BankAccName_02 { get; set; }
        
        [Description("Số tài khoản ngân hàng 02")]
        public string BankAccNo_02 { get; set; }

        [Description("Tên ngân hàng 02")]
        public string BankName_02 { get; set; }

        [Description("Chi nhánh ngân hàng 02")]
        public string SubBranchName_02 { get; set; }  
        
        
        [Description("Chủ tài khoản ngân hàng 03")]
        public string BankAccName_03 { get; set; }
        
        [Description("Số tài khoản ngân hàng 03")]
        public string BankAccNo_03 { get; set; }

        [Description("Tên ngân hàng 03")]
        public string BankName_03 { get; set; }

        [Description("Chi nhánh ngân hàng 03")]
        public string SubBranchName_03 { get; set; }
        
        
        [Description("Chủ tài khoản ngân hàng 04")]
        public string BankAccName_04 { get; set; }
        
        [Description("Số tài khoản ngân hàng 04")]
        public string BankAccNo_04 { get; set; }

        [Description("Tên ngân hàng 04")]
        public string BankName_04 { get; set; }

        [Description("Chi nhánh ngân hàng 04")]
        public string SubBranchName_04 { get; set; }
        
        
        [Description("Chủ tài khoản đổi ứng 1")]
        public string AccountContraName_01{get;set;}
        
        [Description("Số tài khoản đối ứng 1")]
        public string AccountContraNo_01 { get; set; } 
        
        [Description("Chủ tài khoản đổi ứng 2")]
        public string AccountContraName_02{get;set;}
        
        [Description("Số tài khoản đối ứng 2")]
        public string AccountContraNo_02 { get; set; }
    }

    /*public class GetAccountDetailExtendResult
    {
        [Description("Nghề nghiệp")]
        public string Job { get; set; }
        
        [Description("Nơi làm việc")]
        public string WorkPlace { get; set; }
        
        [Description("Khả năng tài chính")]
        public string FinancialCapability { get; set; }
        
        [Description("Kinh nghiệm đầu tư")]
        public string InvestmentExperience { get; set; }
        
        [Description("Tham gia trái phiếu từ")]
        public string JonedBond { get; set; }
        
        [Description("Chập nhận rủi ro")]
        public string TakeRisks { get; set; }
        
        [Description("Sở thích đầu tư")]
        public string InvestmentPrefer { get; set; }
        
        [Description("Quan hệ với Sale")]
        public string SaleRelationship { get; set; }
        
        [Description("Lưu ý khi giao tiếp")]
        public string NoteCommunication { get; set; }
    }*/
    
    public class InfoExtendDetailResult
    {
           
        // iw co
        [Description("Nghề nghiệp")]
        public int Occupation  { get; set; }
        
        [Description("Nghề nghiệp")]
        public string OccupationStr  { get; set; }
        // iw co
        [Description("Nơi làm việc")]
        public int Workplace  { get; set; }   
        [Description("Nơi làm việc")]
        public string WorkplaceStr  { get; set; }
        
        // iw co
        [Description("Khả năng tài chính")]
        public int FinancialResources  { get; set; }  
        // iw co
        [Description("Khả năng tài chính")]
        public string FinancialResourcesStr  { get; set; } 
        
        // iw co
        [Description("Kinh nghiệm đầu tư")]
        public int InvestmentExperience  { get; set; }   
        // iw co
        [Description("Kinh nghiệm đầu tư")]
        public string InvestmentExperienceStr  { get; set; } 
        
        // iw co
        [Description("Đã tham trái phiếu từ")]
        public int JoinedBondSince  { get; set; }
        // iw co
        [Description("Đã tham trái phiếu từ")]
        public string JoinedBondSinceStr  { get; set; } 
        
        // iw co
        [Description("Khả năng chấp nhận rủi ro")]
        public int RiskAcceptance  { get; set; }        
        // iw co
        [Description("Khả năng chấp nhận rủi ro")]
        public string RiskAcceptanceStr  { get; set; }    
        
        // iw co
        [Description("Sở thích đầu tư")]
        public int HobbyInvestment  { get; set; }  
        // iw co
        [Description("Sở thích đầu tư")]
        public string HobbyInvestmentStr  { get; set; } 
        
        // iw co
        [Description("Có quan hệ với sale")]
        public int RelationshipSale  { get; set; }     
        // iw co
        [Description("Có quan hệ với sale")]
        public string RelationshipSaleStr  { get; set; }
        
        // iw co
        [Description("Lưu ý khi nói chuyện")]
        public int NoteTalking  { get; set; }  
        // iw co
        [Description("Lưu ý khi nói chuyện")]
        public string NoteTalkingStr  { get; set; }
        
        
        [Description("Kiến thức, Kinh nghiệm")]
        public int KnowledExpID { get; set; }
        
        [Description("Kiến thức, Kinh nghiệm")]
        public string KnowledExpStr { get; set; }
        
        [Description("Quan điểm đầu tư")]
        public int OpinionInvestID { get; set; }
        
        [Description("Quan điểm đầu tư")]
        public string OpinionInvestStr { get; set; }
        
        [Description("Nguồn khai thác")]
        public int SourceID { get; set; }    
        
        [Description("Nguồn khai thác")]
        public string SourceStr { get; set; }
        
        
        
    }
    public class InfoExtendDetailTotalResult
    {
           
        // iw co
        [Description("Nghề nghiệp")]
        public int Occupation  { get; set; }
        
        [Description("Nghề nghiệp")]
        public string OccupationStr  { get; set; }
        // iw co
        [Description("Nơi làm việc")]
        public int Workplace  { get; set; }   
        [Description("Nơi làm việc")]
        public string WorkplaceStr  { get; set; }
        
        // iw co
        [Description("Khả năng tài chính")]
        public int FinancialResources  { get; set; }  
        // iw co
        [Description("Khả năng tài chính")]
        public string FinancialResourcesStr  { get; set; } 
        
        // iw co
        [Description("Kinh nghiệm đầu tư")]
        public int InvestmentExperience  { get; set; }   
        // iw co
        [Description("Kinh nghiệm đầu tư")]
        public string InvestmentExperienceStr  { get; set; } 
        
        // iw co
        [Description("Đã tham trái phiếu từ")]
        public int JoinedBondSince  { get; set; }
        // iw co
        [Description("Đã tham trái phiếu từ")]
        public string JoinedBondSinceStr  { get; set; } 
        
        // iw co
        [Description("Khả năng chấp nhận rủi ro")]
        public int RiskAcceptance  { get; set; }        
        // iw co
        [Description("Khả năng chấp nhận rủi ro")]
        public string RiskAcceptanceStr  { get; set; }    
        
        // iw co
        [Description("Sở thích đầu tư")]
        public int HobbyInvestment  { get; set; }  
        // iw co
        [Description("Sở thích đầu tư")]
        public string HobbyInvestmentStr  { get; set; } 
        
        // iw co
        [Description("Có quan hệ với sale")]
        public int RelationshipSale  { get; set; }     
        // iw co
        [Description("Có quan hệ với sale")]
        public string RelationshipSaleStr  { get; set; }
        
        // iw co
        [Description("Lưu ý khi nói chuyện")]
        public int NoteTalking  { get; set; }  
        // iw co
        [Description("Lưu ý khi nói chuyện")]
        public string NoteTalkingStr  { get; set; }
        
        [Description("Kiến thức, Kinh nghiệm")]
        public int KnowledExpID { get; set; }   
        [Description("Kiến thức, Kinh nghiệm")]
        
        public string KnowledExpIDStr { get; set; }
        
        [Description("Quan điểm đầu tư")]
        public int OpinionInvestID { get; set; } 
        
        [Description("Quan điểm đầu tư")]
        public string OpinionInvestIDStr { get; set; }
        
        [Description("Nguồn khai thác")]
        public int SourceID { get; set; }   
        
        [Description("Nguồn khai thác")]
        public string SourceIDStr { get; set; }
        
    }
    public class GetAccountDetailExtendDetailResult
    {
        [Description("Nghề nghiệp")]
        public string Job { get; set; }
        
        [Description("Nơi làm việc")]
        public string WorkPlace { get; set; }
        
        [Description("Khả năng tài chính")]
        public string FinancialCapability { get; set; }
        
        [Description("Kinh nghiệm đầu tư")]
        public string InvestmentExperience { get; set; }
        
        [Description("Tham gia trái phiếu từ")]
        public string JonedBond { get; set; }
        
        [Description("Chập nhận rủi ro")]
        public string TakeRisks { get; set; }
        
        [Description("Sở thích đầu tư")]
        public string InvestmentPrefer { get; set; }
        
        [Description("Quan hệ với Sale")]
        public string SaleRelationship { get; set; }
        
        [Description("Lưu ý khi giao tiếp")]
        public string NoteCommunication { get; set; }
        
        
        
    }

    public class ExtendCategories
    {
        public int CategoryType { get; set; }
        public string CategoryName { get; set; }
        
        public int Category { get; set; }
    }

    public class ImageDataCommondb
    {
        public int ProfileId { get; set; }
        public string NameFront { get; set; }
        public byte[] FileDataFront { get; set; }
        public byte[] FileDataBack { get; set; }
        public string NameBack { get; set; }
    }

    public class AccountValidation
    {
        public string RetErrorField { get; set; }
        public string RetErrorMsg { get; set; }
    }

    public class GetCardIssue
    {
        public int id { get; set; }
        public string CardIssueName { get; set; }
    }

    public class ManagementCodeList
    {
        public string CodeBranch { get; set; }
        public string NameDepartment { get; set; }
    }
    
    
    public class SaleProfileModel
    {
        public string SaleID { get; set; }
        public string SaleName { get; set; }
        public int Level { get; set; }
        public int Sex { get; set; }


        public string BirthDayOn
        {
            get
            {
                return BirthDay.ToString("dd/MM/yyyy");
            }
        }

        [JsonIgnore]
        public DateTime BirthDay { get; set; }
        
        public string Address { get; set; }
        public string Department { get; set; }      // Phòng ban
        public string Mobile { get; set; }
        public string Email { get; set; }

        public string LevelStr
        {
            get
            {
                var ret = "Sale";

                switch (Level)
                {
                    case (int)CrmConst.SaleLevel.Director:
                        ret += " (GĐCN)";
                        break;
                    case (int)CrmConst.SaleLevel.Leader:
                        ret += " (Trưởng nhóm)";
                        break;
                }
               
                return ret;
            }
        }

        public string SexStr
        {
            get { return Sex == 1 ? "Nam" : "Nữ"; }
        }
        public string NameAvatar { get; set; }

    }

    public class ChangeImageUser : BaseRequest
    {
        public string NameAvatar { get; set; }
    }
    public class TvsiTaiKhoanKhachHang
    {
        public string ma_khach_hang { get; set; }

        public int loai_khach_hang { get; set; }

        
    }
    
    public class TvsiDangKyMoTk
    {
        public int loai_ho_so { get; set; }

        [Required]
        public string ma_khach_hang { get; set; }

        public string trang_thai_tk { get; set; }
    }
    
    public class DetailAccountRequest : BaseRequest
    {
        [Description("Customer code")]
        public string CustCode { get; set; }
    }

    public class DetailAccount
    {
        public int ho_so_mo_tk_id { get; set; }
        public int loai_ho_so { get; set; }
        public string ma_khach_hang { get; set; }
        public string so_tai_khoan { get; set; }
        public string hop_dong_bo_sung { get; set; }
        public int loai_hinh_tk { get; set; }
        public string ho_ten_kh { get; set; }
        public string ngay_sinh { get; set; }
        public int gioi_tinh { get; set; }
        public string ma_quoc_tich { get; set; }
        public string quoc_tich { get; set; }
        public string so_cmnd { get; set; }
        public string ngay_cap_cmnd { get; set; }
        public string noi_cap_cmnd { get; set; }
        public string dia_chi_lien_he { get; set; }
        public int hop_dong_chinh { get; set; }
        public string phuong_thuc_gd { get; set; }
        public string phuong_thuc_nhan_kqgd { get; set; }
        public string phuong_thuc_sao_ke { get; set; }
        public string dv_chuyen_tien { get; set; }
        public string so_dien_thoai_01 { get; set; }
        public string so_dien_thoai_02 { get; set; }
        public string email { get; set; }
        public string chu_tai_khoan_nh_01 { get; set; }
        public string so_tai_khoan_nh_01 { get; set; }
        public string ma_ngan_hang_01 { get; set; }
        public string ngan_hang_01 { get; set; }
        public string ma_chi_nhanh_01 { get; set; }
        public string chi_nhanh_nh_01 { get; set; }
        public string tinh_tp_nh_01 { get; set; }
        public string chu_tai_khoan_nh_02 { get; set; }
        public string so_tai_khoan_nh_02 { get; set; }
        public string ma_ngan_hang_02 { get; set; }
        public string ngan_hang_02 { get; set; }
        public string ma_chi_nhanh_02 { get; set; }
        public string chi_nhanh_nh_02 { get; set; }
        public string tinh_tp_nh_02 { get; set; }
        public string chu_tai_khoan_nh_03 { get; set; }
        public string so_tai_khoan_nh_03 { get; set; }
        public string ma_ngan_hang_03 { get; set; }
        public string ngan_hang_03 { get; set; }
        public string ma_chi_nhanh_03 { get; set; }
        public string chi_nhanh_nh_03 { get; set; }
        public string tinh_tp_nh_03 { get; set; }
        public string chu_tai_khoan_nh_04 { get; set; }
        public string so_tai_khoan_nh_04 { get; set; }
        public string ma_ngan_hang_04 { get; set; }
        public string ngan_hang_04 { get; set; }
        public string ma_chi_nhanh_04 { get; set; }
        public string chi_nhanh_nh_04 { get; set; }
        public string tinh_tp_nh_04 { get; set; }
        public string chu_tk_doi_ung_01 { get; set; }
        public string so_tk_doi_ung_01 { get; set; }
        public string chu_tk_doi_ung_02 { get; set; }
        public string so_tk_doi_ung_02 { get; set; }
        public int kenh_trao_doi { get; set; }
        public string phuong_thuc_kenh_td { get; set; }
        public string trang_thai_tk { get; set; }
        public string trang_thai_sba { get; set; }
        public string trang_thai_ho_so { get; set; }
        public int trang_thai_dv { get; set; }
        public int trang_thai_dv_ngan_hang { get; set; }
        public int trang_thai_dv_ngan_hang_02 { get; set; }
        public int trang_thai_dv_doi_ung { get; set; }
        public int trang_thai_dv_doi_ung_02 { get; set; }
        public int trang_thai_dv_kenh_tt { get; set; }
        public string trang_thai_inno { get; set; }
        public string ly_do_tu_choi_tk { get; set; }
        public string ly_do_tu_choi_ho_so { get; set; }
        public string ly_do_sba { get; set; }
        public string ly_do_tu_choi_dv_ngan_hang { get; set; }
        public string ly_do_tu_choi_dv_ngan_hang_02 { get; set; }
        public string ly_do_tu_choi_dv_doi_ung { get; set; }
        public string ly_do_tu_choi_dv_doi_ung_02 { get; set; }
        public string ly_do_tu_choi_dv_kenh_tt { get; set; }
        public string ly_do_inno { get; set; }
        public string mo_ta { get; set; }
        public string sale_id { get; set; }
        public string branch_id { get; set; }
        public string branch_name { get; set; }
        public int nguon_du_lieu { get; set; }
        public string tvsi_noi_mo_tk { get; set; }
        public string tvsi_nguoi_dai_dien { get; set; }
        public string tvsi_chuc_vu { get; set; }
        public string tvsi_giay_uq { get; set; }
        public string tvsi_ngay_uq { get; set; }
        public string tvsi_so_cmnd { get; set; }
        public string tvsi_noi_cap_cmnd { get; set; }
        public string tvsi_ngay_cap_cmnd { get; set; }
        public int fatca_kh_usa { get; set; }
        public int fatca_noi_sinh_usa { get; set; }
        public int fatca_noi_cu_tru_usa { get; set; }
        public int fatca_sdt_usa { get; set; }
        public int fatca_lenh_ck_usa { get; set; }
        public int fatca_giay_uy_quyen_usa { get; set; }
        public int fatca_dia_chi_nhan_thu_usa { get; set; }
        public string file_upload { get; set; }
        public string file_upload_backside { get; set; }
        public string face_upload { get; set; }
        public string crm_id { get; set; }
        public string chuky_upload { get; set; }
        public string phieu_yc_upload { get; set; }
        public string nguoi_tao { get; set; }
        public string ngay_tao { get; set; }
        public string nguoi_cap_nhat { get; set; }
        public string thoi_gian_xac_nhan_otp { get; set; }
        public string ngay_cap_nhat { get; set; }
        public string ss_duyet { get; set; }
        public string ngay_ss_duyet { get; set; }
        public string ksv_duyet { get; set; }
        public string ngay_ksv_duyet { get; set; }
        public string note { get; set; }
        public int fatca_tc_1 { get; set; }
        public int fatca_tc_2 { get; set; }
        public int fatca_tc_2a_1 { get; set; }
        public int fatca_tc_2a_2 { get; set; }
        public int fatca_tc_2a_3 { get; set; }
        public int fatca_tc_2b { get; set; }
        public float du_no { get; set; }
        public string ngay_chuyen_no_hop_dong { get; set; }
        public string hop_dong_no_id { get; set; }
        public string han_muc_du_no { get; set; }
        public string thoi_gian_ghi_no { get; set; }
        public string ty_le_ky_quy { get; set; }
        public string fs_phe_duyet { get; set; }
        public string ngay_phe_duyet { get; set; }
        public string ly_do_tu_choi { get; set; }
    }

    public class AccountInfoReponse : BaseRequest
    {
        [Description("ID account")]
        public int AccountId { get; set; }
        [Description("So tai khoan")]
        public string CustCode { get; set; }
        // iw co
        [Description("Họ và Tên")]
        [Required]
        public string FullName { get; set; }

        // iw co
        [Description("Số CMND/CCCD của KH dd/MM/yyyy")]
        [Required]
        public string CardId { get; set; }

        // iw co
        [Description("Ngày cấp")]
        [Required]
        public string IssueDate { get; set; }

        // iw co
        [Description("Nơi cấp CMND/CCCD của KH")]
        [Required]
        public string CardIssuer { get; set; }

        // iw co
        [Description("Giới Tính 1 là Nam, 2 là Nữ,3 là không xác định")]
        [Required]
        public int Sex { get; set; }

        // iw co
        [Description("Ngày sinh dd/MM/yyyy")]
        [Required]
        public string Birthday { get; set; }
        // iw co
        [Description("Địa chỉ liên hệ")]
        [Required]
        public string Address_01 { get; set; }

        // iw co
        [Description("Ma Quốc tịch")]
        [Required]
        public string NationalityCode { get; set; }

        [Description("Ma Quốc tịch")]
        [Required]
        public string NationalityName { get; set; }

        [Description("Hạn mức dư nợ")]
        public string CreditLimit { get; set; } // Hạn mức dư nợ

        [Description("Thời hạn nợ hợp đồng")]
        public string DebtTerm { get; set; } // Thời hạn nợ hợp đồng

        [Description("Tỷ lệ ký quỹ")]
        public string DepositRate { get; set; } // Tỷ lệ ký quỹ

        // iw co
        [Description("Số điện thoại của KH 01")]
        [Required]
        public string Phone_01 { get; set; }

        // iw co
        [Description("Số điện thoại của KH 02")]
        [Required]
        public string Phone_02 { get; set; }

        // iw co
        [Description("Địa chỉ email")]
        [Required]
        public string Email { get; set; }
        

        // iw co
        [Description("Hồ sơ mở hợp đồng 0:HĐ thường, 1:HĐ Margin , 2: Phái sinh ")]
        public int? OpenContact { get; set; }

        // iw co
        [Description("Trạng thái hợp đồng 1: Hoàn thiên, 2: nộp hồ sơ ")]
        public int StatusContact { get; set; }

        // iw co
        [Description("Nơi mở tài khoản")]
        public string AddressOpenAccount { get; set; }

        // iw co
        [Description("Đại diện TVSI")]
        public string RepresentTvsi { get; set; }

        // iw co
        [Description("Chức vụ")]
        public string RollTvsi { get; set; }

        // iw co
        [Description("Giấy ủy quyền số")]
        public string PowerofAttorneyNum { get; set; }

        // iw co
        [Description("Ngày cấp giấu UQ dd/MM/yyyy")]

        public string PowerofAttorneyDate { get; set; }

        // iw co
        [Description("Số CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSICardId { get; set; }

        // iw co
        [Description("Ngày cấp CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSIIssueDate { get; set; }

        // iw co
        [Description("Nơi cấp CMND/CCCD của đại diện tvsi")]
        [Required]
        public string TVSICardIssuer { get; set; }

        // iw co
        [JsonIgnore]
        [Description("Kênh thông tin đặt lệnh")]
        public bool RegistChannelInfo { get; set; }

        // iw co
        [Description("Phương thức giao dịch 1:Qua điện thoại,2:Qua Email,12:Qua điện thoại + Email")]
        public string TradingMethod { get; set; }

        // iw co
        [Description("Phương thức nhận KQGD 1: sms, 2: email")]
        public string TradingResultMethod { get; set; }

        // iw co
        [Description("Phương thức nhận sao kê cuối ngày 1:thư đảm bảo, 2:Qua Email")]
        public string TradingReportMethod { get; set; }

        // iw co
        [Description("Có Đăng ký dịch vụ chuyển tiền 0:Không đăng ký, 1: là đăng ký")]
        public int BankTransferMethod { get; set; }

        // iw co
        [Description("Danh sách tài khoản ngân hàng List<BankAccountData>")]
        public List<BankAccountData> BankAccountList { get; set; }

        // iw co
        [Description("Đường dẫn chưa các video + ảnh upload EKYC")]
        [Required]
        public FolderPath FolderPath { get; set; }

        // iw co
        [Description("Thông tin mở rộng")]
        [Required]
        public InfoExtend InfoExtend { get; set; }

        // iw co
        [Description("Thông tin Fatca")]
        [Required]
        public FatcaInfo FatcaInfo { get; set; }
    }

    public class IdNohoso
    {
        public int idNohoso { get; set; }
    }

    public class GetManInfoResult
    {
        public int ManCode { get; set; }
        public string ManName { get; set; }
        public string ManPosition { get; set; }
        public string ManNo { get; set; }
        public string ManDate { get; set; }
        public string ManCardID { get; set; }
        public string ManCardDate { get; set; }
        public string ManCardIssue { get; set; }
    }

    public class GetManInfoListResult
    {
        public int ManCode { get; set; }
        public string ManName { get; set; }
    }

    public class GetManInfoSql
    {
        public int ma_cau_hinh { get; set; }
        public string gia_tri { get; set; }
    }

    public class GetFullManinfo
    {
        
        [Key]
        public int cau_hinh_id { get; set; }

        [Required]
        [StringLength(5)]
        public string loai_cau_hinh { get; set; }

        [Required]
        [StringLength(25)]
        public string ma_cau_hinh { get; set; }

        [Required]
        [StringLength(500)]
        public string gia_tri { get; set; }

        public int trang_thai { get; set; }

        [StringLength(10)]
        public string ma_chi_nhanh { get; set; }

        [Required]
        [StringLength(50)]
        public string nguoi_tao { get; set; }

        public DateTime ngay_tao { get; set; }

        [StringLength(50)]
        public string nguoi_cap_nhat { get; set; }

        public DateTime? ngay_cap_nhat { get; set; }
    }

    public class GetManInfoRequest : BaseRequest
    {
        [Description("Mã code người đại diện")]
        [Required]
        public int ManCode { get; set; }
    }

    public class CodeManager
    {
        public string ma_chi_nhanh { get; set; }
        public string ten_bo_phan { get; set; }
    }
    
    public class DataImage
    {
        public string Front_Image { get; set; }
        public string Back_Image { get; set; }
        public string Face_Image { get; set; }
        public string Signature_Image_0 { get; set; }
    }
    public class GetImageResult
    {
        public string RetCode { get; set; }
        public DataImage RetData { get; set; }
    }

    public class GetAccountStatusResult
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
    
    
    public class TVSI_fDANH_SACH_CAP_DO_CON_VA_CHINH_NO_Result
    {
        public int? cap_do_he_thong_id { get; set; }
        public string ma_cap_do_he_thong { get; set; }
        public string ma_chi_nhanh { get; set; }
        public string ten_cap_do_he_thong { get; set; }
        public int? cap_do_he_thong_muc_cha_id { get; set; }
        public int? loai_cap_do_he_thong { get; set; }
        public double? he_so_phan_bo_chi_tieu { get; set; }
        public int? trang_thai { get; set; }
        public DateTime? ngay_cap_nhat_cuoi { get; set; }
        public string nguoi_cap_nhat_cuoi { get; set; }
    }

    public class CalllogRequest : BaseRequest
    {
        public string CallId { get; set; }
        public string CustCode { get; set; }
        public string Phone { get; set; }
    }
    public class ProvinceResult
    {
        public int ProvinceID { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceCode { get; set; }
    }
    public class DistrictRequest : BaseRequest
    {
        [Required]
        public string ProvinceCode { get; set; }
    }
    
    public class DistrictResult
    {
        public int DistrictID { get; set; }
        public string DistrictCode { get; set; }
        public string DistrictName { get; set; }
    }
    public class WardResult
    {
        public int WardID { get; set; }
        public string WardCode { get; set; }
        public string WardName { get; set; }
    }
    public class WardRequest : BaseRequest
    {
        public string DistrictCode { get; set; }
    }

    public class CardIDRequest : BaseRequest
    {
        public string CardId { get; set; }
    }

    public class ConfirmeKYC : BaseRequest
    {
        [Required]
        public int ProfileId { get; set; }
        
        [Required]
        public string Code { get; set; }
        [Required]
        public int SourceAccount { get; set; }
    }

    public class ConfirmeKYCResult
    {
        public string ConfirmCode { get; set; }
        public string ConfirmSms { get; set; }
    }

}