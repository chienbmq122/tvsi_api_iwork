﻿using System;
using System.ComponentModel;

namespace TVSI.CRM.WebAPI.Lib.Utilities.EnumExtension
{
    public static class ENumCommon
    {
        public static string ToEnumDescription(this Enum val, bool isUpper = false)
        {
            var attributes = (DescriptionAttribute[]) val.GetType().GetField(val.ToString())
                ?.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0
                ? isUpper ? attributes[0].Description.ToUpper() : attributes[0].Description
                : string.Empty;
        }
    }
    
}