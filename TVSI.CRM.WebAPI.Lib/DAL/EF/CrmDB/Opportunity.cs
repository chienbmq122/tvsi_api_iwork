﻿
namespace TVSI.CRM.WebAPI.Lib.DAL.EF.CrmDB
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Opportunity")]
    public partial class Opportunity
    {
        [Key]
        public int OpportunityID { get; set; }

        [StringLength(250)]
        public string OpportunityName { get; set; }

        [StringLength(5)]
        public string BranchID { get; set; }

        [StringLength(50)]
        public string AssignUser { get; set; }

        public long LeadID { get; set; }

        [StringLength(25)]
        public string CustCode { get; set; }

        public double ExpectedRevenue { get; set; }

        public DateTime ExpectedCloseDate { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public int Status { get; set; }

        [StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(50)]
        public string UpdatedBy { get; set; }

        public DateTime? UpdatedDate { get; set; }
    }
}
