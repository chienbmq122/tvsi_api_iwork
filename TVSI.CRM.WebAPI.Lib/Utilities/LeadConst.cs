﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Utilities
{
    public class LeadConst
    {
        public const string EXLEAD_TYPE_1 = "1"; // Nghề Nghiệp
        public const string EXLEAD_TYPE_2 = "2"; // Khả năng tài chính
        public const string EXLEAD_TYPE_3 = "3"; // Mức độ rủi ro
        public const string EXLEAD_TYPE_4 = "4"; // Kinh nghiệm đầu tư
        public const string EXLEAD_TYPE_5 = "5"; // Quan điểm đầu tư

        public const string LEAD_STATUS_99 = "99"; // Trạng thái không hoạt động

    }
}
