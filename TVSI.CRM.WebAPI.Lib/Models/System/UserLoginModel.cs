﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.CRM.WebAPI.Lib.Models.System
{
    public class UserLoginRequest : BaseRequest
    {
        [Description("Mật khẩu")]
        [Required]
        public string Password { get; set; }
    }

    /// <summary>
    /// Thông tin của User đăng nhập
    /// </summary>
    public class UserLoginResult
    {
        [Description("Tên đăng nhập")]
        public string UserName { get; set; }

        [Description("Họ tên")]
        public string FullName { get; set; }

        [Description("Mã Nhân viên quản lý")]
        public string SaleID { get; set; }

        [Description("Level: 0 - Sale, 1- GĐ chi nhánh, 2: Trưởng nhóm")]
        public int Level { get; set; }

        [Description("Cấp độ hệ thống")]
        public int SystemID { get; set; }

        [Description("Mã chi nhánh")]
        public string BranchID { get; set; }

        [Description("Nhân sự ID")]
        public int UserID { get; set; }

        [Description("Mã cấp độ hệ thống")]
        public string SystemCode { get; set; }

        [Description("Danh sách quyền")]
        public List<UserRole> RoleList { get; set; }
    }

    public class UserRole
    {
        [Description("Mã chức năng")]
        public string FunctionCode { get; set; }
        [Description("Được quyền Xem?")]
        public bool IsRead { get; set; }
        [Description("Được quyền Thêm mới?")]
        public bool IsAdd { get; set; }
        [Description("Được quyền Chỉnh sửa?")]
        public bool IsEdit { get; set; }
        [Description("Được quyền Xóa?")]
        public bool IsDelete { get; set; }
    }

    public class TVSI_sDANH_MUC_QUYEN_ByTenTruyCap_Result
    {
        public int IsAllBranch { get; set; }

        public int IsApproval { get; set; }

        public int IsCreate { get; set; }

        public int IsDelete { get; set; }

        public int IsEdit { get; set; }

        public int IsFind { get; set; }

        public int IsPrint { get; set; }

        public int IsRead { get; set; }

        public int IsRekey { get; set; }

        public string ma_chuc_nang { get; set; }

        public string ten_chuc_nang { get; set; }
    }
}
